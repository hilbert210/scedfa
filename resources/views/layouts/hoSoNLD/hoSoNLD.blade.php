@extends('master')
@section('title','SCEDFA | Hồ Sơ Người Lao Động')
@section('content')

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.min.css')}} " />
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.toast.min.css')}} " />
@endpush
<div>
<div class="filter p-3">
  <div class=" d-flex justify-content-around p-3">
    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>Tình Trạng Hồ Sơ </span>
      </div>
      <select id="TinhTrangHoSoFilter" multiple class="TinhTrangHoSoFilter col-12">
        <option value="1">Hồ Sơ Đang Lao Động TCNN</option>
        <option value="2">Hồ Sơ Kết Thúc hợp Đồng Lao Động</option>
        <option value="0">Hồ Sơ Xin Việc</option>
      </select>
    </div>
    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>Giới Tính</span>
      </div>
      <select id="GioiTinhFilter" class="GioiTinhFilter col-12">
        <option value="all">Tất Cả</option>
        <option value="0">Nữ</option>
        <option value="1">Nam</option>
      </select>
    </div>
    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>Tỉnh thành làm việc</span>
      </div>
      <select id="TinhThanhLamViecFilter" class="TinhThanhLamViecFilter col-12">
      </select>
    </div>
    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>Loại Hình TC,CN NN:</span>
      </div>
      <select id="LoaiTCNN" class="LoaiTCNN col-12" style="width: 80%">
        <option value=""></option>
      </select>
    </div>
    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>TCNN</span>
      </div>
      <select id="DmTCNNFilter" class="DmTCNNFilter col-12">
        <option value=""></option>
      </select>
    </div>

    <div class="input-group row col">
      <div class="input-group-prepend col-12">
        <span>Báo Cáo: </span>
      </div>
      <select id="export" name="export" class="col-12">
      <option value="">Select an option</option>
        <option value="0">Danh sách thay đổi chức danh</option>
        <option value="1">Danh sách NLĐ VN làm việc cho các TC, CNNN tại khu vực miền trung - tây nguyên(để làm việc)</option>
        <option value="7">Danh sách NLĐ VN làm việc cho các TC, CNNN tại khu vực miền trung - tây nguyên</option>
        <option value="2">Danh sách địa chỉ Email</option>
        <option value="3">Danh sách báo cáo tình hình thẩm tra, xác minh lý lịch NLĐ VN làm việc cho các TC, CNNN</option>
        {{-- <option value="4">Danh sách ốm đau thai sản</option>
        <option value="5">Danh sách tăng giảm lương</option> --}}
        <option value="6">Danh sách báo cáo thống kê tình hình thôi việc của NLĐ</option>
      </select>
    </div>

    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>Tháng:</span>
      </div>
      <div class="d-flex flex-row col-12">
        <input type="text" id="filterRangeMonth" name="filterRangeMonth" value="" />
      </div>
    </div>

  </div>
  <div class="d-flex justify-content-center">
    <button type="button" id="filterHoSoNLD" style="width: 6rem" class="btn btn-primary m-2">Filter</button>
    <button type="button" id="resetHoSoNLD" style="width: 6rem" class="btn btn-secondary m-2">Reset</button>
  </div>
</div>

{{$dataTable->table(['id' => 'HoSoNLD','class'=>'table table-striped table-bordered'])}}

{{-- modal create and edit --}}

@include('layouts.hoSoNLD.formHoSoNLD')

@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('DataTables/datatables.min.js')}} "></script>
<script>
  $(function() {
    {{$dataTable->generateScripts()}}
    $('#button-close-modal').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-HoSoNLD').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-HoSoNLD').modal('hide');
        $( "form .text-danger").remove();
        $( "form .pb-0").removeClass('pb-0');
        $( "form .border-danger").removeClass('border border-danger');
        $('.update-CD').addClass('hide');
        $('#file-dinh-kem tbody').empty();
        $("#picture").attr("src", "/img/avatar-temp.png");
        $("#picture").hide();
        $("#picture").fadeIn(500);
      } else {
        $('#modal-HoSoNLD').modal('show');
      }
    });
    $('#button-close-modal-historyChucDanh').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#modal-historyHSNV').modal('hide');
      } else {
        $('#modal-historyHSNV').modal('show');
      }
    });
    $('#button-close-modal-DmChucDanh-ttdk').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#modal-DmChucDanh-ttdk').modal('toggle');
      } else {
        $('#modal-DmChucDanh-ttdk').modal('show');
      }
    });
    $('#button-close-modal-DmChucDanh-cvht').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#modal-DmChucDanh-cvht').modal('toggle');
      } else {
        $('#modal-DmChucDanh-cvht').modal('show');
      }
    });
    $('#button-close-modal-DmChucDanh-historyCD').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#modal-DmChucDanh-historyCD').modal('toggle');
      } else {
        $('#modal-DmChucDanh-historyCD').modal('show');
      }
    });
  })
</script>

<script type="text/javascript" src="{{asset('js/moment.min.js')}} "></script>
<script type="text/javascript" src="{{asset('chosen/chosen.jquery.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.yadcf.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/app.js')}} "></script>
<script type="text/javascript" src="{{asset('js/HoSoNLD.js')}} "></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.toast.min.js')}} "></script>
@endpush
</div>