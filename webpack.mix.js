const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/tcnn.js', 'public/js/tcnn.js')
    .js('resources/js/HoSoNLD.js', 'public/js/HoSoNLD.js')
    .js('resources/js/HoSoTTThuHo.js', 'public/js/HoSoTTThuHo.js')
    .js('resources/js/LuongNhanVien.js', 'public/js/LuongNhanVien.js')
    .sass('resources/sass/app.scss', 'public/css');
mix.setResourceRoot("../");