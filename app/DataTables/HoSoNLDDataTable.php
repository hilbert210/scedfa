<?php

namespace App\DataTables;

use App\HoSoNhanVienTCNN;
use App\Transformers\HoSoNhanVienTCNNTranformer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class HoSoNLDDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())
        ->setTransformer(new HoSoNhanVienTCNNTranformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $LoaiTCNN = $this->request()->get('LoaiTCNN');
        $IdTCNN =  $this->request()->get('IdTCNN');
        $MaTT_NoiLamViec = $this->request()->get('TinhThanhLamViec');
        $CheckGender = is_numeric($this->request()->get('GioiTinh')) ? true  : false;
        $GioiTinh = $this->request()->get('GioiTinh');
        $TinhTrangHoSo = $this->request()->get('TinhTrangHoSo');
        $query =   HoSoNhanVienTCNN::leftJoin('DmTinhThanh', 'HoSoNhanVienTCNN.MaTT_NoiLamViec', '=', 'DmTinhThanh.MATT')
            ->leftJoin('DmChucDanh', 'HoSoNhanVienTCNN.CVHT_ChucDanh', '=', 'DmChucDanh.IdChucDanh')
            ->leftJoin('DmTCNN', 'HoSoNhanVienTCNN.IdTCNN', '=', 'DmTCNN.IdTCNN')
            ->when($LoaiTCNN, function ($query, $LoaiTCNN) {
                $query->where('DmTCNN.LoaiTCNN', '=', $LoaiTCNN);
            })
            ->leftJoin('DmTonGiao', 'HoSoNhanVienTCNN.IdTonGiao', '=', 'DmTonGiao.IdTonGiao')
            ->when($IdTCNN, function ($query, $IdTCNN) {
                $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
            })
            ->when($CheckGender, function ($query) use ($GioiTinh) {
                $query->where('HoSoNhanVienTCNN.GioiTinh', '=', $GioiTinh);
            })
            ->when($MaTT_NoiLamViec, function ($query) use ($MaTT_NoiLamViec) {
                $query->where('HoSoNhanVienTCNN.MaTT_NoiLamViec', '=', $MaTT_NoiLamViec);
            })
            ->when($TinhTrangHoSo, function ($query) use ($TinhTrangHoSo) {
                $query->whereIn('HoSoNhanVienTCNN.TinhTrangHoSo' ,$TinhTrangHoSo);
            })
            ->select(
                'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', 'HoSoNhanVienTCNN.TinhTrangHoSo',
                'HoSoNhanVienTCNN.Ho', 'HoSoNhanVienTCNN.Ten',
                'HoSoNhanVienTCNN.NguyenQuan', 'HoSoNhanVienTCNN.NgaySinh',
                'HoSoNhanVienTCNN.ThongBaoNghiViec', 'HoSoNhanVienTCNN.NgayThongBaoNghiViec',
                'HoSoNhanVienTCNN.SoCMND',
                'HoSoNhanVienTCNN.DienThoaiNhaRieng',
                'HoSoNhanVienTCNN.DangDoan',
                'HoSoNhanVienTCNN.TrinhDoChuyenMon',
                'HoSoNhanVienTCNN.SoNha_ThuongTru',
                'HoSoNhanVienTCNN.SoNha_HoKhau',
                'DmTinhThanh.TENTT',
                'DmTCNN.TenTiengVietTCNN',
                'DmTCNN.TenTiengAnhTCNN',
                'DmTCNN.TenTatTCNN',
                'DmChucDanh.ChucDanh',
                'HoSoNhanVienTCNN.NgayBatDauCongTac',
                'DmTonGiao.TonGiao',
                DB::raw("CASE WHEN HoSoNhanVienTCNN.GioiTinh = 1 THEN N'Nam' WHEN HoSoNhanVienTCNN.GioiTinh = 0 THEN N'Nữ' ELSE '' END AS Gender ")
            )
            ->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['export', 'reload', 'bangluongNV'];
        if(Gate::allows('HoSoNLD_edit')){
            array_unshift($buttons,'edit');
        }
        if(Gate::allows('HoSoNLD_create')){
            array_unshift($buttons,'create');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(
                '',
                'data.IdTCNN = $("#DmTCNNFilter").val();
                data.GioiTinh = $("#GioiTinhFilter").val();
                data.TinhThanhLamViec = $("#TinhThanhLamViecFilter").val();
                data.TinhTrangHoSo = $("#TinhTrangHoSoFilter").val();'
            )
            ->createdRow('function(row, data, dataIndex){
                if(data.ThongBaoNghiViec==true){
                    if(moment().isAfter(moment(data.NgayThongBaoNghiViec).add(2,"days"))){
                        $(row).addClass("bg-danger");
                    }
                    if(moment().isAfter(moment(data.NgayThongBaoNghiViec))){
                        $(row).addClass("bg-warning");
                    }
                }
            }')
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => true,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                //'order'   => [[0, 'desc']],
                'order'   => [[3, 'asc']],
                'buttons'      => $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $NgaySinh = Column::make('NgaySinh')
            ->title('Ngày Sinh')
            ->searchable(false)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");
                }
                return e;
            }')
            ->exportable(true)
            ->printable(true);
        $NgayBDCT = Column::make('NgayBatDauCongTac')
            ->title('Ngày Bắt Đầu Công Tác')
            ->searchable(false)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");
                }
                return e;
            }')
            ->exportable(true)
            ->printable(true);
        return [
            ["data" => "IdHoSoNhanVienTCNN", 'title' => 'ID', 'exportable' => false, 'printable' => false],
            ["data" => "TinhTrangHoSo", 'title' => 'Tình trạng hồ sơ', 'className'=>'hide' ],
            ["data" => "Ho", 'title' => 'Họ' ],
            ["data" => "Ten", 'title' => 'Tên' ],
            $NgaySinh,
            ["data" => "Gender", 'title' => 'Giới Tính' ],
            ["data" => "TonGiao", 'title' => 'Tôn Giáo' ],
            ["data" => "SoCMND", 'title' => 'CMND/CCCD' ],
            ["data" => "TENTT", 'title' => 'Tỉnh Thành Làm Việc' ],
            ["data" => "DienThoaiNhaRieng", 'title' => 'Điện Thoại' ],
            ["data" => "TrinhDoChuyenMon", 'title' => 'Trình Độ' ],
            ["data" => "ChucDanh", 'title' => 'Chức Danh' ],
            $NgayBDCT,
            ["data" => "SoNha_ThuongTru", 'title' => 'Địa Chỉ Liên Hệ' ],
            ["data" => "TenTiengVietTCNN", 'title' => 'TCNN' ],
            ["data" => "DangDoan", 'title' => 'Đảng/Đoàn' ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'HoSoNLD_' . date('YmdHis');
    }
}
