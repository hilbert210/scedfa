<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoSoNhanVienTCNN extends Model
{
    protected $guarded  = ['IdHoSoNhanVienTCNN'];
    protected $primaryKey = "IdHoSoNhanVienTCNN";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'HoSoNhanVienTCNN';
}