<form method="POST" id="form-OptionConfig" class="form-OptionConfig">
    @csrf
    <div class="modal fade " id="modal-OptionConfig" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 45%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 style="text-transform:uppercase; color: red;">Cài đặt biến hệ thống</h3>
                    <button type="button" id="button-close-modal" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" >
                    <input type="hidden" class="form-control" name="id" id="id">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Tên biến</span>
                        </div>
                        <input type="text" class="form-control" name="option_name" id="option_name">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Giá trị</span>
                        </div>
                        <input type="text" class="form-control money" name="option_value" id="option_value">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Mô tả</span>
                        </div>
                        <textarea class="form-control" name="description" id="description"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="saveBtn-OptionConfig" class="btn btn-primary-outline">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</form>