<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Option;

class LuongNhanVienDichVuExportEmployees implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    protected $pitRelief;
    protected $dependencyPitRelief;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection,$filter)
    {
        $this->collection = $collection;
        $this->filter = $filter;
        $this->pitRelief = Option:: getSystemConfig('GIAMTRU_THUE');
        $this->dependencyPitRelief = Option:: getSystemConfig('GIAMTRU_NGUOIPHUTHUOC');
        $dateSal = strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"]));
        $year = date('Y', $dateSal);
        $month = date('m', $dateSal);
        if ($year < 2020) {
            $this->pitRelief = config('scedfa.SysConf.GIAMTRU_OLD');
            $this->dependencyPitRelief = config('scedfa.SysConf.GIAMTRU_PHUTHUOC_OLD');
        } else if($year == 2020){
            if ($month < 7) {
                $this->pitRelief = config('scedfa.SysConf.GIAMTRU_OLD');
                $this->dependencyPitRelief = config('scedfa.SysConf.GIAMTRU_PHUTHUOC_OLD');
            }
        }
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:T' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:T' . ($HighestRow + 1))
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('A1', 'SERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS')->mergeCells('A1:G1')
                    ->setCellValue('A2', $this->collection[0]["TCNN"])->mergeCells('A2:G2')
                    ->setCellValue('A4', 'PAYROLL IN ' .  strtoupper(date('m/Y',strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"])))))->mergeCells('A4:T4')
                    ->setCellValue('S5', 'Exchange rate:')
                    ->setCellValue('T5', number_format($this->collection[0]["TyGia"]))
                    ->setCellValue('S6', 'Unit:')
                    ->setCellValue('T6', 'VND')
                    ->setCellValue('B7', 'Full name')->mergeCells('B7:B8')
                    ->setCellValue('C7', 'Basic salary')->mergeCells('C7:D7')
                    ->setCellValue('C8', 'USD')
                    ->setCellValue('D8', 'VND')
                    ->setCellValue('E7', 'Working days')->mergeCells('E7:E8')
                    ->setCellValue('F7', 'Paid leave days')->mergeCells('F7:F8')
                    ->setCellValue('G7', 'Salary paid for actual number of working days')->mergeCells('G7:H7')
                    ->setCellValue('G8', 'USD')
                    ->setCellValue('H8', 'VND')
                    ->setCellValue('I7', 'Allowances')->mergeCells('I7:J7')
                    ->setCellValue('I8', 'USD')
                    ->setCellValue('J8', 'VND')
                    ->setCellValue('K7', 'Amount for calculation
(Exchange rate: '. number_format($this->collection[0]["TyGia_Insurance"]) .')')->mergeCells('K7:L7')
                    ->setCellValue('k8', 'SI,HI')
                    ->setCellValue('L8', 'UI')
                    ->setCellValue('M7', 'SI, HI and UI to employees ('. ($this->collection[0]["TrichNop_BHXH"] + $this->collection[0]["BaoHiemYTe"] + $this->collection[0]["BaoHiemThatNghiep"]) .'%)' )->mergeCells('M7:N7')
                    ->setCellValue('M8', 'SI,HI')
                    ->setCellValue('N8', 'UI')
                    ->setCellValue('O7', 'Number of depentdents')->mergeCells('O7:O8')
                    ->setCellValue('P7', 'Amount not for PIT')->mergeCells('P7:P8')
                    ->setCellValue('Q7', 'Income for PIT')->mergeCells('Q7:Q8')
                    ->setCellValue('R7', 'PIT')->mergeCells('R7:R8')
                    ->setCellValue('S7', 'Wire Transfer Fee')->mergeCells('S7:S8')
                    ->setCellValue('T7', 'Remaining salary')->mergeCells('T7:T8')
                    ->setCellValue('A'.($HighestRow+1),'Total')->mergeCells('A'.($HighestRow+1).':B'.($HighestRow+1))
                    ->setCellValue('C'.($HighestRow+1),'=SUM(C9:C'.$HighestRow.')')
                    ->setCellValue('D'.($HighestRow+1),'=SUM(D9:D'.$HighestRow.')')
                    ->setCellValue('E'.($HighestRow+1),'=SUM(E9:E'.$HighestRow.')')
                    ->setCellValue('F'.($HighestRow+1),'=SUM(F9:F'.$HighestRow.')')
                    ->setCellValue('G'.($HighestRow+1),'=SUM(G9:G'.$HighestRow.')')
                    ->setCellValue('H'.($HighestRow+1),'=SUM(H9:H'.$HighestRow.')')
                    ->setCellValue('I'.($HighestRow+1),'=SUM(I9:I'.$HighestRow.')')
                    ->setCellValue('J'.($HighestRow+1),'=SUM(J9:J'.$HighestRow.')')
                    ->setCellValue('K'.($HighestRow+1),'=SUM(K9:K'.$HighestRow.')')
                    ->setCellValue('L'.($HighestRow+1),'=SUM(L9:L'.$HighestRow.')')
                    ->setCellValue('M'.($HighestRow+1),'=SUM(M9:M'.$HighestRow.')')
                    ->setCellValue('N'.($HighestRow+1),'=SUM(N9:N'.$HighestRow.')')
                    ->setCellValue('O'.($HighestRow+1),'=SUM(O9:O'.$HighestRow.')')
                    ->setCellValue('P'.($HighestRow+1),'=SUM(P9:P'.$HighestRow.')')
                    ->setCellValue('Q'.($HighestRow+1),'=SUM(Q9:Q'.$HighestRow.')')
                    ->setCellValue('R'.($HighestRow+1),'=SUM(R9:R'.$HighestRow.')')
                    //->setCellValue('S'.($HighestRow+1),'=SUM(S9:S'.$HighestRow.')')
                    ->setCellValue('T'.($HighestRow+1),'=SUM(T9:T'.$HighestRow.')')
                    ->setCellValue('B'.($HighestRow+3),'Salary to be paid:')
                    ->setCellValue('B'.($HighestRow+4),'Deductions for the insurances :')
                    ->setCellValue('B'.($HighestRow+5),'Deduction for PIT:')
                    ->setCellValue('B'.($HighestRow+6),'Total')
                    ->setCellValue('D'.($HighestRow+3),'=T' . ($HighestRow+1) )
                    ->setCellValue('D'.($HighestRow+4),'=M' . ($HighestRow+1) . '+N' . ($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+5),'=R' . ($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+6),'=D' . ($HighestRow+3) . '+D' . ($HighestRow+4) . '+D' . ($HighestRow+5))
                    ->setCellValue('E'.($HighestRow+3),'VND')
                    ->setCellValue('E'.($HighestRow+4),'VND')
                    ->setCellValue('E'.($HighestRow+5),'VND')
                    ->setCellValue('E'.($HighestRow+6),'VND')

                    ->setCellValue('G'.($HighestRow+4),'Lương')
                    ->setCellValue('G'.($HighestRow+5),'BH')
                    ->setCellValue('G'.($HighestRow+6),'PIT')
                    ->setCellValue('G'.($HighestRow+7),'DVP')

                    ->setCellValue('C'.($HighestRow+11),'Prepared by ')
                    ->setCellValue('C'.($HighestRow+16),'Dư Tú Quyên')
                    ->setCellValue('O'.($HighestRow+11),'Director')
                    ->setCellValue('O'.($HighestRow+16),'Trần Hiếu')
                    ->setCellValue('N'.($HighestRow+9),'Da Nang,         ')->mergeCells('N'.($HighestRow+9).':P'.($HighestRow+9))
                    ->setCellValue('N'.($HighestRow+10),'SERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS')->mergeCells('N'.($HighestRow+10).':P'.($HighestRow+10));
                $event->sheet->getDelegate()->getStyle('A1:T8')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow+1).':R'.($HighestRow+16))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A9:T' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B9:C' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C9:T' . ($HighestRow+1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'No.')->mergeCells('A7:A8');
                for ($i = 9; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 8);
                }
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

     /**
     * @return string
     */
    public function title(): string
    {
        return 'Employees';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B9';
    }

    public function columnFormats(): array
    {
        return [
            'C' => '#,##0',
            'D' => '#,##0',
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
            'P' => '#,##0',
            'O' => '#,##0',
            'Q' => '#,##0',
            'R' => '#,##0',
            'S' => '#,##0',
            'T' => '#,##0',
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    private function incomeForPIT($row) {
        //= lương thực nhận + phụ cấp - các khoản bảo hiểm - amout not for PIT (VND)
        // $amt =  ((((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"])
        //     + (((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"])
        //     + ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief)
        //     - ((((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
        //     ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
        //     ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100)))
        //     + (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
        //     ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
        //     ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100)))));
        $amt =  ((((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"])
        + (((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"])
        - ((((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
        ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
        ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100)))
        + (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
        ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
        ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100)))));
        $amt = $amt - ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief);
        return $amt > 0 ? $amt : 0;
    }

    //private function calPIT($salary, $numOfDependency) {
    private function calPIT($incomeUnderPit) {
        $pit = 0;
        //$incomeUnderPit = $salary - ($this->pitRelief + $this->dependencyPitRelief*$numOfDependency);

        if ($incomeUnderPit <= 0) {
            return $pit;
        }
        if ($incomeUnderPit > 80000000) {
            $pit = 15180000 + 0.35*($incomeUnderPit - 80000000);
            return $pit;
        }
        if ($incomeUnderPit > 52000000 && $incomeUnderPit <= 80000000) {
            $pit = 9750000 + 0.3*($incomeUnderPit  - 52000000);
            return $pit;
        }
        if ($incomeUnderPit > 32000000 && $incomeUnderPit <= 52000000) {
            $pit = 4750000 + 0.25*($incomeUnderPit  - 32000000);
            return $pit;
        }
        if ($incomeUnderPit > 18000000 && $incomeUnderPit <= 32000000) {
            $pit = 1950000 + 0.2*($incomeUnderPit - 18000000);
            return $pit;
        }
        if ($incomeUnderPit > 10000000 && $incomeUnderPit <= 18000000) {
            $pit = 750000 + 0.15*($incomeUnderPit - 10000000);
            return $pit;
        }
        if ($incomeUnderPit > 5000000 && $incomeUnderPit <= 10000000) {
            $pit =250000 + 0.1*($incomeUnderPit - 5000000);
            return $pit;
        }
        if ($incomeUnderPit <= 5000000) {
            $pit = 0.05*$incomeUnderPit;
            return $pit;
        }
        return $pit;
    }

    public function map($row): array
    {
        // $salary = ($row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"];
        // $pit = $this->calPIT($salary, $row["SoNguoiPhuThuoc"]);
        $amtNotForPIT = ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief);
        $incomeForPIT = $this->incomeForPIT($row);
        $pit = $this->calPIT($incomeForPIT);
        $vnd_paidWorkingDays = ((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"];
        $vnd_allowances = ((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"];
        $si_hi = ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
            ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100));
        $ui = ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100));
        $phi_ck = $row['Phi_CK']? $row['Phi_CK'] : 0;

        $remainingSalary = round($vnd_paidWorkingDays + $vnd_allowances - $si_hi - $ui - $pit - $phi_ck, 0);

        return [
            $row["Họ"] .' '. $row["Tên"],
            ((boolean) $row["USD"]) ? $row["Luong"] : 0,
            ((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"],
            $row["SoNgayLamViec"],
            $row["SoNgayNghiCoLuong"],
            ((boolean) $row["USD"]) ? $row["LuongTheoSoNgayLamViec"] : 0,
            $vnd_paidWorkingDays,//H
            ((boolean) $row["USD"]) ? $row["ThuNhapKhac"] : 0,
            $vnd_allowances,//J

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"])>$row["GoiHanTinhBHXH"])?
            $row["GoiHanTinhBHXH"] :
            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]),

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            $row["GoiHanTinhBHTN"]:
            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]),

            $si_hi,//M

            $ui,//N

            $row["SoNguoiPhuThuoc"],

            $amtNotForPIT, //($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief),
            $incomeForPIT,

            // ((((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"])
            // + (((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"])
            // + ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief)
            // - ((((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
            // ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
            // ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100)))
            // + (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            // ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
            // ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100))))),
            $pit,
            $phi_ck,
            $remainingSalary && $remainingSalary >=0 ? $remainingSalary : 0
        ];
    }
}
