<form method="POST" id="form-SettingUser" class="form-SettingUser">
    @csrf
    <div class="modal fade " id="modal-SettingUser" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 45%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="button-close-modal" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" >
                    <input type="hidden" class="form-control" name="id" id="id">
                    <input type="hidden" class="form-control" name="level" id="level">
                    <input type="hidden" class="form-control" name="password" id="password">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Name</span>
                        </div>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">User Name</span>
                        </div>
                        <input type="text" class="form-control" name="username" id="username">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Active</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="active" name="active" class="custom-control-input">
                                <label class="custom-control-label" for="active">actived</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Hồ sơ TC, CNNN</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSVPTCNN_create" name="HSVPTCNN_create" class="custom-control-input">
                                <label class="custom-control-label" for="HSVPTCNN_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSVPTCNN_edit" name="HSVPTCNN_edit" class="custom-control-input">
                                <label class="custom-control-label" for="HSVPTCNN_edit">edit</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Hồ sơ người lao động</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSNLD_create" name="HSNLD_create" class="custom-control-input">
                                <label class="custom-control-label" for="HSNLD_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSNLD_edit" name="HSNLD_edit" class="custom-control-input">
                                <label class="custom-control-label" for="HSNLD_edit">edit</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Hồ sơ thông tin thu hộ</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSTTTH_create" name="HSTTTH_create" class="custom-control-input">
                                <label class="custom-control-label" for="HSTTTH_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSTTTH_edit" name="HSTTTH_edit" class="custom-control-input">
                                <label class="custom-control-label" for="HSTTTH_edit">edit</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="HSTTTH_delete" name="HSTTTH_delete" class="custom-control-input">
                                <label class="custom-control-label" for="HSTTTH_delete">delete</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Lương-BH nhân viên</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="LBHNV_create" name="LBHNV[]" value="create" class="custom-control-input">
                                <label class="custom-control-label" for="LBHNV_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="LBHNV_edit" name="LBHNV[]" value="edit" class="custom-control-input">
                                <label class="custom-control-label" for="LBHNV_edit">edit</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="LBHNV_delete" name="LBHNV[]" value="delete" class="custom-control-input">
                                <label class="custom-control-label" for="LBHNV_delete">delete</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Cập nhật tỉnh thành</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="DmTinhThanh_create" name="DmTinhThanh_create" class="custom-control-input">
                                <label class="custom-control-label" for="DmTinhThanh_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="DmTinhThanh_edit" name="DmTinhThanh_edit" class="custom-control-input">
                                <label class="custom-control-label" for="DmTinhThanh_edit">edit</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Cập nhật quận huyện</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="DmQuanHuyen_create" name="DmQuanHuyen_create" class="custom-control-input">
                                <label class="custom-control-label" for="DmQuanHuyen_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="DmQuanHuyen_edit" name="DmQuanHuyen_edit" class="custom-control-input">
                                <label class="custom-control-label" for="DmQuanHuyen_edit">edit</label>
                            </div>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Cập nhật phường xã</span>
                        </div>
                        <div style="align-self: center; padding-left: 10px;">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="DmPhuongXa_create" name="DmPhuongXa_create" class="custom-control-input">
                                <label class="custom-control-label" for="DmPhuongXa_create">create</label>
                            </div>
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" id="DmPhuongXa_edit" name="DmPhuongXa_edit" class="custom-control-input">
                                <label class="custom-control-label" for="DmPhuongXa_edit">edit</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="saveBtn-SettingUser" class="btn btn-primary-outline">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</form>