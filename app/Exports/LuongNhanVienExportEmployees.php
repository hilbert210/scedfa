<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\LuongNhanVien;
use Carbon\Carbon;

class LuongNhanVienExportEmployees implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithDrawings, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection, $parameters)
    {
        $this->collection = $collection;
        $filter = [];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(83);
        $drawing->setOffsetX(80);
        $drawing->setCoordinates('B1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:M' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:N' . ($HighestRow + 1))
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('B1', 'BẢNG XÁC ĐỊNH SỐ TIỀN NỘP BẢO HIỂM XÃ HỘI')->mergeCells('B1:K1')
                    ->setCellValue('B2', 'Tháng ' . strtoupper(date('m/Y', strtotime(str_replace('/', '-', $this->collection[0]["Ngày Tính Lương"])))))->mergeCells('B2:K2')
                    ->setCellValue('C4', 'Tên đơn vị:')
                    ->setCellValue('E4', $this->collection[0]["TCNN"])->mergeCells('E4:H4')
                    ->setCellValue('A7', 'TT')->mergeCells('A7:A9')
                    ->setCellValue('B7', 'Họ và Tên')->mergeCells('B7:B9')
                    ->setCellValue('C7', 'Mức lương theo HĐ')->mergeCells('C7:E8')
                    ->setCellValue('C9', 'USD')
                    ->setCellValue('D9', 'Tỷ Giá')
                    ->setCellValue('E9', 'VNĐ')
                    ->setCellValue('F7', 'Phụ cấp')->mergeCells('F7:G8')
                    ->setCellValue('F9', 'USD')
                    ->setCellValue('G9', 'VNĐ')
                    ->setCellValue('H7', 'Quỹ tiền lương đóng BH')->mergeCells('H7:I7')
                    ->setCellValue('H8', 'BHXH, YT')
                    ->setCellValue('I8', 'BHTN')
                    ->setCellValue('H9', 'VNĐ')
                    ->setCellValue('I9', 'VNĐ')
                    ->setCellValue('J7', 'Số tiền nộp Bảo hiểm bắt buộc')->mergeCells('J7:L7')
                    ->setCellValue('J8', 'Quỹ hưu trí tử tuất')
                    ->setCellValue('K8', 'Quỹ KCB')
                    ->setCellValue('L8', 'Quỹ BHTN')
                    ->setCellValue('J9', $this->collection[0]["TrichNop_BHXH"] . '%')
                    ->setCellValue('K9', $this->collection[0]["BaoHiemYTe"] . '%')
                    ->setCellValue('L9', $this->collection[0]["BaoHiemThatNghiep"] . '%')
                    ->setCellValue('M7', 'Cộng tháng')->mergeCells('M7:M9')
                    ->setCellValue('N7', 'Ghi chú')->mergeCells('N7:N9')
                    ->setCellValue('A' . ($HighestRow + 1), 'CỘNG')->mergeCells('A' . ($HighestRow + 1) . ':B' . ($HighestRow + 1))
                    ->setCellValue('C' . ($HighestRow + 1), '=SUM(C10:C' . $HighestRow . ')')
                    ->setCellValue('E' . ($HighestRow + 1), '=SUM(E10:E' . $HighestRow . ')')
                    ->setCellValue('H' . ($HighestRow + 1), '=SUM(H10:H' . $HighestRow . ')')
                    ->setCellValue('I' . ($HighestRow + 1), '=SUM(I10:I' . $HighestRow . ')')
                    ->setCellValue('J' . ($HighestRow + 1), '=SUM(J10:J' . $HighestRow . ')')
                    ->setCellValue('K' . ($HighestRow + 1), '=SUM(K10:K' . $HighestRow . ')')
                    ->setCellValue('L' . ($HighestRow + 1), '=SUM(L10:L' . $HighestRow . ')')
                    ->setCellValue('M' . ($HighestRow + 1), '=SUM(M10:M' . $HighestRow . ')')
                    ->setCellValue('B' . ($HighestRow + 3), 'Số tiền phải nộp:')
                    ->setCellValue('C' . ($HighestRow + 3), '=M' . ($HighestRow + 1))->mergeCells('C' . ($HighestRow + 3) . ':D' . ($HighestRow + 3))
                    ->setCellValue('E' . ($HighestRow + 3), 'đồng')
                    ->setCellValue('B' . ($HighestRow + 6), 'Lập bảng')
                    ->setCellValue('G' . ($HighestRow + 6), 'Kiểm tra')
                    ->setCellValue('N' . ($HighestRow + 5), 'Đà Nẵng, ngày      tháng      năm 2020')
                    ->setCellValue('M' . ($HighestRow + 6), 'Thủ trưởng đơn vị');

                $event->sheet->getDelegate()->getStyle('B' . ($HighestRow + 6) . ':M' . ($HighestRow + 6))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('N' . ($HighestRow + 5))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'italic'    => true,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('N' . ($HighestRow + 5))
                    ->getAlignment()->setWrapText(false)->setHorizontal('right');
                $event->sheet->getDelegate()->getStyle('A1:N9')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));

                $event->sheet->getDelegate()->getStyle('A' . ($HighestRow + 1) . ':N' . ($HighestRow + 4))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      => 13,
                        'bold'      =>  true
                    )
                ));

                $event->sheet->getDelegate()->getStyle('A10:M' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B10:B' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C10:M' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT');
                for ($i = 10; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 9);
                }
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

    public function columnFormats(): array
    {
        return [
            'C' => '#,##0',
            'D' => '#,##0',
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Employees';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B10';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        $luong = $row["Luong"];
        $phucap = 0;
        if ((bool)$row["USD"]) {
            $luong = $row["Luong"] * $row["TyGia"];
        }

        $bhxh = $bhtn = $luong;
        if ( ($row["GoiHanTinhBHXH"] > 0)  && ($luong > $row["GoiHanTinhBHXH"]) ) {
            $bhxh = $row["GoiHanTinhBHXH"];
        }
        if ( ($row["GoiHanTinhBHTN"] > 0) && ($luong > $row["GoiHanTinhBHTN"]) ) {
            $bhtn = $row["GoiHanTinhBHTN"];
        }
        //luong va phu cap
        $bhxhVaPc = $bhxh;
        $bhtnVaPc = $bhtn;
        $phucap = 0;
        if (isset($row["PhuCap"]) && $row["PhuCap"] > 0) {
            $phucap =  $row["PhuCap"];
            // if ($row["USD"]) {
            //     $bhxhVaPc = $bhxhVaPc + $phucap * $row["TyGia"];
            //     $bhtnVaPc = $bhtnVaPc + $phucap * $row["TyGia"];
            // } else {
            //     $bhxhVaPc = $bhxhVaPc + $phucap;
            //     $bhtnVaPc = $bhtnVaPc + $phucap;
            // }
            if (isset($row["PhuCap_USD"])  && $row["PhuCap_USD"] == 1) {
                $bhxhVaPc = $bhxhVaPc + $phucap*$row["TyGia"];
                $bhtnVaPc = $bhtnVaPc + $phucap*$row["TyGia"];
            } else {
                $bhxhVaPc = $bhxhVaPc + $phucap;
                $bhtnVaPc = $bhtnVaPc + $phucap;
            }
        }
        if ( ($row["GoiHanTinhBHXH"] > 0)  && ($bhxhVaPc > $row["GoiHanTinhBHXH"]) ) {
            $bhxhVaPc = $row["GoiHanTinhBHXH"];
        }
        if ( ($row["GoiHanTinhBHTN"] > 0) && ($bhtnVaPc > $row["GoiHanTinhBHTN"]) ) {
            $bhtnVaPc = $row["GoiHanTinhBHTN"];
        }
        // $quyTuTuat = $bhxh * ($row["TrichNop_BHXH"] / 100);
        // $quyKCB = $bhxh * ($row["BaoHiemYTe"] / 100); // quy kcb = 4.5% cua bhyt, updated 18.11.2020
        // $quyBHTN = $bhtn * ($row["BaoHiemThatNghiep"] / 100); // quy bhtn
        $quyTuTuat = $bhxhVaPc * ($row["TrichNop_BHXH"] / 100);
        $quyKCB = $bhxhVaPc * ($row["BaoHiemYTe"] / 100); // quy kcb = 4.5% cua bhyt, updated 18.11.2020
        $quyBHTN = $bhtnVaPc * ($row["BaoHiemThatNghiep"] / 100); // quy bhtn
        $ghiChu = $row["GhiChu"];
        // $latest = LuongNhanVien::where('IdHoSoNhanVienTCNN', $row['IdHoSoNhanVienTCNN'])
        // ->orderBy('IdLuong', 'desc')->first();
        // if ($latest->GhiChu) {
        //     $ghiChu =   $latest->GhiChu;
        // }
        if (empty($ghiChu)) {
            $lastMonth = Carbon::parse($row['Ngày Tính Lương'])->subMonth();
            $prevMonthDate = date('Y-m-d H:i:s', $lastMonth->timestamp);
            $prevSalary = LuongNhanVien::where('NgayTinhLuong', $prevMonthDate)
             ->orderBy('NgayTinhLuong', 'desc')->first();
             if ($prevSalary) {
                $ghiChu = $prevSalary->GhiChu;
              }
        }
        if (isset($row['LuongDieuChinh'])) {
            $luongDC = $row['LuongDieuChinh'];
            $luonggoc = LuongNhanVien::where('IdLuong', $row['IdLuong'])->first();
            $luongTruocDC = $luonggoc->LuongMoi;
            $row["Luong"] = $luongDC - $luongTruocDC;
            if ($row["USD"]) {
                $luongDC = $row["LuongDieuChinh"] * $row["TyGia"];
                $luongTruocDC = $luonggoc->LuongMoi * $luonggoc->TyGia;
            }

            $luong = $luongDC - $luongTruocDC;
            $bhxh = $bhtn = $luong;
            $quyTuTuat = $bhxh * ($row["TrichNop_BHXH"] / 100);
            $quyKCB = $bhxh * ($row["BaoHiemYTe"] / 100); // quy kcb = 4.5% cua bhyt, updated 18.11.2020
            $quyBHTN = $bhtn * ($row["BaoHiemThatNghiep"] / 100); // quy bhtn
            $orgCurrency = $luonggoc->USD ? 'USD' : 'VNĐ';
            $destCurrency = $row['USD'] ? 'USD' : 'VNĐ';
            $bhxhVaPc = $bhtnVaPc = $luong;
            $ghiChu = $row["Note"] ? $row["Note"] : 'Truy thu tăng lương tháng ' . strtoupper(date('m/Y', strtotime(str_replace('/', '-', $row['NgayKTHieuLuc']))));
        }
        if (isset($row["DieuChinhLuong"])) {
            $ghiChu = "Tăng lương tháng " . strtoupper(date('m/Y', strtotime(str_replace('/', '-', $row['NgayTinhLuong']))));
        }
        return [
            $row["Họ"] . ' ' . $row["Tên"],
            ((bool) $row["USD"]) ? $row["Luong"] : 0,
            $row["TyGia"],
            $luong, //vnd
            isset($row["PhuCap_USD"]) && ((bool) $row["PhuCap_USD"]) ? $phucap : '',
            isset($row["PhuCap_USD"]) && $row["PhuCap_USD"] ? '' : $phucap,
            $bhxhVaPc, //bhxh vnd
            $bhtnVaPc, //bhtn vnd
            $quyTuTuat, //quy huu tu tuat
            $quyKCB, //quy KCB
            $quyBHTN, //quy BHTN
            $quyTuTuat + $quyKCB + $quyBHTN, //cong thang
            $ghiChu
        ];
    }
}
