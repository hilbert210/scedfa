<form method="POST" id="form-DmPhuongXa" class="form-DmPhuongXa">
    @csrf
    <div class="modal fade " id="modal-DmPhuongXa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cập nhật Phường Xã</h5>
                    <button type="button" id="button-close-modal" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" name="inputMAPHUONGXA" id="inputMAPHUONGXA">Mã phường xã</span>
                                    </div>
                                    <input type="text" class="form-control" name="MAPHUONGXA" id="MAPHUONGXA">
                                </div>
                                <div class="input-group dropdown-selects">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tên quận huyện</span>
                                    </div>
                                    <select class="form-control" name="MAQU_PXA" id="MAQU_PXA">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tên phường xã</span>
                                    </div>
                                    <input type="text" class="form-control" name="TENPXA" id="TENPXA">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Id phường xã</span>
                                    </div>
                                    <input type="text" class="form-control" name="IdPhuong" id="IdPhuong">
                                </div>
                            </div>
                            <div class="col-6">
                                <fieldset style="text-align: justify;">
                                    <legend>Hướng dẫn cập nhật Phường xã</legend>
                                    <h6>- Mã phường xã:
                                        <ul>
                                            <li>Mã phường xã <b>KHÔNG</b> được trùng.</li>
                                            <li>Kiểm tra mã phường xã cuối cùng ứng với mã quận huyện trước khi thêm tỉnh thành mới.</li>
                                            <li>Bao gồm 7 chữ số, 5 chữ số đầu là mã quận huyện, 2 chữ số cuối tăng dần từ 01 (Ví dụ: mã quận huyện là 10101 thì mã phường xã bắt đầu từ 1010101, mã quận huyện là 80102 thì mã phường xã bắt đầu từ 8010201).</li>
                                        </ul>
                                        <h6>- Id phường xã: Hai chữ số cuối trong mã phường xã.</h6>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="saveBtn-DmPhuongXa" class="btn btn-primary-outline">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</form>