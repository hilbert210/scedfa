<?php

namespace App\DataTables;

use App\DmQuanHuyen;
use App\User;
use App\Transformers\DmQuanHuyenTransformer;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Gate;

class DmQuanHuyenDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($this->query())->setTransformer(new DmQuanHuyenTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $MATT = $this->request()->get('MATT_QU');
        $query = DmQuanHuyen::leftJoin('DmTinhThanh', 'DmQuanHuyen.MATT_QU', '=', 'DmTinhThanh.MATT')
        ->when($MATT, function ($query) use ($MATT) {
            $query->where('DmQuanHuyen.MATT_QU', '=', $MATT);
        })
        ->groupBy('DmQuanHuyen.MAQU', 'DmTinhThanh.TENTT', 'DmQuanHuyen.MATT_QU', 'DmQuanHuyen.TENQUAN', 'DmQuanHuyen.IdQuan')
        ->select('DmQuanHuyen.MAQU', 'DmTinhThanh.TENTT', 'DmQuanHuyen.MATT_QU', 'DmQuanHuyen.TENQUAN', 'DmQuanHuyen.IdQuan')
        ->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['reload'];
        if(Gate::allows('DmQuanHuyen_edit')){
            array_unshift($buttons,'edit');
        }
        if(Gate::allows('DmQuanHuyen_create')){
            array_unshift($buttons,'create');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => false,
                'responsive' => false,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'asc']],
                'buttons'      =>  $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ["data" => "MAQU", 'title' => 'Mã Quận huyện'],
            ["data" => "MATT_QU", 'title' => 'Mã Tỉnh thành'],
            ["data" => "TENTT", 'title' => 'Tên Tỉnh thành'],
            ["data" => "TENQUAN", 'title' => 'Tên Quận huyện'],
            ["data" => "IdQuan", 'title' => 'Id Quận huyện']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DmQuanHuyen_' . date('YmdHis');
    }
}
