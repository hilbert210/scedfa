<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DmQuanHuyen extends Model
{
    protected $guarded  = [];
    protected $primaryKey = "MAQU";
    public $timestamps = false;
    protected $table = 'DmQuanHuyen';
}
