<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class LuongNhanVienDichVuExportCompany implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection,$filter)
    {
        $this->collection = $collection;
        $this->filter = $filter;

    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:R' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:R' . ($HighestRow+1))
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('A1', 'SERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS')->mergeCells('A1:G1')
                    ->setCellValue('A2', $this->collection[0]["TCNN"])->mergeCells('A2:G2')
                    ->setCellValue('A4', 'PAYROLL IN ' .  strtoupper(date('m/Y',strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"])))))->mergeCells('A4:S4')
                    ->setCellValue('Q5', 'Exchange rate:')
                    ->setCellValue('R5', number_format($this->collection[0]["TyGia"]))
                    ->setCellValue('Q6', 'Unit:')
                    ->setCellValue('R6', 'VND')
                    ->setCellValue('B7', 'Full name')->mergeCells('B7:B8')
                    ->setCellValue('C7', 'Title')->mergeCells('C7:C8')
                    ->setCellValue('D7', 'Basic salary')->mergeCells('D7:E7')
                    ->setCellValue('D8', 'USD')
                    ->setCellValue('E8', 'VND')
                    ->setCellValue('F7', 'Working days')->mergeCells('F7:F8')
                    ->setCellValue('G7', 'Paid leave days')->mergeCells('G7:G8')
                    ->setCellValue('H7', 'Salary paid for actual number of working days')->mergeCells('H7:I7')
                    ->setCellValue('H8', 'USD')
                    ->setCellValue('I8', 'VND')
                    ->setCellValue('J7', 'Allowances
& Over time')->mergeCells('J7:K7')
                    ->setCellValue('J8', 'USD')
                    ->setCellValue('K8', 'VND')
                    ->setCellValue('L7', 'The salary level used as the basis for calculation of insurance payment
(Exchange rate: '. number_format($this->collection[0]["TyGia_Insurance"]) .')')->mergeCells('L7:M7')
                    ->setCellValue('L8', 'SI,HI')
                    ->setCellValue('M8', 'UI')
                    ->setCellValue('N7', 'SI, HI and UI payments made by  '. $this->collection[0]["TenTatTCNN"] .'
(' . ($this->collection[0]["SI_Company"] + $this->collection[0]["HI_Company"] + $this->collection[0]["UI_Company"]) . '%)')->mergeCells('N7:O7')
                    ->setCellValue('N8', 'SI,HI')
                    ->setCellValue('O8', 'UI')
                    ->setCellValue('P7', 'Service fee
('. $this->collection[0]["PhiDichVu"] .'%)')->mergeCells('P7:P8')
                    ->setCellValue('Q7', 'Sub-Total')->mergeCells('Q7:Q8')
                    ->setCellValue('R7', 'Notes')->mergeCells('R7:R8')
                    ->setCellValue('A'.($HighestRow+1),'Total')->mergeCells('A'.($HighestRow+1).':B'.($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+1),'=SUM(D9:D'.$HighestRow.')')
                    ->setCellValue('E'.($HighestRow+1),'=SUM(E9:E'.$HighestRow.')')
                    ->setCellValue('F'.($HighestRow+1),'=SUM(F9:F'.$HighestRow.')')
                    ->setCellValue('G'.($HighestRow+1),'=SUM(G9:G'.$HighestRow.')')
                    ->setCellValue('H'.($HighestRow+1),'=SUM(H9:H'.$HighestRow.')')
                    ->setCellValue('I'.($HighestRow+1),'=SUM(I9:I'.$HighestRow.')')
                    ->setCellValue('J'.($HighestRow+1),'=SUM(J9:J'.$HighestRow.')')
                    ->setCellValue('K'.($HighestRow+1),'=SUM(K9:K'.$HighestRow.')')
                    ->setCellValue('L'.($HighestRow+1),'=SUM(L9:L'.$HighestRow.')')
                    ->setCellValue('M'.($HighestRow+1),'=SUM(M9:M'.$HighestRow.')')
                    ->setCellValue('N'.($HighestRow+1),'=SUM(N9:N'.$HighestRow.')')
                    ->setCellValue('O'.($HighestRow+1),'=SUM(O9:O'.$HighestRow.')')
                    ->setCellValue('P'.($HighestRow+1),'=SUM(P9:P'.$HighestRow.')')
                    ->setCellValue('Q'.($HighestRow+1),'=SUM(Q9:Q'.$HighestRow.')')
                    ->setCellValue('B'.($HighestRow+3),'Total amount paid by '. $this->collection[0]["TenTatTCNN"] .':')
                    ->setCellValue('B'.($HighestRow+4),'Including:')
                    ->setCellValue('C'.($HighestRow+4),'Salary: ')
                    ->setCellValue('C'.($HighestRow+5),'SI, HI, UI: ')
                    ->setCellValue('C'.($HighestRow+6),'Service fee: ')
                    ->setCellValue('E'.($HighestRow+3),'=Q' . ($HighestRow+1) )
                    ->setCellValue('E'.($HighestRow+4),'=I' . ($HighestRow+1) . '+K' . ($HighestRow+1))
                    ->setCellValue('E'.($HighestRow+5),'=N' . ($HighestRow+1) . '+O' . ($HighestRow+1))
                    ->setCellValue('E'.($HighestRow+6),'=P' . ($HighestRow+1))
                    ->setCellValue('F'.($HighestRow+3),'VND')
                    ->setCellValue('F'.($HighestRow+4),'VND')
                    ->setCellValue('F'.($HighestRow+5),'VND')
                    ->setCellValue('F'.($HighestRow+6),'VND')
                    ->setCellValue('C'.($HighestRow+11),'Made by')
                    ->setCellValue('C'.($HighestRow+16),'Dư Tú Quyên')
                    ->setCellValue('O'.($HighestRow+11),'Director')
                    ->setCellValue('O'.($HighestRow+16),'Trần Hiếu')
                    ->setCellValue('N'.($HighestRow+9),'Da Nang,         ')->mergeCells('N'.($HighestRow+9).':P'.($HighestRow+9))
                    ->setCellValue('N'.($HighestRow+10),'SERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS')->mergeCells('N'.($HighestRow+10).':P'.($HighestRow+10));
                $event->sheet->getDelegate()->getStyle('A1:R8')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow+1).':R'.($HighestRow+16))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A9:R' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B9:B' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('D9:Q' . ($HighestRow+1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('R9:R' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'No.')->mergeCells('A7:A8');
                for ($i = 9; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 8);
                }

            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

    public function columnFormats(): array
    {
        return [
            'D' => '#,##0',
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
            'P' => '#,##0',
            'O' => '#,##0',
            'Q' => '#,##0',
        ];
    }

     /**
     * @return string
     */
    public function title(): string
    {
        return 'Company';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B9';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        // dd($row);
        return [
            $row["Họ"] .' '. $row["Tên"],
            $row["ChucDanh"],
            ((boolean) $row["USD"]) ? $row["Luong"] : 0,
            ((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"],
            $row["SoNgayLamViec"],
            $row["SoNgayNghiCoLuong"],
            ((boolean) $row["USD"]) ? $row["LuongTheoSoNgayLamViec"] : 0,
            ((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"],
            ((boolean) $row["USD"]) ? $row["ThuNhapKhac"] : 0,
            ((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"],

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"])>$row["GoiHanTinhBHXH"])?
            $row["GoiHanTinhBHXH"] :
            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]),

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            $row["GoiHanTinhBHTN"]:
            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]),

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
            ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["SI_Company"] + $row["HI_Company"])/100)),

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["UI_Company"]/100)),

            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"] ) * ($row["PhiDichVu"]/100),

            (((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"])
            + (((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"])
            +  (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
            ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["SI_Company"] + $row["HI_Company"])/100)))

            + (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["UI_Company"]/100)))

            + ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"] ) * ($row["PhiDichVu"]/100)),

            $row["GhiChu"]
        ];
    }
}
