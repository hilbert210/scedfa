<nav class="navbar navbar-expand-sm navbar-light">
    <a class="navbar-brand pr-5 pl-5"><img src="{{asset('img/logo.png')}}" alt="logo" height="70px"></a>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a @if(request()->segment(1) == 'tcnn')
                    class="nav-link nav-link-fade-up nav-link-current" @else class="nav-link nav-link-fade-up" @endif
                    href="/tcnn">
                    Hồ sơ TC, CNNN
                </a>
            </li>
            <li class="nav-item">
                <a @if (request()->segment(1) == 'HoSoNLD')
                    class="nav-link nav-link-fade-up nav-link-current" @else class="nav-link nav-link-fade-up" @endif
                    href="/HoSoNLD">
                    Hồ sơ Người Lao Động
                </a>
            </li>
            <li class="nav-item">
                <a @if (request()->segment(1) == 'HoSoTTThuHo')
                    class="nav-link nav-link-fade-up nav-link-current" @else class="nav-link nav-link-fade-up" @endif
                    href="/HoSoTTThuHo">
                    Hồ sơ thông tin thu hộ
                </a>
            </li>
            <li class="nav-item">
                <a @if (request()->segment(1) == 'LuongNhanVien')
                    class="nav-link nav-link-fade-up nav-link-current" @else class="nav-link nav-link-fade-up" @endif
                    href="/LuongNhanVien">
                    Lương - BH Nhân Viên
                </a>
            </li>
        </ul>
        <div class="my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown dmenu">
                    <a class="nav-link nav-link-fade-up" href="#">
                        CHỈNH SỬA TT-QH-PX
                    </a>
                    <div class="dropdown-menu sm-menu">
                        <a class="dropdown-item" href="/DmTinhThanh">Tỉnh thành</a>
                        <a class="dropdown-item" href="/DmQuanHuyen">Quận huyện</a>
                        <a class="dropdown-item" href="/DmPhuongXa">Phường xã</a>
                    </div>
                </li>
                <li class="nav-item dropdown dmenu">
                    <a @if (request()->segment(1) == 'settings')
                        class="nav-link nav-link-fade-up nav-link-current" @else class="nav-link nav-link-fade-up" @endif href="#">
                        Welcome {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu sm-menu">
                        <?php if (Gate::allows('Settings')): ?>
                            <a class="dropdown-item" href="/SettingUser">Settings</a>
                            <a class="dropdown-item" href="/OptionConfig">Options</a>
                        <?php  endif;?>
                        <a class="dropdown-item" id='changePassword'>Change password</a>
                        <a class="dropdown-item" href="/logout">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<form method="POST" id="form-updatePassword" class="form-updatePassword">
    @csrf
    <div class="modal fade " id="modal-updatePassword" >
        <div class="modal-dialog modal-lg modal-dialog-centered"   style="width: 35%" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change Password</h5>
                    <button type="button" id="button-close-modal-updatePassword" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">New password</span>
                        </div>
                        <input id="new_password" name="new_password" type="password" class="form-control" required autofocus>
                    </div>
                    <div class="modal-footer">
                        <button id="saveBtnCP" class="btn btn-primary-outline">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>