<?php
namespace App\Transformers;

use App\HoSoTTThuHo;
use League\Fractal\TransformerAbstract;

class HoSoTTThuHoTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(HoSoTTThuHo $HoSoTTThuHo)
    {
        return [
            'IdChungTu'         => (int) $HoSoTTThuHo->IdChungTu,
            'TenTiengVietTCNN'  =>(string) ($HoSoTTThuHo->TenTiengAnhTCNN ? $HoSoTTThuHo->TenTiengAnhTCNN : $HoSoTTThuHo->TenTiengVietTCNN),
            'SoChungTu'         => (string) $HoSoTTThuHo->SoChungTu,
            'NgayChungTu'       => $HoSoTTThuHo->NgayChungTu,
            'NopTuThang'        =>$HoSoTTThuHo->NopTuThang ,
            'NopDenThang'       => $HoSoTTThuHo->NopDenThang,
            'TongTienNop'       =>  number_format($HoSoTTThuHo->TongTienNop).' VND',
            'BHXH'        => number_format($HoSoTTThuHo->BHXH) ,
            'TTN'       =>  number_format($HoSoTTThuHo->TTN),
            'Luong'        => number_format($HoSoTTThuHo->Luong),
            'BHTN'       =>  number_format($HoSoTTThuHo->BHTN),
            'DVP'        => number_format($HoSoTTThuHo->DVP),
            'TroCapBHXH'       =>  number_format($HoSoTTThuHo->TroCapBHXH),
            'PhiNganHang'        => number_format($HoSoTTThuHo->PhiNganHang),
            'TenTatTCNN'        => $HoSoTTThuHo->TenTatTCNN
        ];
    }
}