<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VPLVTCT extends Model
{
    protected $guarded  = ['Id'];
    protected $primaryKey = "Id";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'DmVPLVTCT';
}
