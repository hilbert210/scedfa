<?php
namespace App\Transformers;

use App\DmTCNN;
use League\Fractal\TransformerAbstract;

class TCNNTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(DmTCNN $DmTCNN)
    {
        return [
           'IdTCNN' => $DmTCNN->IdTCNN,
           'ThoiSuDung' => $DmTCNN->ThoiSuDung,
           'NgayThoiHoatDong' => $DmTCNN->NgayThoiHoatDong,
           'DangGiaHan' => $DmTCNN->DangGiaHan,
           'TenTiengVietTCNN' => (string)($DmTCNN->TenTiengAnhTCNN ? $DmTCNN->TenTiengAnhTCNN : $DmTCNN->TenTiengVietTCNN),
           'TenTatTCNN' => $DmTCNN->TenTatTCNN,
           'TruSoChinh' => $DmTCNN->TruSoChinh,
           'QuocTich' => $DmTCNN->QuocTich,
           'DiaBanHoatDong' => $DmTCNN->DiaBanHoatDong,
           'TenToChuc' => $DmTCNN->TenToChuc,
           'LoaiGP' => $DmTCNN->LoaiGP,
           'LinhVucHoatDong' => $DmTCNN->LinhVucHoatDong,
           'DiaChi' => $DmTCNN->DiaChi,
           'SLLaoDongVN' => $DmTCNN->SLLaoDongVN,
           'SoLuong' => $DmTCNN->SoLuong,
           'TenNguoiDaiDien' => $DmTCNN->TenNguoiDaiDien,
           'Email' => $DmTCNN->Email,
           'NamSinh' => $DmTCNN->NamSinh,
           'NgayHetHanGiayDKHD' => $DmTCNN->NgayHetHanGiayDKHD,
           'DienThoaiNguoiDaiDien' => $DmTCNN->DienThoaiNguoiDaiDien,
           'HoVaTen_admin' => $DmTCNN->HoVaTen_admin,
           'SDT_admin' => $DmTCNN->SDT_admin,
           'Email_admin' => $DmTCNN->Email_admin,
           'ChucDanh' => $DmTCNN->ChucDanh,
           'ThamGiaBHXH' => $DmTCNN->ThamGiaBHXH
        ];
    }
}
