<?php
namespace App\Transformers;

use App\DmTinhThanh;
use League\Fractal\TransformerAbstract;

class DmTinhThanhTransformer extends TransformerAbstract {
    public function transform(DmTinhThanh $DmTinhThanh) {
        return [
            'MATT' => $DmTinhThanh->MATT,
            'TENTT' => $DmTinhThanh->TENTT,
            'MAVUNG' => $DmTinhThanh->MAVUNG,
            'IdTinhThanh' => $DmTinhThanh->IdTinhThanh
        ];
    }
}