<?php

namespace App\Exports;

use App\HoSoNhanVienTCNN;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class HoSoNLDExport implements FromQuery, WithCustomStartCell, WithEvents, WithMapping, WithDrawings
{
    use Exportable;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     */
    public function __construct($parameters)
    {
        $filter = [
            'TinhTrangHoSo' => array_key_exists('TinhTrangHoSo', $parameters) ? $parameters['TinhTrangHoSo'] : [],
            'TinhThanhLamViec' => array_key_exists('TinhThanhLamViec', $parameters) ? $parameters['TinhThanhLamViec'] : null,
            'IdTCNN' => array_key_exists('IdTCNN', $parameters) ? $parameters['IdTCNN'] : null,
            'GioiTinh' => array_key_exists('GioiTinh', $parameters) ? $parameters['GioiTinh'] : null,
            'LoaiTCNN' => array_key_exists('LoaiTCNN', $parameters) ? $parameters['LoaiTCNN'] : null,
        ];
        $this->filter = $filter;
    }
    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(60);
        $drawing->setOffsetX(40);
        $drawing->setCoordinates('G1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:J' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:J' . $HighestRow)
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('G1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('G1:J1')
                ->setCellValue('G2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('G2:J2')
                ->setCellValue('G4', 'DANH SÁCH NGƯỜI LAO ĐỘNG VIỆT NAM LÀM VIỆC CHO CÁC TC, CNNN')->mergeCells('G4:J4')
                ->setCellValue('G5', 'TẠI MIỀN TRUNG - TÂY NGUYÊN')->mergeCells('G5:J5')->getStyle('G4:J5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  18,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B8:C' . $HighestRow)
                ->getAlignment()->setWrapText(true)->setHorizontal('left');
                $event->sheet->getDelegate()->getStyle('B5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('G1:J2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A7:I7')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $columns = ['I' => 45, 'J' => 45];
                foreach ($columns as $key => $value) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setWidth($value);
                };
                $columnAutoSize = ['A', 'B','C','D','E','F','G','H'];
                foreach ($columnAutoSize as $key ) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setAutoSize(true);
                };
                // Add column STT
               $event->sheet->getDelegate()->setCellValue('A7', 'STT')
               ->setCellValue('B7', 'Họ và tên')->mergeCells('B7:C7')
               ->setCellValue('D7', 'Ngày sinh')
               ->setCellValue('E7', 'Giới tính')
               ->setCellValue('F7', 'CMND/CCCD')
               ->setCellValue('G7', 'Tỉnh thành làm việc')
               ->setCellValue('H7', 'Số điện thoại')
               ->setCellValue('I7', 'Chức danh làm việc')
               ->setCellValue('J7', 'TC, CNNN');
                for ($i = 8; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 7);
                }

                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('I' . ($HighestRow + 2), 'Đà Nẵng, Ngày         Tháng         Năm 20         ')->mergeCells('I' . ($HighestRow + 2) . ':I' . ($HighestRow + 2))
                    ->setCellValue('I' . ($HighestRow + 3), 'Người Báo Cáo')->mergeCells('I' . ($HighestRow + 3) . ':I' . ($HighestRow + 3));
                $event->sheet->getDelegate()->getStyle('A' . ($HighestRow + 2) . ':I' . ($HighestRow + 3))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                        'italic'    => true
                    )
                ));
            },
        ];
    }

    public function query()
    {
        $IdTCNN = $this->filter['IdTCNN'];
        $TinhTrangHoSo = $this->filter['TinhTrangHoSo'];
        $MaTT_NoiLamViec = $this->filter['TinhThanhLamViec'];
        $CheckGender = is_numeric($this->filter['GioiTinh']) ? true  : false;
        $GioiTinh = $this->filter['GioiTinh'];
        $LoaiTCNN = $this->filter['LoaiTCNN'];
        $HoSoNhanVienTCNN =  HoSoNhanVienTCNN::query()
            ->leftJoin('DmChucDanh', 'DmChucDanh.IdChucDanh', '=', 'HoSoNhanVienTCNN.CVHT_ChucDanh')
            ->leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN.IdTCNN')
            ->leftJoin('DmTinhThanh', 'DmTinhThanh.MaTT', '=', 'HoSoNhanVienTCNN.MaTT_NoiLamViec')
            ->when($IdTCNN, function ($query, $IdTCNN) {
                $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
            })
            ->when($TinhTrangHoSo, function ($query, $TinhTrangHoSo) {
                $query->where('HoSoNhanVienTCNN.TinhTrangHoSo', '=', $TinhTrangHoSo);
            })
            ->when($CheckGender, function ($query) use ($GioiTinh) {
                $query->where('HoSoNhanVienTCNN.GioiTinh', '=', $GioiTinh);
            })
            ->when($MaTT_NoiLamViec, function ($query) use ($MaTT_NoiLamViec) {
                $query->where('HoSoNhanVienTCNN.MaTT_NoiLamViec', '=', $MaTT_NoiLamViec);
            })

            ->when($LoaiTCNN, function ($query, $LoaiTCNN) {
                $query->where('DmTCNN.LoaiTCNN', '=', $LoaiTCNN);
            })
            ->select(
                'HoSoNhanVienTCNN.Ho',
                'HoSoNhanVienTCNN.Ten',
                'HoSoNhanVienTCNN.NgaySinh',
                'HoSoNhanVienTCNN.SoCMND',
                'HoSoNhanVienTCNN.DienThoaiNhaRieng',
                'HoSoNhanVienTCNN.GioiTinh',
                'DmChucDanh.ChucDanh',
                'DmTinhThanh.TENTT',
                'DmTCNN.TenTatTCNN',
                DB::raw("CASE WHEN HoSoNhanVienTCNN.GioiTinh = 1 THEN N'Nam' WHEN HoSoNhanVienTCNN.GioiTinh = 0 THEN N'Nữ' ELSE '' END AS Gender ")
            );
            //->orderBy('DmTCNN.TenTatTCNN', 'desc');
        return $HoSoNhanVienTCNN;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B8';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Ho"],
            $row["Ten"],
            date('d/m/Y',strtotime($row["NgaySinh"])),
            $row["Gender"],
            $row["SoCMND"],
            $row["TENTT"],
            $row["DienThoaiNhaRieng"],
            $row["ChucDanh"],
            $row["TenTatTCNN"],
        ];
    }
}
