/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/HoSoNLD.js":
/*!*********************************!*\
  !*** ./resources/js/HoSoNLD.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var table;
$(document).ready(function () {
  $(window).keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  table = new $.fn.dataTable.Api('#HoSoNLD');

  function Address(type) {
    $('#MaTT_' + type).on("change", function () {
      var MaTT = $(this).val();

      if (MaTT) {
        $.ajax({
          url: route('DmQuanHuyenId', MaTT),
          type: "GET",
          dataType: "json",
          success: function success(data) {
            $('#MaQU_' + type).empty();
            $('#MaQU_' + type).prop('disabled', false).trigger("chosen:updated");
            $('#MaPhuongXa_' + type).empty();
            $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
            $('#MaQU_' + type).append('<option value=""></option>');
            $.map(data, function (item) {
              $('#MaQU_' + type).append('<option value="' + item.MAQU + '">' + item.TENQUAN + '</option>');
            });
            $('#MaQU_' + type).trigger("chosen:updated");
          }
        });
      } else {
        $('#MaQU_' + type).empty();
        $('#MaQU_' + type).prop('disabled', true).trigger("chosen:updated");
        $('#MaPhuongXa_' + type).empty();
        $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
        $('#SoNha_' + type).val('');
      }
    });
    $('#MaQU_' + type).on("change", function () {
      var MaQU = $(this).val();

      if (MaQU) {
        $.ajax({
          url: route('DmPhuongXaId', MaQU),
          type: "GET",
          dataType: "json",
          success: function success(data) {
            $('#MaPhuongXa_' + type).empty();
            $('#MaPhuongXa_' + type).append('<option value=""></option>');
            $('#MaPhuongXa_' + type).prop('disabled', false).trigger("chosen:updated");
            $.map(data, function (item) {
              $('#MaPhuongXa_' + type).append('<option value="' + item.MAPHUONGXA + '">' + item.TENPXA + '</option>');
            });
            $('#MaPhuongXa_' + type).trigger("chosen:updated");
          }
        });
      } else {
        $('#MaPhuongXa_' + type).empty();
        $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
      }
    });
  }

  Address('KhaiSinh');
  Address('NguyenQuan');
  Address('HoKhau');
  Address('ThuongTru');
  var DmTCNN = $('#IdTCNN, #TTDK_IdTCNN');

  if (DmTCNN.length > 0) {
    $.ajax({
      url: route('DmTCNN'),
      dataType: "json",
      success: function success(data) {
        $.map(DmTCNN, function (itemDmTCNN) {
          $(itemDmTCNN).chosen({
            placeholder_text_single: " "
          });
          $.map(data, function (item) {
            $(itemDmTCNN).append('<option value="' + item.IdTCNN + '">' + item.TenTiengVietTCNN + ' - [' + item.TenTatTCNN + ']</option>');
          });
          $(itemDmTCNN).trigger("chosen:updated");
        });
      }
    });
    DmTCNN.chosen({
      placeholder_text_single: " ",
      width: "100%"
    });
  }

  $('#TinhTrangHoSoFilter').chosen({
    placeholder_text_single: " ",
    width: "85%"
  });
  $('#TinhTrangHoSoFilter').trigger("chosen:updated");
  yadcf.init(table, [{
    column_number: 3,
    filter_type: "text",
    filter_delay: 500,
    filter_default_label: "",
    select_type_options: {
      theme: "bootstrap",
      width: "85%"
    }
  }]);
  $("#filterHoSoNLD").on("click", function () {
    yadcf.exFilterExternallyTriggered(table);
  });
  $("#resetHoSoNLD").on("click", function () {
    $('#DmTCNNFilter').val("");
    $('#DmTCNNFilter').trigger("chosen:updated");
    $('#TinhThanhLamViecFilter').val("");
    $('#TinhThanhLamViecFilter').trigger("chosen:updated");
    $('#TinhTrangHoSoFilter').val("");
    $('#TinhTrangHoSoFilter').trigger("chosen:updated");
    $('#filterRangeMonth').val("");
    yadcf.exResetAllFilters(table);
  });
  $('select[name="TinhTrangHoSo"]').on('change', function () {
    if (this.value == '0') {
      $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(4)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(1)').removeClass('hide');
      $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
    }

    if (this.value == '1') {
      $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
      $('.nav-tabs-bottom li:nth-child(2)').removeClass('hide');
      $('.nav-tabs-bottom li:nth-child(2) a').tab('show');
    }

    if (this.value == '2') {
      $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
      $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
      $('.nav-tabs-bottom li:nth-child(3)').removeClass('hide');
      $('.nav-tabs-bottom li:nth-child(3) a').tab('show');
    }
  });
  $('.chucVuHienTai select, .chucVuHienTai input').on('change', function () {
    $('.update-CD').removeClass('hide');
  });
  $('#button-upload-avatar').on('click', function () {
    $('#upload-avatar').click();
  });
  $('#upload-avatar').change(function (event) {
    readURL(this);
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      var filename = $('#upload-avatar').val();
      filename = filename.substring(filename.lastIndexOf('\\') + 1);

      reader.onload = function (e) {
        $('#picture').attr('src', e.target.result);
        $('#picture').hide();
        $('#picture').fadeIn(500);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  $('#button-upload-file-dinh-kem').on('click', function () {
    $('#upload-file-dinh-kem').click();
  });
  $('#upload-file-dinh-kem').change(function (event) {
    var dataURLs = new FormData();
    $('#loader').removeClass('d-none');
    $.each($('#upload-file-dinh-kem')[0].files, function (i, file) {
      dataURLs.append('file[' + i + ']', file);
    });
    dataURLs.append('Idtype', $('#MaNhanVien').val());
    dataURLs.append('type', 1);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      data: dataURLs,
      url: route('uploadfile'),
      type: "POST",
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      success: function success(data) {
        if (data) {
          $('#file-dinh-kem tbody').empty();
          data.forEach(function (item, index) {
            $('#file-dinh-kem tbody').append("<tr><td>".concat(item.IdPicture, "</td><td> <a target=\"_blank\" href=\"").concat(item.url, "\">").concat(item.name, "\n                        </a></td><td><button type=\"button\" class=\"btn btn-primary btn-sm mb-2 mt-1 remove-file\"  \n                        onclick=\"window.removeFile('").concat(item.IdPicture, "','").concat(item.name, "')\">remove</button></td></tr>"));
          });
        }

        $.toast({
          heading: "Success",
          text: "Upload file thàng công",
          showHideTransition: "fade",
          icon: "success"
        });
        $('#loader').addClass('d-none');
      },
      error: function error(data) {
        $.toast({
          heading: "Error",
          text: "Upload file không thàng công",
          showHideTransition: "fade",
          icon: "error"
        });
        $('#loader').addClass('d-none');
      }
    });
  });

  window.removeFile = function (IdPicture, name) {
    var dataURLs = new FormData();
    $('#loader').removeClass('d-none');
    dataURLs.append('IdLoai', $('#MaNhanVien').val());
    dataURLs.append('loai', 1);
    dataURLs.append('IdPicture', IdPicture);
    dataURLs.append('name', name);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      data: dataURLs,
      url: route('removeFile'),
      type: "POST",
      dataType: "json",
      cache: false,
      contentType: false,
      processData: false,
      success: function success(data) {
        if (data) {
          $('#file-dinh-kem tbody').empty();
          data.forEach(function (item, index) {
            $('#file-dinh-kem tbody').append("<tr><td>".concat(item.IdPicture, "</td><td> <a target=\"_blank\" href=\"").concat(item.url, "\">").concat(item.name, "\n                    </a></td><td><button type=\"button\" class=\"btn btn-primary btn-sm mb-2 mt-1 remove-file\"  \n                    onclick=\"window.removeFile('").concat(item.IdPicture, "','").concat(item.name, "')\">remove</button></td></tr>"));
          });
        }

        $.toast({
          heading: "Success",
          text: "Xóa file thàng công",
          showHideTransition: "fade",
          icon: "success"
        });
        $('#loader').addClass('d-none');
      },
      error: function error(data) {
        $.toast({
          heading: "Error",
          text: "Xóa file không thàng công",
          showHideTransition: "fade",
          icon: "error"
        });
        $('#loader').addClass('d-none');
      }
    });
  };

  function loadTableHistoryHSVN(dataID) {
    var historyHSNV = new $.fn.DataTable.Api('#historyHSNV');
    historyHSNV.clear().draw(false);
    $.get(route('QuaTrinhLamViec', dataID), function (data) {
      if (data) {
        if (!$.fn.DataTable.isDataTable("#historyHSNV")) {
          $("#historyHSNV").DataTable({
            lengthMenu: [[3, 10, 20, -1], [3, 10, 20, "All"]],
            select: true,
            buttons: ['edit'],
            dom: '<"top row"<"col-6"B><"col-6"p>>rt'
          });
        }

        var _table = $.fn.DataTable.Api('#historyHSNV');

        _table.on("select deselect", function (e, dt, type, indexes) {
          dt.button(".buttons-edit").enable(dt.rows({
            selected: true
          }).any());
        });

        data.forEach(function (item, index) {
          var ngayKetThuc = item.NgayKetThuc ? moment(item.NgayKetThuc).format('DD/MM/YYYY') : '';

          _table.row.add([item.Id, moment(item.NgayThayDoiChucDanh).format('DD/MM/YYYY'), ngayKetThuc, item.ChucDanh, item.TenTiengVietTCNN, item.TENTT]).draw(false);
        });
      }
    });
  }

  $('#button-update-CD').on('click', function () {
    IdHoSoNhanVienTCNN = $("#IdHoSoNhanVienTCNN").val();
    NgayThayDoiChucDanh = $("#NgayThayDoiChucDanh").val();
    IdChucDanh = $("#IdChucDanh").val();
    IdTCNN = $("#IdTCNN").val();
    MaTT_NoiLamViec = $("#MaTT_NoiLamViec").val();
    NoiLamViec = $("#NoiLamViec").val();
    SoVBNghiViec = $("#SoVBNghiViec").val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      data: {
        'IdHoSoNhanVienTCNN': IdHoSoNhanVienTCNN,
        'NgayThayDoiChucDanh': NgayThayDoiChucDanh,
        'IdChucDanh': IdChucDanh,
        'IdTCNN': IdTCNN,
        'MaTT_NoiLamViec': MaTT_NoiLamViec,
        'NoiLamViec': NoiLamViec,
        'SoVBNghiViec': SoVBNghiViec
      },
      url: route('updateChucDanh'),
      type: "POST",
      dataType: "json",
      success: function success(data) {
        $('.update-CD').addClass('hide');
        $.toast().reset("all");
        $.toast({
          heading: "Success",
          text: data.success,
          showHideTransition: "fade",
          icon: "success"
        });
        var dataHoSoNLD = $.fn.DataTable.Api('#HoSoNLD').rows('.selected').data()[0];
        var dataID = dataHoSoNLD[Object.keys(dataHoSoNLD)[0]];
        loadTableHistoryHSVN(dataID);
        table.cell(table.row({
          selected: true
        }).index(), 5).data(data.ChucDanh);
      },
      error: function error(data) {
        var errors = data.responseJSON.errors;
        $("form .text-danger").remove();
        $("form .pb-0").removeClass('pb-0');
        $("form .border-danger").removeClass('border border-danger');
        $.each(errors, function (key, value) {
          var ItemDOM = document.getElementById(key);
          var ErrorMessage = value;
          ItemDOM.parentElement.classList.add('pb-0');
          ItemDOM.parentElement.insertAdjacentHTML('afterend', "<div class=\"text-danger text-right\">".concat(ErrorMessage, "</div>"));
          ItemDOM.classList.add('border', 'border-danger');
        });
      }
    });
  });
  $('input[name="filterRangeMonth"]').daterangepicker({
    showDropdowns: true,
    autoUpdateInput: false,
    // startDate: moment().startOf('month').format('YYYY/MM/DD'),
    // endDate: moment().endOf('month').format('YYYY/MM/DD'),
    locale: {
      cancelLabel: "Clear",
      format: 'YYYY/MM/DD'
    }
  });
  $('input[name="filterRangeMonth"]').on("apply.daterangepicker", function (ev, picker) {
    $(this).val(moment(picker.startDate).startOf('month').format('YYYY/MM/DD') + "-" + moment(picker.endDate).endOf('month').format('YYYY/MM/DD'));
  });
});

/***/ }),

/***/ 2:
/*!***************************************!*\
  !*** multi ./resources/js/HoSoNLD.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/davishilbert/Documents/Enclave/Projects/scedfa/resources/js/HoSoNLD.js */"./resources/js/HoSoNLD.js");


/***/ })

/******/ });