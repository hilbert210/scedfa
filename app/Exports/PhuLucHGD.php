<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\Option;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
class PhuLucHGD implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithDrawings, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection, $parameters)
    {
        $this->collection = $collection;
        $filter = [];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(83);
        $drawing->setOffsetX(80);
        $drawing->setCoordinates('B1');
        return $drawing;
    }
    private function loadOption($dropDownCol, $options, $rcount, $lcount, $event, $prompt = null)
    {
        // set dropdown list for first data row
        $validation = $event->sheet->getCell("{$dropDownCol}10")->getDataValidation();
        $validation->setType(DataValidation::TYPE_LIST);
        $validation->setErrorStyle(DataValidation::STYLE_INFORMATION);
        $validation->setAllowBlank(false);
        $validation->setShowInputMessage(true);
        $validation->setShowErrorMessage(true);
        $validation->setShowDropDown(true);
        $validation->setErrorTitle('Input error');
        $validation->setError('Value is not in list.');
        if ($prompt) {
            $validation->setPrompt($prompt);
        }

        $validation->setFormula1(sprintf('"%s"', implode(',', $options)));
        // clone validation to remaining rows

        for ($i = 11; $i <= $rcount; $i++) {
            $event->sheet->getCell("{$dropDownCol}{$i}")->setDataValidation(clone $validation);
        }

        // set columns to autosize
        for ($i = 1; $i <= $lcount; $i++) {
            $column = Coordinate::stringFromColumnIndex($i);
            $event->sheet->getColumnDimension($column)->setAutoSize(true);
        }
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                $options = [
                    'Có',
                    'Không'
                ];
                $genders = [
                    'Nam',
                    'Nữ'
                ];
                $row_count = $event->sheet->getHighestRow();
                $column_count = $event->sheet->getHighestColumn();
                $this->loadOption('F', $options, $row_count, $column_count, $event);
                $this->loadOption('H', $genders, $row_count, $column_count, $event);
                // text center
                $event->sheet->getDelegate()->getStyle('A1:R' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:R' . ($HighestRow + 1))
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()
                    ->setCellValue('B1', 'BÁO CÁO BẢO HIỂM MẪU DC-02')->mergeCells('B1:K1')
                    ->setCellValue('B2', 'Tháng ' . strtoupper(date('m/Y', strtotime(str_replace('/', '-', $this->collection[0]["Ngày Tính Lương"])))))->mergeCells('B2:K2')
                    // ->setCellValue('D4', 'Tên đơn vị:')
                    // ->setCellValue('F4', $this->collection[0]["TCNN"])->mergeCells('F4:I4')
                    ->setCellValue('A7', 'TT')->mergeCells('A7:A9')
                    ->setCellValue('B7', 'Họ và Tên')->mergeCells('B7:B9')
                    ->setCellValue('C7', 'Mã số BHXH')->mergeCells('C7:C9')
                    ->setCellValue('D7', 'Số sổ BHXH')->mergeCells('D7:D9')
                    ->setCellValue('E7', 'Ngày sinh(Chỉ nhập năm nếu chỉ nhớ năm sinh)')->mergeCells('E7:E9')
                    ->setCellValue('F7', 'Chỉ có năm sinh (Có/Không)')->mergeCells('F7:F9')
                    ->setCellValue('G7', 'Mối quan hệ với chủ hộ')->mergeCells('G7:G9')
                    ->setCellValue('H7', 'Giới tính (Nam/Nữ)')->mergeCells('H7:H9')
                    ->setCellValue('I7', 'Nơi cấp giấy khai sinh')->mergeCells('I7:I9')
                    ->setCellValue('J7', 'Số CMND')->mergeCells('J7:J9')
                    ->setCellValue('K7', 'Ghi chú')->mergeCells('K7:K9')
                    ->setCellValue('L7', 'Họ tên chủ hộ')->mergeCells('L7:L9')
                    ->setCellValue('M7', 'Số điện thoại chủ hộ')->mergeCells('M7:M9')
                    ->setCellValue('N7', 'Loại giấy tờ')->mergeCells('N7:N9')
                    ->setCellValue('O7', 'Số giấy tờ')->mergeCells('O7:O9')
                    ->setCellValue('P7', 'Địa chỉ')->mergeCells('P7:P9')
                    ->setCellValue('Q7', 'Quốc tịch')->mergeCells('Q7:Q9')
                    ->setCellValue('R7', 'Dân tộc')->mergeCells('R7:R9');

                $event->sheet->getDelegate()->getStyle('B' . ($HighestRow + 6) . ':R' . ($HighestRow + 6))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('R' . ($HighestRow + 5))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'italic'    => true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('R' . ($HighestRow + 5))
                    ->getAlignment()->setWrapText(false)->setHorizontal('right');
                $event->sheet->getDelegate()->getStyle('A1:R9')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  10,
                        'bold'      =>  true
                    )
                ));

                $event->sheet->getDelegate()->getStyle('A' . ($HighestRow + 1) . ':R' . ($HighestRow + 4))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      => 10,
                        'bold'      =>  true
                    )
                ));

                $event->sheet->getDelegate()->getStyle('A10:M' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  10,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B10:B' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C10:L' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT');
                for ($i = 10; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 9);
                }
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        // echo '<pre>';
        // print_r($this->collection);
        // echo '</pre>';
        // var_dump($this->collection);
        return $this->collection;
    }

    public function columnFormats(): array
    {
        return [
            'C' => '#,##0',
            'D' => '####',
            'E' => '####',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
            'O' => '#,##0',
            'P' => '#,##0',
            'Q' => '#,##0',
            'R' => '#,##0'
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Phu luc HGD';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B10';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        //BHXH, YT and BHTN
        $luong = $row["Luong"];
        if ($row["USD"]) {
            $luong =  $row["Luong"] * $row["TyGia"];
        }
        $bhxh = $bhtn = $luong;
        if ($luong > $row["GoiHanTinhBHXH"]) {
            $bhxh = $row["GoiHanTinhBHXH"];
        }
        if ($luong > $row["GoiHanTinhBHTN"]) {
            $bhtn = $row["GoiHanTinhBHTN"];
        }

        //luong va phu cap
        $bhxhVaPc = $bhxh;
        $bhtnVaPc = $bhtn;
        $phucap = 0;
        if (isset($row["PhuCap"]) && $row["PhuCap"] > 0) {
            $phucap =  $row["PhuCap"];
            if ($row["USD"]) {
                $bhxhVaPc = $bhxhVaPc + $phucap * $row["TyGia"];
                $bhtnVaPc = $bhtnVaPc + $phucap * $row["TyGia"];
            } else {
                $bhxhVaPc = $bhxhVaPc + $phucap;
                $bhtnVaPc = $bhtnVaPc + $phucap;
            }
        }

        // $bhCSD = $bhxh*($row["PhanTram_CSD"]-1)/100 + 0.01*$bhtn;
        // $bhNLD = $bhxh*($row["PhanTram_NLD"]-1)/100 + 0.01*$bhtn;
        $conf = Option::getSystemConfig();
        $bhxh_csd = $conf['BHXH_BHYT_CSD'] / 100;
        $bhtn_csd = $conf['BHTN_CSD'] / 100;
        $bhxh_nld = $conf['BHXH_BHYT_NLD'] / 100;
        $bhtn_nld = $conf['BHTN_NLD'] / 100;
        $bhCSD = $bhxh * $bhxh_csd + $bhtn_csd * $bhtn;
        $bhNLD = $bhxh * $bhxh_nld + $bhtn_nld * $bhtn;

        return [
            $row["Họ"] . ' ' . $row["Tên"],
            $row['MaBHYT'] ? substr($row['MaBHYT'], strlen($row['MaBHYT']) - 10) : '', //ma bao hiem
            $row["SoSoBHXH"],
            $row["NgaySinh"] ? date('d/m/Y', strtotime($row["NgaySinh"])) : '', //ngay sinh
            '', //chi co nam sinh ?
            '',//moi quan he voi chu ho
            $row['GioiTinh'], //gioi tinh
            $row["SoNha_KhaiSinh"], //noi cap giay khai sinh
            $row["SoCMND"], //so cmnd
            $row["GhiChu"], //ghi chu
            '',//ho ten chu ho
            '',//so dien thoai chu ho
            '',//loai giay to
            '', //so giay to
            $row["SoNha_HoKhau"], //dia chi
            $row['QuocTich'], //quoc tich
            $row['TenDanToc'], //dan toc
        ];
    }
}