<?php
namespace App\Transformers;

use App\DmPhuongXa;
use League\Fractal\TransformerAbstract;

class DmPhuongXaTransformer extends TransformerAbstract {
    public function transform(DmPhuongXa $DmPhuongXa) {
        return [
            'MAPHUONGXA' => $DmPhuongXa->MAPHUONGXA,
            'MATT_QU' => $DmPhuongXa->MATT_QU,
            'TENTT' => $DmPhuongXa->TENTT,
            'MAQU_PXA' => $DmPhuongXa->MAQU_PXA,
            'TENQUAN' => $DmPhuongXa->TENQUAN,
            'TENPXA' => $DmPhuongXa->TENPXA,
            'VIETTAT' => $DmPhuongXa->VIETTAT,
            'IdPhuong' => $DmPhuongXa->IdPhuong
        ];
    }
}