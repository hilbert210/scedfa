@extends('master')
@section('title','Login')
@section('content')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" />
@endpush
<?php //Hiển thị thông báo thành công?>
@if ( Session::has('success') )
<div class="alert alert-success alert-dismissible" role="alert">
    <strong>{{ Session::get('success') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
</div>
@endif
<?php //Hiển thị thông báo lỗi?>
@if ( Session::has('error') )
<div class="alert alert-danger alert-dismissible" role="alert">
    <strong>{{ Session::get('error') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
</div>
@endif
@if ($errors->any())
<div class="alert alert-danger alert-dismissible" role="alert">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        <span class="sr-only">Close</span>
    </button>
</div>
@endif
<div class="container login">
    <div class="row justify-content-center p-5">
        <div class="logo">
            <img src="{{asset('img/logo-full.png')}}" alt="Logo" class="logo-login" />
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header text-center">
                <h3>Đăng Nhập</h3>
            </div>
            <div class="card-body">
                <form role="form" action="{{ url('/login') }}" method="POST">
                    {!! csrf_field() !!}
                    <div class="input-group form-group">
                        <div class="input-group-prepend" style="width: auto;">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Tài Khoản" name="username"
                            value="{{ old('username') }}" autofocus>

                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend" style="width: auto;">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="Mật Khẩu" name="password"
                           value="">
                    </div>
                    <div class="row align-items-center remember">
                        <input type="checkbox" checked name='remember_me' >Ghi Nhớ Tài Khoản
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Đăng Nhập" class="btn float-right login_btn">
                    </div>
                </form>
            </div>
        </div>


    </div>
</div>

@endsection