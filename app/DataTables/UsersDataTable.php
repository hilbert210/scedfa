<?php

namespace App\DataTables;

use App\Transformers\UserTransformers;
use App\User;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())->setTransformer(new UserTransformers);;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = User::select(
            'id',
            'name',
            'username',
            'active',
            'HSVPTCNN',
            'HSNLD',
            'HSTTTH',
            'LBHNV',
            'DmTinhThanh',
            'DmQuanHuyen',
            'DmPhuongXa'
        )
        ->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if(Gate::allows('Settings')){
            $buttons = ['create', 'edit'];
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->createdRow('function(row, data, dataIndex){
                if(data.active==false){
                        $(row).addClass("bg-danger");
                }
            }')
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => false,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'desc']],
                'buttons'      => $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ["data" => "id", 'title' => 'ID'],
            ["data" => "name", 'title' => 'Name'],
            ["data" => "username", 'title' => 'User Name'],
            ["data" => "active", 'title' => 'Activation'],
            ["data" => "HSVPTCNN", 'title' => 'Hồ Sơ VP TCNN'],
            ["data" => "HSNLD", 'title' => 'Hồ Sơ Người Lao Động'],
            ["data" => "HSTTTH", 'title' => 'Hồ Sơ TTTH'],
            ["data" => "LBHNV", 'title' => 'Lương-BH Nhân Viên'],
            ["data" => "DmTinhThanh", 'title' => 'Cập nhật TT'],
            ["data" => "DmQuanHuyen", 'title' => 'Cập nhật QH'],
            ["data" => "DmPhuongXa", 'title' => 'Cập nhật PX']

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Users_' . date('YmdHis');
    }
}
