<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DmPhuongXa;
use App\DataTables\DmPhuongXaDataTable;

class DmPhuongXaController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DmPhuongXaDataTable $dataTable)
    {
        return $dataTable->render('layouts.phuongxa.phuongxa');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rule = [
            'MAPHUONGXA' => 'required|unique:DmPhuongXa|min:7|max:7',
            'MAQU_PXA' => 'required',
            'TENPXA' => 'required',
            //'IdPhuong' => 'required|min:2|max:2',
        ];
        $customMessages = [
            'MAPHUONGXA.required' => 'Mã phường xã không được để trống!',
            'MAPHUONGXA.unique' => 'Trùng mã phường xã, mời nhập lại!',
            'MAPHUONGXA.min' => "Mã phường xã chỉ bao gồm 7 kí tự!",
            'MAPHUONGXA.max' => "Mã phường xã chỉ bao gồm 7 kí tự!",
            'MAQU_PXA.required' => 'Tên quận huyện không được để trống!',
            'TENPXA.required' => 'Tên phường xã không được để trống!',
            'IdPhuong.required' => 'Id phường xã không được để trống!',
            'IdPhuong.min' => "Id phường xã chỉ bao gồm 2 kí tự!",
            'IdPhuong.max' => "Id phường xã chỉ bao gồm 2 kí tự!",
        ];
        if ($request->get('isUpdatePhuongXa')) {
            $MAPHUONGXA = $request->get('MAPHUONGXA');
            $rule = [
                'MAPHUONGXA' => "required|unique:DmPhuongXa,MAPHUONGXA,{$MAPHUONGXA},MAPHUONGXA",
                //'IdPhuong' => "required|min:2|max:2",
            ];
            $customMessages = [
                'MAPHUONGXA.required' => 'Mã phường xã không được để trống!',
                'MAPHUONGXA.unique' => 'Trùng mã phường xã, mời nhập lại!',
                'MAPHUONGXA.min' => "Mã phường xã chỉ bao gồm 7 kí tự!",
                'MAPHUONGXA.max' => "Mã phường xã chỉ bao gồm 7 kí tự!",
                'IdPhuong.required' => 'Id phường xã không được để trống!',
                'IdPhuong.min' => "Id phường xã chỉ bao gồm 2 kí tự!",
                'IdPhuong.max' => "Id phường xã chỉ bao gồm 2 kí tự!",
            ];
        }
        $this->validate($request,$rule,$customMessages);
        $MAPHUONGXA = $request->get('MAPHUONGXA');

        $DmPhuongXa = DmPhuongXa::updateOrCreate(
            ['MAPHUONGXA' => $MAPHUONGXA],
            [
                'MAQU_PXA' => $request->get('MAQU_PXA'),
                'TENPXA' => $request->get('TENPXA'),
                'IdPhuong' => $request->get('IdPhuong')
            ]
        );
        $DmPhuongXa->save();
        return response()->json(['success' => 'Phường xã được lưu thành công.', 'data'=> $DmPhuongXa]);
    }

    public function edit($MAPHUONGXA)
    {
        $DmPhuongXa  = DmPhuongXa::find($MAPHUONGXA);
        return response()->json($DmPhuongXa);
    }
}
