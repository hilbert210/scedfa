<?php

namespace App\DataTables;

use App\Option;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;
use App\Transformers\OptionTransformer;
use Illuminate\Support\Facades\Gate;

class OptionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($this->query())->setTransformer(new OptionTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\Option $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = Option::select(
            'id',
            'name',
            'value',
            'description'
        )
            ->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (Gate::allows('Settings')) {
            $buttons = ['create', 'edit', 'delete'];
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => false,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'desc']],
                'buttons'      => $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ["data" => "id", 'title' => 'ID'],
            ["data" => "option_name", 'title' => 'Tên biến'],
            ["data" => "option_value", 'title' => 'Giá trị'],
            ["data" => "description", 'title' => 'Mô tả'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Option_' . date('YmdHis');
    }
}