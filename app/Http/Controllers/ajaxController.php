<?php

namespace App\Http\Controllers;

use App\DmTCNN;
use App\HoSoNLD_ChucDanh;
use App\LuongNhanVien;
use App\DieuChinhLuong;
use App\VPLVTCT;
use App\DmChucDanh;
use ArrayObject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ajaxController extends Controller
{
  public function __construct() {
    $this->middleware('auth');
  }
    public function DmTT()
  {
    return json_encode(DB::table('DmTinhThanh')->orderBy('TENTT', 'asc')->get());
  }
  public function DmTC()
  {
    return json_encode(DB::table('DmToChuc')->get());
  }
  public function DmQT()
  {
    return json_encode(DB::table('DmQuocTich')->orderBy('QuocTich', 'asc')->get());
  }
  public function DmCD()
  {
    return json_encode(DB::table('DmChucDanh')->orderBy('ChucDanh', 'asc')->get());
  }
  public function DmChucDanh() {
    $cd =DB::table('DmChucDanh')
    ->select('IdChucDanh','ChucDanh')->get();
    return $cd;
  }
  public function AddDmCD(Request $request)
  {
    $rule = [
      'ChucDanh' => 'required',
    ];
    $customMessages = [
      'ChucDanh.required' => 'Chức danh không được để trống!',
    ];
    $this->validate($request, $rule, $customMessages);
    $IdChucDanh = $request->get('IdChucDanh');
    $ChucDanh =  DmChucDanh::updateOrCreate(
      ['IdChucDanh' => $IdChucDanh],
      [
        'ChucDanh'=> $request->get('ChucDanh'),
        'GhiChu'=> null,
      ]
    );
    $ChucDanh->save();
    return response()->json(['success' => 'Thêm chức danh thành công!', 'ChucDanh' => $ChucDanh]);
  }

  public function AddDmQT(Request $request)
  {
    $MaQuocTich =DB::table('DmQuocTich')->orderByDesc('IdQuocTich')->select('DmQuocTich.IdQuocTich')->first()->IdQuocTich+1;
    $IdQuocTich =  DB::table('DmQuocTich')->insertGetId([
      'QuocTich'=> $request->get('QuocTich'),
      'MaQuocTich'=> $MaQuocTich,
      ]);
    return response()->json(['success' => 'Thêm quốc tịch thành công!', 'IdQuocTich' => $IdQuocTich]);
  }

  public function DmChauLuc()
  {
    $dmChauLucs = DB::table('DmChauLuc')
    ->join('DmQuocTich','DmQuocTich.IdChauLuc','=','DmChauLuc.ID')
    ->select('DmChauLuc.ChauLuc','DmQuocTich.MaQuocTich')
    ->get();
    $result = collect($dmChauLucs)
      ->groupBy('ChauLuc')
      ->map(function ($items) {
        $value=[];
        foreach ($items as $item) {
           array_push($value,$item->MaQuocTich);
        }
        return $value;
      });
    return json_encode($result);
  }
  public function DmDanToc()
  {
    return json_encode(DB::table('DmDanToc')->orderBy('TenDanToc', 'asc')->get());
  }
  public function AddDmDT(Request $request) {
    $MaDanToc =DB::table('DmDanToc')->orderByDesc('MaDanToc')->select('DmDanToc.MaDanToc')->first()->MaDanToc+1;
    $IdDanToc =  DB::table('DmDanToc')->insertGetId([
      'TenDanToc'=> $request->get('TenDanToc'),
      'MaDanToc'=> $MaDanToc,
      ]);
    return response()->json(['success' => 'Thêm dân tộc thành công!', 'IdDanToc' => $IdDanToc]);
  }
  public function DmTonGiao()
  {
    return json_encode(DB::table('DmTonGiao')->orderBy('TonGiao', 'asc')->get());
  }
  public function getMaxMATTbyMAVUNG($MAVUNG=null) {
    return json_encode(DB::table('DmTinhThanh')->when($MAVUNG,function($query,$MAVUNG){
      $query->where('MAVUNG', $MAVUNG)->max('MATT');
    }));
  }
  public function maVung(){
    return json_encode((DB::table('DmTinhThanh')->orderBy('MAVUNG', 'asc')->groupBy('MAVUNG')->get('MAVUNG')));
  }
  public function DmQuanHuyen($id=null)
  {
    return json_encode(DB::table('DmQuanHuyen')->when($id,function($query,$id){
      $query->where('MATT_QU','=',$id);
    })->get());
  }
  public function DmPhuongXa($id=null)
  {
    return json_encode(DB::table('DmPhuongXa')->when($id,function($query,$id){
      $query->where('MAQU_PXA','=',$id);
    })->get());
  }
  public function MaNhanVien(){
    $maNhanVien =DB::table('HoSoNhanVienTCNN')->orderByDesc('MaNhanVien')->select('HoSoNhanVienTCNN.MaNhanVien')->first()->MaNhanVien+1;
    if(strlen((string)$maNhanVien)<5){
      return '0'.$maNhanVien;
    }
    return $maNhanVien;
  }
  public function MaTCNN(){
    $MaTCNN =DB::table('DmTCNN')->orderByDesc('IdTCNN')->select('DmTCNN.IdTCNN')->first()->IdTCNN+1;
    if(strlen((string)$MaTCNN)<4){
      return '0'.$MaTCNN;
    }
    return $MaTCNN;
  }
  public function DmTCNN(){
    $DmTCNN =DB::table('DmTCNN')->where('ThoiSuDung','=',0)->orderBy('TenTiengVietTCNN')->select('DmTCNN.TenTiengVietTCNN', 'DmTCNN.TenTiengAnhTCNN', 'DmTCNN.TenTatTCNN', 'DmTCNN.IdTCNN', 'DmTCNN.LoaiTCNN')->get();
    return $DmTCNN;
  }
  public function QuaTrinhLamViec($id){
    $QuaTrinhLamViec =DB::table('HoSoNhanVienTCNN_ChucDanh')
    ->where('HoSoNhanVienTCNN_ChucDanh.IdHoSoNhanVienTCNN','=',$id)
    ->leftJoin('DmTCNN','HoSoNhanVienTCNN_ChucDanh.IdTCNN','=','DmTCNN.IdTCNN')
    ->leftJoin('DmChucDanh','HoSoNhanVienTCNN_ChucDanh.History_ChucDanh','=','DmChucDanh.IdChucDanh')
    ->leftJoin('DmTinhThanh','HoSoNhanVienTCNN_ChucDanh.MaTT_NoiLamViec','=','DmTinhThanh.MATT')
    ->select('HoSoNhanVienTCNN_ChucDanh.NgayThayDoiChucDanh','HoSoNhanVienTCNN_ChucDanh.NgayKetThuc',
    'HoSoNhanVienTCNN_ChucDanh.NoiLamViec',
    'HoSoNhanVienTCNN_ChucDanh.Id',
    'DmTCNN.TenTiengVietTCNN',
    'DmChucDanh.ChucDanh',
    'DmTinhThanh.TENTT')->orderByDesc('HoSoNhanVienTCNN_ChucDanh.NgayThayDoiChucDanh')
  ->get();
    return $QuaTrinhLamViec;
  }
  public function uploadfile(Request $request){
    $files = $request->file('file');
    $Idtype = $request->get('Idtype');
    $type = $request->get('type');
    $insert=[];
    foreach($files as $file){
      if ($file->isValid()) {
        $name = $file->getClientOriginalName();
        if(file_exists(public_path() . '/fileDinhKem/'. $name)){
          $name = pathinfo($name)['filename'] . "-" . time(). "." .pathinfo($name)['extension'];
        }
        $file->move(public_path() . '/fileDinhKem/', $name);
        array_push($insert, ['IdLoai' => $Idtype, 'loai' => $type, 'url' => "/fileDinhKem/" . $name, 'name' => $name]);
      }
    }
    if (!empty($insert)) {
      DB::table('DmHinhAnh')->insert($insert);
    }
    return DB::table('DmHinhAnh')->where('IdLoai','=',$Idtype)->where('loai','=',$type)->orderByDesc('IdPicture')->get();
  }
  public function removeFile(Request $request){
    $name = $request->get('name');
    $IdPicture = $request->get('IdPicture');
    $IdLoai = $request->get('IdLoai');
    $loai = $request->get('loai');
    unlink(public_path() . '/fileDinhKem/' . $name);
    DB::table('DmHinhAnh')->where('IdPicture','=',$IdPicture)->delete();
    return DB::table('DmHinhAnh')->where('IdLoai','=',$IdLoai)->where('loai','=',$loai)->orderByDesc('IdPicture')->get();
  }
  public function FileDinhKem($IdLoai,$loai){
    return DB::table('DmHinhAnh')->where('IdLoai','=',$IdLoai)->where('loai','=',$loai)->orderByDesc('IdPicture')->get();
  }
  public function updateChucDanh(Request $request){
    $rule = [
      //'IdHoSoNhanVienTCNN' => 'required',
      'NgayThayDoiChucDanh' => 'required',
      'CVHT_ChucDanh' => 'required',
      'IdTCNN' => 'required',
      'MaTT_NoiLamViec' => 'required',
    ];
    $customMessages = [
      'NgayThayDoiChucDanh.required' => 'Ngày thay đổi chức vụ không được để trống!',
      'CVHT_ChucDanh.required' => 'Chức danh không được để trống!',
      'IdTCNN.required' => 'TCNN không được để trống!',
      'MaTT_NoiLamViec.required' => 'Tỉnh Thành nơi làm việc không được để trống!',
      'NoiLamViec.required' => 'Nơi làm việc không được để trống!',
      'SoVBNghiViec.required' =>  'Số văn bản nghỉ việc không được để trống!'
    ];
    $this->validate($request, $rule, $customMessages);
    DB::table('HoSoNhanVienTCNN_ChucDanh')->insert([
      'IdHoSoNhanVienTCNN'=> $request->get('IdHoSoNhanVienTCNN'),
      'NgayThayDoiChucDanh'=> getDateFormat($request->get('NgayThayDoiChucDanh')),
      'History_ChucDanh'=> $request->get('History_ChucDanh'),
      'IdTCNN'=> $request->get('IdTCNN'),
      'MaTT_NoiLamViec'=> $request->get('MaTT_NoiLamViec'),
      'NoiLamViec'=> $request->get('NoiLamViec'),
      'SoVBNghiViec'=> $request->get('SoVBNghiViec')
      ]);
    DB::table('HoSoNhanVienTCNN')->where('IdHoSoNhanVienTCNN',$request->get('IdHoSoNhanVienTCNN'))->update([
      'NoiLamViec'=>$request->get('NoiLamViec'),
      'MaTT_NoiLamViec'=>$request->get('MaTT_NoiLamViec'),
      'CVHT_ChucDanh'=>$request->get('CVHT_ChucDanh'),
      'IdTCNN'=>$request->get('IdTCNN')
      ]);
    $chucDanh = DB::table('DmChucDanh')->where('IdChucDanh','=', $request->get('CVHT_ChucDanh'))->select('DmChucDanh.ChucDanh')->first()->ChucDanh;
    return response()->json(['success' => 'Thay đổi chức danh thành công!', 'ChucDanh' => $chucDanh]);
  }
  public function updateHistoryChucDanh(Request $request){
    $rule = [
      'NgayThayDoiChucDanh' => 'required',
      'History_ChucDanh' => 'required',
      'IdTCNN' => 'required',
      'MaTT_NoiLamViec' => 'required',
      //'SoVBNghiViec' => 'required'
    ];
    $customMessages = [
      'NgayThayDoiChucDanh.required' => 'Ngày thay đổi chức vụ không được để trống!',
      'History_ChucDanh.required' => 'Chức danh không được để trống!',
      'IdTCNN.required' => 'TCNN không được để trống!',
      'MaTT_NoiLamViec.required' => 'Tỉnh Thành nơi làm việc không được để trống!',
      'NoiLamViec.required' => 'Nơi làm việc không được để trống!',
      'SoVBNghiViec.required' =>  'Số văn bản nghỉ việc không được để trống!'
    ];
    $this->validate($request, $rule, $customMessages);
    DB::table('HoSoNhanVienTCNN_ChucDanh')->where('HoSoNhanVienTCNN_ChucDanh.Id', '=',$request->get('Id') )->update([
      'NgayThayDoiChucDanh'=> getDateFormat($request->get('NgayThayDoiChucDanh')),
      'NgayKetThuc'=> getDateFormat($request->get('NgayKetThuc')),
      'History_ChucDanh'=> $request->get('History_ChucDanh'),
      'IdTCNN'=> $request->get('IdTCNN'),
      'MaTT_NoiLamViec'=> $request->get('MaTT_NoiLamViec'),
      'NoiLamViec'=> $request->get('NoiLamViec'),
      'GhiChu'=> $request->get('GhiChu'),
      'SoVBNghiViec'=> $request->get('SoVBNghiViec')
      ]);
    return response()->json(['success' => 'Thay đổi chức danh thành công!']);
  }
  public function thongBaoNghiViec($id, $type)
  {
    if ($type=='true') {
      DB::table('HoSoNhanVienTCNN')->where('IdHoSoNhanVienTCNN', '=', $id)->update(['ThongBaoNghiViec' => true, 'NgayThongBaoNghiViec' => date('Y-m-d', time())]);
      return response()->json(['success' => 'Thay đổi thông báo nghỉ việc thành công!']);
    }
    else {
      DB::table('HoSoNhanVienTCNN')->where('IdHoSoNhanVienTCNN', '=', $id)->update(['ThongBaoNghiViec' => false]);
      return response()->json(['success' => 'Thay đổi thông báo nghỉ việc thành công!']);
    }
  }
  
  public function createSalary($IdTCNN,$type)
  {
    $NhanVienDaCoBangLuong = DB::table('HoSoNhanVienTCNN_LUONG_BH')
      ->leftJoin('HoSoNhanVienTCNN', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN')
      ->when($IdTCNN, function ($query, $IdTCNN) {
        $query->where('HoSoNhanVienTCNN.IdTCNN', '=', $IdTCNN);
      });
    $date = $NhanVienDaCoBangLuong->max('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong');
    $NhanVienDaCoBangLuong->where(function ($query) use ($date) {
      $start = date('Y-m-01 00:00:00', strtotime($date));
      $end  = date('Y-m-t 23:59:59', strtotime($date));
      $query->whereBetween('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', [$start, $end]);
    })
      ->select(
        'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN',
        'HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong',
        'HoSoNhanVienTCNN_LUONG_BH.LuongMoi',
        'HoSoNhanVienTCNN_LUONG_BH.LuongCu',
        'HoSoNhanVienTCNN_LUONG_BH.NgayHieuLuc',
        'HoSoNhanVienTCNN_LUONG_BH.TyGia',
        'HoSoNhanVienTCNN_LUONG_BH.USD',
        'HoSoNhanVienTCNN_LUONG_BH.NgayTao',
        'HoSoNhanVienTCNN_LUONG_BH.DoiTuong',
        'HoSoNhanVienTCNN_LUONG_BH.MaPA',
        'HoSoNhanVienTCNN_LUONG_BH.GhiChu',
        'HoSoNhanVienTCNN_LUONG_BH.Lai',
        'HoSoNhanVienTCNN_LUONG_BH.TrichNop_BHXH',
        'HoSoNhanVienTCNN_LUONG_BH.BaoHiemYTe',
        'HoSoNhanVienTCNN_LUONG_BH.BaoHiemThatNghiep',
        'HoSoNhanVienTCNN_LUONG_BH.GoiHanTinhBHXH',
        'HoSoNhanVienTCNN_LUONG_BH.GoiHanTinhBHTN',
        'HoSoNhanVienTCNN_LUONG_BH.TyGia_Insurance',
        'HoSoNhanVienTCNN_LUONG_BH.SoNgayLamViec',
        'HoSoNhanVienTCNN_LUONG_BH.SoNgayNghiCoLuong',
        'HoSoNhanVienTCNN_LUONG_BH.LuongTheoSoNgayLamViec',
        'HoSoNhanVienTCNN_LUONG_BH.PhiDichVu',
        'HoSoNhanVienTCNN_LUONG_BH.SoNguoiPhuThuoc',
        'HoSoNhanVienTCNN_LUONG_BH.ThuNhapKhac',
        'HoSoNhanVienTCNN_LUONG_BH.PhuCapDongBH',
        'HoSoNhanVienTCNN_LUONG_BH.PhuCap_USD',
        'HoSoNhanVienTCNN_LUONG_BH.ThuNhapKhac_USD',
        'HoSoNhanVienTCNN_LUONG_BH.SI_Company',
        'HoSoNhanVienTCNN_LUONG_BH.HI_Company',
        'HoSoNhanVienTCNN_LUONG_BH.UI_Company',
        'HoSoNhanVienTCNN_LUONG_BH.PhanTram_CSD',
        'HoSoNhanVienTCNN_LUONG_BH.PhanTram_NLD',
        'HoSoNhanVienTCNN_LUONG_BH.IdLuong',
        'HoSoNhanVienTCNN_LUONG_BH.Phi_CK',
        'HoSoNhanVienTCNN.Ho',
        'HoSoNhanVienTCNN.Ten',
        'HoSoNhanVienTCNN.NgayBatDauCongTac',
        'HoSoNhanVienTCNN.NgayThoiThamGiaBHXH',
        'HoSoNhanVienTCNN.NgayThamGiaBHXH'
      );
    $NhanVienDaCoBangLuong = $NhanVienDaCoBangLuong->get();
    $data = new ArrayObject();
    $monthAfter = !$NhanVienDaCoBangLuong->isEmpty() ? $NhanVienDaCoBangLuong[0]->NgayTinhLuong : date('Y-m-t');
    
    $NhanVienALL = DB::table('HoSoNhanVienTCNN')
      ->where('HoSoNhanVienTCNN.IdTCNN', '=', $IdTCNN)
      ->where(function($query) use ($monthAfter,$type){
        $date = null;
        if($type == 'create'){
          $date = date('Y-m-01 H:i:s',strtotime('+5 day',strtotime($monthAfter)));
        }else{
          $date = date('Y-m-01 H:i:s',strtotime($monthAfter));
        }
        $query->where('HoSoNhanVienTCNN.TinhTrangHoSo', '=', 1)
        ->orWhereDate('HoSoNhanVienTCNN.NgayThoiThamGiaBHXH','>=', $date );
      })
      ->select(
        'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN',
        'HoSoNhanVienTCNN.Ho',
        'HoSoNhanVienTCNN.Ten',
        'HoSoNhanVienTCNN.NgayBatDauCongTac',
        'HoSoNhanVienTCNN.NgayThoiThamGiaBHXH',
        'HoSoNhanVienTCNN.NgayThamGiaBHXH'
      )->get();
      $amtDieuChinh = 0;
    foreach ($NhanVienALL as $nhanvien) {
      $item = $this->search($nhanvien->IdHoSoNhanVienTCNN, $NhanVienDaCoBangLuong);
      $dieuchinh = DieuChinhLuong::where('IdHoSoNhanVienTCNN', $nhanvien->IdHoSoNhanVienTCNN)
      // ->where('NgayHieuLuc', '<=', $monthAfter)
      // ->where('NgayKTHieuLuc', '>=', $monthAfter)
      ->first();
      if ($dieuchinh && $dieuchinh->LuongDieuChinh) {
        $amtDieuChinh = $dieuchinh->LuongDieuChinh;
      }
      if ($item) {
        $item->NgayTinhLuong = $monthAfter;
        if(!$item->GhiChu) { $item->GhiChu = '';}
        if(!$item->Lai) { $item->Lai = 0;}
        if ($type =='create') {
          $item->MaPA =  null;
          // $item->ThuNhapKhac =  0;
          // $item->PhuCapDongBH =  0;
          $item->Lai =  0;
          $item->NgayHieuLuc =  null;
          $item->GhiChu =  '';
          $item->DoiTuong = null;
          $item->LuongCu =  $item->LuongMoi;
          //$item->LuongMoi = $amtDieuChinh;
        }
        $data->append((array) $item);
      } else {
        if((date('Y-m-01',strtotime($nhanvien->NgayThamGiaBHXH)) <= date('Y-m-t',strtotime(date('Y-m-t',($type == 'create') ? strtotime('+5 day',strtotime($monthAfter)): strtotime($monthAfter))))) || $nhanvien->NgayThamGiaBHXH == null ){
          $DoiTuong = 1;
          if ((strtotime(date('Y-m-t', strtotime(date('Y-m-t', ($type == 'create') ? strtotime('+5 day',strtotime($monthAfter)) : strtotime($monthAfter))))) - strtotime(date('Y-m-t', strtotime($nhanvien->NgayThamGiaBHXH)))) > 0) {
            $DoiTuong = 6;
          }
          $data->append([
            'IdHoSoNhanVienTCNN' => $nhanvien->IdHoSoNhanVienTCNN,
            'NgayTinhLuong' => $monthAfter, 'LuongMoi' => $amtDieuChinh ? $amtDieuChinh : 0, 'LuongCu' => 0, 'USD' => 0,
             'TyGia' => null, 'NgayHieuLuc' => null,
             'TrichNop_BHXH' => null, 'BaoHiemYTe' => null,
             'BaoHiemThatNghiep' => null, 'GoiHanTinhBHXH' => null,
            'NgayTao' => null,
             "Ho"=>$nhanvien->Ho,
             "Ten"=>$nhanvien->Ten,
             "DoiTuong"=> $DoiTuong,
             "MaPA"=> '',
             "GhiChu"=> '',
             "Lai"=> 0,
             'GoiHanTinhBHTN'=> null,
             'TyGia_Insurance'=> null,
             'SoNgayLamViec'=> 0,
             'SoNgayNghiCoLuong'=> 0,
             'LuongTheoSoNgayLamViec'=> 0,
             'PhiDichVu'=> 0,
             'SoNguoiPhuThuoc'=> 0,
             'ThuNhapKhac'=> 0,
            'PhuCapDongBH' => 0,
             'SI_Company'=> 0,
             'HI_Company'=> 0,
             'UI_Company'=> 0,
             'PhanTram_CSD'=> 0,
             'PhanTram_NLD'=> 0,
          ]);
        }
      }
    }
    $DichVu = $this->TypeExport($IdTCNN);
    $options = [];
    $options['PhuongAn'] = config('scedfa.PhuongAn');
    $options['DoiTuong'] = config('scedfa.DoiTuong');
    return response()->json(['data'=>$data, 'DichVu' => $DichVu, 'options' => $options]);
  }
  public function getSalaryByMonth(Request $request)
  {
    
    $IdTCNN =  $request->get('IdTCNN');
    $dateRange =  $request->get('dateRange');
    $NhanVienDaCoBangLuong = DB::table('HoSoNhanVienTCNN_LUONG_BH')
      ->leftJoin('HoSoNhanVienTCNN', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN')
      ->when($IdTCNN, function ($query, $IdTCNN) {
        $query->where('HoSoNhanVienTCNN.IdTCNN', '=', $IdTCNN);
      })
      ->when($dateRange, function ($query, $dateRange) {
        list($start, $end) = explode('-', $dateRange);
        $start = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $start)));
        $end  = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $end)));
        $query->whereBetween('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', [$start, $end]);
      })
      ->select(
        'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN',
        'HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong',
        'HoSoNhanVienTCNN_LUONG_BH.LuongMoi',
        'HoSoNhanVienTCNN_LUONG_BH.TyGia',
        'HoSoNhanVienTCNN_LUONG_BH.USD',
        'HoSoNhanVienTCNN_LUONG_BH.Lai',
        'HoSoNhanVienTCNN_LUONG_BH.SI_Company',
        'HoSoNhanVienTCNN_LUONG_BH.HI_Company',
        'HoSoNhanVienTCNN_LUONG_BH.UI_Company',
        'HoSoNhanVienTCNN_LUONG_BH.PhiDichVu',
        'HoSoNhanVienTCNN_LUONG_BH.PhanTram_CSD',
        'HoSoNhanVienTCNN_LUONG_BH.PhanTram_NLD',
        'HoSoNhanVienTCNN_LUONG_BH.TrichNop_BHXH',
        'HoSoNhanVienTCNN_LUONG_BH.BaoHiemYTe',
        'HoSoNhanVienTCNN_LUONG_BH.BaoHiemThatNghiep'
      )->get();
    $DichVu = $this->TypeExport($IdTCNN);
    $options = [];
    $options['PhuongAn'] = config('scedfa.PhuongAn');
    $options['DoiTuong'] = config('scedfa.DoiTuong');
    return response()->json(['data'=>$NhanVienDaCoBangLuong, 'DichVu' => $DichVu, 'options' => $options]);
  }
  public function search($value, $object)
  {
    foreach ($object as $item) {
      if ($item->IdHoSoNhanVienTCNN == $value) {
          return  $item;
      }
    }
    return false;
  }
  public function TypeExport($IdTCNN)
  {
    return DmTCNN::where('IdTCNN','=', $IdTCNN)->select('DmTCNN.DichVu')->first()->DichVu;
  }
  public function getHoSoNLDChucDanh($id)
    {
        $HoSoNLD_ChucDanh  = HoSoNLD_ChucDanh::find($id);
        return response()->json($HoSoNLD_ChucDanh);
    }
  public function getVPLVTCT($id)
  {
    $getVPLVTCT  = VPLVTCT::find($id);
    return response()->json($getVPLVTCT);
  }
  public function getVPLVTCTbyIdTCNN($id){
    $getVPLVTCTbyIdTCNN =DB::table('DmVPLVTCT')
    ->where('DmVPLVTCT.IdTCNN','=',$id)
    ->leftJoin('DmTinhThanh','DmVPLVTCT.MaTT','=','DmTinhThanh.MATT')
    ->select('DmVPLVTCT.Id',
    'DmVPLVTCT.DiaChi',
    'DmVPLVTCT.DienThoai',
    'DmVPLVTCT.HoVaTen_LH',
    'DmVPLVTCT.SDT_LH',
    'DmVPLVTCT.Email_LH',
    'DmTinhThanh.TENTT')->orderByDesc('DmVPLVTCT.Id')
  ->get();
    return $getVPLVTCTbyIdTCNN;
  }
  public function editOrCreateVPLVTCT(Request $request){
    $rule = [
        'MaTT' => 'required' 
    ];
    $customMessages = [
        'MaTT.required' => "TCNN không được để trống!"
    ];
    $this->validate($request, $rule, $customMessages);
    $Id = $request->get('Id');
    $VPLVTCT = VPLVTCT::updateOrCreate(
        ['Id' => $Id],
        [
            'IdTCNN' =>  $request->get('IdTCNN'),
            'MaTT' => $request->get('MaTT'),
            'DiaChi' =>  $request->get('DiaChi'),
            'DienThoai' =>  $request->get('DienThoai'),
            'HoVaTen_LH' =>  $request->get('HoVaTen_LH'),
            'SDT_LH' =>  $request->get('SDT_LH'),
            'Email_LH' =>  $request->get('Email_LH'),
        ]
    );
    $VPLVTCT->save();
    return response()->json(['success' => 'Văn phòng làm việc tại các tỉnh lưu thành công.', 'data'=> $VPLVTCT]);
  }
  public function destroyVPLVTCT($id)
  {
    VPLVTCT::destroy($id);
    return response()->json(['success' => 'Văn phòng làm việc tại các tỉnh xóa thành công.']);
  }
  public function updatePassword(Request $request)
  {
    $user = Auth::user();
    $user->password = Hash::make($request->get('new_password'));
    $user->save();
    return response()->json(['success' => 'Thay đổi mật khẩu thành công']);
  }

  public function createSalaryAll(Request $request)
  {
    $IdTCNN = $request->get('IdTCNN');
    $listLuongNhanVien =  LuongNhanVien::leftJoin('HoSoNhanVienTCNN', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN')
    ->where('HoSoNhanVienTCNN.TinhTrangHoSo','=',1)
    ->whereIn('HoSoNhanVienTCNN.IdTCNN',$IdTCNN)
    ->where(function ($query) {
      $date = $query->max('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong');
      $start = date('Y-m-01 00:00:00', strtotime($date));
      $end  = date('Y-m-t 23:59:59', strtotime($date));
      $query->whereBetween('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', [$start, $end]);
    })
    ->select(
      'HoSoNhanVienTCNN_LUONG_BH.*'
    )->get();
    foreach($listLuongNhanVien as $item){
      unset($item->IdLuong);
      $item->NgayTinhLuong =date('Y-m-t',strtotime($item->NgayTinhLuong."+10 day"));
      $item->LuongCu = $item->LuongMoi;
      $item->USD_LuongCu = $item->USD;
      $item->TyGia_LuongCu = $item->TyGia;
      $item->NgayHieuLuc = null;
      $item->NgayThucTe = null;
      $item->DoiTuong = null;
      $item->PhuCapDongBH = null;
      $item->ThuongKhac = null;
      $item->GhiChu = null;
      $item->ThuNhapKhac = null;
      $item->Lai = null;
      $item->MaPA = null;
      DB::table('HoSoNhanVienTCNN_LUONG_BH')->insert(json_decode(json_encode($item), true));
    }
    return response()->json(['success' => 'Tạo bảng lương thành công!']);
  }
  public function getSalaryByID($id){
    $getSalaryByID =LuongNhanVien::leftJoin('HoSoNhanVienTCNN', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN')
    ->where('HoSoNhanVienTCNN_LUONG_BH.IdLuong','=',$id)
    ->select(
      'HoSoNhanVienTCNN_LUONG_BH.IdLuong',
      'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN',
      'HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong',
      'HoSoNhanVienTCNN_LUONG_BH.LuongMoi',
      'HoSoNhanVienTCNN_LUONG_BH.LuongCu',
      'HoSoNhanVienTCNN_LUONG_BH.USD',
      'HoSoNhanVienTCNN_LUONG_BH.Lai',
      'HoSoNhanVienTCNN_LUONG_BH.NgayHieuLuc',
      'HoSoNhanVienTCNN_LUONG_BH.SoNguoiPhuThuoc',
      'HoSoNhanVienTCNN_LUONG_BH.ThuNhapKhac',
      'HoSoNhanVienTCNN_LUONG_BH.PhuCapDongBH',
      'HoSoNhanVienTCNN_LUONG_BH.GhiChu',
      'HoSoNhanVienTCNN_LUONG_BH.DoiTuong',
      'HoSoNhanVienTCNN_LUONG_BH.MaPA',
      "HoSoNhanVienTCNN.Ho",
      "HoSoNhanVienTCNN.Ten"
    )->get()->first();
    return $getSalaryByID;
  }

  public function destroyHistoryChucDanh($id){
    HoSoNLD_ChucDanh::destroy($id);
    return response()->json(['success' => 'Quá trình công tác xóa thành công.']);
  }
}