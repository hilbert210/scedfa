<?php
namespace App\Transformers;

use App\DmQuanHuyen;
use League\Fractal\TransformerAbstract;

class DmQuanHuyenTransformer extends TransformerAbstract {
    public function transform(DmQuanHuyen $DmQuanHuyen) {
        return [
            'MAQU' => $DmQuanHuyen->MAQU,
            'MATT_QU' => $DmQuanHuyen->MATT_QU,
            'TENTT' => $DmQuanHuyen->TENTT,
            'TENQUAN' => $DmQuanHuyen->TENQUAN,
            'IdQuan' => $DmQuanHuyen->IdQuan
        ];
    }
}