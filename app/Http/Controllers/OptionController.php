<?php

namespace App\Http\Controllers;

use App\DataTables\OptionDataTable;
use App\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(OptionDataTable $dataTable)
    {
        return $dataTable->render('layouts.option.option');
    }

    public function store(Request $request)
    {
        $rule = [
            'option_name' => 'required',
            'option_value' => 'required',
        ];
        $customMessages = [
            'option_name.required' => 'Tên biến không được để trống!',
            'option_value.required' => 'Giá trị biến không được để trống!'
        ];
        $this->validate($request, $rule, $customMessages);
        $data = json_decode($request->get('data'), true);

        if ($data['id'] != "") {
            $id = $data['id'];
            $option = Option::where('id', $id)->update(
                [
                    'name' => $data['option_name'],
                    'value' => floatval(preg_replace("/[^-0-9\.]/", "", $data['option_value'])),
                    'description' => $data['description'],
                ]
            );

        } else {
            $Option = new Option();
            $Option->name = $data['option_name'];
            $Option->value = floatval(preg_replace("/[^-0-9\.]/", "", $data['option_value']));
            $Option->description = !empty($data['description']) ? $data['description'] : '';
            $option = $Option->save();
        }

        if ($option ) {
            return response()->json(['success' => 'Option saved successfully.', 'data' => $option]);
        }
        return response()->json(['error' => 'Failed while creating new record.', 'data' => $option]);
    }

    public function edit($id)
    {
        $Option = Option::where('id', $id)->select('id', 'name', 'value', 'description', 'created_at', 'updated_at')->first();
        if ($Option) {
            return $Option;
        }
        return [];
    }
    public function destroy($id) {
        Option::destroy($id);
        return response()->json(['success' => 'Đã xóa biến thành công.']);
    }
}
