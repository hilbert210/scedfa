<?php

namespace App\Http\Controllers;

use App\DataTables\DmTCNNDataTable;
use App\DmTCNN;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DmTCNNController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DmTCNNDataTable $dataTable)
    {
        return $dataTable->render('layouts.tcnn.tcnn');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            // 'TenTiengAnhTCNN' => 'required',
            'TenTatTCNN' => 'required',
            'TruSoChinh' => 'required',
            'QuocTichTSC' => 'required',
            'LoaiTCNN' => 'required',
            'CoQuanQuanLy' =>  'required',
            'DiaBanHoatDong' => 'required',
            'LinhVucHoatDong' => 'required',
            'MaTT' => 'required',
            'DiaChi' => 'required',
            'DienThoai' => 'required',
            'DichVu' => 'required',
        ];
        $ruleGP = [
            'SoGiayPhep' => 'required',
            'NgayCapGiayPhep' => 'required',
            'NgayHetHanGiayDKHD' => 'required',
            'SLLaoDongVN' => 'required | numeric',
        ];
        if ($request->get('LoaiTCNN') != 3) {
            $rule = array_merge($ruleGP, $rule);
        }
        if ($request->get('LoaiTCNN') == 2) {
            $rule = [];
        }
        $customMessages = [
            'TenTiengVietTCNN.required' => 'Tên Tiếng việt không được để trống!',
            'TenTiengAnhTCNN.required' => 'Tên Tiếng anh không được để trống!',
            'TenTatTCNN.required' => 'Tên Tiếng viết tắt không được để trống!',
            'TruSoChinh.required' => 'Trụ sở chính không được để trống!',
            'QuocTichTSC.required' => 'Quốc tịch trụ sở chính không được để trống!',
            'LoaiTCNN.required' => 'Loại Hình TC,CN NN không được để trống!',
            'CoQuanQuanLy.required' =>  'Cơ Quan Quản Lý không được để trống!',
            'DiaBanHoatDong.required' => 'Địa Bàn Hoạt Động không được để trống!',
            'LinhVucHoatDong.required' => 'Lĩnh Vực Hoạt Động không được để trống!',
            'MaTT.required' => 'Tỉnh Thành không được để trống!',
            'DiaChi.required' => 'Địa Chỉ không được để trống!',
            'DienThoai.required' => 'Điện Thoại không được để trống!',
            'SoGiayPhep.required' => 'Số Giấy Phép không được để trống!',
            'NgayCapGiayPhep.required' => 'Ngày Cấp không được để trống!',
            'NgayHetHanGiayDKHD.required' => 'Ngày Hết Hạn không được để trống!',
            'SLLaoDongVN.required' => 'Số Lượng NLĐ CP không được để trống!',
        ];
        $this->validate($request, $rule, $customMessages);
        $IdTCNN = $request->get('IdTCNN');
        $MaTCNN = $request->get('MaTCNN');
        if (!$IdTCNN) {
            $MaTCNN = DB::table('DmTCNN')->orderByDesc('IdTCNN')->select('DmTCNN.IdTCNN')->first()->IdTCNN + 1;
            if (strlen((string)$MaTCNN) < 4) {
                $MaTCNN = '0' . $MaTCNN;
            }
        }

        $DmTCNN = DmTCNN::updateOrCreate(
            ['IdTCNN' => $IdTCNN],
            [
                'MaTCNN' => $MaTCNN,
                'TenTiengVietTCNN' => $request->get('TenTiengVietTCNN'),
                'TenTiengAnhTCNN' => $request->get('TenTiengAnhTCNN'),
                'TenTatTCNN' => $request->get('TenTatTCNN'),
                'LoaiTCNN' => $request->get('LoaiTCNN'),
                'MaTT' => $request->get('MaTT'),
                'DiaChi' => $request->get('DiaChi'),
                'DienThoai' => $request->get('DienThoai'),
                'CoQuanQuanLy' => $request->get('CoQuanQuanLy'),
                'SoGiayPhep' => $request->get('SoGiayPhep'),
                'NgayCapGiayPhep' => getDateFormat($request->get('NgayCapGiayPhep')),
                'LinhVucHoatDong' => $request->get('LinhVucHoatDong'),
                'DiaBanHoatDong' => convertArrayToString($request->get('DiaBanHoatDong')),
                'TenNguoiDaiDien' => $request->get('TenNguoiDaiDien'),
                'NamSinh' => getDateFormat($request->get('NamSinh')),
                'GioiTinh' => $request->get('GioiTinh'),
                'DienThoaiNguoiDaiDien' => $request->get('DienThoaiNguoiDaiDien'),
                'Email' => $request->get('Email'),
                'QuocTich' => $request->get('QuocTich'),
                'TCNN_ChucDanh' => $request->get('TCNN_ChucDanh'),
                'ThoiSuDung' => $request->get('ThoiSuDung') ? 1 : 0,
                'NgayHetHanGiayDKHD' => getDateFormat($request->get('NgayHetHanGiayDKHD')),
                'NgayThoiHoatDong' => getDateFormat($request->get('NgayThoiHoatDong')),
                'DangGiaHan' => $request->get('DangGiaHan') ? 1 : 0,
                'SLLaoDongVN' =>  $request->get('SLLaoDongVN'),
                'TruSoChinh' =>  $request->get('TruSoChinh'),
                'QuocTichTSC' =>  $request->get('QuocTichTSC'),
                'GhiChuNDD' =>  $request->get('GhiChuNDD'),
                'GhiChuTCNN' =>  $request->get('GhiChuTCNN'),
                'DichVu' => $request->get('DichVu'),
                'HoVaTen_admin' =>  $request->get('HoVaTen_admin'),
                'SDT_admin' =>  $request->get('SDT_admin'),
                'Email_admin' => $request->get('Email_admin'),
                'LoaiGP' => $request->get('LoaiGP'),
                'ThamGiaBHXH' => $request->get('TCBHXH')
            ]
        );
        $DmTCNN->save();
        $QuocTich = null;
        $TenToChuc = '';
        //$TenToChuc = DB::table('DmToChuc')->where('IdToChuc', '=', $request->get('LoaiTCNN'))->select('DmToChuc.TenToChuc')->first()->TenToChuc;
        if (DB::table('DmToChuc')->where('IdToChuc', '=', $request->get('LoaiTCNN'))->select('DmToChuc.TenToChuc')->first()) {
            $TenToChuc = DB::table('DmToChuc')->where('IdToChuc', '=', $request->get('LoaiTCNN'))->select('DmToChuc.TenToChuc')->first()->TenToChuc;
        }
        if (DB::table('DmQuocTich')->where('MaQuocTich', '=', $request->get('QuocTichTSC'))->select('DmQuocTich.QuocTich')->first()) {
            $QuocTich = DB::table('DmQuocTich')->where('MaQuocTich', '=', $request->get('QuocTichTSC'))->select('DmQuocTich.QuocTich')->first()->QuocTich;
        }
        $DmTCNN->TenToChuc = $TenToChuc;
        $DmTCNN->QuocTich = $QuocTich;
        $DmTCNN->SoLuong = $request->get('SoLuong');
        return response()->json(['success' => 'TCNN saved successfully.', 'data' => $DmTCNN]);
    }
    public function edit($id)
    {
        $TCNN  = DmTCNN::find($id);
        return response()->json($TCNN);
    }
}