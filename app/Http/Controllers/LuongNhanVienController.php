<?php

namespace App\Http\Controllers;

use App\DataTables\HoSoTTThuHoDataTable;
use App\DataTables\LuongNhanVienDataTable;
use App\HoSoNhanVienTCNN;
use App\LuongNhanVien;
use App\DieuChinhLuong;
use App\Option;
use App\LogThongTin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateInterval;
class LuongNhanVienController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LuongNhanVienDataTable $dataTable)
    {
        $tochuc = DB::table('DmTCNN')->select('idTCNN as Id', 'TenTiengVietTCNN as TenToChuc')->get();
        return $dataTable->render('layouts.luongNhanVien.luongNhanVien', ['tochuc' => $tochuc]);
    }
    public function showTCNN(LuongNhanVienDataTable $dataTable, $IdTCNN)
    {
        $tochuc = DB::table('DmTCNN')->select('idTCNN as Id', 'TenTiengVietTCNN as TenToChuc')->get();
        return $dataTable->with(['IdTCNN'=> $IdTCNN])->render('layouts.luongNhanVien.luongNhanVien', ['tochuc' => $tochuc]);
    }

    public function showNLD(LuongNhanVienDataTable $dataTable, $IdHoSoNhanVienTCNN)
    {
        return $dataTable->with(['IdHoSoNhanVienTCNN'=> $IdHoSoNhanVienTCNN])->render('layouts.luongNhanVien.luongNhanVien');
    }
    public function showTTThuHo(HoSoTTThuHoDataTable $dataTable, $IdTCNN)
    {
        return $dataTable->with(['IdTCNN'=> $IdTCNN])->render('layouts.hoSoTTThuHo.hoSoTTThuHo');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post') && $request->input('IdHoSoNhanVienTCNN') ) {
            $item = $request->all();
            $insert = LuongNhanVien::updateOrCreate(
                ['IdHoSoNhanVienTCNN' => $item['IdHoSoNhanVienTCNN'], 'IdLuong' => $item['IdLuong']],
                [
                    'LuongCu' => $item['LuongCu'],
                    'LuongMoi' => $item['LuongMoi'],
                    'USD' => isset($item['USD']) ? $item['USD'] : 0,
                    'NgayHieuLuc' => $item['NgayHieuLuc'] ? date('Y-m-d', strtotime($item['NgayHieuLuc'])) : null,
                    'ThuNhapKhac' => $item['ThuNhapKhac'],
                    'DoiTuong' => $item['DoiTuong'],
                    'MaPA' => $item['MaPA'],
                    'GhiChu' => $item['GhiChu'],
                    'Lai' => $item['Lai'],
                    'SoNguoiPhuThuoc' => $item['SoNguoiPhuThuoc'],
                ]
            );
            $insert->save();
            //save the log
            $oldData = LuongNhanVien::where('IdHoSoNhanVienTCNN', $item['IdHoSoNhanVienTCNN'])
            ->where('IdLuong', $item['IdLuong'])->first();
            LogThongTin::saveLog($insert, LogThongTin::TYPE_SUA_LUONG, $oldData, $item);
        } else {
             $data = json_decode($request->get('data'), true);
            $ngayDC = [];
            $bhCSD = 0.01*(Option::getSystemConfig('BHXH_BHYT_CSD') + Option::getSystemConfig('BHTN_CSD'));
            $bhNLD = 0.01*(Option::getSystemConfig('BHXH_BHYT_NLD') + Option::getSystemConfig('BHTN_NLD'));
            // echo '<pre>';
            // print_r($data);
            // echo '</pre>';

            foreach ($data as $item) {
                if (!$item['DieuChinh']) { //edit or create new salary
                    $type = LogThongTin::TYPE_TAO_LUONG;
                    $oldData = LuongNhanVien::where('IdHoSoNhanVienTCNN',  trim($item['IdHoSoNhanVienTCNN']))
                    ->where('NgayTinhLuong', $item['NgayTinhLuong'])->orderBy('IdLuong','DESC')->first();
                    $IdLuong = null;
                    if ($oldData) {
                        $type = LogThongTin::TYPE_SUA_LUONG;
                        $IdLuong = $oldData->IdLuong;
                    }
                    $insert = LuongNhanVien::updateOrCreate(
                        ['IdHoSoNhanVienTCNN' => trim($item['IdHoSoNhanVienTCNN']), 'NgayTinhLuong' => $item['NgayTinhLuong'],  'IdLuong' => $IdLuong],
                        [
                            'LuongCu' => $item['LuongCu'],
                            'LuongMoi' => $item['LuongMoi'],
                            'USD' => $item['USD'],
                            'NgayHieuLuc' => $item['NgayHieuLuc'] ? date('Y-m-d', strtotime($item['NgayHieuLuc'])) : null,
                            'ThuNhapKhac' => $item['ThuNhapKhac'],
                            'PhuCapDongBH' => $item['PhuCapDongBH'],
                            'DoiTuong' => $item['DoiTuong'],
                            'MaPA' => $item['MaPA'],
                            'GhiChu' => $item['GhiChu'],
                            'TyGia' => $item['TyGia'], //
                            'Lai' => $item['Lai'],
                            'TrichNop_BHXH' => $item['TrichNop_BHXH'], //
                            'BaoHiemYTe' => $item['BaoHiemYTe'], //
                            'BaoHiemThatNghiep' => $item['BaoHiemThatNghiep'], //
                            'GoiHanTinhBHXH' => $item['GoiHanTinhBHXH'], //
                            'GoiHanTinhBHTN' => $item['GoiHanTinhBHTN'], //
                            'TyGia_Insurance' => $item['TyGia_Insurance'], //
                            'SoNgayLamViec' => $item['SoNgayLamViec'], //
                            'SoNgayNghiCoLuong' => $item['SoNgayNghiCoLuong'], //
                            'LuongTheoSoNgayLamViec' => $item['LuongTheoSoNgayLamViec'], //
                            'PhiDichVu' => $item['PhiDichVu'], //
                            'SoNguoiPhuThuoc' => $item['SoNguoiPhuThuoc'],
                            'SI_Company' => $item['SI_Company'], //
                            'HI_Company' => $item['HI_Company'], //
                            'UI_Company' => $item['UI_Company'], //
                            'PhanTram_CSD' => $item['PhanTram_CSD'], //
                            'PhanTram_NLD' => $item['PhanTram_NLD'], //
                            'PhuCap_USD' => $item['PhuCap_USD'],
                            'ThuNhapKhac_USD' => $item['ThuNhapKhac_USD'],
                            'Phi_CK' => isset($item['Phi_CK']) ? $item['Phi_CK'] : null
                        ]
                    );
                    $insert->save();
                    LogThongTin::saveLog($insert, $type, $oldData, $item);
                } else { //dieu chinh luong
                    if (!isset($item['ItemID']) || $item['IdHoSoNhanVienTCNN'] != $item['ItemID']) {
                        continue;
                    }
                    $start    = (new \DateTime($item['NgayHieuLuc']))->modify('first day of this month');
                    $end      = (new \DateTime($item['Ngay_KT_HL']))->modify('first day of next month');
                    $interval = DateInterval::createFromDateString('1 month');
                    $period   = new \DatePeriod($start, $interval, $end);
                    foreach ($period as $dt) {
                         $ngayDC[] =  $dt->modify('last day of this month')->format("Y-m-d");

                         $namDC =  $dt->modify('last day of this month')->format("Y");
                         $thangDC = $dt->modify('last day of this month')->format("m");
                         $luongCanDC = LuongNhanVien::where('IdHoSoNhanVienTCNN',
                            trim($item['IdHoSoNhanVienTCNN']))
                            ->whereYear('NgayTinhLuong', '=', $namDC)
                            ->whereMonth('NgayTinhLuong', '=', $thangDC)
                            ->first();
                        if (!empty($luongCanDC)) { //dieu chinh luong cac thang truoc thoi diem bang luong hien tai
                            $truythuBH_CSD = 0;
                            $truythuBH_NLD = 0;
                            if ($item['LuongMoi'] > $luongCanDC->LuongMoi) {
                                $truythuBH_CSD = ($item['LuongMoi'] - $luongCanDC->LuongMoi )*$bhCSD;
                                $truythuBH_NLD = ($item['LuongMoi'] - $luongCanDC->LuongMoi) * $bhNLD;
                            }
                            $rDieuChinh = DieuChinhLuong::updateOrCreate(
                                ['IdHoSoNhanVienTCNN' => trim($item['IdHoSoNhanVienTCNN']), 'IdLuong' => $luongCanDC->IdLuong],
                                [
                                    'LuongTruocDC' => $luongCanDC->LuongMoi,
                                    'LuongDieuChinh' => $item['LuongMoi'],
                                    'USD' => $luongCanDC->USD,
                                    'NgayHieuLuc' => $dt->modify('first day of this month')->format("Y-m-d H:i:s"),
                                    'TruyThuBH_CSD' => $truythuBH_CSD,
                                    'TruyThuBH_NLD' => $truythuBH_NLD,
                                    'NgayDieuChinh' => $item['NgayTinhLuong'] ? date('Y-m-d', strtotime($item['NgayTinhLuong'])) : null,
                                    'NgayKTHieuLuc' => $luongCanDC->NgayTinhLuong ? date('Y-m-d', strtotime($luongCanDC->NgayTinhLuong)) : null,
                                    //'PhuCap_USD' => $luongCanDC->PhuCap_USD
                                   // 'Note' => $item['GhiChu'] ? $item['GhiChu'] : ''
                                ]
                                );
                            $rDieuChinh->save();
                            //if ($item["NgayTinhLuong"] == $rDieuChinh->NgayKTHieuLuc) {
                                $insertDC = LuongNhanVien::updateOrCreate(
                                    //['IdHoSoNhanVienTCNN' => trim($item['IdHoSoNhanVienTCNN']), 'NgayTinhLuong' => $item['NgayTinhLuong']],
                                    ['IdHoSoNhanVienTCNN' => trim($item['IdHoSoNhanVienTCNN']), 'NgayTinhLuong' => $dt->modify('last day of this month')->format("Y-m-d")],
                                    [
                                        'LuongMoi'=>$rDieuChinh->LuongDieuChinh,
                                        'DieuChinhLuong' => 1
                                    ]);
                                $insertDC->save();
                            LogThongTin::saveLog($insertDC, LogThongTin::TYPE_DIEU_CHINH_LUONG, $luongCanDC, $item);
                            //}
                        } else { //dieu chinh cho thang sau
                            $truythuBH_CSD = 0;
                            $truythuBH_NLD = 0;
                            $dieuchinh = new DieuChinhLuong();
                            $dieuchinh->IdHoSoNhanVienTCNN = trim($item['IdHoSoNhanVienTCNN']);
                            $dieuchinh->LuongTruocDC = $item['LuongCu'] ? $item['LuongCu'] : 0;
                            $dieuchinh->LuongDieuChinh = $item['LuongMoi'];
                            $dieuchinh->USD = $item['USD'];
                            $dieuchinh->PhuCap_USD = $item['PhuCap_USD'];
                            $dieuchinh->NgayHieuLuc = $dt->modify('first day of this month')->format("Y-m-d H:i:s");
                            $dieuchinh->TruyThuBH_CSD = $truythuBH_CSD;
                            $dieuchinh->TruyThuBH_NLD = $truythuBH_NLD;
                            $dieuchinh->NgayDieuChinh = $item['NgayTinhLuong'] ? date('Y-m-d', strtotime($item['NgayTinhLuong'])) : null;
                            $dieuchinh->NgayKTHieuLuc = $dt->modify('last day of this month')->format("Y-m-d H:i:s");
                            $dieuchinh->Note = $item['GhiChu'] ? $item['GhiChu'] : '';
                            $dieuchinh->save();
                            LogThongTin::saved($dieuchinh, LogThongTin::TYPE_DIEU_CHINH_LUONG, null, $item);
                        }
                    }
                }
            }
        }
        return response()->json(['success' => 'Tạo Lương thành công!']);
    }

    public function edit($id)
    {
        $LuongNhanVien = LuongNhanVien::find($id);
        return response()->json($LuongNhanVien);
    }

    public function destroy($id)
    {
        LuongNhanVien::destroy($id);
        return response()->json(['success' => 'Hồ sơ người lao động xóa thành công.']);
    }
}
