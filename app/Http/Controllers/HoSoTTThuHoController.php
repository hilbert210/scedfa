<?php

namespace App\Http\Controllers;

use App\DataTables\HoSoTTThuHoDataTable;
use App\HoSoTTThuHo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HoSoTTThuHoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HoSoTTThuHoDataTable $dataTable)
    {
        return $dataTable->render('layouts.hoSoTTThuHo.hoSoTTThuHo');
    }

    public function show(HoSoTTThuHoDataTable $dataTable, $IdTCNN)
    {
        die($IdTCNN);
        return $dataTable->with('IdTCNN',$IdTCNN)->render('layouts.hoSoTTThuHo.hoSoTTThuHo');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            
        ];
        $customMessages = [
        ];
        $this->validate($request, $rule, $customMessages);
        $IdChungTu = $request->get('IdChungTu');
        $HoSoTTThuHo = HoSoTTThuHo::updateOrCreate(
            ['IdChungTu' => $IdChungTu],
            [
                'SoChungTu' =>  $request->get('SoChungTu'),
                'NgayChungTu' => getDateFormat($request->get('NgayChungTu')),
                'IdTCNN' =>  $request->get('IdTCNN'),
                'NopTuThang' =>  getDateFormat($request->get('NopTuThang')),
                'NopDenThang' =>  getDateFormat($request->get('NopDenThang')),
                'GhiChu' =>  $request->get('GhiChu'),
                'NguoiNop' =>  $request->get('NguoiNop'),
                'TongTienNop' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('TongTienNop'))),
                'BHXH' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('BHXH'))),
                'TTN' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('TTN'))),
                'Luong' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('Luong'))),
                'BHTN' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('BHTN'))),
                'DVP' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('DVP'))),
                'TroCapBHXH' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('TroCapBHXH'))),
                'PhiNganHang' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('PhiNganHang'))),
                'Lai' =>  floatval(preg_replace("/[^-0-9\.]/","",$request->get('Lai')))
            ]
        );
        $HoSoTTThuHo->save();
        $TenTiengVietTCNN =$request->get('IdTCNN') ?   DB::table('DmTCNN')->where('IdTCNN','=',$request->get('IdTCNN'))->select('DmTCNN.TenTiengVietTCNN')->first()->TenTiengVietTCNN : null;
        $HoSoTTThuHo->TenTiengVietTCNN = $TenTiengVietTCNN;
        return response()->json(['success' => 'Hồ so thu hộ lưu thành công.', 'data'=> $HoSoTTThuHo]);
    }
    public function edit($id)
    {
        $HoSoTTThuHo = HoSoTTThuHo::find($id);
        return response()->json($HoSoTTThuHo);
    }
    public function destroy($id)
    {
        HoSoTTThuHo::destroy($id);
        return response()->json(['success' => 'Hồ sơ thu hộ xóa thành công.']);
    }
}

