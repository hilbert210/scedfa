<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DmChucDanh extends Model
{
    protected $guarded  = ['IdChucDanh'];
    protected $primaryKey = "IdChucDanh";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'DmChucDanh';
}
