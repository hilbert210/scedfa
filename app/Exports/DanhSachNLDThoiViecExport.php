<?php

namespace App\Exports;

use App\HoSoNhanVienTCNN;
use App\HoSoNLD_ChucDanh;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class DanhSachNLDThoiViecExport implements FromQuery, WithCustomStartCell, WithEvents, WithDrawings, WithMapping
{
    use Exportable;

    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     */
    public function __construct($parameters)
    {
        $filter = [
            'filterRangeMonth' =>  $parameters['filterRangeMonth'],
            'TinhTrangHoSo' => array_key_exists('TinhTrangHoSo', $parameters) ? $parameters['TinhTrangHoSo'] : [],
            'TinhThanhLamViec' => array_key_exists('TinhThanhLamViec', $parameters) ? $parameters['TinhThanhLamViec'] : null,
            'IdTCNN' => array_key_exists('IdTCNN', $parameters) ? $parameters['IdTCNN'] : null,
            'GioiTinh' => array_key_exists('GioiTinh', $parameters) ? $parameters['GioiTinh'] : null,
        ];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(60);
        $drawing->setOffsetX(40);
        $drawing->setCoordinates('C1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:J' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:J' . $HighestRow)
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('C1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('C1:F1')
                    ->setCellValue('C2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('C2:F2')
                    ->setCellValue('C4', 'BÁO CÁO THỐNG KÊ TÌNH HÌNH THÔI VIỆC CỦA NGƯỜI LAO ĐỘNG')->mergeCells('C4:F4')
                    ->setCellValue('C5', 'TẠI MIỀN TRUNG - TÂY NGUYÊN')->mergeCells('C5:F5')->getStyle('C4:F5')->applyFromArray(array(
                        'font' => array(
                            'name'      =>  'Times New Roman',
                            'size'      =>  18,
                            'bold'      =>  true
                        )
                    ));
                $event->sheet->getDelegate()->getStyle('B5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('C1:F2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A7:J8')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $columns = ['A' => 7, 'B' => 30, 'C' => 30, 'D' => 25, 'E' => 30, 'F' => 25, 'G' => 25, 'H' => 25, 'I' => 30, 'J' => 30];
                foreach ($columns as $key => $value) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setWidth($value);
                };

                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT')->mergeCells('A7:A8')
                ->setCellValue('B7', 'Họ Tên')->mergeCells('B7:B8')
                ->setCellValue('C7', 'Chức Danh')->mergeCells('C7:C8')
                ->setCellValue('D7', 'Đơn Vị')->mergeCells('D7:D8')
                ->setCellValue('E7', 'Tỉnh Thành')->mergeCells('E7:E8')
                ->setCellValue('F7', 'Nơi Làm Việc')->mergeCells('F7:F8')
                ->setCellValue('G7', 'Chấm dứt HĐLĐ ngày')->mergeCells('G7:G8')
                ->setCellValue('H7', 'Lý Do')->mergeCells('H7:H8')
                ->setCellValue('I7', 'QĐ chấm dứt quản lý LĐ')->mergeCells('I7:J7')
                ->setCellValue('I8', 'Số')
                ->setCellValue('J8', 'Ngày')
                ;
                for ($i = 9; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 8);
                }

                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('I' . ($HighestRow + 2), 'Đà Nẵng, Ngày         Tháng         Năm 20         ')->mergeCells('I' . ($HighestRow + 2) . ':I' . ($HighestRow + 2))
                    ->setCellValue('I' . ($HighestRow + 3), 'Người Báo Cáo')->mergeCells('I' . ($HighestRow + 3) . ':I' . ($HighestRow + 3));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow + 2) .':I'.($HighestRow + 3))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                        'italic'    => true
                    )
                ));
            },
        ];
    }

    public function query()
    {
        $filterRangeMonth = $this->filter['filterRangeMonth'];
        $IdTCNN = $this->filter['IdTCNN'];
        $TinhTrangHoSo = $this->filter['TinhTrangHoSo'];
        $MaTT_NoiLamViec = $this->filter['TinhThanhLamViec'];
        $CheckGender = is_numeric($this->filter['GioiTinh']) ? true  : false;
        $GioiTinh = $this->filter['GioiTinh'];
        $HoSoNLD_ChucDanh =  HoSoNhanVienTCNN::query()
        ->leftJoin('DmChucDanh', 'DmChucDanh.IdChucDanh', '=', 'HoSoNhanVienTCNN.CVHT_ChucDanh')
        ->leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN.IdTCNN')
        ->leftJoin('DmTinhThanh', 'DmTinhThanh.MaTT', '=', 'HoSoNhanVienTCNN.MaTT_NoiLamViec')
        ->when($filterRangeMonth,function($query, $filterRangeMonth){
            list($start, $end) = explode('-', $filterRangeMonth);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:00', strtotime($end));
                $query->whereBetween('HoSoNhanVienTCNN.ThongBaoNghiViec_Ngay', [$start, $end]);
        })
        ->when($IdTCNN, function ($query, $IdTCNN) {
            $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
        })
        ->when($TinhTrangHoSo, function ($query, $TinhTrangHoSo) {
            $query->where('HoSoNhanVienTCNN.TinhTrangHoSo', '=', $TinhTrangHoSo);
        })
        ->when($CheckGender, function ($query) use ($GioiTinh) {
            $query->where('HoSoNhanVienTCNN.GioiTinh', '=', $GioiTinh);
        })
        ->when($MaTT_NoiLamViec, function ($query) use ($MaTT_NoiLamViec) {
            $query->where('HoSoNhanVienTCNN.MaTT_NoiLamViec', '=', $MaTT_NoiLamViec);
        })
        ->select(
            'HoSoNhanVienTCNN.Ho',
            'HoSoNhanVienTCNN.Ten',
            'DmChucDanh.ChucDanh',
            'DmTinhThanh.TENTT',
            'HoSoNhanVienTCNN.NoiLamViec',
            'HoSoNhanVienTCNN.ThongBaoNghiViec_Ngay',
            'HoSoNhanVienTCNN.ThongBaoNghiViec_So',
            'DmTCNN.TenTatTCNN'
        )->latest('HoSoNhanVienTCNN.ThongBaoNghiViec_Ngay');
        return $HoSoNLD_ChucDanh;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B9';
    }

     /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Ho"] .' ' . $row["Ten"] ,
            $row["ChucDanh"],
            $row["TenTatTCNN"],
            $row["TENTT"],
            $row["NoiLamViec"],
            $row["ThongBaoNghiViec_Ngay"] ? date('d/m/Y',strtotime($row["ThongBaoNghiViec_Ngay"])) : '',
            '',
            $row["ThongBaoNghiViec_So"],
            $row["ThongBaoNghiViec_Ngay"] ? date('d/m/Y',strtotime($row["ThongBaoNghiViec_Ngay"])) : '',
        ];
    }
}
