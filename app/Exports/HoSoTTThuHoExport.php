<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class HoSoTTThuHoExport implements FromCollection, WithCustomStartCell, WithEvents, WithDrawings, WithMapping, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;
    protected $collection;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     */
    public function __construct(Collection $collection,$parameters)
    {
        $this->startPos = 4;
        $this->collection = $collection;
        $filter = [
            'DmTCNNFilter' => array_key_exists('IdTCNN', $parameters) ? $parameters['IdTCNN'] : null,
            'filterRangeMonth' => array_key_exists('month', $parameters) ? $parameters['month'] : null,
        ];
        $this->filter = $filter;
        $this->collection = $collection;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(50);
        $drawing->setOffsetX(20);
        $drawing->setCoordinates('A1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                if (!\is_null($this->filter['DmTCNNFilter']) ) {
                    $this->startPos = 5;
                }
                // text center
                $event->sheet->getDelegate()->getStyle('A1:N' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A'.$this->startPos.':N' . ($HighestRow +1))
                    ->applyFromArray($styleArray);
                // title
                // $event->sheet->getDelegate()->setCellValue('E1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('E1:I1')
                // ->setCellValue('E2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('E2:I2');
                // if (!\is_null($this->filter['DmTCNNFilter']) ) {
                //     $event->sheet->getDelegate()->setCellValue('E3', 'Đơn vị: '. $this->collection[0]["TCNN"])
                //         ->mergeCells('E3:I3')->getStyle('E3:I3')->applyFromArray(array(
                //             'font' => array(
                //                 'name'      =>  'Times New Roman',
                //                 'size'      =>  11,
                //                 'bold'      =>  false
                //             )
                //         ));
                // }
                $event->sheet->getDelegate()->setCellValue('E1', 'BẢNG THEO DÕI TÌNH HÌNH NỘP CHẾ ĐỘ')->mergeCells('E1:I1')->getStyle('E1:I1')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  16,
                        'bold'      =>  true
                    )
                ));

                if (!\is_null($this->filter['DmTCNNFilter']) ) {
                    $event->sheet->getDelegate()->setCellValue('E2', 'Đơn vị: '. $this->collection[0]["TCNN"])
                        ->mergeCells('E2:I2')->getStyle('E2:I2')->applyFromArray(array(
                            'font' => array(
                                'name'      =>  'Times New Roman',
                                'size'      =>  11,
                                'bold'      =>  false
                            )
                    ));
                }
                if(!\is_null( $this->filter['filterRangeMonth'] )){
                    list($start, $end) = explode('-', $this->filter['filterRangeMonth']);
                    $event->sheet->getDelegate()->setCellValue('E'.($this->startPos-2),'(Từ ngày: '. date('d/m/Y',strtotime($start)).' đến ngày: '. date('d/m/Y',strtotime($end)).')')
                    ->mergeCells('E'.($this->startPos-2).':I'.($this->startPos-2))->getStyle('E'.($this->startPos-2).':I'.($this->startPos-2))->applyFromArray(array(
                        'font' => array(
                            'name'      =>  'Times New Roman',
                            'size'      =>  11,
                            'bold'      =>  false
                        )
                    ));
                }
                $event->sheet->getDelegate()->getStyle('B2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('E1:I1')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A'.$this->startPos.':N'.($this->startPos+1))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));

                // // Add column STT
                $event->sheet->getDelegate()->setCellValue('A'.$this->startPos, 'STT')->mergeCells('A'.$this->startPos.':A'.($this->startPos+1))
                ->setCellValue('B'.$this->startPos, 'Chứng từ')->mergeCells('B'.$this->startPos.':C'.$this->startPos)
                ->setCellValue('B'.($this->startPos+1), 'Số chứng từ')
                ->setCellValue('C'.($this->startPos+1), 'Ngày chứng từ')
                ->setCellValue('D'.$this->startPos, 'Tên đơn vị')->mergeCells('D'.$this->startPos.':D'.($this->startPos+1))
                ->setCellValue('E'.$this->startPos, 'Thời gian')->mergeCells('E'.$this->startPos.':F'.$this->startPos)
                ->setCellValue('E'.($this->startPos+1), 'Từ tháng')
                ->setCellValue('F'.($this->startPos+1), 'Đến tháng')
                ->setCellValue('G'.$this->startPos, 'Tổng')->mergeCells('G'.$this->startPos.':G'.($this->startPos+1))
                ->setCellValue('H'.$this->startPos, 'BHXH')->mergeCells('H'.$this->startPos.':H'.($this->startPos+1))
                ->setCellValue('I'.$this->startPos, 'TTN')->mergeCells('I'.$this->startPos.':I'.($this->startPos+1))
                ->setCellValue('J'.$this->startPos, 'Lương')->mergeCells('J'.$this->startPos.':J'.($this->startPos+1))
                ->setCellValue('K'.$this->startPos, 'BHTN')->mergeCells('K'.$this->startPos.':K'.($this->startPos+1))
                ->setCellValue('L'.$this->startPos, 'DVP')->mergeCells('L'.$this->startPos.':L'.($this->startPos+1))
                ->setCellValue('M'.$this->startPos, 'Trợ cấp BHXH')->mergeCells('M'.$this->startPos.':M'.($this->startPos+1))
                ->setCellValue('N'.$this->startPos, 'Phí NH')->mergeCells('N'.$this->startPos.':N'.($this->startPos+1))
                ;
                $event->sheet->getDelegate()->getStyle('G'.($this->startPos+1).':N' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('B'.($this->startPos +1).':F' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                for ($i = $this->startPos+2; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - ($this->startPos+1));
                }

                $event->sheet->getDelegate()->setCellValue('A'. ($HighestRow + 1), 'Tổng cộng: ')->mergeCells('A'. ($HighestRow + 1).':'.'D'.($HighestRow + 1))
                ->setCellValue('G'. ($HighestRow + 1), '=SUM(G'.($this->startPos+2).':G'.$HighestRow.')')
                ->setCellValue('H'. ($HighestRow + 1), '=SUM(H'.($this->startPos+2).':H'.$HighestRow.')')
                ->setCellValue('I'. ($HighestRow + 1), '=SUM(I'.($this->startPos+2).':I'.$HighestRow.')')
                ->setCellValue('J'. ($HighestRow + 1), '=SUM(J'.($this->startPos+2).':J'.$HighestRow.')')
                ->setCellValue('K'. ($HighestRow + 1), '=SUM(K'.($this->startPos+2).':K'.$HighestRow.')')
                ->setCellValue('L'. ($HighestRow + 1), '=SUM(L'.($this->startPos+2).':L'.$HighestRow.')')
                ->setCellValue('M'. ($HighestRow + 1), '=SUM(M'.($this->startPos+2).':M'.$HighestRow.')')
                ->setCellValue('N'. ($HighestRow + 1), '=SUM(N'.($this->startPos+2).':N'.$HighestRow.')')
                ;
                $event->sheet->getDelegate()->getStyle('A'. ($HighestRow + 1).':N'. ($HighestRow + 1))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('I' . ($HighestRow + 4), 'Đà Nẵng, ngày         tháng         năm 20         ')->mergeCells('I' . ($HighestRow + 4) . ':L' . ($HighestRow + 4))
                    ->setCellValue('I' . ($HighestRow + 5), 'Thủ trưởng đơn vị')->mergeCells('I' . ($HighestRow + 5) . ':K' . ($HighestRow + 5));
                    $event->sheet->getDelegate()->setCellValue('G'. ($HighestRow + 5), 'Phụ trách kế toán')->getStyle('H'. ($HighestRow + 5))->getAlignment()->setWrapText(false);
                    $event->sheet->getDelegate()->setCellValue('D'. ($HighestRow + 5), 'Người kiểm tra')->getStyle('E'. ($HighestRow + 5))->getAlignment()->setWrapText(false);
                    $event->sheet->getDelegate()->setCellValue('B'. ($HighestRow + 5), 'Người lập')->getStyle('B'. ($HighestRow + 5))->getAlignment()->setWrapText(false);
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow + 4) .':I'.($HighestRow + 4))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'italic'    => true,
                    )
                    ));
                $event->sheet->getDelegate()->getStyle('A'. ($HighestRow + 5).':N'. ($HighestRow + 5))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true,
                        'italic'    => false,
                    )
                ));
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }


     /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
        ];
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B'.($this->startPos+2);
    }

     /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Số Chứng Từ"],
            date('d/m/Y', strtotime($row["Ngày Chứng Từ"])),
            $row["TenTatTCNN"],
            date('d/m/Y', strtotime($row["Nộp từ tháng"])),
            $row["Nộp đến tháng"] ? date('d/m/Y', strtotime($row["Nộp đến tháng"])) : '',
            floatval(preg_replace('/[^\d.]/', '', $row["Tổng tiền nộp"])),
            floatval(preg_replace('/[^\d.]/', '',$row["BHXH"])),
            floatval(preg_replace('/[^\d.]/', '', $row["TTN"])),
            floatval(preg_replace('/[^\d.]/', '',  $row["Luong"])),
            floatval(preg_replace('/[^\d.]/', '', $row["BHTN"])),
            floatval(preg_replace('/[^\d.]/', '', $row["DVP"])),
            floatval(preg_replace('/[^\d.]/', '', $row["TroCapBHXH"])),
            floatval(preg_replace('/[^\d.]/', '', $row["PhiNganHang"]))
        ];
    }
}
