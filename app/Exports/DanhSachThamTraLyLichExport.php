<?php

namespace App\Exports;

use App\HoSoNhanVienTCNN;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class DanhSachThamTraLyLichExport implements FromQuery, WithCustomStartCell, WithEvents, WithDrawings, WithMapping
{
    use Exportable;

    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     */
    public function __construct($parameters)
    {
        $filter = [
            'filterRangeMonth' =>  $parameters['filterRangeMonth'],
            'TinhTrangHoSo' => array_key_exists('TinhTrangHoSo', $parameters) ? $parameters['TinhTrangHoSo'] : [],
            'TinhThanhLamViec' => array_key_exists('TinhThanhLamViec', $parameters) ? $parameters['TinhThanhLamViec'] : null,
            'IdTCNN' => array_key_exists('IdTCNN', $parameters) ? $parameters['IdTCNN'] : null,
            'GioiTinh' => array_key_exists('GioiTinh', $parameters) ? $parameters['GioiTinh'] : null,
        ];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(60);
        $drawing->setOffsetX(40);
        $drawing->setCoordinates('C1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:L' . ($HighestRow + 7))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:L' . $HighestRow)
                    ->applyFromArray($styleArray);
                // title
                    $event->sheet->getDelegate()->setCellValue('C1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('C1:E1')
                    ->setCellValue('C2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('C2:E2')
                    ->setCellValue('C4', 'BÁO CÁO THỐNG KÊ TÌNH HÌNH THẨM TRA, XÁC MINH')->mergeCells('C4:E4')
                    ->setCellValue('C5', 'LÝ LỊCH NGƯỜI LAO ĐỘNG VIỆT NAM LÀM VIỆC CHO CÁC TC, CNNN')->mergeCells('C5:E5')->getStyle('C4:E5')->applyFromArray(array(
                        'font' => array(
                            'name'      =>  'Times New Roman',
                            'size'      =>  18,
                            'bold'      =>  true
                        )
                    ));

                $event->sheet->getDelegate()->setCellValue('D5', $this->filter['filterRangeMonth'] )
                ->mergeCells('D5:I5')->getStyle('D5:I5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  18,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('C1:E2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A7:L8')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $columns = ['A' => 7, 'B' => 35,'C'=>45,'D'=>30,'E'=>30];
                foreach ($columns as $key => $value) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setWidth($value);
                };

                $columnAutoSize = ['F', 'G','H','I','J','K','L'];
                foreach ($columnAutoSize as $key ) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setAutoSize(true);
                };
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT')->mergeCells('A7:A8')
                ->setCellValue('B7', 'Họ và Tên')->mergeCells('B7:B8')
                ->setCellValue('C7', 'Chức Danh Được Tuyển')->mergeCells('C7:C8')
                ->setCellValue('D7', 'TC, CNNN Tuyển Dụng')->mergeCells('D7:D8')
                ->setCellValue('E7', 'Nơi Làm Việc MT-TN')->mergeCells('E7:E8')
                ->setCellValue('F7', 'CV Thẩm Tra')->mergeCells('F7:G7')
                ->setCellValue('H7', 'CV Cơ Quan An Ninh ')->mergeCells('H7:J7')
                ->setCellValue('K7', 'CV Chấp Thuận ')->mergeCells('K7:L7')
                ->setCellValue('F8', 'Số')
                ->setCellValue('G8', 'Ngày')
                ->setCellValue('H8', 'Số')
                ->setCellValue('I8', 'Ngày')
                ->setCellValue('J8', 'Đơn Vị Thực Hiện')
                ->setCellValue('K8', 'Số')
                ->setCellValue('L8', 'Ngày');
                for ($i = 9; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 8);
                }

                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('I' . ($HighestRow + 2), 'Đà Nẵng, Ngày         Tháng         Năm 20         ')->mergeCells('I' . ($HighestRow + 2) . ':L' . ($HighestRow + 2))
                    ->setCellValue('I' . ($HighestRow + 3), 'Người Báo Cáo')->mergeCells('I' . ($HighestRow + 3) . ':L' . ($HighestRow + 3))
                    ->setCellValue('I' . ($HighestRow + 6), Auth::user()->name )->mergeCells('I' . ($HighestRow + 6) . ':L' . ($HighestRow + 6));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow + 2) .':L'.($HighestRow + 7))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                        'italic'    => true
                    )
                ));
            },
        ];
    }

    public function query()
    {
        $filterRangeMonth = $this->filter['filterRangeMonth'];
        $IdTCNN = $this->filter['IdTCNN'];
        $TinhTrangHoSo = $this->filter['TinhTrangHoSo'];
        $MaTT_NoiLamViec = $this->filter['TinhThanhLamViec'];
        $CheckGender = is_numeric($this->filter['GioiTinh']) ? true  : false;
        $GioiTinh = $this->filter['GioiTinh'];
        $HoSoNhanVienTCNN =  HoSoNhanVienTCNN::query()
        ->leftJoin('DmChucDanh', 'DmChucDanh.IdChucDanh', '=', 'HoSoNhanVienTCNN.CVHTChucDanh')
        ->leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN.IdTCNN')
        ->leftJoin('DmTinhThanh', 'DmTinhThanh.MaTT', '=', 'HoSoNhanVienTCNN.MaTT_NoiLamViec')
        ->when($filterRangeMonth,function($query, $filterRangeMonth){
            list($start, $end) = explode('-', $filterRangeMonth);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:00', strtotime($end));
                $query->whereBetween('HoSoNhanVienTCNN.NgayThamTra', [$start, $end]);
        })->when($IdTCNN, function ($query, $IdTCNN) {
            $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
        })
        ->when($TinhTrangHoSo, function ($query, $TinhTrangHoSo) {
            $query->where('HoSoNhanVienTCNN.TinhTrangHoSo', '=', $TinhTrangHoSo);
        })
        ->when($CheckGender, function ($query) use ($GioiTinh) {
            $query->where('HoSoNhanVienTCNN.GioiTinh', '=', $GioiTinh);
        })
        ->when($MaTT_NoiLamViec, function ($query) use ($MaTT_NoiLamViec) {
            $query->where('HoSoNhanVienTCNN.MaTT_NoiLamViec', '=', $MaTT_NoiLamViec);
        })
        ->select(
            'HoSoNhanVienTCNN.Ho',
            'HoSoNhanVienTCNN.Ten',
            'HoSoNhanVienTCNN.SoVBThamTra',
            'HoSoNhanVienTCNN.NgayThamTra',
            'HoSoNhanVienTCNN.SoVBThamTraAnNinh',
            'HoSoNhanVienTCNN.NgayVBThamTraAnNinh',
            'HoSoNhanVienTCNN.LoaiCQAN',
            'HoSoNhanVienTCNN.SoVBChapThuan',
            'HoSoNhanVienTCNN.NgayVBChapThuan',
            'DmChucDanh.ChucDanh',
            'DmTinhThanh.TENTT',
            'DmTCNN.TenTatTCNN'
        )->latest('HoSoNhanVienTCNN.NgayThamTra');
        return $HoSoNhanVienTCNN;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B9';
    }

     /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Ho"] .' ' . $row["Ten"] ,
            $row["ChucDanh"],
            $row["TenTatTCNN"],
            $row["TENTT"],
            $row["SoVBThamTra"],
            $row["NgayThamTra"] ? date('d/m/Y',strtotime($row["NgayThamTra"])) : '',
            $row["SoVBThamTraAnNinh"],
            $row["NgayVBThamTraAnNinh"] ? date('d/m/Y',strtotime($row["NgayVBThamTraAnNinh"])) : '',
            $row["LoaiCQAN"],
            $row["SoVBChapThuan"],
            $row["NgayVBChapThuan"] ? date('d/m/Y',strtotime($row["NgayVBChapThuan"])) : '',
        ];
    }
}
