<?php

namespace App\DataTables;

use App\DmPhuongXa;
use App\User;
use Yajra\DataTables\Services\DataTable;
use App\Transformers\DmPhuongXaTransformer;
use Illuminate\Support\Facades\Gate;

class DmPhuongXaDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($this->query())->setTransformer(new DmPhuongXaTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $MAQU = $this->request()->get('MAQU_PXA');
        $MATT = $this->request()->get('MATT_QU');
        $query = DmPhuongXa::leftJoin('DmQuanHuyen', 'DmPhuongXa.MAQU_PXA', '=', 'DmQuanHuyen.MAQU')
        ->leftJoin('DmTinhThanh', 'DmQuanHuyen.MATT_QU', '=', 'DmTinhThanh.MATT')
        ->when($MAQU, function ($query) use ($MAQU) {
            $query->where('DmPhuongXa.MAQU_PXA', '=', $MAQU);
        })
        ->when($MATT, function ($query) use ($MATT) {
            $query->where('DmQuanHuyen.MATT_QU', '=', $MATT);
        })
        ->groupBy('DmPhuongXa.MAPHUONGXA', 'DmQuanHuyen.MATT_QU', 'DmTinhThanh.TENTT', 'DmPhuongXa.MAQU_PXA', 'DmQuanHuyen.TENQUAN', 'DmPhuongXa.TENPXA', 'DmPhuongXa.VIETTAT', 'DmPhuongXa.IdPhuong')
        ->select('DmPhuongXa.MAPHUONGXA', 'DmQuanHuyen.MATT_QU', 'DmTinhThanh.TENTT', 'DmPhuongXa.MAQU_PXA', 'DmQuanHuyen.TENQUAN', 'DmPhuongXa.TENPXA', 'DmPhuongXa.VIETTAT', 'DmPhuongXa.IdPhuong')
        ->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['reload'];
        if(Gate::allows('DmPhuongXa_edit')){
            array_unshift($buttons,'edit');
        }
        if(Gate::allows('DmPhuongXa_create')){
            array_unshift($buttons,'create');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => false,
                'responsive' => false,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'asc']],
                'buttons'      =>  $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ["data" => "MAPHUONGXA", 'title' => 'Mã Phường Xã'],
            ["data" => "MATT_QU", 'title' => 'Mã Tỉnh thành'],
            ["data" => "TENTT", 'title' => 'Tên Tỉnh thành'],
            ["data" => "MAQU_PXA", 'title' => 'Mã Quận huyện'],
            ["data" => "TENQUAN", 'title' => 'Tên Quận huyện'],
            ["data" => "TENPXA", 'title' => 'Tên Phường Xã'],
            ["data" => "VIETTAT", 'title' => 'Tên viết tắt', 'className' => 'hide'],
            ["data" => "IdPhuong", 'title' => 'Id Phường Xã']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DmPhuongXa_' . date('YmdHis');
    }
}
