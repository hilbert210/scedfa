var table;
$(document).ready(async function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    if (window.location.pathname.split("/")[1] != "tcnn") {
        $(".buttons-create_salary")[0].hidden = true;
        $(".buttons-edit_salary")[0].hidden = true;
    } else {
        $(".buttons-create_salary_all")[0].hidden = true;
        $(".buttons-edit_salary_1")[0].hidden = true;
    }
    table = new $.fn.dataTable.Api('#LuongNhanVien');
    yadcf.init(
        table, [
            // {
            //     column_number: 1,
            //     filter_container_id: "TinhTrangHoSoFilter",
            //     filter_type: "select",
            //     select_type: "chosen",
            //     data: ToChucFilter,
            //     select_type_options: { theme: "bootstrap", width: "85%" }
            // }
        ], { externally_triggered: true }
    );
    $("#filterLuongNhanVien").on("click", function() {
        yadcf.exFilterExternallyTriggered(table);
    });
    $("#resetLuongNhanVien").on("click", function() {
        $('input[name="filterRangeMonth"]').val('');
        $('input[name="filterTenNhanVien"]').val('');
        $('select[name="DoiTuong"]').val('');
        $('select[name="ToChuc"]').val('');
        $('select[name="PhuongAn"]').val('');
        $('select[name="filterMonth"]').val('');
        $('select[name="filterYear"]').val('');
        $('input[name="luong"]').val('');
        yadcf.exResetAllFilters(table);
    });
    $('input[name="filterRangeMonth"]').daterangepicker({
        showDropdowns: true,
        autoUpdateInput: false,
        // startDate: moment().startOf('month').format('YYYY/MM/DD'),
        // endDate: moment().endOf('month').format('YYYY/MM/DD'),
        locale: {
            cancelLabel: "Clear",
            format: 'YYYY/MM/DD'
        }
    });
    $('input[name="filterRangeMonth"]').on("apply.daterangepicker", function(
        ev,
        picker
    ) {
        $(this).val(
            moment(picker.startDate).startOf('month').format('YYYY/MM/DD') +
            "-" +
            moment(picker.endDate).endOf('month').format('YYYY/MM/DD')
        );
    });
});