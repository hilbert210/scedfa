<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LuongNhanVien extends Model
{
    protected $guarded  = ['IdLuong'];
    protected $primaryKey = "IdLuong";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'HoSoNhanVienTCNN_LUONG_BH';
}
