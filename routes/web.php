<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route as FacadesRoute;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return  redirect('tcnn');
  });

Route::resource('tcnn', 'DmTCNNController')->except([
    'show'
]);
Route::resource('HoSoNLD', 'HoSoNLDController')->except([
    'show'
]);

Route::resource('HoSoTTThuHo', 'HoSoTTThuHoController');
Route::resource('LuongNhanVien', 'LuongNhanVienController');

Route::get('tcnn/{IdTCNN}', 'LuongNhanVienController@showTCNN');
Route::get('tcnn/ttthuho/{IdTCNN}', 'LuongNhanVienController@showTTThuHo');
Route::get('HoSoNLD/{IdHoSoNhanVienTCNN}', 'LuongNhanVienController@showNLD');

Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::get('/logout', function(){
    Auth::logout();
    return redirect()->route('login');
})->name('logout');
Route::post('login', 'Auth\LoginController@postLogin')->name('login');

Route::resource('SettingUser', 'SettingController');
Route::resource('OptionConfig', 'OptionController');
Route::resource('DmTinhThanh', 'DmTinhThanhController');
Route::resource('DmQuanHuyen', 'DmQuanHuyenController');
Route::resource('DmPhuongXa', 'DmPhuongXaController');

Route::group(['prefix' => 'ajax'], function () {
    Route::get('DmTT', 'ajaxController@DmTT')->name('DmTT');
    Route::get('DmTC', 'ajaxController@DmTC')->name('DmTC');
    Route::get('DmQT', 'ajaxController@DmQT')->name('DmQT');
    Route::get('DmCD', 'ajaxController@DmCD')->name('DmCD');
    Route::get('DmChucDanh', 'ajaxController@DmChucDanh')->name('DmChucDanh');
    Route::post('AddDmCD', 'ajaxController@AddDmCD')->name('AddDmCD');
    Route::post('AddDmQT', 'ajaxController@AddDmQT')->name('AddDmQT');
    Route::get('DmChauLuc', 'ajaxController@DmChauLuc')->name('DmChauLuc');
    Route::get('DmTonGiao', 'ajaxController@DmTonGiao')->name('DmTonGiao');
    Route::get('DmDanToc', 'ajaxController@DmDanToc')->name('DmDanToc');
    Route::post('AddDmDT', 'ajaxController@AddDmDT')->name('AddDmDT');
    Route::get('DmQuanHuyen/{id}', 'ajaxController@DmQuanHuyen')->name('DmQuanHuyenId');
    Route::get('DmQuanHuyen', 'ajaxController@DmQuanHuyen')->name('DmQuanHuyen');
    Route::get('DmPhuongXa/{id}', 'ajaxController@DmPhuongXa')->name('DmPhuongXaId');
    Route::get('DmPhuongXa', 'ajaxController@DmPhuongXa')->name('DmPhuongXa');
    Route::get('MaNhanVien', 'ajaxController@MaNhanVien')->name('MaNhanVien');
    Route::get('MaTCNN', 'ajaxController@MaTCNN')->name('MaTCNN');
    Route::get('DmTCNN', 'ajaxController@DmTCNN')->name('DmTCNN');
    Route::get('QuaTrinhLamViec/{id}', 'ajaxController@QuaTrinhLamViec')->name('QuaTrinhLamViec');
    Route::get('destroyHistoryChucDanh{id}', 'ajaxController@destroyHistoryChucDanh')->name('destroyHistoryChucDanh');
    Route::post('uploadfile', 'ajaxController@uploadfile')->name('uploadfile');
    Route::post('removeFile', 'ajaxController@removeFile')->name('removeFile');
    Route::get('FileDinhKem/{IdLoai}/loai/{loai}', 'ajaxController@FileDinhKem')->name('FileDinhKem');
    Route::post('updateChucDanh', 'ajaxController@updateChucDanh')->name('updateChucDanh');
    Route::get('thongBaoNghiViec/{id}/loai/{type}', 'ajaxController@thongBaoNghiViec')->name('thongBaoNghiViec');
    Route::get('createSalary/{IdTCNN}/type/{type}', 'ajaxController@createSalary')->name('createSalary');
    Route::get('getSalaryByMonth', 'ajaxController@getSalaryByMonth')->name('getSalaryByMonth');
    Route::get('TypeExport/{IdTCNN}', 'ajaxController@TypeExport')->name('TypeExport');
    Route::get('getHoSoNLDChucDanh/{id}', 'ajaxController@getHoSoNLDChucDanh')->name('getHoSoNLDChucDanh');
    Route::post('updateHistoryChucDanh', 'ajaxController@updateHistoryChucDanh')->name('updateHistoryChucDanh');
    Route::post('editOrCreateVPLVTCT', 'ajaxController@editOrCreateVPLVTCT')->name('editOrCreateVPLVTCT');
    Route::get('getVPLVTCT/{id}', 'ajaxController@getVPLVTCT')->name('getVPLVTCT');
    Route::get('getVPLVTCTbyIdTCNN/{id}', 'ajaxController@getVPLVTCTbyIdTCNN')->name('getVPLVTCTbyIdTCNN');
    Route::get('destroyVPLVTCT/{id}', 'ajaxController@destroyVPLVTCT')->name('destroyVPLVTCT');
    Route::post('updatePassword', 'ajaxController@updatePassword')->name('updatePassword');
    Route::post('createSalaryAll', 'ajaxController@createSalaryAll')->name('createSalaryAll');
    Route::get('getSalaryByID/{id}', 'ajaxController@getSalaryByID')->name('getSalaryByID');
    Route::get('getMaxMATTbyMAVUNG/{MAVUNG}', 'ajaxController@getMaxMATTbyMAVUNG')->name('getMaxMATT');
    Route::get('maVung', 'ajaxController@maVung')->name('maVung');
});