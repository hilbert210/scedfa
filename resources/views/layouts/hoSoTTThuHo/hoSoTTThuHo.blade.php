@extends('master')
@section('title','SCEDFA | Thông Tin Thu Hộ ')
@section('content')

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.min.css')}} " />
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.toast.min.css')}} " />
@endpush

<div class="filter p-3">
  <div class=" d-flex justify-content-around p-3">
    <div class="input-group row col-2 ">
      <div class="input-group-prepend col-12">
        <span>TCNN</span>
      </div>
      <select id="DmTCNNFilter" class="DmTCNNFilter col-12">
        <option value=""></option>
      </select>
    </div>
    <div class="input-group row col-2 ">
      <div class="input-group-prepend col-12">
        <span>Tháng:</span>
      </div>
      <div class="d-flex flex-row col-12">
        <input type="text" id="filterRangeMonth" name="filterRangeMonth" value="" />
      </div>
    </div>
  </div>
  <div class="d-flex justify-content-center">
    <button type="button" id="filterHoSoTTThuHo" style="width: 6rem" class="btn btn-primary m-2">Filter</button>
    <button type="button" id="resetHoSoTTThuHo" style="width: 6rem" class="btn btn-secondary m-2">Reset</button>
  </div>
</div>

{{$dataTable->table(['id' => 'HoSoTTThuHo','class'=>'table table-striped table-bordered'])}}

{{-- modal create and edit --}}

@include('layouts.hoSoTTThuHo.formHoSoTTThuHo')

@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('DataTables/datatables.min.js')}} "></script>
<script>
  $(function() {
    {{$dataTable->generateScripts()}}
    $('#button-close-modal').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-HoSoTTThuHo').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-HoSoTTThuHo').modal('hide');
      } else {
        $('#modal-HoSoTTThuHo').modal('show');
      }
    });
  })
</script>

<script type="text/javascript" src="{{asset('js/moment.min.js')}} "></script>
<script type="text/javascript" src="{{asset('chosen/chosen.jquery.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.yadcf.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/app.js')}} "></script>
<script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/lodash.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/HoSoTTThuHo.js')}} "></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.toast.min.js')}} "></script>
@endpush
