<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DmTinhThanh extends Model
{
    protected $guarded = [];
    protected $primaryKey = "MATT";
    public $timestamps = false;
    protected $table = 'DmTinhThanh';
}
