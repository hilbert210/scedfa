<form method="POST" id="form-tcnn" class="form-tcnn">
  @csrf
  <div class="modal fade " id="modal-tcnn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" id="button-close-modal" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs nav-tabs-bottom" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="TTTCNN-tab" data-toggle="tab" href="#TTTCNN" role="tab" aria-controls="TTTCNN" aria-selected="true">Thông Tin Tổ Chức Cá Nhân Nước Ngoài</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="FDK-tab" data-toggle="tab" href="#FDK" role="tab" aria-controls="FDK" aria-selected="false">File đính kèm</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="DmVPLVTCT-tab" data-toggle="tab" href="#DmVPLVTCT" role="tab" aria-controls="DmVPLVTCT" aria-selected="false">Văn phòng làm việc tại các tỉnh</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="TTTCNN" role="tabpanel" aria-labelledby="TTTCNN-tab">
              <div class="row mt-2">
                <div class="col-4">
                  <input type="hidden" class="form-control" name="IdTCNN" id="IdTCNN">
                  <input type="hidden" class="form-control" name="MaTCNN" id="MaTCNN">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Tên Tiếng Việt</span>
                    </div>
                    <input type="text" class="form-control" name="TenTiengVietTCNN" id="TenTiengVietTCNN">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Tên Tiếng Anh</span>
                    </div>
                    <input type="text" class="form-control" name="TenTiengAnhTCNN" id="TenTiengAnhTCNN">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Tên Viết Tắt</span>
                    </div>
                    <input type="text" class="form-control" name="TenTatTCNN" id="TenTatTCNN">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Trụ Sở Chính</span>
                    </div>
                    <input type="text" class="form-control" name="TruSoChinh" id="TruSoChinh">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Quốc Tịch</span>
                    </div>
                    <select class="form-control" name="QuocTichTSC" id="QuocTichTSC">
                      <option>
                        <option />
                    </select>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Loại Hình TC,CN NN</span>
                    </div>
                    <select class="form-control" name="LoaiTCNN" id="LoaiTCNN">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Loại Giấy Phép</span>
                    </div>
                    <select class="form-control" name="LoaiGP" id="LoaiGP">
                      <option value=""></option>
                      <option value="Giấy đăng ký lập văn phòng đại diện">Giấy đăng ký lập văn phòng đại diện</option>
                      <option value="Giấy đăng ký lập văn phòng dự án">Giấy đăng ký lập văn phòng dự án</option>
                      <option value="Giấy đăng ký hoạt động">Giấy đăng ký hoạt động </option>
                      <option value="Khác">Khác</option>
                    </select>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Cơ Quan Cấp Phép</span>
                    </div>
                    <select class="form-control" name="CoQuanQuanLy" id="CoQuanQuanLy">
                      <option value="Cục Ngoại vụ - Bộ Ngoại giao">Cục Ngoại vụ - Bộ Ngoại giao</option>
                      <option value="Sở Công thương">Sở Công thương</option>
                      <option value="Bộ Tư pháp">Bộ Tư pháp</option>
                      <option value="Khác">Khác</option>
                    </select>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Địa Bàn Hoạt Động</span>
                    </div>
                    <select class="form-control" multiple name="DiaBanHoatDong[]" id="DiaBanHoatDong">
                      <option>
                        <option />
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="LinhVucHoatDong">Lĩnh Vực Hoạt Động</label>
                    <textarea rows="2" cols="50" class="form-control" name="LinhVucHoatDong" id="LinhVucHoatDong"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="GhiChuTCNN">Ghi Chú</label>
                    <textarea rows="2" cols="50" class="form-control" name="GhiChuTCNN" id="GhiChuTCNN"></textarea>
                  </div>
                </div>
                <div class="col-4">
                  <fieldset>
                    <legend>Giấy Phép ĐKHD</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số Giấy Phép</span>
                      </div>
                      <input type="text" class="form-control" name="SoGiayPhep" id="SoGiayPhep">
                    </div>
                    <div class="input-group ">
                      <div class="input-group-prepend ">
                        <span class="input-group-text">Ngày Cấp</span>
                      </div>
                      <input id="NgayCapGiayPhep" name="NgayCapGiayPhep" type="text" class="form-control datepicker">
                    </div>
                    <div class="input-group ">
                      <div class="input-group-prepend ">
                        <span class="input-group-text">Ngày Hết Hạn</span>
                      </div>
                      <input id="NgayHetHanGiayDKHD" name="NgayHetHanGiayDKHD" type="text" class="form-control datepicker">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số Lượng NLĐ CP</span>
                      </div>
                      <input type="text" class="form-control" name="SLLaoDongVN" id="SLLaoDongVN">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox mb-2" style="padding-left: 36px;">
                        <input type="checkbox" class="custom-control-input" name="DangGiaHan" id="DangGiaHan">
                        <label class="custom-control-label" for="DangGiaHan">Đang Gia Hạn</label>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset>
                    <legend>TC, CNNN tại Miền Trung - Tây Nguyên</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tỉnh Thành</span>
                      </div>
                      <select class="form-control" name="MaTT" id="MaTT">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Địa Chỉ</span>
                      </div>
                      <input type="text" class="form-control" name="DiaChi" id="DiaChi">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Điện Thoại</span>
                      </div>
                      <input type="text" class="form-control" name="DienThoai" id="DienThoai">
                    </div>
                  </fieldset>
                  <fieldset>
                    <legend>Liên hệ Admin</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Họ Và Tên</span>
                      </div>
                      <input type="text" class="form-control" name="HoVaTen_admin" id="HoVaTen_admin">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">SĐT</span>
                      </div>
                      <input type="text" class="form-control" name="SDT_admin" id="SDT_admin">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Email</span>
                      </div>
                      <input type="text" class="form-control" name="Email_admin" id="Email_admin">
                    </div>
                  </fieldset>
                </div>
                <div class="col-4">
                  <fieldset>
                    <legend>Người Đại Diện</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Họ Và Tên</span>
                      </div>
                      <input type="text" class="form-control" name="TenNguoiDaiDien" id="TenNguoiDaiDien">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Năm Sinh</span>
                      </div>
                      <input type="text" class="form-control datepicker" name="NamSinh" id="NamSinh">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Giới Tính</span>
                      </div>
                      <div style="align-self: center; padding-left: 10px;">
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="GioiTinhNam" name="GioiTinh" class="custom-control-input" value="1">
                          <label class="custom-control-label" for="GioiTinhNam">Nam</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="GioiTinhNu" name="GioiTinh" class="custom-control-input" value="0">
                          <label class="custom-control-label" for="GioiTinhNu">Nữ</label>
                        </div>
                      </div>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">SĐT</span>
                      </div>
                      <input type="text" class="form-control" name="DienThoaiNguoiDaiDien" id="DienThoaiNguoiDaiDien">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Email</span>
                      </div>
                      <input type="text" class="form-control" name="Email" id="Email">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Quốc Tịch</span>
                      </div>
                      <select class="form-control" name="QuocTich" id="QuocTich">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Chức Danh</span>
                      </div>
                      <select class="form-control" name="TCNN_ChucDanh" id="TCNN_ChucDanh">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <table id="DmChucDanh-tcnn" style="font-size: 12px;" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Chức danh</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="form-group">
                      <label for="GhiChuNDD">Ghi Chú</label>
                      <textarea rows="2" cols="50" class="form-control" name="GhiChuNDD" id="GhiChuNDD"></textarea>
                    </div>
                  </fieldset>
                  <div class="input-group">
                    <div class="input-group-prepend" style="width:42%">
                      <span class="input-group-text">Dịch Vụ Tính Thuế</span>
                    </div>
                    <div id="DichVu" style="align-self: center; padding-left: 10px;">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="DichVuCo" name="DichVu" class="custom-control-input" value="1">
                        <label class="custom-control-label" for="DichVuCo">Có</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="DichVuKhong" name="DichVu" checked class="custom-control-input" value="0">
                        <label class="custom-control-label" for="DichVuKhong">Không</label>
                      </div>
                    </div>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend" style="width:42%">
                      <span class="input-group-text">Tham Gia BHXH</span>
                    </div>
                    <div id="TCBHXH" style="align-self: center; padding-left: 10px;">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="CoTgBHXH" name="TCBHXH" class="custom-control-input" value="1">
                        <label class="custom-control-label" for="CoTgBHXH">Có</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="KhongTgBHXH" name="TCBHXH" checked class="custom-control-input" value="0">
                        <label class="custom-control-label" for="KhongTgBHXH">Không</label>
                      </div>
                    </div>
                  </div>
                  <div class="input-group ">
                    <div class="input-group-prepend" style="width:42%">
                      <div class="input-group-text">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" name="ThoiSuDung" id="ThoiSuDung">
                          <label class="custom-control-label" for="ThoiSuDung">Chấm Dứt Hoạt Động</label>
                        </div>
                      </div>
                    </div>
                    <input id="NgayThoiHoatDong" name="NgayThoiHoatDong" type="text" class="form-control datepicker">
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="FDK" role="tabpanel" aria-labelledby="FDK-tab">
              <input type="file" id="upload-file-dinh-kem" name="upload-file-dinh-kem[]" multiple style="display:none;" />
              <button id="button-upload-file-dinh-kem" type="button" class="btn btn-primary btn-sm mb-2 mt-2">Chọn
                Files Đính Kèm</button>
              <table id="file-dinh-kem" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col" style="width: 5rem">ID</th>
                    <th scope="col">Tên File</th>
                    <th scope="col">action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="tab-pane fade" id="DmVPLVTCT" role="tabpanel" aria-labelledby="DmVPLVTCT-tab">
              <table id="VPLVTCT" class="table table-striped table-bordered mt-2">
                <thead>
                  <tr>
                    <th scope="col" style="width: 5rem">ID</th>
                    <th scope="col">Tỉnh thành</th>
                    <th scope="col">Địa chỉ</th>
                    <th scope="col">Điện thoại</th>
                    <th scope="col">Tên người liên hệ</th>
                    <th scope="col">SĐT người liên hệ</th>
                    <th scope="col">Email người liên hệ</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="saveBtn-tcnn" class="btn btn-primary-outline">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST" id="form-VPLVTCT" class="form-VPLVTCT">
  @csrf
  <div class="modal fade " id="modal-VPLVTCT" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 35%" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Văn phòng làm việc tại các tỉnh thành</h5>
          <button type="button" id="button-close-modal-VPLVTCT" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-4">
          <div class="row">
            <input type="hidden" class="form-control" name="Id" id="Id">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Tỉnh Thành</span>
              </div>
              <select class="form-control" name="MaTT" id="MaTT">
                <option value=""></option>
              </select>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Địa Chỉ</span>
              </div>
              <input id="DiaChi" name="DiaChi" type="text" class="form-control">
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Điện Thoại</span>
              </div>
              <input id="DienThoai" name="DienThoai" type="text" class="form-control">
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Họ Và Tên</span>
              </div>
              <input id="HoVaTen_LH" name="HoVaTen_LH" type="text" class="form-control">
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">SĐT</span>
              </div>
              <input id="SDT_LH" name="SDT_LH" type="text" class="form-control">
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Email</span>
              </div>
              <input class="form-control" name="Email_LH" id="Email_LH">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtnVPLVTCT" class="btn btn-primary-outline">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST" id="form-DmChucDanh-tcnn" class="form-DmChucDanh-tcnn">
  @csrf
  <!--FormAddCD-->
  <div class="modal fade" id="modal-DmChucDanh-tcnn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 40%" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-title">Cập nhật Chức danh</h5>
          <button type="button" id="button-close-modal-DmChucDanh-tcnn" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Chức Danh</span>
            </div>
            <input type="text" class="form-control" name="ChucDanh" id="ChucDanh">
          </div>
          <span id="cdError-tcnn" class="text-danger"></span>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtnDmChucDanh-tcnn" class="btn btn-primary-outline">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</form>