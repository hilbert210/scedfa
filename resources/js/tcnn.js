var table;
$(document).ready(async function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    table = new $.fn.dataTable.Api('#tcnn');
    const DiaBanHoatDong = await $.ajax({
        url: route("DmTT"),
        dataType: "json",
        async: true
    });
    let DiaBanHoatDongFilter = [];
    $.map(DiaBanHoatDong, function(item) {
        DiaBanHoatDongFilter.push({ value: item.TENTT, label: item.TENTT });
    });

    const ToChuc = await $.ajax({
        url: route("DmTC"),
        dataType: "json",
        async: true
    });
    let ToChucFilter = [];
    $.map(ToChuc, function(item) {
        ToChucFilter.push({ value: item.TenToChuc, label: item.TenToChuc });
    });
    yadcf.init(
        table,
        [
            {
                column_number: 1,
                filter_container_id: "ThoiSuDungFilter",
                filter_type: "select",
                select_type: 'chosen',
                data: [
                    { value: 1, label: "Chấm Dứt Hoạt Động" },
                    { value: 0, label: "Đang Hoạt Động" }
                ],
                select_type_options: { theme: "bootstrap", width: '85%' }
            },
            {
                column_number: 8,
                filter_container_id: "DiaBanHoatDongFilter",
                filter_type: "select",
                select_type: "chosen",
                data: DiaBanHoatDongFilter,
                select_type_options: { theme: "bootstrap",  width: '85%' }
            },
            {
                column_number: 9,
                filter_container_id: "ToChucFilter",
                filter_type: "select",
                select_type: "chosen",
                data: ToChucFilter,
                select_type_options: { theme: "bootstrap",  width: '85%' }
            },
            {
                column_number: 3,
                filter_container_id: "GiaHanFilter",
                filter_type: "select",
                select_type: 'chosen',
                data: [
                    { value: 1, label: "Đang Gia Hạn" },
                    { value: 0, label: "Không Gia Hạn" }
                ],
                select_type_options: { theme: "bootstrap",  width: '85%' }
            }
        ],
        { externally_triggered: true }
    );
    const DmCL =  $('.LoaiGPFilter');
    if(DmCL.length>0){
        $.map(DmCL,function(itemDmCL){
            $(itemDmCL).chosen({
            width: "85%"
            });
       const data = 
            {"Giấy đăng ký lập văn phòng đại diện":'Giấy đăng ký lập văn phòng đại diện',
            'Giấy đăng ký lập văn phòng dự án':'Giấy đăng ký lập văn phòng dự án',
            'Giấy đăng ký hoạt động':'Giấy đăng ký hoạt động',
            'Khác':'Khác'}
            $(itemDmCL).append('<option value=""></option>');
            $.map(data, function( item,key ) {
                $(itemDmCL).append('<option value="'+item+'">' + key + '</option>');
            })
            $(itemDmCL).trigger("chosen:updated");
        })
    }
    $("#yadcf-filter--tcnn-1").change(function() {
        if ($("#yadcf-filter--tcnn-1").val() == 1) {
            $("#NgayThoiGiaHanFilter").removeClass("d-none");
        } else {
            if (!$("#NgayThoiGiaHanFilter").hasClass("d-none")) {
                $("#NgayThoiGiaHanFilter").addClass("d-none");
                $('input[name="datefilter"]').val("");
            }
        }
    });
    $("#filterTCNN").on("click", function() {
        yadcf.exFilterExternallyTriggered(table);
    });
    $("#resetTCNN").on("click", function() {
        $('input[name="datefilter"]').val("");
        $('.LoaiGPFilter').val("");
        $("#NgayThoiGiaHanFilter").addClass("d-none");
        yadcf.exResetAllFilters(table);
    });

    $('#button-upload-file-dinh-kem').on('click',function(){
        $('#upload-file-dinh-kem').click();
    })
    $('#upload-file-dinh-kem').change(function(event) {
        let dataURLs = new FormData();
        $('#loader').removeClass('d-none');
        $.each($('#upload-file-dinh-kem')[0].files, function(i, file) {
            dataURLs.append('file['+i+']', file);
        });
        dataURLs.append('Idtype',$('#MaTCNN').val());
        dataURLs.append('type',0);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            data: dataURLs,
            url: route('uploadfile'),
            type: "POST",
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                if(data){
                    $('#file-dinh-kem tbody').empty();
                    data.forEach(function(item,index) {
                        $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                        </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"  
                        onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                    });
                    
                }
                $.toast({
                    heading: "Success",
                    text: "Upload file thàng công",
                    showHideTransition: "fade",
                    icon: "success"
                });
                $('#loader').addClass('d-none');
            },
            error: function(data) {
                $.toast({
                    heading: "Error",
                    text: "Upload file không thàng công",
                    showHideTransition: "fade",
                    icon: "error"
                });
                $('#loader').addClass('d-none');
            }
        });
    });
   window.removeFile =function(IdPicture,name){
       let dataURLs = new FormData();
       $('#loader').removeClass('d-none');
       dataURLs.append('IdLoai',$('#MaTCNN').val());
       dataURLs.append('loai',0);
       dataURLs.append('IdPicture',IdPicture);
       dataURLs.append('name',name);
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
       $.ajax({
           data: dataURLs,
           url: route('removeFile'),
           type: "POST",
           dataType: "json",
           cache: false,
           contentType: false,
           processData: false,
           success: function(data) {
               if(data){
                    $('#file-dinh-kem tbody').empty();
                   data.forEach(function(item,index) {
                    $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                    </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"  
                    onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                 });
                   
               }
               $.toast({
                heading: "Success",
                text: "Xóa file thàng công",
                showHideTransition: "fade",
                icon: "success"
                });
                $('#loader').addClass('d-none');
        },
        error: function(data) {
            $.toast({
                heading: "Error",
                text: "Xóa file không thàng công",
                showHideTransition: "fade",
                icon: "error"
            });
            $('#loader').addClass('d-none');
        }});
    };

    $('#tcnn').on('draw.dt', function () { 
         highLightTSD();
    })
    function highLightTSD() {
        var table = $.fn.dataTable.Api('#tcnn');
        table.rows().every(function (index, element) { 
            var rData = this.data();
            if (rData.DangGiaHan == "1" || rData.ThoiSuDung == "1") {
                this.nodes().to$().addClass('thoisudung');
            }
        });
    }
    highLightTSD();
});
