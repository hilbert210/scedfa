<?php

namespace App\Transformers;

use App\Option;
use League\Fractal\TransformerAbstract;

class OptionTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(Option $Option)
    {
        return [
            'id'         => $Option->id,
            'option_name'    => $Option->name,
            'option_value'   => $Option->value,
            'description'    => $Option->description,
        ];
    }
}