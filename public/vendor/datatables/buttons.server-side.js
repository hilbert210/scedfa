(function ($, DataTable) {
    "use strict";
    var _buildParams = function (dt, action, onlyVisibles) {
        var params = dt.ajax.params();
        params.action = action;
        params._token = $('meta[name="csrf-token"]').attr("content");

        if (onlyVisibles) {
            params.visible_columns = _getVisibleColumns();
        } else {
            params.visible_columns = null;
        }

        return params;
    };

    var _getVisibleColumns = function () {
        var visible_columns = [];
        $.each(DataTable.settings[0].aoColumns, function (key, col) {
            if (col.bVisible) {
                visible_columns.push(col.name);
            }
        });

        return visible_columns;
    };

    var _downloadFromUrl = function (url, params) {
        var postUrl = url + "/export";
        var xhr = new XMLHttpRequest();
        xhr.open("POST", postUrl, true);
        xhr.responseType = "arraybuffer";
        xhr.onload = function () {
            if (this.status === 200) {
                var filename = "";
                var disposition = xhr.getResponseHeader("Content-Disposition");
                if (disposition && disposition.indexOf("attachment") !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1])
                        filename = matches[1].replace(/['"]/g, "");
                }
                var type = xhr.getResponseHeader("Content-Type");

                var blob = new Blob([this.response], { type: type });
                if (typeof window.navigator.msSaveBlob !== "undefined") {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === "undefined") {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () {
                        URL.revokeObjectURL(downloadUrl);
                    }, 100); // cleanup
                }
            }
        };
        xhr.setRequestHeader(
            "Content-type",
            "application/x-www-form-urlencoded"
        );
        xhr.send($.param(params));
    };

    var _buildUrl = function (dt, action, TypeExport = null) {
        var url = dt.ajax.url() || "";
        var params = dt.ajax.params();
        params.action = action;
        if (TypeExport != null) {
            params.TypeExport = TypeExport;
        }
        if (window.location.pathname.split("/")[1] == "HoSoNLD" && isNaN(window.location.pathname.split("/")[2])) {
            params.export = $('#export').val();
            params.filterRangeMonth = $('#filterRangeMonth').val();
            params.TinhTrangHoSo = $('#TinhTrangHoSoFilter').val();
        }
        if (window.location.pathname.split("/")[1] == "tcnn" && isNaN(window.location.pathname.split("/")[2])) {
            params.export = $('#LoaiExport').val();
        }
        params.exportBHXH = $('#exportLuongNhanVienBH').val();
        if (url.indexOf("?") > -1) {
            return url + "&" + $.param(params);
        }
        return url + "?" + $.param(params);
    };
    var _listOption = function (list) {
        let options = [];
        $.each(list, function (key, value) {
            const str = `<option value="${key}">${value}</option>`;
            options.push(str);
        })
        return options;
    };
    var applyDatePicker = function (elm) {
        $(elm).daterangepicker({
            autoUpdateInput: false,
            showDropdowns: true,
            singleDatePicker: true,
            linkedCalendars: false,
            locale: {
                format: "DD/MM/YYYY",
                cancelLabel: "Clear"
            }
        });
        $(elm).each(function () {
            if ($(this).val()) {
                $(this).val(moment($(this).val()).format("DD/MM/YYYY"));
                $(this).daterangepicker({
                    autoUpdateInput: false,
                    showDropdowns: true,
                    singleDatePicker: true,
                    linkedCalendars: false,
                    locale: {
                        format: "DD/MM/YYYY",
                        cancelLabel: "Clear"
                    }
                });
            }
        });
        $(elm).on("apply.daterangepicker", function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY"));
        });
        $("input[type='text'], input[type='number']").on("click", function () {
            $(this).select();
        });
        $(elm).on("show.daterangepicker", function (ev, picker) {
            if (
                picker.element.offset().top -
                $(window).scrollTop() +
                picker.container.outerHeight() >
                $(window).height()
            ) {
                picker.drops = "up";
            } else {
                picker.drops = "down";
            }
            picker.move();
        });
    }
    var viewEditEmployee = function (dt, idNV = null) {
        var dataID;
        var extraInfo;
        if (!idNV) {
            const data = dt.rows('.selected').data()[0];
            dataID = data.IdHoSoNhanVienTCNN;
        } else {
            dataID = idNV;
        }
        const idForm = 'HoSoNLD';
        const idModal = '#modal-' + idForm;
        $('#loader').removeClass('d-none');
        $.get(route(idForm + '.index') + '/' + dataID + '/edit', function (data) {
            extraInfo = data;
            $('#' + idForm + '-modal-title').html("Edit " + idForm);
            $("#saveBtn-" + idForm).val("edit-" + idForm);
            $(idModal).modal('show');
            $.each(data, function (key, value) {
                if (key == "DiaBanHoatDong") {
                    const dataSelect = value ? value.split(", ") : 'Slect';
                    $("#DiaBanHoatDong").val(dataSelect);
                } else if (key == "GioiTinh") {
                    if (value == 1) {
                        $("#" + key + "Nam").prop("checked", true);
                    }
                    if (value == 0) {
                        $("#" + key + "Nu").prop("checked", true);
                    }
                } else if (key == "DichVu") {
                    if (value == 1) {
                        $("#" + key + "Co").prop("checked", true);
                    }
                    if (value == 0) {
                        $("#" + key + "Khong").prop("checked", true);
                    }
                } else if (key == "HSVPTCNN") {
                    $.each(JSON.parse(value), function (k, v) {
                        $("#HSVPTCNN_" + k).val(v == true ? 1 : 0);
                    });
                } else if (key == "HSNLD") {
                    $.each(JSON.parse(value), function (k, v) {
                        $("#HSNLD_" + k).val(v == true ? 1 : 0);
                    });
                } else if (key == "HSTTTH") {
                    $.each(JSON.parse(value), function (k, v) {
                        $("#HSTTTH_" + k).val(v == true ? 1 : 0);
                    });
                } else if (key == "LBHNV") {
                    $.each(JSON.parse(value), function (k, v) {
                        $("#LBHNV_" + k).val(v == true ? 1 : 0);
                    });
                } else {
                    $(`input[id="${key}"]`).val(value);
                }
            });
            $("input[type='checkbox']").each(function () {
                if ($(this).val() == '1') {
                    $(this).prop('checked', true);
                }
            });
            $('form select').trigger("chosen:updated");
            $(".form-HoSoNLD .datepicker").each(function () {
                if ($(this).val()) {
                    $(this).val(moment($(this).val()).format("DD/MM/YYYY"));
                    $(this).daterangepicker({
                        autoUpdateInput: false,
                        showDropdowns: true,
                        singleDatePicker: true,
                        linkedCalendars: false,
                        locale: {
                            format: "DD/MM/YYYY",
                            cancelLabel: "Clear"
                        }
                    });
                }
            });
            $(".datepicker").on("apply.daterangepicker", function (
                ev,
                picker
            ) {
                $(this).val(
                    picker.startDate.format("DD/MM/YYYY")
                );
            });
            $(".datepicker").on("show.daterangepicker", function (ev, picker) {
                if (
                    picker.element.offset().top -
                    $(window).scrollTop() +
                    picker.container.outerHeight() >
                    $(window).height()
                ) {
                    picker.drops = "up";
                } else {
                    picker.drops = "down";
                }
                picker.move();
            });

            if (data["avatar"]) {
                $("#picture").attr("src", data["avatar"]);
            } else {
                $("#picture").attr("src", "/img/avatar-temp.png");
            }
            $("#picture").hide();
            $("#picture").fadeIn(500);
            $('#file-dinh-kem tbody').empty();
            loadTableHistoryHSVN(dataID);
            $.get(route('FileDinhKem', [data['MaNhanVien'], 1]), function (data) {
                if (data) {
                    data.forEach(function (item, index) {
                        $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                        </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"
                        onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                    });
                }
            });
            $(".nav-tabs-bottom li:nth-child(5) a").removeClass("disabled");
            $("#button-upload-avatar").removeClass('disabled');
            const valueTinhTrangHoSo = data["TinhTrangHoSo"];
            if (parseInt(valueTinhTrangHoSo) == 0) {
                $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
                $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
                $('.nav-tabs-bottom li:nth-child(4)').addClass('hide');
                $('.nav-tabs-bottom li:nth-child(1)').removeClass('hide');
                $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
                $('select[name="TinhTrangHoSo"] option[value="0"]').removeAttr('disabled');
                $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                $('select[name="TinhTrangHoSo"] option[value="2"]').attr('disabled', 'disabled');
            }
            if ([1, 2].includes(parseInt(valueTinhTrangHoSo))) {
                $('select[name="TinhTrangHoSo"] option[value="2"]').removeAttr('disabled');
                $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                $('select[name="TinhTrangHoSo"] option[value="0"]').attr('disabled', 'disabled');
                $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
                $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
                if (parseInt(valueTinhTrangHoSo) == 1) {
                    $('.nav-tabs-bottom li:nth-child(2) a').tab('show');
                    $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
                    $('.nav-tabs-bottom li:nth-child(2)').removeClass('hide');
                } else {
                    $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
                    $('.nav-tabs-bottom li:nth-child(3)').removeClass('hide');
                    $('.nav-tabs-bottom li:nth-child(3) a').tab('show');
                }
            }
            addressEdit("KhaiSinh", data);
            addressEdit("NguyenQuan", data);
            addressEdit("HoKhau", data);
            addressEdit("ThuongTru", data);

            $('#loader').addClass('d-none');
        })
        saveBtnClick(idForm, idModal, dt, 'edit', extraInfo);

    }
    DataTable.ext.buttons.excel = {
        className: "buttons-excel",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.excel", "Excel")
            );
        },

        action: async function (e, dt, button, config) {
            let TypeExport = null;
            if (window.location.pathname.split("/")[1] == "tcnn" && !isNaN(window.location.pathname.split("/")[2])) {
                let IdTCNN = window.location.pathname.split("/")[2];
                TypeExport = await $.ajax({
                    url: route("TypeExport", IdTCNN),
                    dataType: "json",
                    async: true
                });
            }
            $('#exportLuongNhanVienBH').val('');
            var url = _buildUrl(dt, "excel", TypeExport);
            window.location = url;
        }
    };
    //export bhxh
    DataTable.ext.buttons.bhxh = {
        className: "buttons-excel",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.excel", "Bảo hiểm")
            );
        },

        action: async function (e, dt, button, config) {
            let TypeExport = null;
            if (window.location.pathname.split("/")[1] == "tcnn" && !isNaN(window.location.pathname.split("/")[2])) {
                let IdTCNN = window.location.pathname.split("/")[2];
                TypeExport = await $.ajax({
                    url: route("TypeExport", IdTCNN),
                    dataType: "json",
                    async: true
                });
            }
            $('#exportLuongNhanVienBH').val(1);
            var url = _buildUrl(dt, "excel", TypeExport);
            window.location = url;
        }
    }

    DataTable.ext.buttons.postExcel = {
        className: "buttons-excel",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.excel", "Excel")
            );
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, "excel");

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.postExcelVisibleColumns = {
        className: "buttons-excel",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.excel", "Excel (only visible columns)")
            );
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, "excel", true);

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.export = {
        extend: "collection",

        className: "buttons-export",

        text: function (dt) {
            return (
                '<i class="fa fa-download"></i> ' +
                dt.i18n("buttons.export", "Export") +
                '&nbsp;<span class="caret"/>'
            );
        },
        buttons: ((window.location.pathname.split("/")[1] == "tcnn" && !isNaN(window.location.pathname.split("/")[2])) || window.location.pathname.split("/")[1] == "LuongNhanVien") ? ["excel", "bhxh"] : ["excel"]
    };

    DataTable.ext.buttons.csv = {
        className: "buttons-csv",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.csv", "CSV")
            );
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, "csv");
            window.location = url;
        }
    };

    DataTable.ext.buttons.postCsvVisibleColumns = {
        className: "buttons-csv",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.csv", "CSV (only visible columns)")
            );
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, "csv", true);

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.postCsv = {
        className: "buttons-csv",

        text: function (dt) {
            return (
                '<i class="fa fa-file-excel-o"></i> ' +
                dt.i18n("buttons.csv", "CSV")
            );
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, "csv");

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.pdf = {
        className: "buttons-pdf",

        text: function (dt) {
            return (
                '<i class="fa fa-file-pdf-o"></i> ' +
                dt.i18n("buttons.pdf", "PDF")
            );
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, "pdf");
            window.location = url;
        }
    };

    DataTable.ext.buttons.postPdf = {
        className: "buttons-pdf",

        text: function (dt) {
            return (
                '<i class="fa fa-file-pdf-o"></i> ' +
                dt.i18n("buttons.pdf", "PDF")
            );
        },

        action: function (e, dt, button, config) {
            var url = dt.ajax.url() || window.location.href;
            var params = _buildParams(dt, "pdf");

            _downloadFromUrl(url, params);
        }
    };

    DataTable.ext.buttons.print = {
        className: "buttons-print",

        text: function (dt) {
            return (
                '<i class="fa fa-print"></i> ' +
                dt.i18n("buttons.print", "Print")
            );
        },

        action: function (e, dt, button, config) {
            var url = _buildUrl(dt, "print");
            window.location = url;
        }
    };

    DataTable.ext.buttons.reset = {
        className: "buttons-reset",

        text: function (dt) {
            return (
                '<i class="fa fa-undo"></i> ' +
                dt.i18n("buttons.reset", "Reset")
            );
        },

        action: function (e, dt, button, config) {
            dt.search("");
            dt.columns().search("");
            dt.draw();
        }
    };

    DataTable.ext.buttons.reload = {
        className: "buttons-reload",

        text: function (dt) {
            return (
                '<i class="fa fa-refresh"></i> ' +
                dt.i18n("buttons.reload", "Reload")
            );
        },

        action: function (e, dt, button, config) {
            dt.rows('.selected').deselect();
            dt.draw(false);
        }
    };

    DataTable.ext.buttons.bangluongNV = {
        className: "buttons-bangluongNV",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.bangluongNV", "Bảng Lương Nhân Viên")
            );
        },

        action: function (e, dt, button, config) {
            const data = dt.rows('.selected').data()[0];
            const url = '/HoSoNLD/' + data.IdHoSoNhanVienTCNN;
            window.location = url;
        }
    };
    DataTable.ext.buttons.bangluongTCNN = {
        className: "buttons-bangluongTCNN",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.bangluongTCNN", "Bảng Lương TCNN")
            );
        },

        action: function (e, dt, button, config) {
            const data = dt.rows('.selected').data()[0];
            const url = '/tcnn/' + data.IdTCNN;
            window.location = url;
        }
    };

    DataTable.ext.buttons.hoSoTTThuHo = {
        className: "buttons-hoSoTTThuHo",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.hoSoTTThuHo", "Hồ Sơ Thông Tin Thu Hộ")
            );
        },

        action: function (e, dt, button, config) {
            const data = dt.rows('.selected').data()[0];
            const url = '/tcnn/ttthuho/' + data.IdTCNN;
            window.location = url;
        }
    };

    DataTable.ext.buttons.create_salary_all = {
        className: "buttons-create_salary_all",

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.create_salary_all", "Tạo Lương Tháng Tiếp Theo")
            );
        },

        action: function (e, dt, button, config) {
            const idModal = '#modal-CreateSalaryAll';
            $(idModal).modal('show');
            saveBtnCreateSalary();
        }
    };

    DataTable.ext.buttons.edit_salary_1 = {
        className: "buttons-edit_salary_1",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.edit_salary_1", "Chỉnh Sửa Lương")
            );
        },

        action: function (e, dt, button, config) {
            const data = dt.rows('.selected').data()[0];
            const dataID = data[Object.keys(data)[0]];
            const idForm = dt.tables().nodes().to$().attr('id');
            const idModal = '#modal-EditSalary';
            $('#loader').removeClass('d-none');
            $.ajax({
                url: route("getSalaryByID", dataID),
                dataType: "json"
            }).then(data => {
                $.each(data, function (key, value) {
                    $("#" + key).val(value);
                })
                $("input[type='checkbox']").each(function () {
                    if ($(this).val() == '1') {
                        $(this).prop('checked', true);
                    }
                });
                $('form select').trigger("chosen:updated");
                $(".datepicker").each(function () {
                    if ($(this).val()) {
                        $(this).val(moment($(this).val()).format("DD/MM/YYYY"));
                        $(this).daterangepicker({
                            autoUpdateInput: false,
                            showDropdowns: true,
                            singleDatePicker: true,
                            linkedCalendars: false,
                            locale: {
                                format: "DD/MM/YYYY",
                                cancelLabel: "Clear"
                            }
                        });
                    }
                });
                $(".datepicker").on("apply.daterangepicker", function (
                    ev,
                    picker
                ) {
                    $(this).val(
                        picker.startDate.format("DD/MM/YYYY")
                    );
                });
                $(".datepicker").on("show.daterangepicker", function (ev, picker) {
                    if (
                        picker.element.offset().top -
                        $(window).scrollTop() +
                        picker.container.outerHeight() >
                        $(window).height()
                    ) {
                        picker.drops = "up";
                    } else {
                        picker.drops = "down";
                    }
                    picker.move();
                });
                $('#loader').addClass('d-none');
                // editOrCreateVPLVTCT(data,idModal,idForm);
                // $('#loader').addClass('d-none');
            });
            $(idModal).modal('show');
            // saveBtnEditSalary();
        }
    };

    function dataSalary(IdTCNN, type) {
        const createSalary = new $.fn.DataTable.Api('#createSalary');
        createSalary.clear().draw(true);
        $.ajax({
            url: route('createSalary', [IdTCNN, type]),
            type: "GET",
            dataType: "json",
            success: function (response) {
                let data = response.data;
                let DichVu = response.DichVu;
                let lstPA = response.options.PhuongAn;
                let lstDT = response.options.DoiTuong; //doi tuong
                if (!$.fn.DataTable.isDataTable("#createSalary")) {
                    $("#createSalary").DataTable({
                        scrollY: '40vh',
                        scrollCollapse: true,
                        deferRender: true,
                        paging: false,
                        scrollX: true,
                        fixedColumns: {
                            leftColumns: 3
                        },
                        columnDefs: [
                            { "width": "1rem", "targets": 0 },
                            { "width": "5rem", "targets": 1 },
                            { "width": "2rem", "targets": 2 },
                            { "width": "20rem", "targets": 8 }
                        ]
                    });
                }
                $('select[name="DichVu"]').val(DichVu);
                if (DichVu == 0) {
                    $('#no_service').removeClass('d-none').addClass('d-flex');
                } else {
                    $('#service').removeClass('d-none').addClass('d-flex');
                }
                if (!_.isEmpty(data)) {
                    if (type == 'create') {
                        $('input[name="NgayTinhLuong"]').val(
                            moment(data[0].NgayTinhLuong).add(5, 'day').endOf('month').format("DD/MM/YYYY")
                        );
                    };
                    if (type == 'edit') {
                        $('input[name="NgayTinhLuong"]').val(
                            moment(data[0].NgayTinhLuong).endOf('month').format("DD/MM/YYYY")
                        );
                    };
                    if (DichVu == 1) {
                        $('input[name="PhiDichVu"]').val(
                            _.findLast(data, function (item) { return item.PhiDichVu > 0; }) ? _.findLast(data, function (item) { return item.PhiDichVu > 0; }).PhiDichVu : ''
                        );
                        $('input[name="TyGia_Insurance"]').val(
                            _.findLast(data, function (item) { return item.TyGia_Insurance > 0; }) ? _.findLast(data, function (item) { return item.TyGia_Insurance > 0; }).TyGia_Insurance : ''
                        );
                        $('input[name="SI_Company"]').val(
                            _.findLast(data, function (item) { return item.SI_Company > 0; }) ? _.findLast(data, function (item) { return item.SI_Company > 0; }).SI_Company : ''
                        );
                        $('input[name="HI_Company"]').val(
                            _.findLast(data, function (item) { return item.HI_Company > 0; }) ? _.findLast(data, function (item) { return item.HI_Company > 0; }).HI_Company : ''
                        );
                        $('input[name="UI_Company"]').val(
                            _.findLast(data, function (item) { return item.UI_Company > 0; }) ? _.findLast(data, function (item) { return item.UI_Company > 0; }).UI_Company : ''
                        );
                    } else {
                        $('input[name="TyGia_Insurance"]').parent().parent().addClass('hide');
                        $('input[name="PhanTram_CSD"]').val(
                            _.findLast(data, function (item) { return item.PhanTram_CSD > 0; }) ? _.findLast(data, function (item) { return item.PhanTram_CSD > 0; }).PhanTram_CSD : ''
                        );
                        $('input[name="PhanTram_NLD"]').val(
                            _.findLast(data, function (item) { return item.PhanTram_NLD > 0; }) ? _.findLast(data, function (item) { return item.PhanTram_NLD > 0; }).PhanTram_NLD : ''
                        );
                    }
                    $('input[name="TyGia"]').val(
                        _.findLast(data, function (item) { return item.TyGia > 0; }) ? _.findLast(data, function (item) { return item.TyGia > 0; }).TyGia : ''
                    );
                    $('input[name="TrichNop_BHXH"]').val(
                        _.findLast(data, function (item) { return item.TrichNop_BHXH > 0; }) ? _.findLast(data, function (item) { return item.TrichNop_BHXH > 0; }).TrichNop_BHXH : ''
                    );
                    $('input[name="BaoHiemYTe"]').val(
                        _.findLast(data, function (item) { return item.BaoHiemYTe > 0; }) ? _.findLast(data, function (item) { return item.BaoHiemYTe > 0; }).BaoHiemYTe : ''
                    );
                    $('input[name="BaoHiemThatNghiep"]').val(
                        _.findLast(data, function (item) { return item.BaoHiemThatNghiep > 0; }) ? _.findLast(data, function (item) { return item.BaoHiemThatNghiep > 0; }).BaoHiemThatNghiep : ''
                    );
                    $('input[name="GoiHanTinhBHXH"]').val(
                        _.findLast(data, function (item) { return item.GoiHanTinhBHXH > 0; }) ? _.findLast(data, function (item) { return item.GoiHanTinhBHXH > 0; }).GoiHanTinhBHXH : ''
                    );
                    $('input[name="GoiHanTinhBHTN"]').val(
                        _.findLast(data, function (item) { return item.GoiHanTinhBHTN > 0; }) ? _.findLast(data, function (item) { return item.GoiHanTinhBHTN > 0; }).GoiHanTinhBHTN : ''
                    );
                    const table = $.fn.DataTable.Api('#createSalary');
                    Object.values(data).forEach(function (item, index) {
                        table.row.add([
                            //item.IdHoSoNhanVienTCNN,
                            `<p class= "details-employee ${'nv-' + item.IdHoSoNhanVienTCNN}" >${item.IdHoSoNhanVienTCNN}</p>
                            <button type="button" class="dieuchinhLuong"  data-id="${item.IdHoSoNhanVienTCNN}" id="${'dieuchinh-' + item.IdHoSoNhanVienTCNN}" data-ho="${item.Ho}" data-ten="${item.Ten}"><i class="fa fa-plus" aria-hidden="true"></i></button>`,
                            `<span class="details-honv" data-id="${item.IdHoSoNhanVienTCNN}">${item.Ho}</span>`,
                            `<span class="details-tennv" data-id="${item.IdHoSoNhanVienTCNN}">${item.Ten}</span>`,
                            `<input type="text" disabled class="money" id="${item.IdHoSoNhanVienTCNN + 'LuongCu'}"  name="${item.IdHoSoNhanVienTCNN + 'LuongCu'}" value="${item.LuongCu}">`,
                            `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'luongmoi'}" name="${item.IdHoSoNhanVienTCNN + 'luongmoi'}" value="${item.LuongMoi}">`,
                            `<input type="checkbox" id="${item.IdHoSoNhanVienTCNN + 'USD'}" name="${item.IdHoSoNhanVienTCNN + 'USD'}" ${item.USD == 1 ? 'checked' : ''} value="${item.IdHoSoNhanVienTCNN + 'USD'}">`,
                            `<input type="text"  id="${item.IdHoSoNhanVienTCNN + 'NgayHieuLuc'}" name="${item.IdHoSoNhanVienTCNN + 'NgayHieuLuc'}" class="datepicker" ${item.NgayHieuLuc ? 'value="' + moment(item.NgayHieuLuc).format('DD/MM/YYYY') + '"' : ''}>`,
                            `<input type="number" min="0" max="100" step="1" id="${item.IdHoSoNhanVienTCNN + 'SoNguoiPhuThuoc'}" name="${item.IdHoSoNhanVienTCNN + 'SoNguoiPhuThuoc'}" value="${item.SoNguoiPhuThuoc ? item.SoNguoiPhuThuoc : 0}">`,
                            `<input type="number" min="0" max="31" step="0.5" ${(DichVu == 0) ? 'disabled' : ''}  id="${item.IdHoSoNhanVienTCNN + 'SoNgayLamViec'}" name="${item.IdHoSoNhanVienTCNN + 'SoNgayLamViec'}" value="${item.SoNgayLamViec ? item.SoNgayLamViec : 0}">`,
                            `<input type="number" min="0" max="31" step="0.5" ${(DichVu == 0) ? 'disabled' : ''} id="${item.IdHoSoNhanVienTCNN + 'SoNgayNghiCoLuong'}" name="${item.IdHoSoNhanVienTCNN + 'SoNgayNghiCoLuong'}" value="${item.SoNgayNghiCoLuong ? item.SoNgayNghiCoLuong : 0}">`,
                            `<input type="text" ${(DichVu == 0) ? 'disabled' : ''} class="money" id="${item.IdHoSoNhanVienTCNN + 'LuongTheoSoNgayLamViec'}" name="${item.IdHoSoNhanVienTCNN + 'LuongTheoSoNgayLamViec'}" value="${item.LuongTheoSoNgayLamViec ? item.LuongTheoSoNgayLamViec : 0}">`,
                            `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'PhuCapDongBH'}" name="${item.IdHoSoNhanVienTCNN + 'PhuCapDongBH'}" value="${item.PhuCapDongBH ? item.PhuCapDongBH : 0}"> <span style="margin-top:10px; float:right; font-weight: bold;"><input  type="checkbox" id="${item.IdHoSoNhanVienTCNN + 'PhuCap_USD'}" name="${item.IdHoSoNhanVienTCNN + 'PhuCap_USD'}" ${item.PhuCap_USD == 1 ? 'checked' : ''} value="${item.IdHoSoNhanVienTCNN + 'PhuCap_USD'}"><label style="font-weight:bold;margin-left:5px;" for="USD">USD</label></span>`,
                            `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac'}" name="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac'}" value="${item.ThuNhapKhac ? item.ThuNhapKhac : 0}"><span style="margin-top:10px; float:right; font-weight: bold;"><input  type="checkbox" id="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac_USD'}" name="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac_USD'}" ${item.ThuNhapKhac_USD == 1 ? 'checked' : ''} value="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac_USD'}"><label style="font-weight:bold;margin-left:5px;" for="USD">USD</label></span>`,
                            `<select style="height: 24px;" name="${item.IdHoSoNhanVienTCNN + 'DoiTuong'}" id="${item.IdHoSoNhanVienTCNN + 'DoiTuong'}">
                                <option value=""></option>
                                ${_listOption(lstDT)}
                            </select>`,
                            `<select style="height: 24px;" name="${item.IdHoSoNhanVienTCNN + 'MaPA'}" id="${item.IdHoSoNhanVienTCNN + 'MaPA'}">
                               <option value=""></option>
                               ${_listOption(lstPA)}
                            `,
                            `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'Lai'}"  name="${item.IdHoSoNhanVienTCNN + 'Lai'}" value="${item.Lai}">`,
                            `<textarea id="${item.IdHoSoNhanVienTCNN + 'GhiChu'}"  name="${item.IdHoSoNhanVienTCNN + 'GhiChu'}">${item.GhiChu}</textarea>`,
                            `<input type="text"  id="${item.IdHoSoNhanVienTCNN + 'NgayKT'}" name="${item.IdHoSoNhanVienTCNN + 'NgayKT'}" class="datepicker" >`,
                            `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'Phi_CK'}" name="${item.IdHoSoNhanVienTCNN + 'Phi_CK'}" value="${item.Phi_CK ? item.Phi_CK : 0}">`,
                        ]).draw(false);
                        $('.money').autoNumeric('init', {
                            //mDec: 0
                            decimalCharacter: ',',
                            digitGroupSeparator: '.',
                        });
                        $('#' + item.IdHoSoNhanVienTCNN + 'DoiTuong').val(item.DoiTuong);
                        $('#' + item.IdHoSoNhanVienTCNN + 'MaPA').val(item.MaPA);
                        $('#dieuchinh-' + item.IdHoSoNhanVienTCNN).data("item", item);
                    });
                    $('#createSalary').on('click', 'tbody tr button', function (evt) {
                        //var id = $(this).data('id');
                        var item = $(this).data("item");
                        if ($(this).hasClass('dieuchinhLuong')) {
                            var ho = $(this).data("ho");
                            var ten = $(this).data("ten");
                            var content = [
                                //item.IdHoSoNhanVienTCNN,
                                `<p class="${'nv-' + item.IdHoSoNhanVienTCNN}">${item.IdHoSoNhanVienTCNN}</p>
                                <input type="hidden" name="dieuchinhLuong" value="1" />
                                <div>
                                <div style="float: left;"><button type="button" class="luuDieuChinh"  data-id="${item.IdHoSoNhanVienTCNN}" data-ho="${item.Ho}" data-ten="${item.Ten}"><i class="fa fa-save" aria-hidden="true"></i></button></div>
                                <div style="float: left;"><button type="button" class="xoaDieuChinh"  data-id="${item.IdHoSoNhanVienTCNN}" data-ho="${item.Ho}" data-ten="${item.Ten}"><i class="fa fa-times" aria-hidden="true"></i></button></div>
                                </div>`,
                                item.Ho,
                                item.Ten,
                                `<input type="text"  class="money" id="${item.IdHoSoNhanVienTCNN + 'DCLuongCu'}"  name="${item.IdHoSoNhanVienTCNN + 'DCLuongCu'}" value="${item.LuongCu}">`,
                                `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'dcluongmoi'}" name="${item.IdHoSoNhanVienTCNN + 'dcluongmoi'}" value="${item.LuongMoi}">`,
                                `<input type="checkbox" id="${item.IdHoSoNhanVienTCNN + 'USD'}" name="${item.IdHoSoNhanVienTCNN + 'USD'}" ${item.USD == 1 ? 'checked' : ''} value="${item.IdHoSoNhanVienTCNN + 'USD'}">`,
                                `<input type="text"  id="${item.IdHoSoNhanVienTCNN + 'NgayHieuLuc'}" name="${item.IdHoSoNhanVienTCNN + 'NgayHieuLuc'}" class="dcngayHL datepicker" ${item.NgayHieuLuc ? 'value="' + moment(item.NgayHieuLuc).format('DD/MM/YYYY') + '"' : ''}>`,
                                `<input type="number" min="0" max="100" step="1" id="${item.IdHoSoNhanVienTCNN + 'DCSoNguoiPhuThuoc'}" name="${item.IdHoSoNhanVienTCNN + 'DCSoNguoiPhuThuoc'}" value="${item.SoNguoiPhuThuoc ? item.SoNguoiPhuThuoc : 0}">`,
                                `<input type="number" min="0" max="31" step="0.5" ${(DichVu == 0) ? 'disabled' : ''}  id="${item.IdHoSoNhanVienTCNN + 'DCSoNgayLamViec'}" name="${item.IdHoSoNhanVienTCNN + 'DCSoNgayLamViec'}" value="${item.SoNgayLamViec ? item.SoNgayLamViec : 0}">`,
                                `<input type="number" min="0" max="31" step="0.5" ${(DichVu == 0) ? 'disabled' : ''} id="${item.IdHoSoNhanVienTCNN + 'DCSoNgayNghiCoLuong'}" name="${item.IdHoSoNhanVienTCNN + 'DCSoNgayNghiCoLuong'}" value="${item.SoNgayNghiCoLuong ? item.SoNgayNghiCoLuong : 0}">`,
                                `<input type="text" ${(DichVu == 0) ? 'disabled' : ''} class="money" id="${item.IdHoSoNhanVienTCNN + 'DCLuongTheoSoNgayLamViec'}" name="${item.IdHoSoNhanVienTCNN + 'DCLuongTheoSoNgayLamViec'}" value="${item.LuongTheoSoNgayLamViec ? item.LuongTheoSoNgayLamViec : 0}">`,
                                `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'DCPhuCapDongBH'}" name="${item.IdHoSoNhanVienTCNN + 'DCPhuCapDongBH'}" value="${item.PhuCapDongBH ? item.PhuCapDongBH : 0}"> <span style="margin-top:10px; float:right; font-weight: bold;"><input  type="checkbox" id="${item.IdHoSoNhanVienTCNN + 'PhuCap_USD'}" name="${item.IdHoSoNhanVienTCNN + 'PhuCap_USD'}" ${item.USD == 1 ? 'checked' : ''} value="${item.IdHoSoNhanVienTCNN + 'PhuCap_USD'}"><label style="font-weight:bold;margin-left:5px;" for="USD">USD</label></span>`,
                                `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'DCThuNhapKhac'}" name="${item.IdHoSoNhanVienTCNN + 'DCThuNhapKhac'}" value="${item.ThuNhapKhac ? item.ThuNhapKhac : 0}"> <span style="margin-top:10px; float:right; font-weight: bold;"><input  type="checkbox" id="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac_USD'}" name="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac_USD'}" ${item.USD == 1 ? 'checked' : ''} value="${item.IdHoSoNhanVienTCNN + 'ThuNhapKhac_USD'}"><label style="font-weight:bold;margin-left:5px;" for="USD">USD</label></span>`,
                                `<select style="height: 24px;" name="${item.IdHoSoNhanVienTCNN + 'DCDoiTuong'}" id="${item.IdHoSoNhanVienTCNN + 'DCDoiTuong'}">
                                <option value=""></option>
                                ${_listOption(lstDT)}
                            </select>`,
                                `<select style="height: 24px;" name="${item.IdHoSoNhanVienTCNN + 'MaPA'}" id="${item.IdHoSoNhanVienTCNN + 'MaPA'}">
                               <option value=""></option>
                               ${_listOption(lstPA)}
                            `,
                                `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'Lai'}"  name="${item.IdHoSoNhanVienTCNN + 'Lai'}" value="${item.Lai}">`,
                                `<textarea id="${item.IdHoSoNhanVienTCNN + 'GhiChu'}"  name="${item.IdHoSoNhanVienTCNN + 'GhiChu'}">${item.GhiChu}</textarea>`,
                                `<input type="text"  id="${item.IdHoSoNhanVienTCNN + 'NgayKT'}" name="${item.IdHoSoNhanVienTCNN + 'NgayKT'}" class="dcngayHL datepicker" ${item.Ngay_KT_HL ? 'value="' + moment(item.Ngay_KT_HL).format('DD/MM/YYYY') + '"' : ''}>`,
                                `<input type="text" class="money" id="${item.IdHoSoNhanVienTCNN + 'Phi_CK'}" name="${item.IdHoSoNhanVienTCNN + 'Phi_CK'}" value="${item.Phi_CK ? item.Phi_CK : 0}">`,
                            ]
                            table.row
                                .add(content).draw(false);
                            $('.money').autoNumeric('init', {
                                //mDec: 0
                                decimalCharacter: ',',
                                digitGroupSeparator: '.',
                            });
                            applyDatePicker(".dcngayHL");
                        } else if ($(this).hasClass('luuDieuChinh')) {
                            themDieuChinh(evt);
                        } else { //remove dieu chinh
                            let r = confirm("Bạn có muốn xóa dòng điều chỉnh này không?");
                            if (r) {
                                table
                                    .row($(this).parents('tr'))
                                    .remove()
                                    .draw();
                            }
                        }
                    })
                    /** view and edit nhan vien from edit luong */
                    // $('#createSalary tbody').on('click', 'tr a', function () {
                    //     var tbl = $('#LuongNhanVien').DataTable();
                    //     var id = $(this).data("id");
                    //     viewEditEmployee(tbl, id);
                    // })
                    $('#createSalary tbody').on('click', 'tr .details-honv, tr .details-tennv', function () {
                        var tbl = $('#LuongNhanVien').DataTable();
                        var id = $(this).data("id");
                        viewEditEmployee(tbl, id);
                    });
                    if (DichVu == 0) {
                        table.columns([8, 9, 10, 18]).visible(false);
                    }

                    $(".datepicker").daterangepicker({
                        autoUpdateInput: false,
                        showDropdowns: true,
                        singleDatePicker: true,
                        linkedCalendars: false,
                        locale: {
                            format: "DD/MM/YYYY",
                            cancelLabel: "Clear"
                        }
                    });
                    $(".datepicker").on("apply.daterangepicker", function (ev, picker) {
                        $(this).val(picker.startDate.format("DD/MM/YYYY"));
                    });
                    $("input[type='text'], input[type='number']").on("click", function () {
                        $(this).select();
                    });
                    $(".datepicker").on("show.daterangepicker", function (ev, picker) {
                        if (
                            picker.element.offset().top -
                            $(window).scrollTop() +
                            picker.container.outerHeight() >
                            $(window).height()
                        ) {
                            picker.drops = "up";
                        } else {
                            picker.drops = "down";
                        }
                        picker.move();
                    });
                } else {
                    if (type == 'edit') {
                        $('input[name="NgayTinhLuong"]').val(
                            moment(moment().format('DD/MM/YYYY'), 'DD/MM/YYYY').endOf('month').format("DD/MM/YYYY")
                        );
                    };
                    if (type == 'create') {
                        $('input[name="NgayTinhLuong"]').val(
                            moment(moment().format('DD/MM/YYYY'), 'DD/MM/YYYY').add(5, 'day').endOf('month').format("DD/MM/YYYY")
                        );
                    };
                }
                $('#loader').addClass('d-none');
            }
        });
    }
    DataTable.ext.buttons.edit_salary = {
        className: "buttons-edit_salary",

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.edit_salary", "Chỉnh Sửa Lương Tháng Này")
            );
        },

        action: function (e, dt, button, config) {
            const createSalary = new $.fn.DataTable.Api('#createSalary');
            createSalary.clear().draw(false);
            const idForm = dt.tables().nodes().to$().attr('id');
            const idModal = '#modal-' + idForm;
            $("#saveBtn-" + idForm).val("create-" + idForm);
            $('#loader').removeClass('d-none');
            $(idModal).modal('show');
            const IdTCNN = window.location.pathname.split("/")[2];
            dataSalary(IdTCNN, 'edit');
            saveBtnClick(idForm, idModal, dt, 'edit');
        }
    };
    DataTable.ext.buttons.create_salary = {
        className: "buttons-create_salary",

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.create_salary", "Tạo Lương Tháng Tiếp Theo")
            );
        },

        action: function (e, dt, button, config) {
            const createSalary = new $.fn.DataTable.Api('#createSalary');
            createSalary.clear().draw(false);
            const idForm = dt.tables().nodes().to$().attr('id');
            const idModal = '#modal-' + idForm;
            $("#saveBtn-" + idForm).val("create-" + idForm);
            $('#loader').removeClass('d-none');
            $(idModal).modal('show');
            const IdTCNN = window.location.pathname.split("/")[2];
            dataSalary(IdTCNN, 'create');
            saveBtnClick(idForm, idModal, dt, 'create');
        }
    };

    DataTable.ext.buttons.delete = {
        className: "buttons-delete",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-delete"></i> ' +
                dt.i18n("buttons.delete", "Delete")
            );
        },

        action: function (e, dt, button, config) {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                }
            });
            const data = dt.rows(".selected").data()[0];
            const dataID = data[Object.keys(data)[0]];
            const idForm = dt.tables().nodes().to$().attr('id');
            let r = confirm("Bạn có muốn xóa!");
            if (r) {
                if (idForm === 'VPLVTCT') {
                    $.ajax({
                        url: route('destroyVPLVTCT', dataID),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('#loader').addClass('d-none');
                            $.toast({
                                heading: "Information",
                                text: data.success,
                                showHideTransition: "fade",
                                icon: "info"
                            });
                            dt.rows(".selected").remove().draw(false);
                        }
                    });
                } else if (idForm === 'historyHSNV') {
                    $.ajax({
                        url: route('destroyHistoryChucDanh', dataID),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('#loader').addClass('d-none');
                            $.toast({
                                heading: "Information",
                                text: data.success,
                                showHideTransition: "fade",
                                icon: "info"
                            });
                            dt.rows(".selected").remove().draw(false);
                        }
                    });
                } else {
                    $('#loader').removeClass('d-none');
                    $.ajax({
                        url: route(idForm + ".destroy", dataID),
                        type: "DELETE",
                        dataType: "json",
                        success: function (data) {
                            $('#loader').addClass('d-none');
                            $.toast({
                                heading: "Information",
                                text: data.success,
                                showHideTransition: "fade",
                                icon: "info"
                            });
                            dt.rows(".selected").remove().draw(false);
                        }
                    });
                }
            }
        }
    };

    function loadTableHistoryHSVN(dataID) {
        const historyHSNV = new $.fn.DataTable.Api('#historyHSNV');
        historyHSNV.clear().draw(false);
        $.get(route('QuaTrinhLamViec', dataID), function (data) {
            if (data) {
                if (!$.fn.DataTable.isDataTable("#historyHSNV")) {
                    $("#historyHSNV").DataTable({
                        lengthMenu: [
                            [3, 10, 20, -1],
                            [3, 10, 20, "All"]
                        ],
                        select: true,
                        buttons: ['edit', 'delete'],
                        dom: '<"top row"<"col-6"B><"col-6"p>>rt'
                    });
                }
                const table = $.fn.DataTable.Api('#historyHSNV');
                table.on("select deselect", function (e, dt, type, indexes) {
                    dt.button(".buttons-edit").enable(dt.rows({ selected: true }).any());
                    dt.button(".buttons-delete").enable(dt.rows({ selected: true }).any());
                });
                data.forEach(function (item, index) {
                    const ngayKetThuc = item.NgayKetThuc ? moment(item.NgayKetThuc).format('DD/MM/YYYY') : '';
                    table.row.add([
                        item.Id,
                        moment(item.NgayThayDoiChucDanh).format('DD/MM/YYYY'),
                        ngayKetThuc,
                        item.ChucDanh,
                        item.TenTiengVietTCNN,
                        item.TENTT
                    ]).draw(false);
                });
            }
        });
    }

    function loadTableChucDanh() {
        $.get(route('DmCD'), function (data) {
            if (data) {
                $("#DmChucDanh-ttdk,#DmChucDanh-cvht,#DmChucDanh-historyCD,#DmChucDanh-tcnn").DataTable({
                    buttons: [{
                        extend: 'create',
                        text: 'Thêm Chức Danh',
                        className: 'btn btn-default btn-xs',
                    }],
                    dom: '<"top row"<"col-6"B><"col-6"p>>rt',
                    paging: false,
                    searching: false,
                    "columnDefs": [
                        {
                            "targets": [0, 1],
                            "visible": false,
                            "searchable": false,
                        }
                    ],
                    destroy: true
                })
                $("#DmChucDanh-ttdk,#DmChucDanh-cvht,#DmChucDanh-historyCD,#DmChucDanh-tcnn").hide();
            }
        });
    }

    function addChucDanhTTDK(data, idModal, idForm) {
        let ttdk = $.fn.DataTable.Api('#DmChucDanh-ttdk');
        $(idModal).modal('show');
        $.each(data, function (key, value) {
            $("#form-DmChucDanh-ttdk" + key).val(value);
        });
        $("#saveBtnDmChucDanh-ttdk").off('click');
        $("#saveBtnDmChucDanh-ttdk").on('click', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let ChucDanh = new FormData($("#form-" + idForm)[0]);
            // var ChucDanh = this.value;
            $.ajax({
                data: ChucDanh,
                url: route('AddDmCD'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $(idModal).modal("hide");
                    ttdk.draw(false);
                    $("#saveBtnDmChucDanh-ttdk").html("Save Changes");
                    loadTableChucDanh();
                    var option = $("<option>").val(data.ChucDanh.IdChucDanh).text(data.ChucDanh.ChucDanh);
                    $("#TTDK_ChucDanh").prepend(option);
                    $("#TTDK_ChucDanh").find(option).prop('selected', true);
                    $("#TTDK_ChucDanh").trigger("chosen:updated");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (error) {
                    if (error) {
                        $('#cdError-ttdk').html(error.responseJSON.errors.ChucDanh);
                    }
                }
            });
        });
    }
    function addChucDanhCVHT(data, idModal, idForm) {
        let ttdk = $.fn.DataTable.Api('#DmChucDanh-cvht');
        $(idModal).modal('show');
        $.each(data, function (key, value) {
            $("#form-DmChucDanh-cvht" + key).val(value);
        });
        $("#saveBtnDmChucDanh-cvht").off('click');
        $("#saveBtnDmChucDanh-cvht").on('click', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let ChucDanh = new FormData($("#form-" + idForm)[0]);
            $.ajax({
                data: ChucDanh,
                url: route('AddDmCD'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $(idModal).modal("hide");
                    ttdk.draw(false);
                    $("#saveBtnDmChucDanh-cvht").html("Save Changes");
                    loadTableChucDanh();
                    var option = $("<option>").val(data.ChucDanh.IdChucDanh).text(data.ChucDanh.ChucDanh);
                    $("#CVHT_ChucDanh").prepend(option);
                    $("#CVHT_ChucDanh").find(option).prop('selected', true);
                    $("#CVHT_ChucDanh").trigger("chosen:updated");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (error) {
                    if (error) {
                        $('#cdError-cvht').html(error.responseJSON.errors.ChucDanh);
                    }
                }
            });
        });
    }
    function addChucDanhHistoryCD(data, idModal, idForm) {
        let ttdk = $.fn.DataTable.Api('#DmChucDanh-historyCD');
        $(idModal).modal('show');
        $.each(data, function (key, value) {
            $("#form-DmChucDanh-historyCD" + key).val(value);
        });
        $("#saveBtnDmChucDanh-historyCD").off('click');
        $("#saveBtnDmChucDanh-historyCD").on('click', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let ChucDanh = new FormData($("#form-" + idForm)[0]);
            $.ajax({
                data: ChucDanh,
                url: route('AddDmCD'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $(idModal).modal("hide");
                    ttdk.draw(false);
                    $("#saveBtnDmChucDanh-historyCD").html("Save Changes");
                    loadTableChucDanh();
                    var option = $("<option>").val(data.ChucDanh.IdChucDanh).text(data.ChucDanh.ChucDanh);
                    $("#History_ChucDanh").prepend(option);
                    $("#History_ChucDanh").find(option).prop('selected', true);
                    $("#History_ChucDanh").trigger("chosen:updated");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (error) {
                    if (error) {
                        $('#cdError-history').html(error.responseJSON.errors.ChucDanh);
                    }
                }
            });
        });
    }
    function addChucDanhTCNN(data, idModal, idForm) {
        let ttdk = $.fn.DataTable.Api('#DmChucDanh-tcnn');
        $(idModal).modal('show');
        $.each(data, function (key, value) {
            $("#form-DmChucDanh-tcnn" + key).val(value);
        });
        $("#saveBtnDmChucDanh-tcnn").off('click');
        $("#saveBtnDmChucDanh-tcnn").on('click', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let ChucDanh = new FormData($("#form-" + idForm)[0]);
            $.ajax({
                data: ChucDanh,
                url: route('AddDmCD'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $(idModal).modal("hide");
                    ttdk.draw(false);
                    $("#saveBtnDmChucDanh-tcnn").html("Save Changes");
                    loadTableChucDanh();
                    var option = $("<option>").val(data.ChucDanh.IdChucDanh).text(data.ChucDanh.ChucDanh);
                    $("#TCNN_ChucDanh").prepend(option);
                    $("#TCNN_ChucDanh").find(option).prop('selected', true);
                    $("#TCNN_ChucDanh").trigger("chosen:updated");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (error) {
                    if (error) {
                        $('#cdError-tcnn').html(error.responseJSON.errors.ChucDanh);
                    }
                }
            });
        });
    }

    function loadTableVPLVTCT(dataID) {
        const VPLVTCT = new $.fn.DataTable.Api('#VPLVTCT');
        VPLVTCT.clear().draw(false);
        $.get(route('getVPLVTCTbyIdTCNN', dataID), function (data) {
            if (data) {
                if (!$.fn.DataTable.isDataTable("#VPLVTCT")) {
                    $("#VPLVTCT").DataTable({
                        lengthMenu: [
                            [8, 10, 20, -1],
                            [8, 10, 20, "All"]
                        ],
                        select: true,
                        buttons: ['create', 'edit', 'delete'],
                        dom: '<"top row"<"col-6"B><"col-6"p>>rt'
                    });
                }
                const table = $.fn.DataTable.Api('#VPLVTCT');
                table.on("select deselect", function (e, dt, type, indexes) {
                    dt.button(".buttons-edit").enable(dt.rows({ selected: true }).any());
                    dt.button(".buttons-delete").enable(dt.rows({ selected: true }).any());
                });
                data.forEach(function (item, index) {
                    table.row.add([
                        item.Id,
                        item.TENTT,
                        item.DiaChi,
                        item.DienThoai,
                        item.HoVaTen_LH,
                        item.SDT_LH,
                        item.Email_LH
                    ]).draw(false);
                });
            }
        });
    }


    function saveBtnCreateSalary() {
        $("#saveBtnCreateSalary").off('click');
        $("#saveBtnCreateSalary").on('click', function (e) {
            e.preventDefault();
            $(this).html("Sending..");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let data = new FormData($("#form-CreateSalaryAll")[0]);
            $.ajax({
                data: data,
                url: route('createSalaryAll'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-CreateSalaryAll").trigger("reset");
                    $("form select").trigger("chosen:updated");
                    $('modal-CreateSalaryAll').modal("hide");
                    dt.rows(".selected").deselect();
                    $("#saveBtnCreateSalary").html("Save Changes");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    clearErrors();
                    $.each(errors, function (key, value) {
                        const ItemDOM = document.getElementById(key);
                        const ErrorMessage = value;
                        ItemDOM.parentElement.classList.add('pb-0');
                        ItemDOM.parentElement.insertAdjacentHTML('afterend', `<div class="text-danger text-right">${ErrorMessage}</div>`);
                        ItemDOM.classList.add('border', 'border-danger');
                    });
                    $("#saveBtnCreateSalary").html("Save Changes");
                }
            });
        });
    }

    function saveBtnEditSalary() {
        $("#saveBtnEditSalary").off('click');
        $("#saveBtnEditSalary").on('click', function (e) {
            e.preventDefault();
            $(this).html("Sending..");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let data = new FormData($("#form-EditSalary")[0]);
            $.ajax({
                data: data,
                url: route('editSalary'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-EditSalary").trigger("reset");
                    $("form select").trigger("chosen:updated");
                    $('modal-EditSalary').modal("hide");
                    dt.rows(".selected").deselect();
                    $("#saveBtnEditSalary").html("Save Changes");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    clearErrors();
                    $.each(errors, function (key, value) {
                        const ItemDOM = document.getElementById(key);
                        const ErrorMessage = value;
                        ItemDOM.parentElement.classList.add('pb-0');
                        ItemDOM.parentElement.insertAdjacentHTML('afterend', `<div class="text-danger text-right">${ErrorMessage}</div>`);
                        ItemDOM.classList.add('border', 'border-danger');
                    });
                    $("#saveBtnEditSalary").html("Save Changes");
                }
            });
        });
    }

    function saveBtnClick(idForm, idModal, dt, type, extraInfo = null) {
        $("#saveBtn-" + idForm).off('click');
        $("#saveBtn-" + idForm).on('click', function (e) {
            e.preventDefault();
            $(this).html("Sending..");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let data = new FormData($("#form-" + idForm)[0]);
            if (idForm == 'tcnn') {
                data.append('SoLuong', (type == 'edit') ? dt.row({ selected: true }).data().SoLuong : 0);
            }
            if (idForm == 'HoSoNLD' && type == 'edit') {
                data.append('ThongBaoNghiViec', dt.row({ selected: true }).data().ThongBaoNghiViec);
                data.append('actions', dt.row({ selected: true }).data().actions);
                data.append('isUpdate', true);
            }
            if (idForm == 'LuongNhanVien') {
                let json = $('#createSalary tbody tr').map(function () {
                    let IdHoSoNhanVienTCNN = $('td', this).eq(0).text(),
                        DichVu = $('#DichVu').val(),
                        NgayTinhLuong = $('#NgayTinhLuong').val(),
                        TyGia = $('#TyGia').autoNumeric('get'),
                        PhiDichVu = null,
                        TyGia_Insurance = null,
                        //TrichNop_BHXH = $('#TrichNop_BHXH').val(),
                        TrichNop_BHXH = null,
                        BaoHiemYTe = null,
                        BaoHiemThatNghiep = null,
                        GoiHanTinhBHXH = $('#GoiHanTinhBHXH').autoNumeric('get'),
                        GoiHanTinhBHTN = $('#GoiHanTinhBHTN').autoNumeric('get'),
                        LuongCu = $('td', this).eq(3).find('input').autoNumeric('get'),
                        LuongMoi = $('td', this).eq(4).find('input').autoNumeric('get'),
                        USD = $('td', this).eq(5).find('input')[0].checked,
                        NgayHieuLuc = $('td', this).eq(6).find('input').val(),
                        SoNguoiPhuThuoc = $('td', this).eq(7).find('input').val(),
                        SoNgayLamViec = null,
                        SoNgayNghiCoLuong = null,
                        LuongTheoSoNgayLamViec = null,
                        ThuNhapKhac = null,
                        PhuCapDongBH = null,
                        //PhuCap_USD = $('td', this).eq(8).find('input')[1].checked,
                        PhuCap_USD = $('td', this).eq(8).find('input[type="checkbox"]:checked').length,
                        ThuNhapKhac_USD = $('td', this).eq(9).find('input[type="checkbox"]:checked').length,
                        DoiTuong = null,
                        MaPA = null,
                        Lai = null,
                        GhiChu = null,
                        PhanTram_CSD = null,
                        PhanTram_NLD = null,
                        SI_Company = null,
                        HI_Company = null,
                        Ngay_KT_HL = null,
                        UI_Company = null,
                        Phi_CK = null;

                    let nv_bhxh = Array.from(document.querySelectorAll("#TrichNop_BHXH")).filter(item => { return (item.value != null && item.value != ""); }).map(item => item.value) || "";
                    TrichNop_BHXH = Array.isArray(nv_bhxh) ? nv_bhxh[0] : nv_bhxh;
                    let nv_bhyt = Array.from(document.querySelectorAll("#BaoHiemYTe")).filter(item => { return (item.value != null && item.value != ""); }).map(item => item.value) || "";
                    BaoHiemYTe = Array.isArray(nv_bhyt) ? nv_bhyt[0] : nv_bhyt;
                    let nv_bhtn = Array.from(document.querySelectorAll("#BaoHiemThatNghiep")).filter(item => { return (item.value != null && item.value != ""); }).map(item => item.value) || "";
                    BaoHiemThatNghiep = Array.isArray(nv_bhtn) ? nv_bhtn[0] : nv_bhtn;
                    var BienDieuChinh = $('td', this).eq(0).find('#dieuchinh-' + IdHoSoNhanVienTCNN).hasClass('dieuchinhLuong') ? 0 : 1;
                    if (BienDieuChinh) {
                        Ngay_KT_HL = $('td', this).eq(14).find('input').val();
                    }

                    if (DichVu == 0) {
                        ThuNhapKhac = $('td', this).eq(9).find('input').autoNumeric('get');
                        PhuCapDongBH = $('td', this).eq(8).find('input[type="text"]').autoNumeric('get');
                        //PhuCap_USD = $('td', this).eq(8).find('input[type="checkbox"]').checked;
                        DoiTuong = $('td', this).eq(10).find('select').val();
                        MaPA = $('td', this).eq(11).find('select').val();
                        Lai = $('td', this).eq(12).find('input').autoNumeric('get');
                        GhiChu = $('td', this).eq(13).find('textarea').val();
                        PhanTram_CSD = $('#PhanTram_CSD').val();
                        PhanTram_NLD = $('#PhanTram_NLD').val();
                    } else {
                        SI_Company = $('#SI_Company').val();
                        HI_Company = $('#HI_Company').val();
                        UI_Company = $('#UI_Company').val();
                        PhiDichVu = $('#PhiDichVu').val();
                        TyGia_Insurance = $('#TyGia_Insurance').autoNumeric('get');
                        SoNgayLamViec = $('td', this).eq(8).find('input').val();
                        SoNgayNghiCoLuong = $('td', this).eq(9).find('input').val();
                        LuongTheoSoNgayLamViec = $('td', this).eq(10).find('input').autoNumeric('get');
                        ThuNhapKhac = $('td', this).eq(12).find('input').autoNumeric('get');
                        PhuCapDongBH = $('td', this).eq(11).find('input[type="text"]').autoNumeric('get');
                        // PhuCap_USD = $('td', this).eq(8).find('input[type="checkbox"]').checked;
                        PhuCap_USD = $('td', this).eq(11).find('input[type="checkbox"]:checked').length,
                            ThuNhapKhac_USD = $('td', this).eq(12).find('input[type="checkbox"]:checked').length,
                            DoiTuong = $('td', this).eq(13).find('select').val();
                        MaPA = $('td', this).eq(14).find('select').val();
                        Lai = $('td', this).eq(15).find('input').autoNumeric('get');
                        GhiChu = $('td', this).eq(16).find('textarea').val();
                        //Phi_CK = $('td', this).eq(17).find('text').val();
                        Phi_CK = $('td', this).eq(18).find('input').autoNumeric('get'),
                            console.log('dich vu ' + IdHoSoNhanVienTCNN);
                        console.log(Phi_CK);
                    }
                    return {
                        'IdHoSoNhanVienTCNN': IdHoSoNhanVienTCNN,
                        'NgayTinhLuong': moment(NgayTinhLuong, 'DD/MM/YYYY').format('YYYY-MM-DD'),
                        'LuongCu': LuongCu,
                        'LuongMoi': LuongMoi,
                        'USD': USD,
                        'PhuCap_USD': PhuCap_USD,
                        'ThuNhapKhac_USD': ThuNhapKhac_USD,
                        'NgayHieuLuc': NgayHieuLuc ? moment(NgayHieuLuc, 'DD/MM/YYYY').format('YYYY-MM-DD') : '',
                        'ThuNhapKhac': ThuNhapKhac,
                        'PhuCapDongBH': PhuCapDongBH,
                        'DoiTuong': DoiTuong,
                        'MaPA': MaPA,
                        'GhiChu': GhiChu,
                        'TyGia': TyGia,
                        'TyGia_Insurance': (DichVu == 1 && USD) ? TyGia_Insurance : null,
                        'Lai': Lai,
                        'TrichNop_BHXH': TrichNop_BHXH,
                        'BaoHiemYTe': BaoHiemYTe,
                        'BaoHiemThatNghiep': BaoHiemThatNghiep,
                        'GoiHanTinhBHXH': GoiHanTinhBHXH,
                        'GoiHanTinhBHTN': GoiHanTinhBHTN,
                        'SoNgayLamViec': (DichVu == 1) ? SoNgayLamViec : null,
                        'SoNgayNghiCoLuong': (DichVu == 1) ? SoNgayNghiCoLuong : null,
                        'LuongTheoSoNgayLamViec': (DichVu == 1) ? LuongTheoSoNgayLamViec : null,
                        'PhiDichVu': (DichVu == 1) ? PhiDichVu : null,
                        'SoNguoiPhuThuoc': SoNguoiPhuThuoc,
                        'SI_Company': SI_Company,
                        'HI_Company': HI_Company,
                        'UI_Company': UI_Company,
                        'PhanTram_CSD': PhanTram_CSD,
                        'PhanTram_NLD': PhanTram_NLD,
                        'DieuChinh': BienDieuChinh,
                        'Phi_CK': Phi_CK
                        //'Ngay_KT_HL': Ngay_KT_HL ? moment(Ngay_KT_HL,'DD/MM/YYYY').format('YYYY-MM-DD'): '',
                    }
                }).get(); //console.log('log 2'); console.log(json); return;
                data.append('data', JSON.stringify(json));
            }
            if (idForm == 'SettingUser') {
                const DataSettingUser = {
                    'id': $('#id').val(),
                    'name': $('#name').val(),
                    'username': $('#username').val(),
                    'active': $('#active').val(),
                    'level': $('#level').val() ? $('#level').val() : 3,
                    'password': $('#password').val() ? $('#password').val() : '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                    'HSVPTCNN': { 'create': $('#HSVPTCNN_create').val() == 1 ? true : false, 'edit': $('#HSVPTCNN_edit').val() == 1 ? true : false },
                    'HSNLD': { 'create': $('#HSNLD_create').val() == 1 ? true : false, 'edit': $('#HSNLD_edit').val() == 1 ? true : false },
                    'HSTTTH': { 'create': $('#HSTTTH_create').val() == 1 ? true : false, 'edit': $('#HSTTTH_edit').val() == 1 ? true : false, 'delete': $('#HSTTTH_delete').val() == 1 ? true : false },
                    'LBHNV': { 'create': $('#LBHNV_create').val() == 1 ? true : false, 'edit': $('#LBHNV_edit').val() == 1 ? true : false, 'delete': $('#LBHNV_delete').val() == 1 ? true : false },
                    'DmTinhThanh': { 'create': $('#DmTinhThanh_create').val() == 1 ? true : false, 'edit': $('#DmTinhThanh_edit').val() == 1 ? true : false },
                    'DmQuanHuyen': { 'create': $('#DmQuanHuyen_create').val() == 1 ? true : false, 'edit': $('#DmQuanHuyen_edit').val() == 1 ? true : false },
                    'DmPhuongXa': { 'create': $('#DmPhuongXa_create').val() == 1 ? true : false, 'edit': $('#DmPhuongXa_edit').val() == 1 ? true : false },
                }
                data.append('data', JSON.stringify(DataSettingUser));
            }
            //Add config option
            if (idForm == 'OptionConfig') {
                const Option = {
                    'id': $('#id').val(),
                    'option_name': $('#option_name').val(),
                    'option_value': $('#option_value').val(),
                    'description': $('#description').val()
                }
                data.append('data', JSON.stringify(Option));
            }
            if (idForm == 'DmTinhThanh' && type == 'edit') {
                data.append('isUpdateTinhThanh', true);
            }
            if (idForm == 'DmQuanHuyen' && type == 'edit') {
                data.append('isUpdateQuanHuyen', true);
            }
            if (idForm == 'DmPhuongXa' && type == 'edit') {
                data.append('isUpdatePhuongXa', true);
            }
            $.ajax({
                data: data,
                url: route(idForm + ".store"),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    //console.log(data);
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $("form select").trigger("chosen:updated");
                    $(idModal).modal("hide");
                    if (type == 'edit') {
                        dt.draw(false);
                    } else {
                        if (idForm == "LuongNhanVien") {
                            dt.draw(false);
                        } else {
                            dt.row.add(data.data).draw(false);
                        }
                    }
                    dt.rows(".selected").deselect();
                    $("#saveBtn-" + idForm).html("Save Changes");
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                },
                error: function (data) {
                    const errors = data.responseJSON.errors;
                    clearErrors();
                    $.each(errors, function (key, value) {
                        const ItemDOM = document.getElementById(key);
                        const ErrorMessage = value;
                        ItemDOM.parentElement.classList.add('pb-0');
                        ItemDOM.parentElement.insertAdjacentHTML('afterend', `<div class="text-danger text-right">${ErrorMessage}</div>`);
                        ItemDOM.classList.add('border', 'border-danger');
                    });
                    $("#saveBtn-" + idForm).html("Save Changes");
                }
            });
        });
    }

    //nelson
    //dieu chinh luong trong khoang thoi gian
    function themDieuChinh(evt) {
        var elm = evt.target;
        var idForm = 'LuongNhanVien';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        const ItemID = $(elm).data("id") || $(elm).parents('button').data("id");
        //const tbldc = $.fn.DataTable.Api('#createSalary');

        let data = new FormData($("#form-LuongNhanVien")[0]);
        let json = $('#createSalary tbody tr').map(function () {
            //let json = tbldc.row($(elm)).data().map(function(){
            let IdHoSoNhanVienTCNN = $('td', this).eq(0).text(),
                DichVu = $('#DichVu').val(),
                NgayTinhLuong = $('#NgayTinhLuong').val(),
                TyGia = $('#TyGia').autoNumeric('get'),
                PhiDichVu = null,
                TyGia_Insurance = null,
                TrichNop_BHXH = $('#TrichNop_BHXH').val(),
                BaoHiemYTe = $('#BaoHiemYTe').val(),
                BaoHiemThatNghiep = $('#BaoHiemThatNghiep').val(),
                GoiHanTinhBHXH = $('#GoiHanTinhBHXH').autoNumeric('get'),
                GoiHanTinhBHTN = $('#GoiHanTinhBHTN').autoNumeric('get'),
                LuongCu = $('td', this).eq(3).find('input').autoNumeric('get'),
                LuongMoi = $('td', this).eq(4).find('input').autoNumeric('get'),
                USD = $('td', this).eq(5).find('input')[0].checked,
                PhuCap_USD = $('td', this).eq(8).find('input')[1].checked,
                ThuNhapKhac_USD = $('td', this).eq(9).find('input')[1].checked,
                NgayHieuLuc = $('td', this).eq(6).find('input').val(),
                SoNguoiPhuThuoc = $('td', this).eq(7).find('input').val(),
                SoNgayLamViec = null,
                SoNgayNghiCoLuong = null,
                LuongTheoSoNgayLamViec = null,
                ThuNhapKhac = null,
                PhuCapDongBH = null,
                DoiTuong = null,
                MaPA = null,
                Lai = null,
                GhiChu = null,
                PhanTram_CSD = null,
                PhanTram_NLD = null,
                SI_Company = null,
                HI_Company = null,
                Ngay_KT_HL = null,
                UI_Company = null;
            Phi_CK = null;
            var BienDieuChinh = $('td', this).eq(0).find('#dieuchinh-' + IdHoSoNhanVienTCNN).hasClass('dieuchinhLuong') ? 0 : 1;
            if (BienDieuChinh) {
                Ngay_KT_HL = $('td', this).eq(14).find('input').val();
            }
            if (DichVu == 0) {
                ThuNhapKhac = $('td', this).eq(9).find('input').autoNumeric('get');
                PhuCapDongBH = $('td', this).eq(8).find('input[type="text"]').autoNumeric('get');
                //PhuCap_USD = $('td', this).eq(8).find('input[type="checkbox"]').autoNumeric('get');
                DoiTuong = $('td', this).eq(10).find('select').val();
                MaPA = $('td', this).eq(11).find('select').val();
                Lai = $('td', this).eq(12).find('input').autoNumeric('get');
                GhiChu = $('td', this).eq(13).find('textarea').val();
                PhanTram_CSD = $('#PhanTram_CSD').val();
                PhanTram_NLD = $('#PhanTram_NLD').val();
            } else {
                SI_Company = $('#SI_Company').val();
                HI_Company = $('#HI_Company').val();
                UI_Company = $('#UI_Company').val();
                PhiDichVu = $('#PhiDichVu').val();
                TyGia_Insurance = $('#TyGia_Insurance').autoNumeric('get');
                SoNgayLamViec = $('td', this).eq(8).find('input').val();
                SoNgayNghiCoLuong = $('td', this).eq(9).find('input').val();
                LuongTheoSoNgayLamViec = $('td', this).eq(10).find('input').autoNumeric('get');
                ThuNhapKhac = $('td', this).eq(12).find('input').autoNumeric('get');
                PhuCapDongBH = $('td', this).eq(11).find('input[type="text"]').autoNumeric('get');
                //PhuCap_USD = $('td', this).eq(8).find('input[type="checkbox"]').autoNumeric('get');
                DoiTuong = $('td', this).eq(13).find('select').val();
                MaPA = $('td', this).eq(14).find('select').val();
                Lai = $('td', this).eq(15).find('input').autoNumeric('get');
                GhiChu = $('td', this).eq(16).find('textarea').val();
                Phi_CK = $('td', this).eq(18).find('input').autoNumeric('get');
            }
            return {
                'IdHoSoNhanVienTCNN': IdHoSoNhanVienTCNN,
                'NgayTinhLuong': moment(NgayTinhLuong, 'DD/MM/YYYY').format('YYYY-MM-DD'),
                'LuongCu': LuongCu,
                'LuongMoi': LuongMoi,
                'USD': USD,
                'NgayHieuLuc': NgayHieuLuc ? moment(NgayHieuLuc, 'DD/MM/YYYY').format('YYYY-MM-DD') : '',
                'ThuNhapKhac': ThuNhapKhac,
                'PhuCapDongBH': PhuCapDongBH,
                'DoiTuong': DoiTuong,
                'MaPA': MaPA,
                'GhiChu': GhiChu,
                'TyGia': USD ? TyGia : null,
                'TyGia_Insurance': (DichVu == 1 && USD) ? TyGia_Insurance : null,
                'Lai': Lai,
                'TrichNop_BHXH': TrichNop_BHXH,
                'BaoHiemYTe': BaoHiemYTe,
                'BaoHiemThatNghiep': BaoHiemThatNghiep,
                'GoiHanTinhBHXH': GoiHanTinhBHXH,
                'GoiHanTinhBHTN': GoiHanTinhBHTN,
                'SoNgayLamViec': (DichVu == 1) ? SoNgayLamViec : null,
                'SoNgayNghiCoLuong': (DichVu == 1) ? SoNgayNghiCoLuong : null,
                'LuongTheoSoNgayLamViec': (DichVu == 1) ? LuongTheoSoNgayLamViec : null,
                'PhiDichVu': (DichVu == 1) ? PhiDichVu : null,
                'SoNguoiPhuThuoc': SoNguoiPhuThuoc,
                'SI_Company': SI_Company,
                'HI_Company': HI_Company,
                'UI_Company': UI_Company,
                'PhanTram_CSD': PhanTram_CSD,
                'PhanTram_NLD': PhanTram_NLD,
                'DieuChinh': BienDieuChinh,
                'Ngay_KT_HL': Ngay_KT_HL ? moment(Ngay_KT_HL, 'DD/MM/YYYY').format('YYYY-MM-DD') : '',
                'ItemID': ItemID,
                'PhuCap_USD': PhuCap_USD,
                'ThuNhapKhac_USD': ThuNhapKhac_USD,
                'Phi_CK': Phi_CK
            }
        }).get(); //console.log('log 1');console.log(json); return;
        data.append('data', JSON.stringify(json));
        $.ajax({
            data: data,
            url: route(idForm + ".store"),
            type: "POST",
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                $.toast().reset("all");
                $.toast({
                    heading: "Success",
                    text: data.success,
                    showHideTransition: "fade",
                    icon: "success"
                });
                const tbl = $.fn.DataTable.Api('#createSalary');
                tbl.row($(elm).parents('tr'))
                    .remove()
                    .draw();
                clearErrors();
            },
            error: function (data) {
                const errors = data.responseJSON.errors;
                clearErrors();
                $.each(errors, function (key, value) {
                    const ItemDOM = document.getElementById(key);
                    const ErrorMessage = value;
                    ItemDOM.parentElement.classList.add('pb-0');
                    ItemDOM.parentElement.insertAdjacentHTML('afterend', `<div class="text-danger text-right">${ErrorMessage}</div>`);
                    ItemDOM.classList.add('border', 'border-danger');
                });
            }
        });
    }
    async function addressEdit(type, dataColection) {
        const MaTT = dataColection['MaTT_' + type];
        if (MaTT) {
            $.ajax({
                url: route('DmQuanHuyenId', MaTT),
                type: "GET",
                dataType: "json",
                success: function (dataMaQU) {
                    $('#MaQU_' + type).empty();
                    $('#MaQU_' + type).prop('disabled', false).trigger("chosen:updated");
                    $('#MaQU_' + type).append('<option value=""></option>');
                    $.map(dataMaQU, function (item) {
                        $('#MaQU_' + type).append('<option value="' + item.MAQU + '">' + item.TENQUAN + '</option>');
                    });
                    $('#MaQU_' + type).val(dataColection['MaQU_' + type]);
                    $('#MaQU_' + type).trigger("chosen:updated");
                }
            });
            const MaQU = dataColection['MaQU_' + type];
            if (MaQU) {
                $.ajax({
                    url: route('DmPhuongXaId', MaQU),
                    type: "GET",
                    dataType: "json",
                    success: function (dataMaPhuongXa) {
                        $('#MaPhuongXa_' + type).empty();
                        $('#MaPhuongXa_' + type).append('<option value=""></option>');
                        $('#MaPhuongXa_' + type).prop('disabled', false).trigger("chosen:updated");
                        $.map(dataMaPhuongXa, function (item) {
                            $('#MaPhuongXa_' + type).append('<option value="' + item.MAPHUONGXA + '">' + item.TENPXA + '</option>');
                        });
                        $('#MaPhuongXa_' + type).val(dataColection['MaPhuongXa_' + type]);
                        $('#MaPhuongXa_' + type).trigger("chosen:updated");
                    }
                });

            }
        } else {
            $('#MaQU_' + type).empty();
            $('#MaQU_' + type).prop('disabled', true).trigger("chosen:updated");
            $('#MaPhuongXa_' + type).empty();
            $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
            $('#SoNha_' + type).val('');
        }
    }

    function clearErrors() {
        $("form .text-danger").remove();
        $("form .pb-0").removeClass('pb-0')
        $("form .border-danger").removeClass('border border-danger')
    }

    DataTable.ext.buttons.create = {
        className: "buttons-create",

        text: function (dt) {
            return (
                '<i class="fa fa-plus"></i> ' +
                dt.i18n("buttons.create", "Create")
            );
        },

        action: function (e, dt, button, config) {
            const idForm = dt.tables().nodes().to$().attr('id');
            const idModal = '#modal-' + idForm;
            $("#saveBtn-" + idForm).val("create-" + idForm);
            $($("form input")[1]).val("");
            $("#form-" + idForm).trigger("reset");
            $("form select").trigger("chosen:updated");
            $(idModal).modal('show');
            $('#loader').removeClass('d-none');
            if (idForm == "VPLVTCT") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                editOrCreateVPLVTCT(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-ttdk") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhTTDK(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-cvht") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhCVHT(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-historyCD") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhHistoryCD(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-tcnn") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhTCNN(data, idModal, idForm);
            }
            else {
                if (idForm == "tcnn") {
                    $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
                    $('.nav-tabs-bottom li:nth-child(2) a').addClass('disabled');
                    $('.nav-tabs-bottom li:nth-child(3) a').addClass('disabled');
                    $('#VPLVTCT tbody').empty();
                    const VPLVTCT = new $.fn.DataTable.Api('#VPLVTCT');
                    VPLVTCT.clear().draw(false);

                    $.get(route('MaTCNN'), function (data) {
                        $('#MaTCNN').val(data);
                        $('#loader').addClass('d-none');
                    });
                    loadTableChucDanh();
                }

                if (idForm == "HoSoNLD") {
                    $('#picture').attr('src', '/img/avatar-temp.png');
                    $('#picture').hide();
                    $('#picture').fadeIn(500);
                    $('#historyHSNV tbody').empty();
                    const historyHSNV = new $.fn.DataTable.Api('#historyHSNV');
                    historyHSNV.clear().draw(false);
                    $('#file-dinh-kem tbody').empty();
                    $(".nav-tabs-bottom li:nth-child(2)").addClass("hide");
                    $(".nav-tabs-bottom li:nth-child(3)").addClass("hide");
                    $(".nav-tabs-bottom li:nth-child(4)").addClass("hide");
                    $(".nav-tabs-bottom li:nth-child(5) a").addClass("disabled");
                    $("#button-upload-avatar").addClass('disabled');
                    $(".nav-tabs-bottom li:nth-child(1)").removeClass("hide");
                    $(".nav-tabs-bottom li:nth-child(1) a").tab("show");
                    $('#IdHoSoNhanVienTCNN').val('');
                    $.get(route('MaNhanVien'), function (data) {
                        $('#MaNhanVien').val(data);
                    });
                    $('select[name="TinhTrangHoSo"]').val(0);
                    $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                    $('select[name="TinhTrangHoSo"] option[value="0"]').removeAttr('disabled');
                    $('select[name="TinhTrangHoSo"] option[value="2"]').attr('disabled', 'disabled');
                    addressEdit("KhaiSinh", []);
                    addressEdit("NguyenQuan", []);
                    addressEdit("HoKhau", []);
                    addressEdit("ThuongTru", []);
                    loadTableChucDanh();
                    $('#loader').addClass('d-none');
                }
                if (idForm == "HoSoTTThuHo") {
                    if (window.location.pathname.split("/")[1] == "tcnn") {
                        const IdTCNN = window.location.pathname.split("/")[3];
                        $("#IdTCNN").val(IdTCNN);
                        $("select").trigger("chosen:updated");
                    }
                    $("#NopDenThang").attr('disabled', true);
                    $("#NopTuThang").on("apply.daterangepicker", function (
                        ev,
                        picker
                    ) {
                        $('#daterangerpickerStartEnd').val(picker.startDate.format("DD/MM/YYYY") + " - ");
                        $("#NopDenThang").attr('disabled', false);
                        $("#daterangerpickerStartEnd").val($('#NopTuThang').val() + " - " + $('#NopDenThang').val());
                    });
                    $("#NopDenThang").on("apply.daterangepicker", function (
                        ev,
                        picker
                    ) {
                        $('#daterangerpickerStartEnd').val($('#NopTuThang').val() + " - " + $('#NopDenThang').val());
                    });
                    $('#BHXH').on('keyup paste', function () {
                        $('#dc-BHXH').val($('#BHXH').val());
                    })
                    $('#TTN').on('keyup paste', function () {
                        $('#dc-TTN').val($('#TTN').val());
                    })
                    $('#Luong').on('keyup paste', function () {
                        $('#dc-Luong').val($('#Luong').val());
                    })
                    $('#BHTN').on('keyup paste', function () {
                        $('#dc-BHTN').val($('#BHTN').val());
                    })
                    $('#DVP').on('keyup paste', function () {
                        $('#dc-DVP').val($('#DVP').val());
                    })
                    $('#TroCapBHXH').on('keyup paste', function () {
                        $('#dc-TroCapBHXH').val($('#TroCapBHXH').val());
                    })
                    $('#PhiNganHang').on('keyup paste', function () {
                        $('#dc-PhiNganHang').val($('#PhiNganHang').val());
                    })
                    $('#Lai').on('keyup paste', function () {
                        $('#dc-Lai').val($('#Lai').val());
                    })
                    $('#loader').addClass('d-none');
                }
                if (idForm == 'SettingUser' || idForm == 'OptionConfig') {
                    $('#loader').addClass('d-none');
                    $('.money').autoNumeric('init', {
                        //mDec: 0
                        decimalCharacter: ',',
                        digitGroupSeparator: '.',
                    });
                }
                saveBtnClick(idForm, idModal, dt, 'create');
            }
            if (idForm == 'DmTinhThanh') {
                $('#MATT').prop('readonly', false);
                $('#loader').addClass('d-none');
            }
            if (idForm == 'DmQuanHuyen') {
                $('#MAQU').prop('readonly', false);
                $('#loader').addClass('d-none');
            }
            if (idForm == 'DmPhuongXa') {
                $('#MAPHUONGXA').prop('readonly', false);
                $('#loader').addClass('d-none');
            }
            saveBtnClick(idForm, idModal, dt, 'create');
        }
    };

    function editHistoryChucDanh(data, idModal, idForm) {
        let dt = $.fn.DataTable.Api('#historyHSNV');
        const dataHoSoNLD = $.fn.DataTable.Api('#HoSoNLD').rows('.selected').data()[0];
        const dataID = dataHoSoNLD[Object.keys(dataHoSoNLD)[0]];
        $(idModal).modal('show');
        $.each(data, function (key, value) {
            $("#form-historyHSNV #" + key).val(value);
        });
        $('#form-historyHSNV select').trigger("chosen:updated");
        $("#form-historyHSNV .datepicker").each(function () {
            if ($(this).val()) {
                $(this).val(moment($(this).val()).format("DD/MM/YYYY"));
                $(this).daterangepicker({
                    autoUpdateInput: true,
                    showDropdowns: true,
                    singleDatePicker: true,
                    linkedCalendars: false,
                    locale: {
                        format: "DD/MM/YYYY",
                        cancelLabel: "Clear"
                    }
                });
            }
        });
        $("#saveBtnHistory").off('click');
        $("#saveBtnHistory").on('click', function (e) {
            e.preventDefault();
            $(this).html("Sending..");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let data = new FormData($("#form-" + idForm)[0]);
            $.ajax({
                data: data,
                url: route('updateHistoryChucDanh'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $("form select").trigger("chosen:updated");
                    $(idModal).modal("hide");
                    dt.draw(false);
                    dt.rows(".selected").deselect();
                    $("#saveBtnHistory").html("Save Changes");
                    loadTableHistoryHSVN(dataID);
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                }
            });
        });
    }

    function editOrCreateVPLVTCT(data, idModal, idForm) {
        let dt = $.fn.DataTable.Api('#VPLVTCT');
        const datatcnn = $.fn.DataTable.Api('#tcnn').rows('.selected').data()[0];
        const dataID = datatcnn[Object.keys(datatcnn)[0]];
        $(idModal).modal('show');
        $.each(data, function (key, value) {
            $("#form-VPLVTCT #" + key).val(value);
        });
        $('#form-VPLVTCT select').trigger("chosen:updated");
        $("#saveBtnVPLVTCT").off('click');
        $("#saveBtnVPLVTCT").on('click', function (e) {
            e.preventDefault();
            $(this).html("Sending..");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let data = new FormData($("#form-" + idForm)[0]);
            data.append('IdTCNN', dataID);
            $.ajax({
                data: data,
                url: route('editOrCreateVPLVTCT'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (data) {
                    $.toast().reset("all");
                    $("#form-" + idForm).trigger("reset");
                    $("form select").trigger("chosen:updated");
                    $(idModal).modal("hide");
                    dt.draw(false);
                    dt.rows(".selected").deselect();
                    $("#saveBtnVPLVTCT").html("Save Changes");
                    loadTableVPLVTCT(dataID);
                    $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                    });
                    clearErrors();
                }
            });
        });
    }
    DataTable.ext.buttons.detail_employee = {
        className: "buttons-detail_employee",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-money"></i> ' +
                dt.i18n("buttons.detail_employee", "Hồ Sơ Nhân Viên")
            );
        },

        action: function (e, dt, button, config) {
            viewEditEmployee(dt);
            const data = dt.rows('.selected').data()[0];
            const dataID = data.IdHoSoNhanVienTCNN;
            const idForm = 'HoSoNLD';
            const idModal = '#modal-' + idForm;
            $('#loader').removeClass('d-none');
            $.get(route(idForm + '.index') + '/' + dataID + '/edit', function (data) {
                $('#' + idForm + '-modal-title').html("Edit " + idForm);
                $("#saveBtn-" + idForm).val("edit-" + idForm);
                $(idModal).modal('show');
                $.each(data, function (key, value) {
                    if (key == "DiaBanHoatDong") {
                        const dataSelect = value ? value.split(", ") : 'Slect';
                        $("#DiaBanHoatDong").val(dataSelect);
                    } else if (key == "GioiTinh") {
                        if (value == 1) {
                            $("#" + key + "Nam").prop("checked", true);
                        }
                        if (value == 0) {
                            $("#" + key + "Nu").prop("checked", true);
                        }
                    } else if (key == "DichVu") {
                        if (value == 1) {
                            $("#" + key + "Co").prop("checked", true);
                        }
                        if (value == 0) {
                            $("#" + key + "Khong").prop("checked", true);
                        }
                    } else if (key == "HSVPTCNN") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#HSVPTCNN_" + k).val(v == true ? 1 : 0);
                        });
                    } else if (key == "HSNLD") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#HSNLD_" + k).val(v == true ? 1 : 0);
                        });
                    } else if (key == "HSTTTH") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#HSTTTH_" + k).val(v == true ? 1 : 0);
                        });
                    } else if (key == "LBHNV") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#LBHNV_" + k).val(v == true ? 1 : 0);
                        });
                    } else if (key == "DmTinhThanh") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#DmTinhThanh_" + k).val(v == true ? 1 : 0);
                        });
                    } else if (key == "DmQuanHuyen") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#DmQuanHuyen_" + k).val(v == true ? 1 : 0);
                        });
                    } else if (key == "DmPhuongXa") {
                        $.each(JSON.parse(value), function (k, v) {
                            $("#DmPhuongXa_" + k).val(v == true ? 1 : 0);
                        });
                    } else {
                        $("#" + key).val(value);
                        $(`input[id="${key}"]`).val(value);
                        // if (key == 'Ho') {
                        //     console.log(value);
                        //     console.log($("#" + key).val(value));
                        //     $('input[id="Ho"]').val(value);
                        // }
                    }
                });
                $("input[type='checkbox']").each(function () {
                    if ($(this).val() == '1') {
                        $(this).prop('checked', true);
                    }
                });
                $('form select').trigger("chosen:updated");
                $(".datepicker").each(function () {
                    if ($(this).val()) {
                        $(this).val(moment($(this).val()).format("DD/MM/YYYY"));
                        $(this).daterangepicker({
                            autoUpdateInput: false,
                            showDropdowns: true,
                            singleDatePicker: true,
                            linkedCalendars: false,
                            locale: {
                                format: "DD/MM/YYYY",
                                cancelLabel: "Clear"
                            }
                        });
                    }
                });
                $(".datepicker").on("apply.daterangepicker", function (
                    ev,
                    picker
                ) {
                    $(this).val(
                        picker.startDate.format("DD/MM/YYYY")
                    );
                });
                $(".datepicker").on("show.daterangepicker", function (ev, picker) {
                    if (
                        picker.element.offset().top -
                        $(window).scrollTop() +
                        picker.container.outerHeight() >
                        $(window).height()
                    ) {
                        picker.drops = "up";
                    } else {
                        picker.drops = "down";
                    }
                    picker.move();
                });

                if (data["avatar"]) {
                    $("#picture").attr("src", data["avatar"]);
                } else {
                    $("#picture").attr("src", "/img/avatar-temp.png");
                }
                $("#picture").hide();
                $("#picture").fadeIn(500);
                $('#file-dinh-kem tbody').empty();
                loadTableHistoryHSVN(dataID);
                $.get(route('FileDinhKem', [data['MaNhanVien'], 1]), function (data) {
                    if (data) {
                        data.forEach(function (item, index) {
                            $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                            </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"  
                            onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                        });
                    }
                });
                $(".nav-tabs-bottom li:nth-child(5) a").removeClass("disabled");
                $("#button-upload-avatar").removeClass('disabled');
                const valueTinhTrangHoSo = data["TinhTrangHoSo"];
                if (parseInt(valueTinhTrangHoSo) == 0) {
                    $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
                    $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
                    $('.nav-tabs-bottom li:nth-child(4)').addClass('hide');
                    $('.nav-tabs-bottom li:nth-child(1)').removeClass('hide');
                    $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
                    $('select[name="TinhTrangHoSo"] option[value="0"]').removeAttr('disabled');
                    $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                    $('select[name="TinhTrangHoSo"] option[value="2"]').attr('disabled', 'disabled');
                }
                if ([1, 2].includes(parseInt(valueTinhTrangHoSo))) {
                    $('select[name="TinhTrangHoSo"] option[value="2"]').removeAttr('disabled');
                    $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                    $('select[name="TinhTrangHoSo"] option[value="0"]').attr('disabled', 'disabled');
                    $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
                    $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
                    if (parseInt(valueTinhTrangHoSo) == 1) {
                        $('.nav-tabs-bottom li:nth-child(2) a').tab('show');
                        $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
                        $('.nav-tabs-bottom li:nth-child(2)').removeClass('hide');
                    } else {
                        $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
                        $('.nav-tabs-bottom li:nth-child(3)').removeClass('hide');
                        $('.nav-tabs-bottom li:nth-child(3) a').tab('show');
                    }
                }
                addressEdit("KhaiSinh", data);
                addressEdit("NguyenQuan", data);
                addressEdit("HoKhau", data);
                addressEdit("ThuongTru", data);

                $('#loader').addClass('d-none');
            })
            saveBtnClick(idForm, idModal, dt, 'edit');
        }
    };
    DataTable.ext.buttons.edit = {
        className: "buttons-edit",
        enabled: false,

        text: function (dt) {
            return (
                '<i class="fa fa-edit"></i> ' +
                dt.i18n("buttons.edit", "Edit")
            );
        },

        action: function (e, dt, button, config) {
            const data = dt.rows('.selected').data()[0];
            const dataID = data[Object.keys(data)[0]];
            const idForm = dt.tables().nodes().to$().attr('id');
            const idModal = '#modal-' + idForm;
            $('#loader').removeClass('d-none');
            if (idForm === 'VPLVTCT') {
                $.ajax({
                    url: route("getVPLVTCT", dataID),
                    dataType: "json"
                }).then(data => {
                    editOrCreateVPLVTCT(data, idModal, idForm);
                    $('#loader').addClass('d-none');
                });
            }
            if (idForm === 'historyHSNV') {
                $.ajax({
                    url: route("getHoSoNLDChucDanh", dataID),
                    dataType: "json"
                }).then(data => {
                    editHistoryChucDanh(data, idModal, idForm);
                    $('#loader').addClass('d-none');
                });
            }
            if (idForm == "DmChucDanh-ttdk") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhTTDK(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-cvht") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhCVHT(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-historyCD") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhHistoryCD(data, idModal, idForm);
            }
            if (idForm == "DmChucDanh-tcnn") {
                $('#loader').addClass('d-none');
                let data = new FormData($("#form-" + idForm)[0]);
                addChucDanhTCNN(data, idModal, idForm);
            }
            else {
                $.get(route(idForm + '.index') + '/' + dataID + '/edit', function (data) {
                    $('#' + idForm + '-modal-title').html("Edit " + idForm);
                    $("#saveBtn-" + idForm).val("edit-" + idForm);
                    $(idModal).modal('show');
                    if (idForm == 'HoSoNLD') {
                        var checkBHXH = 0;
                        if (data.SoSoBHXH && data.NgayThoiThamGiaBHXH == null) {
                            checkBHXH = 1;
                        }
                        $('#ThamGiaBHXH').val(checkBHXH);
                    }

                    $.each(data, function (key, value) {
                        if (idForm == 'OptionConfig') {
                            if (key == 'name') {
                                $('#option_name').val(value);
                            }
                            if (key == 'value') {
                                $('#option_value').val(value);
                            }
                        }
                        if (key == "DiaBanHoatDong") {
                            const dataSelect = value ? value.split(", ") : 'Slect';
                            $("#DiaBanHoatDong").val(dataSelect);
                        } else if (key == "GioiTinh") {
                            if (value == 1) {
                                $("#" + key + "Nam").prop("checked", true);
                            }
                            if (value == 0) {
                                $("#" + key + "Nu").prop("checked", true);
                            }
                        } else if (key == "DichVu") {
                            if (value == 1) {
                                $("#" + key + "Co").prop("checked", true);
                            }
                            if (value == 0) {
                                $("#" + key + "Khong").prop("checked", true);
                            }
                        } else if (key == "ThamGiaBHXH") {
                            if (value == 1) {
                                $("#CoTgBHXH").prop("checked", true);
                            }
                            if (value == 0 || value == null) {
                                $("#KhongTgBHXH").prop("checked", true);
                            }
                        } else if (key == "HSVPTCNN") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#HSVPTCNN_" + k).val(v == true ? 1 : 0);
                            });
                        } else if (key == "HSNLD") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#HSNLD_" + k).val(v == true ? 1 : 0);
                            });
                        } else if (key == "HSTTTH") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#HSTTTH_" + k).val(v == true ? 1 : 0);
                            });
                        } else if (key == "LBHNV") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#LBHNV_" + k).val(v == true ? 1 : 0);
                            });
                        } else if (key == "DmTinhThanh") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#DmTinhThanh_" + k).val(v == true ? 1 : 0);
                            });
                        } else if (key == "DmQuanHuyen") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#DmQuanHuyen_" + k).val(v == true ? 1 : 0);
                            });
                        } else if (key == "DmPhuongXa") {
                            $.each(JSON.parse(value), function (k, v) {
                                $("#DmPhuongXa_" + k).val(v == true ? 1 : 0);
                            });
                        } else {
                            $("#" + key).val(value);
                        }
                    });
                    $("input[type='checkbox']").each(function () {
                        if ($(this).val() == '1') {
                            $(this).prop('checked', true);
                        }
                    });
                    $('form select').trigger("chosen:updated");
                    $(".datepicker").each(function () {
                        if ($(this).val()) {
                            $(this).val(moment($(this).val()).format("DD/MM/YYYY"));
                            $(this).daterangepicker({
                                autoUpdateInput: false,
                                showDropdowns: true,
                                singleDatePicker: true,
                                linkedCalendars: false,
                                locale: {
                                    format: "DD/MM/YYYY",
                                    cancelLabel: "Clear"
                                }
                            });
                        }
                    });
                    $(".datepicker").on("apply.daterangepicker", function (
                        ev,
                        picker
                    ) {
                        $(this).val(
                            picker.startDate.format("DD/MM/YYYY")
                        );
                    });
                    $(".datepicker").on("show.daterangepicker", function (ev, picker) {
                        if (
                            picker.element.offset().top -
                            $(window).scrollTop() +
                            picker.container.outerHeight() >
                            $(window).height()
                        ) {
                            picker.drops = "up";
                        } else {
                            picker.drops = "down";
                        }
                        picker.move();
                    });
                    if (idForm == "tcnn") {
                        $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
                        $('.nav-tabs-bottom li:nth-child(2) a').removeClass('disabled');
                        $('.nav-tabs-bottom li:nth-child(3) a').removeClass('disabled');
                        $('#file-dinh-kem tbody').empty();
                        loadTableVPLVTCT(dataID);
                        $.get(route('FileDinhKem', [data['MaTCNN'], 0]), function (data) {
                            if (data) {
                                data.forEach(function (item, index) {
                                    $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                                    </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"
                                    onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                                });
                            }
                        });
                        loadTableChucDanh();
                    }
                    if (idForm == "HoSoNLD") {
                        if (data["avatar"]) {
                            $("#picture").attr("src", data["avatar"]);
                        } else {
                            $("#picture").attr("src", "/img/avatar-temp.png");
                        }
                        $("#picture").hide();
                        $("#picture").fadeIn(500);
                        $('#file-dinh-kem tbody').empty();
                        loadTableHistoryHSVN(dataID);
                        $.get(route('FileDinhKem', [data['MaNhanVien'], 1]), function (data) {
                            if (data) {
                                data.forEach(function (item, index) {
                                    $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                                    </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"
                                    onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                                });
                            }
                        });
                        $(".nav-tabs-bottom li:nth-child(5) a").removeClass("disabled");
                        $("#button-upload-avatar").removeClass('disabled');
                        const valueTinhTrangHoSo = data["TinhTrangHoSo"];
                        if (parseInt(valueTinhTrangHoSo) == 0) {
                            $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
                            $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
                            $('.nav-tabs-bottom li:nth-child(4)').addClass('hide');
                            $('.nav-tabs-bottom li:nth-child(1)').removeClass('hide');
                            $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
                            $('select[name="TinhTrangHoSo"] option[value="0"]').removeAttr('disabled');
                            $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                            $('select[name="TinhTrangHoSo"] option[value="2"]').attr('disabled', 'disabled');
                        }
                        if ([1, 2].includes(parseInt(valueTinhTrangHoSo))) {
                            $('select[name="TinhTrangHoSo"] option[value="2"]').removeAttr('disabled');
                            $('select[name="TinhTrangHoSo"] option[value="1"]').removeAttr('disabled');
                            $('select[name="TinhTrangHoSo"] option[value="0"]').attr('disabled', 'disabled');
                            $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
                            $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
                            if (parseInt(valueTinhTrangHoSo) == 1) {
                                $('.nav-tabs-bottom li:nth-child(2) a').tab('show');
                                $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
                                $('.nav-tabs-bottom li:nth-child(2)').removeClass('hide');
                            } else {
                                $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
                                $('.nav-tabs-bottom li:nth-child(3)').removeClass('hide');
                                $('.nav-tabs-bottom li:nth-child(3) a').tab('show');
                            }
                        }
                        addressEdit("KhaiSinh", data);
                        addressEdit("NguyenQuan", data);
                        addressEdit("HoKhau", data);
                        addressEdit("ThuongTru", data);
                        loadTableChucDanh();
                    }
                    if (idForm == 'HoSoTTThuHo') {
                        $('#daterangerpickerStartEnd').val($('#NopTuThang').val() + " - " + $('#NopDenThang').val());
                        $("#NopTuThang").on("apply.daterangepicker", function (
                            ev,
                            picker
                        ) {
                            $('#daterangerpickerStartEnd').val(picker.startDate.format("DD/MM/YYYY") + " - ");
                            $("#daterangerpickerStartEnd").val($('#NopTuThang').val() + " - " + $('#NopDenThang').val());
                        });
                        $("#NopDenThang").on("apply.daterangepicker", function (
                            ev,
                            picker
                        ) {
                            $('#daterangerpickerStartEnd').val($('#NopTuThang').val() + " - " + $('#NopDenThang').val());
                        });
                        $('#dc-BHXH').val($('#BHXH').val());
                        $('#BHXH').on('keyup paste', function () {
                            $('#dc-BHXH').val($('#BHXH').val());
                        })
                        $('#dc-TTN').val($('#TTN').val());
                        $('#TTN').on('keyup paste', function () {
                            $('#dc-TTN').val($('#TTN').val());
                        })
                        $('#dc-Luong').val($('#Luong').val());
                        $('#Luong').on('keyup paste', function () {
                            $('#dc-Luong').val($('#Luong').val());
                        })
                        $('#dc-BHTN').val($('#BHTN').val());
                        $('#BHTN').on('keyup paste', function () {
                            $('#dc-BHTN').val($('#BHTN').val());
                        })
                        $('#dc-DVP').val($('#DVP').val());
                        $('#DVP').on('keyup paste', function () {
                            $('#dc-DVP').val($('#DVP').val());
                        })
                        $('#dc-TroCapBHXH').val($('#TroCapBHXH').val());
                        $('#TroCapBHXH').on('keyup paste', function () {
                            $('#dc-TroCapBHXH').val($('#TroCapBHXH').val());
                        })
                        $('#dc-PhiNganHang').val($('#PhiNganHang').val());
                        $('#PhiNganHang').on('keyup paste', function () {
                            $('#dc-PhiNganHang').val($('#PhiNganHang').val());
                        })
                        $('#dc-Lai').val($('#Lai').val());
                        $('#Lai').on('keyup paste', function () {
                            $('#dc-Lai').val($('#Lai').val());
                        })
                    }
                    if (idForm == 'DmTinhThanh') {
                        $('#MATT').prop('readonly', true);
                    }
                    if (idForm == 'DmQuanHuyen') {
                        $('#MAQU').prop('readonly', true);
                    }
                    if (idForm == 'DmPhuongXa') {
                        $('#MAPHUONGXA').prop('readonly', true);
                    }
                    $('#loader').addClass('d-none');
                })
                saveBtnClick(idForm, idModal, dt, 'edit');
            }
        }
    };

    if (typeof DataTable.ext.buttons.copyHtml5 !== "undefined") {
        $.extend(DataTable.ext.buttons.copyHtml5, {
            text: function (dt) {
                return (
                    '<i class="fa fa-copy"></i> ' +
                    dt.i18n("buttons.copy", "Copy")
                );
            }
        });
    }

    if (typeof DataTable.ext.buttons.colvis !== "undefined") {
        $.extend(DataTable.ext.buttons.colvis, {
            text: function (dt) {
                return (
                    '<i class="fa fa-eye"></i> ' +
                    dt.i18n("buttons.colvis", "Column visibility")
                );
            }
        });
    }
})(jQuery, jQuery.fn.dataTable);