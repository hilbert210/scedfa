<?php
namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformers extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(User $User)
    {
        return [
            'id'         => $User->id,
            'name'       => $User->name,
            'username'   => $User->username,
            'active'    => $User->active==1 ? 'Activated' : 'Deactivated',
            'HSVPTCNN'    => $User->HSVPTCNN,
            'HSNLD'    => $User->HSNLD,
            'HSTTTH'    => $User->HSTTTH,
            'LBHNV'    => $User->LBHNV,
            'DmTinhThanh' => $User->DmTinhThanh,
            'DmQuanHuyen' => $User->DmQuanHuyen,
            'DmPhuongXa' => $User->DmPhuongXa
        ];
    }
}