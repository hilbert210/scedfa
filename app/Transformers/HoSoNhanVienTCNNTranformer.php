<?php
namespace App\Transformers;

use App\HoSoNhanVienTCNN;
use League\Fractal\TransformerAbstract;

class HoSoNhanVienTCNNTranformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(HoSoNhanVienTCNN $HoSoNhanVienTCNN)
    {
        return [
            'IdHoSoNhanVienTCNN'         =>  $HoSoNhanVienTCNN->IdHoSoNhanVienTCNN,
            'TinhTrangHoSo'         =>  $HoSoNhanVienTCNN->TinhTrangHoSo,
            'Ho'         =>  $HoSoNhanVienTCNN->Ho,
            'Ten'         =>  $HoSoNhanVienTCNN->Ten,
            'ThongBaoNghiViec'         =>  $HoSoNhanVienTCNN->ThongBaoNghiViec,
            'NgayThongBaoNghiViec'         =>  $HoSoNhanVienTCNN->NgayThongBaoNghiViec,
            'TenTiengVietTCNN'         =>  $HoSoNhanVienTCNN->TenTatTCNN,
            'ChucDanh'         =>  $HoSoNhanVienTCNN->ChucDanh,
            'NgaySinh'         =>  $HoSoNhanVienTCNN->NgaySinh,
            'Gender'         =>  $HoSoNhanVienTCNN->Gender,
            'TonGiao'         =>  $HoSoNhanVienTCNN->TonGiao,
            'TENTT'         =>  $HoSoNhanVienTCNN->TENTT,
            'NguyenQuan'         =>  $HoSoNhanVienTCNN->NguyenQuan,
            'TrinhDoChuyenMon'         =>  $HoSoNhanVienTCNN->TrinhDoChuyenMon,
            'DienThoaiNhaRieng'         =>  $HoSoNhanVienTCNN->DienThoaiNhaRieng,
            'SoCMND'         =>  $HoSoNhanVienTCNN->SoCMND,
            'NgayBatDauCongTac' => $HoSoNhanVienTCNN->NgayBatDauCongTac,
            'SoNha_ThuongTru'         =>  $HoSoNhanVienTCNN->SoNha_ThuongTru? $HoSoNhanVienTCNN->SoNha_ThuongTru :$HoSoNhanVienTCNN->SoNha_HoKhau ,
            'DangDoan'         =>  $HoSoNhanVienTCNN->DangDoan? (int) $HoSoNhanVienTCNN->DangDoan == 0 ? 'Đảng Viên' : 'Đoàn Viên' : '',
            'actions'         =>  view( "layouts.hoSoNLD.checkNghiViec", ['HoSoNhanVienTCNN'=>$HoSoNhanVienTCNN])->render(),
        ];
    }
}