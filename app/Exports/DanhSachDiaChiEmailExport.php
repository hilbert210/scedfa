<?php

namespace App\Exports;

use App\HoSoNhanVienTCNN;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class DanhSachDiaChiEmailExport implements FromQuery, WithHeadings, WithCustomStartCell, WithEvents, WithMapping, WithDrawings, ShouldAutoSize
{
    use Exportable;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     */
    public function __construct($parameters)
    {
        $filter = [
            'TinhTrangHoSo' => array_key_exists('TinhTrangHoSo', $parameters) ? $parameters['TinhTrangHoSo'] : [],
            'TinhThanhLamViec' => array_key_exists('TinhThanhLamViec', $parameters) ? $parameters['TinhThanhLamViec'] : null,
            'IdTCNN' => array_key_exists('IdTCNN', $parameters) ? $parameters['IdTCNN'] : null,
            'GioiTinh' => array_key_exists('GioiTinh', $parameters) ? $parameters['GioiTinh'] : null,
            'filterRangeMonth' => $parameters['filterRangeMonth'] ? $parameters['filterRangeMonth'] : null,
        ];
        $this->filter = $filter;
    }
    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(60);
        $drawing->setOffsetX(40);
        $drawing->setCoordinates('A1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:E' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:E' . $HighestRow)
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('A1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('A1:E1')
                    ->setCellValue('A2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('A2:E2')
                    ->setCellValue('A4', 'ĐỊA CHỈ EMAIL CỦA NGƯỜI LAO ĐỘNG VIỆT NAM TẠI CÁC TC, CNNN')->mergeCells('A4:E4')
                    ->setCellValue('A5', 'TẠI MIỀN TRUNG - TÂY NGUYÊN')->mergeCells('A5:E5')->getStyle('A4:E5')->applyFromArray(array(
                        'font' => array(
                            'name'      =>  'Times New Roman',
                            'size'      =>  18,
                            'bold'      =>  true
                        )
                    ));
                $event->sheet->getDelegate()->getStyle('B5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A1:E2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A7:I7')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT');
                for ($i = 8; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 7);
                }

                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('E' . ($HighestRow + 2), 'Đà Nẵng, Ngày         Tháng         Năm 20         ')->mergeCells('E' . ($HighestRow + 2) . ':E' . ($HighestRow + 2))
                    ->setCellValue('E' . ($HighestRow + 3), 'Người Báo Cáo')->mergeCells('E' . ($HighestRow + 3) . ':E' . ($HighestRow + 3));
                $event->sheet->getDelegate()->getStyle('A' . ($HighestRow + 2) . ':E' . ($HighestRow + 3))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                        'italic'    => true
                    )
                ));
            },
        ];
    }

    public function query()
    {
        $IdTCNN = $this->filter['IdTCNN'];
        $TinhTrangHoSo = $this->filter['TinhTrangHoSo'];
        $MaTT_NoiLamViec = $this->filter['TinhThanhLamViec'];
        $CheckGender = is_numeric($this->filter['GioiTinh']) ? true  : false;
        $GioiTinh = $this->filter['GioiTinh'];
        $filterRangeMonth = $this->filter['filterRangeMonth'];
        $HoSoNhanVienTCNN =  HoSoNhanVienTCNN::query()
            ->leftJoin('DmChucDanh', 'DmChucDanh.IdChucDanh', '=', 'HoSoNhanVienTCNN.CVHTChucDanh')
            ->leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN.IdTCNN')
            ->leftJoin('DmTinhThanh', 'DmTinhThanh.MaTT', '=', 'HoSoNhanVienTCNN.MaTT_NoiLamViec')
            ->when($IdTCNN, function ($query, $IdTCNN) {
                $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
            })
            ->when($TinhTrangHoSo, function ($query, $TinhTrangHoSo) {
                $query->where('HoSoNhanVienTCNN.TinhTrangHoSo', '=', $TinhTrangHoSo);
            })
            ->when($CheckGender, function ($query) use ($GioiTinh) {
                $query->where('HoSoNhanVienTCNN.GioiTinh', '=', $GioiTinh);
            })
            ->when($MaTT_NoiLamViec, function ($query) use ($MaTT_NoiLamViec) {
                $query->where('HoSoNhanVienTCNN.MaTT_NoiLamViec', '=', $MaTT_NoiLamViec);
            })
            ->when($filterRangeMonth, function ($query, $filterRangeMonth) {
                list($start, $end) = explode('-', $filterRangeMonth);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:00', strtotime($end));
                $query->whereBetween('HoSoNhanVienTCNN.NgayThamTra', [$start, $end]);
            })
            ->select(
                'HoSoNhanVienTCNN.Ho',
                'HoSoNhanVienTCNN.Ten',
                'HoSoNhanVienTCNN.EMail',
                'DmTCNN.TenTatTCNN'
            )->orderBy('DmTCNN.TenTatTCNN', 'desc');
        return $HoSoNhanVienTCNN;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B7';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Ho"],
            $row["Ten"],
            $row["EMail"],
            $row["TenTatTCNN"],
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'HỌ',
            'TÊN',
            'Email',
            'TCNN',
        ];
    }
}
