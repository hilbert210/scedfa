<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class LuongNhanVienDichVuExportCompany implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection,$filter)
    {
        $this->collection = $collection;
        $this->filter = $filter;
        $this->engMonthTrans = [
            '01'=> 'Jan',
            '02'=> 'Feb',
            '03'=> 'Mar',
            '04' => 'Apr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10'=> 'Oct',
            '11' => 'Nov',
            '12'=> 'Dec'
        ];
        $this->ngayTinhLuong = strtoupper(date('d/m/Y',strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"]))));
        $this->arrDate = explode('/',  $this->ngayTinhLuong);
        $this->NgayTinhLuongEn = strtr($this->arrDate[1], $this->engMonthTrans).'/'.$this->arrDate[2];
        $this->dateStrVn = 'Ngày '.$this->arrDate[0]. ' tháng '.$this->arrDate[1].' năm '.$this->arrDate[2];
        $this->dateStrEn = $this->arrDate[0].' '.strtr($this->arrDate[1], $this->engMonthTrans).' '.$this->arrDate[2];
    }

    public function registerEvents(): array
    {
        return [

            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:U' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)
                    ->setHorizontal('center')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('A4:U' . ($HighestRow+1))->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A9:U' . ($HighestRow+1))
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('A1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG')->mergeCells('A1:G1')
                    ->setCellValue('A2', 'SERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS')->mergeCells('A2:G2')
                    ->setCellValue('A4', 'BẢNG CHI PHÍ LƯƠNG VÀ CÁC KHOẢN THEO LƯƠNG - STATEMENT OF THE PAYROLL, SI, HI, UI, AND SERVICE FEES')->mergeCells('A4:S4')
                    ->setCellValue('A5', strtoupper(date('m/Y',strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"])))).' - '.$this->NgayTinhLuongEn )->mergeCells('A5:S5')
                    ->setCellValue('A6', $this->collection[0]["TCNN"])->mergeCells('A6:S6')
                    ->setCellValue('R7', 'Tỷ giá - Exchange rate:')->mergeCells('R7:T7')
                    ->setCellValue('U7', number_format($this->collection[0]["TyGia"]))
                    ->setCellValue('R8', 'ĐVT - Unit:')->mergeCells('R8:T8')
                    ->setCellValue('U8', 'VND')
                    ->setCellValue('B9', "Họ và tên \nFull name")->mergeCells('B9:B11')
                    ->setCellValue('C9', "Chức danh \nTitle")->mergeCells('C9:C11')
                    ->setCellValue('D9', "Lương theo HĐ lao động \nBasic salary")->mergeCells('D9:E10')
                    ->setCellValue('D11', 'USD')
                    ->setCellValue('E11', 'VND')
                    ->setCellValue('F9', "Phụ cấp \nAllowance")->mergeCells('F9:G10')
                    ->setCellValue('F11', 'USD')
                    ->setCellValue('G11', 'VND')
                    ->setCellValue('H9', "Số ngày công trong tháng \nTotal working days per month")->mergeCells('H9:H11')
                    ->setCellValue('I9', "Số ngày làm việc thực tế \nActual working days")->mergeCells('I9:I11')
                    ->setCellValue('J9', "Số ngày nghỉ hưởng lương \nPaid leave days")->mergeCells('J9:J11')
                    ->setCellValue('K9', "Thu nhập khác \nOther pay or wages")->mergeCells('K9:L10')
                    ->setCellValue('K11', 'USD')
                    ->setCellValue('L11', 'VND')
                    ->setCellValue('M9', "Lương thực tế \nGross salary")->mergeCells('M9:N10')
                    ->setCellValue('M11', 'USD')
                    ->setCellValue('N11', 'VND')
                    ->setCellValue('O9', "Mức lương nộp bảo hiểm \nThe salary level for calculating insurance payment(Exchange rate: 23,150)")->mergeCells('O9:P10')
                    ->setCellValue('O11', 'SI, HI')
                    ->setCellValue('P11', 'UI')
                    ->setCellValue('Q9', "BHXH, BHYT, BHTN (21.5%) \nSI, HI, UI(21.5%)")->mergeCells('Q9:R10')
                    ->setCellValue('Q11', 'SI, HI')
                    ->setCellValue('R11', 'UI')
                    ->setCellValue('S9', "Phí dịch vụ \nService fee(0.0%)")->mergeCells('S9:S11')
                    ->setCellValue('T9', "Sub-Total")->mergeCells('T9:T11')
                    ->setCellValue('U9', "Ghi chú \nNotes")->mergeCells('U9:U11')
                    ->setCellValue('A'.($HighestRow+1),'Tổng cộng - Total')->mergeCells('A'.($HighestRow+1).':B'.($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+1),'=SUM(D12:D'.$HighestRow.')')
                    ->setCellValue('E'.($HighestRow+1),'=SUM(E12:E'.$HighestRow.')')
                    ->setCellValue('F'.($HighestRow+1),'=SUM(F12:F'.$HighestRow.')')
                    ->setCellValue('G'.($HighestRow+1),'=SUM(G12:G'.$HighestRow.')')
                    ->setCellValue('K'.($HighestRow+1),'=SUM(K12:K'.$HighestRow.')')
                    ->setCellValue('L'.($HighestRow+1),'=SUM(L12:L'.$HighestRow.')')
                    ->setCellValue('M'.($HighestRow+1),'=SUM(M12:M'.$HighestRow.')')
                    ->setCellValue('N'.($HighestRow+1),'=SUM(N12:N'.$HighestRow.')')
                    ->setCellValue('O'.($HighestRow+1),'=SUM(O12:O'.$HighestRow.')')
                    ->setCellValue('P'.($HighestRow+1),'=SUM(P12:P'.$HighestRow.')')
                    ->setCellValue('Q'.($HighestRow+1),'=SUM(Q12:Q'.$HighestRow.')')
                    ->setCellValue('R'.($HighestRow+1),'=SUM(R12:R'.$HighestRow.')')
                    ->setCellValue('S'.($HighestRow+1),'=SUM(S12:S'.$HighestRow.')')
                    ->setCellValue('T'.($HighestRow+1),'=SUM(T12:T'.$HighestRow.')')
                    ->setCellValue('B'.($HighestRow+3),'Tổng số tiền phải trả - Total amount:')
                    ->setCellValue('B'.($HighestRow+4),'Bao gồm - Including:')
                    ->setCellValue('C'.($HighestRow+4),'Lương - Salary: ')
                    ->setCellValue('C'.($HighestRow+5),'BHXH, BHYT, BHTN - SI, HI, UI: ')
                    ->setCellValue('C'.($HighestRow+6),'Phí dịch vụ - Service fee: ')
                    ->setCellValue('E'.($HighestRow+3),'=T' . ($HighestRow+1) )
                    ->setCellValue('E'.($HighestRow+4),'=N' . ($HighestRow+1) . '+G' . ($HighestRow+1))
                    ->setCellValue('E'.($HighestRow+5),'=Q' . ($HighestRow+1) . '+R' . ($HighestRow+1))
                    ->setCellValue('E'.($HighestRow+6),'=S' . ($HighestRow+1))
                    ->setCellValue('F'.($HighestRow+3),'VND')
                    ->setCellValue('F'.($HighestRow+4),'VND')
                    ->setCellValue('F'.($HighestRow+5),'VND')
                    ->setCellValue('F'.($HighestRow+6),'VND')
                    ->setCellValue('C'.($HighestRow+13),'Lập bảng - Made by')
                    ->setCellValue('C'.($HighestRow+18),'Đặng Phương Thùy')
                    ->setCellValue('I'.($HighestRow+13),'Kiểm tra - checked by')
                    ->setCellValue('I'.($HighestRow+18),'Dư Tú Quyên')
                    ->setCellValue('R'.($HighestRow+13),'Giám đốc - Director')
                    ->setCellValue('R'.($HighestRow+18),'Trần Hiếu')
                    ->setCellValue('Q'.($HighestRow+9),"Đà Nẵng, $this->dateStrVn \nDa Nang, $this->dateStrEn")->mergeCells('Q'.($HighestRow+9).':R'.($HighestRow+10))
                    ->setCellValue('Q'.($HighestRow+11),"TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG \nSERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS")->mergeCells('Q'.($HighestRow+11).':R'.($HighestRow+12));
                $event->sheet->getDelegate()->getStyle('A1:U10')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow+1).':U'.($HighestRow+18))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A9:U' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B9:B' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('D9:Q' . ($HighestRow+1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('R9:R' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C'.($HighestRow+9) .':R'. ($HighestRow+18))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A9', "STT \nNo.")->mergeCells('A9:A11');
                $event->sheet->getDelegate()->getStyle('B9:U11')->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                for ($i = 12; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 11);
                }

            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

    public function columnFormats(): array
    {
        return [
            'D' => '#,##0',
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
            'P' => '#,##0',
            'O' => '#,##0',
            'Q' => '#,##0',
            'R' => '#,##0',
            'S' => '#,##0',
            'T' => '#,##0',
        ];
    }

     /**
     * @return string
     */
    public function title(): string
    {
        return 'Company';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B12';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        // dd($row);
        $basicSalaryUSD = ((boolean) $row["USD"]) ? $row["Luong"] : 0;
        $basicSalary = ((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"];
        $phuCap = ((boolean) $row["PhuCap_USD"]) ? 0 : $row["PhuCap"] ;
        if ($phuCap == NULL) {
            $phuCap = 0;
        }
        $phuCapUSD = ((boolean) $row["PhuCap_USD"]) ? $row["PhuCap"] : 0;
        if ($phuCapUSD == NULL) {
            $phuCapUSD = 0;
        }
        $thuNhapKhac = ((boolean) $row["ThuNhapKhac_USD"]) ? 0 : $row["ThuNhapKhac"];
        $thuNhapKhacUSD = ((boolean) $row["ThuNhapKhac_USD"]) ? $row["ThuNhapKhac"] : 0;
        if ($thuNhapKhac == NULL) {
            $thuNhapKhac = 0;
        }
        if ($thuNhapKhacUSD == NULL) {
            $thuNhapKhacUSD = 0;
        }
        $totalWorkingDays = 22;
        if($row["SoNgayLamViec"] == NULL) {
            $row["SoNgayLamViec"] = 0;
        }
        if ($row["SoNgayNghiCoLuong"] == NULL) {
            $row["SoNgayNghiCoLuong"] = 0;
        }
        $grossSalary = (($basicSalary + $phuCap)/$totalWorkingDays)*($row["SoNgayLamViec"] + $row["SoNgayNghiCoLuong"]) + $thuNhapKhac;
        $grossSalaryUSD = (($basicSalaryUSD + $phuCapUSD)/$totalWorkingDays)*($row["SoNgayLamViec"] + $row["SoNgayNghiCoLuong"]) + $thuNhapKhacUSD;
        $tygiaInsur = floatval($row["TyGia_Insurance"]);
        if ($tygiaInsur == 0) {
            $tygiaInsur = 23150;
        }
        $level_payment_si_hi = ($basicSalaryUSD + $phuCapUSD)*$tygiaInsur;
        $level_payment_ui = ($basicSalaryUSD + $phuCapUSD)*$tygiaInsur;
        if ($level_payment_si_hi > $row["GoiHanTinhBHXH"]) {
            $level_payment_si_hi = $row["GoiHanTinhBHXH"];
        }
        if ($level_payment_ui > $row["GoiHanTinhBHTN"]) {
            $level_payment_ui = $row["GoiHanTinhBHTN"];
        }
        $si_hi = $level_payment_si_hi*0.205;
        $ui = $level_payment_ui*0.01;
        return [
            $row["Họ"] .' '. $row["Tên"],
            $row["ChucDanh"],
            $basicSalaryUSD,
            $basicSalary,
            $phuCapUSD,
            $phuCap ,
            $totalWorkingDays,
            $row["SoNgayLamViec"],
            $row["SoNgayNghiCoLuong"],
            $thuNhapKhacUSD,
            $thuNhapKhac,
            $grossSalaryUSD,
            $grossSalary,
            $level_payment_si_hi,
            $level_payment_ui,
            $si_hi,
            $ui,
            $row['PhiDichVu'],
            $basicSalary + $si_hi + $ui + $row['PhiDichVu'],//sub total
            $row["GhiChu"]
        ];
    }
}
