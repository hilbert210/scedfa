<form method="POST" enctype="multipart/form-data" id="form-HoSoNLD" class="form-HoSoNLD">
  @csrf
  <div class="modal fade " id="modal-HoSoNLD" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Hồ Sơ Nhân Viên</h5>
          <button type="button" id="button-close-modal" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-4">
              <input type="hidden" class="form-control" name="IdHoSoNhanVienTCNN" id="IdHoSoNhanVienTCNN">
              <div class="row">
                <div class="col-4 text-center">
                  <img id="picture" src="{{asset('img/avatar-temp.png')}}" height="180px" width="150px" class="rounded" alt="Ảnh đại diện">
                  <input type="file" name="avatar" id="upload-avatar" style="display:none;" />
                  <button id="button-upload-avatar" type="button" style="margin-left: 15px;" class="btn btn-primary btn-sm mb-2 mt-1">Chọn
                    ảnh</button>
                </div>
                <div class="col-8">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Mã Hồ Sơ</span>
                    </div>
                    <input type="text" readonly class="form-control" name="MaNhanVien" id="MaNhanVien">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Họ</span>
                    </div>
                    <input type="text" class="form-control" name="Ho" id="Ho">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Tên</span>
                    </div>
                    <input type="text" class="form-control" name="Ten" id="Ten">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Ngày Sinh</span>
                    </div>
                    <input type="text" class="form-control datepicker" name="NgaySinh" id="NgaySinh">

                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Giới Tính</span>
                    </div>
                    <div style="align-self: center; padding-left: 10px;">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="GioiTinhNam" name="GioiTinh" class="custom-control-input" value="1">
                        <label class="custom-control-label" for="GioiTinhNam">Nam</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="GioiTinhNu" name="GioiTinh" class="custom-control-input" value="0">
                        <label class="custom-control-label" for="GioiTinhNu">Nữ</label>
                      </div>
                    </div>
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">SĐT</span>
                    </div>
                    <input type="text" class="form-control" name="DienThoaiNhaRieng" id="DienThoaiNhaRieng">
                  </div>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Email</span>
                    </div>
                    <input type="text" class="form-control" name="EMail" id="EMail">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Quốc Tịch</span>
                </div>
                <select class="form-control" name="IdQuocTich" id="IdQuocTich">
                  <option value=""></option>
                </select>
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Dân Tộc</span>
                </div>
                <select class="form-control" name="IdDanToc" id="IdDanToc">
                  <option value=""></option>
                </select>
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Tôn Giáo </span>
                </div>
                <select class="form-control" name="IdTonGiao" id="IdTonGiao">
                  <option value=""></option>
                </select>

              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Đảng/Đoàn</span>
                </div>
                <select class="form-control" name="DangDoan" id="DangDoan">
                  <option value=""></option>
                  <option value="0">Đảng viên</option>
                  <option value="1">Đoàn viên</option>
                </select>
              </div>

              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Trình Độ Văn Hóa</span>
                </div>
                <input type="text" class="form-control" name="TrinhDoVanHoa" id="TrinhDoVanHoa">
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Trình Độ Chuyên Môn</span>
                </div>
                <input type="text" class="form-control" name="TrinhDoChuyenMon" id="TrinhDoChuyenMon">
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Sở Trường</span>
                </div>
                <input type="text" class="form-control" name="SoTruong" id="SoTruong">
              </div>
            </div>
            <div class="col-4">
              <fieldset>
                <legend>CMND</legend>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">CMND/CCCD</span>
                  </div>
                  <input type="text" class="form-control" name="SoCMND" id="SoCMND">

                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Ngày Cấp</span>
                  </div>
                  <input id="NgayCap" name="NgayCap" type="text" class="form-control datepicker">
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Nơi Cấp</span>
                  </div>
                  <input type="text" class="form-control" name="NoiCap" id="NoiCap">
                </div>
              </fieldset>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Số Sổ BHXH</span>
                </div>
                <input type="text" class="form-control" name="SoSoBHXH" id="SoSoBHXH">
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Mã BHYT</span>
                </div>
                <input type="text" class="form-control" name="MaBHYT" id="MaBHYT">
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Nơi Khám Chữa Bệnh</span>
                </div>
                <input type="text" class="form-control" name="IdBenhVien" id="IdBenhVien">
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox mb-2" style="padding-left: 36px;">
                  <input type="checkbox" class="custom-control-input" id="ThamGiaBHXH" value="">
                  <label class="custom-control-label" for="ThamGiaBHXH">Tham Gia BHXH</label>
                </div>
              </div>
            </div>
          </div>
          <div class="input-group dropdown-selects">
            <div class="input-group-prepend" style="width: 11.3%">
              <span class="input-group-text">Nơi Cấp Khai Sinh</span>
            </div>
            <select class="form-control" style="width: 11rem !IMPORTANT;" name="MaTT_KhaiSinh" id="MaTT_KhaiSinh">
              <option value=""></option>
            </select>
            <select class="form-control ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaQU_KhaiSinh" id="MaQU_KhaiSinh">
              <option value=""></option>
            </select>
            <select class="form-control ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaPhuongXa_KhaiSinh" id="MaPhuongXa_KhaiSinh">
              <option value=""></option>
            </select>
            <input type="text" class="form-control ml-2" name="SoNha_KhaiSinh" id="SoNha_KhaiSinh">
          </div>
          <div class="input-group dropdown-selects">
            <div class="input-group-prepend" style="width: 11.3%">
              <span class="input-group-text">Nguyên Quán</span>
            </div>
            <select class="form-control" style="width: 11rem !IMPORTANT;" name="MaTT_NguyenQuan" id="MaTT_NguyenQuan">
              <option value=""></option>
            </select>
            <select class="form-control ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaQU_NguyenQuan" id="MaQU_NguyenQuan">
              <option value=""></option>
            </select>
            <select class="form-control ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaPhuongXa_NguyenQuan" id="MaPhuongXa_NguyenQuan">
              <option value=""></option>
            </select>
            <input type="text" class="form-control ml-2" name="SoNha_NguyenQuan" id="SoNha_NguyenQuan">
          </div>
          <div class="input-group dropdown-selects">
            <div class="input-group-prepend" style="width: 11.3%">
              <span class="input-group-text">Hộ Khẩu/Thường Trú</span>
            </div>
            <select class="form-control" style="width: 11rem !IMPORTANT;" name="MaTT_HoKhau" id="MaTT_HoKhau">
              <option value=""></option>
            </select>
            <select class="form-control ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaQU_HoKhau" id="MaQU_HoKhau">
              <option value=""></option>
            </select>
            <select class="form-control  ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaPhuongXa_HoKhau" id="MaPhuongXa_HoKhau">
              <option value=""></option>
            </select>
            <input type="text" class="form-control  ml-2" name="SoNha_HoKhau" id="SoNha_HoKhau">
          </div>
          <div class="input-group dropdown-selects">
            <div class="input-group-prepend" style="width: 11.3%">
              <span class="input-group-text">Nơi Tạm Trú</span>
            </div>
            <select class="form-control " style="width: 11rem !IMPORTANT;" name="MaTT_ThuongTru" id="MaTT_ThuongTru">
              <option value=""></option>
            </select>
            <select class="form-control  ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaQU_ThuongTru" id="MaQU_ThuongTru">
              <option value=""></option>
            </select>
            <select class="form-control  ml-2" disabled style="width: 11rem !IMPORTANT;" name="MaPhuongXa_ThuongTru" id="MaPhuongXa_ThuongTru">
              <option value=""></option>
            </select>
            <input type="text" class="form-control ml-2" name="SoNha_ThuongTru" id="SoNha_ThuongTru">
          </div>
          <div class="row">
            <div class="col-4">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">Tình Trạng Hồ Sơ</span>
                </div>
                <select class="form-control" name="TinhTrangHoSo" id="TinhTrangHoSo">
                  <option value="1">Hồ Sơ Đang Lao Động Tại TCNN</option>
                  <option value="2">Hồ Sơ Kết Thúc Hợp Đồng Lao Động</option>
                  <option value="0">Hồ Sơ Xin Việc</option>
                </select>
              </div>
            </div>
          </div>
          <ul class="nav nav-tabs nav-tabs-bottom" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="TTDK-tab" data-toggle="tab" href="#TTDK" role="tab" aria-controls="TTDK" aria-selected="true">Thông Tin Đăng Ký</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="TTCT-tab" data-toggle="tab" href="#TTCT" role="tab" aria-controls="TTCT" aria-selected="false">Thông Tin Công Tác</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="TTNghiViec-tab" data-toggle="tab" href="#TTNghiViec" role="tab" aria-controls="TTNghiViec" aria-selected="false">Thông Tin Nghỉ Việc</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="History-tab" data-toggle="tab" href="#History" role="tab" aria-controls="History" aria-selected="false">Quá Trình Làm Việc</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="Attached_Documents-tab" data-toggle="tab" href="#Attached_Documents" role="tab" aria-controls="Attached_Documents" aria-selected="false">Văn Bản Đính kèm</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="VanBanThamTra-tab" data-toggle="tab" href="#VanBanThamTra" role="tab" aria-controls="VanBanThamTra" aria-selected="false">Văn Bản Thẩm Tra</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent" style="min-height: 11rem">
            <div class="tab-pane fade show active" id="TTDK" role="tabpanel" aria-labelledby="TTDK-tab">
              <div class="row">
                <div class="col-4">
                  <fieldset>
                    <legend>Cơ Quan Đăng Ký</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Cơ Quan Đăng Ký</span>
                      </div>
                      <select class="form-control" name="TTDK_IdTCNN" id="TTDK_IdTCNN">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Chức Danh</span>
                      </div>
                      <select class="form-control" name="TTDK_ChucDanh" id="TTDK_ChucDanh">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <table id="DmChucDanh-ttdk" style="font-size: 12px;" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Chức danh</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="TTCT" role="tabpanel" aria-labelledby="TTCT-tab">
              <div class="row mt-1">
                <div class="col-4">
                  <div class="chucVuHienTai">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tên TC, CNNN</span>
                      </div>
                      <select class="form-control" name="IdTCNN" id="IdTCNN">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Loại Hợp Đồng</span>
                      </div>
                      <select class="form-control" name="LoaiHopDong" id="LoaiHopDong">
                        <option value=""></option>
                        <option value="Thử Việc">Thử Việc</option>
                        <option value="Xác Định Thời Hạn">Xác Định Thời Hạn</option>
                        <option value="Không Xác Định Thời Hạn">Không Xác Định Thời Hạn</option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Thời Gian</span>
                      </div>
                      <input type="text" class="form-control" id="ThoiGianHopDong" name="ThoiGianHopDong" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: smaller">Ngày Bắt Đầu Công Tác</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NgayBatDauCongTac" name="NgayBatDauCongTac" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: smaller">Ngày Tham Gia BHXH</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NgayThamGiaBHXH" name="NgayThamGiaBHXH" value="" />
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="chucVuHienTai">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Chức Danh</span>
                      </div>
                      <select class="form-control" name="CVHT_ChucDanh" id="CVHT_ChucDanh">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <table id="DmChucDanh-cvht" style="font-size: 12px;" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Chức danh</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tỉnh Thành Làm Việc</span>
                      </div>
                      <select class="form-control" name="MaTT_NoiLamViec" id="MaTT_NoiLamViec">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Vùng Dự Án</span>
                      </div>
                      <input id="NoiLamViec" name="NoiLamViec" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="GhiChu_ttct">Ghi Chú: </label>
                      <textarea rows="2" cols="50" class="form-control" name="GhiChu_ttct" id="GhiChu_ttct"></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-4">
                  <div class="update-CD hide">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số VB Thay đổi</span>
                      </div>
                      <input type="text" class="form-control" id="SoVBNghiViec">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ngày Thay Đổi</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NgayThayDoiChucDanh">
                    </div>
                    <div class="text-center">
                      <button id="button-update-CD" type="button" class="btn btn-primary btn-sm mb-2 mt-1">Thay Đổi
                        Chức
                        Vụ</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="History" role="tabpanel" aria-labelledby="History-tab">
              <table id="historyHSNV" style="font-size: 12px;" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Từ ngày</th>
                    <th scope="col">Đến ngày</th>
                    <th scope="col">Chức Danh</th>
                    <th scope="col">Tên TCNN</th>
                    <th scope="col">Tỉnh Thành Làm Việc</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="tab-pane fade" id="TTNghiViec" role="tabpanel" aria-labelledby="TTNghiViec-tab">
              <div class="row">
                <div class="col-4">
                  <fieldset>
                    <legend>Văn Bản Nghỉ Việc</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số QĐ Nghỉ Việc</span>
                      </div>
                      <input type="text" class="form-control" name="ThongBaoNghiViec_So" id="ThongBaoNghiViec_So">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ngày Nghỉ Việc</span>
                      </div>
                      <input id="ThongBaoNghiViec_Ngay" name="ThongBaoNghiViec_Ngay" type="text" class="form-control datepicker">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">Ngày Thôi Tham Gia BHXH</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NgayThoiThamGiaBHXH" name="NgayThoiThamGiaBHXH" value="" />
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="Attached_Documents" role="tabpanel" aria-labelledby="Attached_Documents-tab">
              <input type="file" id="upload-file-dinh-kem" name="upload-file-dinh-kem[]" multiple style="display:none;" />
              <button id="button-upload-file-dinh-kem" type="button" class="btn btn-primary btn-sm mb-2 mt-1">Chọn
                Files Đính Kèm</button>
              <table id="file-dinh-kem" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col" style="width: 5rem">ID</th>
                    <th scope="col">Tên File</th>
                    <th scope="col">action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="tab-pane fade" id="VanBanThamTra" role="tabpanel" aria-labelledby="VanBanThamTra-tab">
              <div class="row">
                <div class="col-4">
                  <fieldset>
                    <legend>Văn Bản Thẩm Tra</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số VB Thẩm Tra</span>
                      </div>
                      <input type="text" class="form-control" name="SoVBThamTra" id="SoVBThamTra">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ngày Thẩm Tra</span>
                      </div>
                      <input id="NgayThamTra" name="NgayThamTra" type="text" class="form-control datepicker">
                    </div>
                  </fieldset>
                </div>
                <div class="col-4">
                  <fieldset>
                    <legend>Văn Bản Cơ Quan An Ninh</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số VB Đến</span>
                      </div>
                      <input type="text" class="form-control" name="SoVBThamTraAnNinh" id="SoVBThamTraAnNinh">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ngày VB Đến</span>
                      </div>
                      <input id="NgayVBThamTraAnNinh" name="NgayVBThamTraAnNinh" type="text" class="form-control datepicker">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Đơn vị</span>
                      </div>
                      <select class="form-control" name="LoaiCQAN" id="LoaiCQAN">
                        <option value=""></option>
                        <option value="Phòng An Ninh Đối Ngoại">Phòng An Ninh Đối Ngoại</option>
                        <option value="Cục An Ninh Đối Ngoại">Cục An Ninh Đối Ngoại</option>
                      </select>
                    </div>
                  </fieldset>
                </div>
                <div class="col-4">
                  <fieldset>
                    <legend>Văn Bản Chấp Thuận</legend>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số VB Chấp Thuận</span>
                      </div>
                      <input type="text" class="form-control" name="SoVBChapThuan" id="SoVBChapThuan">
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ngày Chấp Thuận</span>
                      </div>
                      <input id="NgayVBChapThuan" name="NgayVBChapThuan" type="text" class="form-control datepicker">
                    </div>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="saveBtn-HoSoNLD" class="btn btn-primary-outline">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST" id="form-DmChucDanh-ttdk" class="form-DmChucDanh-ttdk">
  @csrf
  <!--FormAddCD-->
  <div class="modal fade" id="modal-DmChucDanh-ttdk" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 40%" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-title">Cập nhật Chức danh</h5>
          <button type="button" id="button-close-modal-DmChucDanh-ttdk" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Chức Danh</span>
            </div>
            <input type="text" class="form-control" name="ChucDanh" id="ChucDanh">
          </div>
          <span id="cdError-ttdk" class="text-danger"></span>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtnDmChucDanh-ttdk" class="btn btn-primary-outline">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST" id="form-DmChucDanh-cvht" class="form-DmChucDanh-cvht">
  @csrf
  <!--FormAddCD-->
  <div class="modal fade" id="modal-DmChucDanh-cvht" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 40%" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-title">Cập nhật Chức danh</h5>
          <button type="button" id="button-close-modal-DmChucDanh-cvht" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Chức Danh</span>
            </div>
            <input type="text" class="form-control" name="ChucDanh" id="ChucDanh">
          </div>
          <span id="cdError-cvht" class="text-danger"></span>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtnDmChucDanh-cvht" class="btn btn-primary-outline">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST" id="form-historyHSNV" class="form-historyHSNV">
  @csrf
  <div class="modal fade " id="modal-historyHSNV" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 25%" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Chỉnh Sửa Lịch Sử Làm Việc</h5>
          <button type="button" id="button-close-modal-historyChucDanh" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-4">
          <div class="row">
            <input type="hidden" class="form-control" name="Id" id="Id">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">TC, CNNN</span>
              </div>
              <select class="form-control" name="IdTCNN" id="IdTCNN">
                <option value=""></option>
              </select>
              <span id="" class="text-danger"></span>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Chức Danh</span>
              </div>
              <select class="form-control" name="History_ChucDanh" id="History_ChucDanh">
                <option value=""></option>
              </select>
              <span id="" class="text-danger"></span>
            </div>
            <div class="input-group">
              <table id="DmChucDanh-historyCD" style="font-size: 12px;" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Chức danh</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Số Văn Bản Nghỉ Việc</span>
              </div>
              <input id="SoVBNghiViec" name="SoVBNghiViec" type="text" class="form-control">
              <span id="" class="text-danger"></span>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Ngày Thay Đổi</span>
              </div>
              <input id="NgayThayDoiChucDanh" name="NgayThayDoiChucDanh" type="text" class="form-control datepicker">
              <span id="" class="text-danger"></span>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Ngày Kết Thúc</span>
              </div>
              <input id="NgayKetThuc" name="NgayKetThuc" type="text" class="form-control datepicker">
              <span id="" class="text-danger"></span>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Tỉnh Thành Làm Việc</span>
              </div>
              <select class="form-control" name="MaTT_NoiLamViec" id="MaTT_NoiLamViec">
                <option value=""></option>
              </select>
              <span id="" class="text-danger"></span>
            </div>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Vùng Dự Án</span>
              </div>
              <input id="NoiLamViec" name="NoiLamViec" type="text" class="form-control">
              <span id="" class="text-danger"></span>
            </div>
            <div class="form-group">
              <label for="GhiChu_ttct">Ghi Chú: </label>
              <textarea rows="2" cols="50" class="form-control" name="GhiChu" id="GhiChu"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtnHistory" class="btn btn-primary-outline">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
</form>

<form method="POST" id="form-DmChucDanh-historyCD" class="form-DmChucDanh-historyCD">
  @csrf
  <!--FormAddCD-->
  <div class="modal fade" id="modal-DmChucDanh-historyCD" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" style="width: 40%" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modal-title">Cập nhật Chức danh</h5>
          <button type="button" id="button-close-modal-DmChucDanh-historyCD" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Chức Danh</span>
            </div>
            <input type="text" class="form-control" name="ChucDanh" id="ChucDanh">
          </div>
          <span id="cdError-history" class="text-danger"></span>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtnDmChucDanh-historyCD" class="btn btn-primary-outline">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</form>