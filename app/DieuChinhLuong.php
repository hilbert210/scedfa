<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DieuChinhLuong extends Model
{
    protected $guarded  = ['Id'];
    protected $primaryKey = "Id";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'DieuChinhLuong';
}