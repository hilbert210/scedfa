<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Option;

class LuongNhanVienDichVuExportEmployees implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    protected $pitRelief;
    protected $dependencyPitRelief;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection,$filter)
    {
        $this->collection = $collection;
        $this->filter = $filter;
        $this->pitRelief = Option:: getSystemConfig('GIAMTRU_THUE');
        $this->dependencyPitRelief = Option:: getSystemConfig('GIAMTRU_NGUOIPHUTHUOC');
        $dateSal = strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"]));
        $year = date('Y', $dateSal);
        $month = date('m', $dateSal);
        if ($year < 2020) {
            $this->pitRelief = config('scedfa.SysConf.GIAMTRU_OLD');
            $this->dependencyPitRelief = config('scedfa.SysConf.GIAMTRU_PHUTHUOC_OLD');
        } else if($year == 2020){
            if ($month < 7) {
                $this->pitRelief = config('scedfa.SysConf.GIAMTRU_OLD');
                $this->dependencyPitRelief = config('scedfa.SysConf.GIAMTRU_PHUTHUOC_OLD');
            }
        }

        $this->engMonthTrans = [
            '01'=> 'Jan',
            '02'=> 'Feb',
            '03'=> 'Mar',
            '04' => 'Apr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10'=> 'Oct',
            '11' => 'Nov',
            '12'=> 'Dec'
        ];
        $this->ngayTinhLuong = strtoupper(date('d/m/Y',strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"]))));
        $this->arrDate = explode('/',  $this->ngayTinhLuong);
        $this->NgayTinhLuongEn = strtr($this->arrDate[1], $this->engMonthTrans).'/'.$this->arrDate[2];
        $this->dateStrVn = 'Ngày '.$this->arrDate[0]. ' tháng '.$this->arrDate[1].' năm '.$this->arrDate[2];
        $this->dateStrEn = $this->arrDate[0].' '.strtr($this->arrDate[1], $this->engMonthTrans).' '.$this->arrDate[2];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:W' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A9:W' . ($HighestRow + 1))
                    ->applyFromArray($styleArray);

                // title
               $event->sheet->getDelegate()->setCellValue('A1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG')->mergeCells('A1:G1')
                    ->setCellValue('A2', 'SERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS')->mergeCells('A2:G2')
                    ->setCellValue('A4', 'BẢNG CHI PHÍ LƯƠNG VÀ CÁC KHOẢN THEO LƯƠNG - STATEMENT OF THE PAYROLL, SI, HI, UI, AND SERVICE FEES')->mergeCells('A4:S4')
                    ->setCellValue('A5', strtoupper(date('m/Y',strtotime(str_replace('/', '-',$this->collection[0]["Ngày Tính Lương"])))).' - '.$this->NgayTinhLuongEn )->mergeCells('A5:S5')
                    ->setCellValue('A6', $this->collection[0]["TCNN"])->mergeCells('A6:S6')
                    ->setCellValue('T7', 'Tỷ giá - Exchange rate:')->mergeCells('T7:U7')
                    ->setCellValue('W7', number_format($this->collection[0]["TyGia"]))
                    ->setCellValue('T8', 'ĐVT - Unit:')->mergeCells('T8:U8')
                    ->setCellValue('W8', 'VND')
                    ->setCellValue('B9', "Họ và tên \nFull name")->mergeCells('B9:B11')
                    ->setCellValue('C9', "Lương theo HĐ \nBasic salary")->mergeCells('C9:D10')
                    ->setCellValue('C11', 'USD')
                    ->setCellValue('D11', 'VND')
                    ->setCellValue('E9', "Phụ cấp \nAllowances")->mergeCells('E9:F10')
                    ->setCellValue('E11', 'USD')
                    ->setCellValue('F11', 'VND')
                    ->setCellValue('G9', "Số ngày công trong tháng \nTotal working days per month")->mergeCells('G9:G11')
                    ->setCellValue('H9', "Số ngày làm việc thực tế \nActual working days")->mergeCells('H9:H11')
                    ->setCellValue('I9', "Số ngày nghỉ hưởng lương \nPaid leave days")->mergeCells('I9:I11')
                    ->setCellValue('J9', "Thu nhập khác \nOther pay or wages")->mergeCells('J9:K10')
                    ->setCellValue('J11', 'USD')
                    ->setCellValue('K11', 'VND')
                    ->setCellValue('L9', "Lương thực tế \nGross salary")->mergeCells('L9:M10')
                    ->setCellValue('L11', 'USD')
                    ->setCellValue('M11', 'VND')
                    ->setCellValue('N9', "Mức lương nộp bảo hiểm \nThe salary level for calculating insurance payment(Exchange rate: ". number_format($this->collection[0]["TyGia_Insurance"]) .")")->mergeCells('N9:O10')
                    ->setCellValue('N11', 'SI,HI')
                    ->setCellValue('O11', 'UI')
                    ->setCellValue('P9', "Trích nộp BHXH, BHYT, BHTN \nSI, HI, UI(". ($this->collection[0]["TrichNop_BHXH"] + $this->collection[0]["BaoHiemYTe"] + $this->collection[0]["BaoHiemThatNghiep"]) .'%)' )->mergeCells('P9:Q10')
                    ->setCellValue('P11', 'SI,HI')
                    ->setCellValue('Q11', 'UI')
                    ->setCellValue('R9', "Số người phụ thuộc \nNumber of depentdents")->mergeCells('R9:R11')
                    ->setCellValue('S9', "Mức giảm trừ gia cảnh \nAmount not for PIT")->mergeCells('S9:S11')
                    ->setCellValue('T9', "Thu nhập phải nộp thuế \nIncome for PIT")->mergeCells('T9:T11')
                    ->setCellValue('U9', "Thuế TNCN \nPIT")->mergeCells('U9:U11')
                    ->setCellValue('V9', "Phí CK \nWire Transfer Fee")->mergeCells('V9:V11')
                    ->setCellValue('W9', "Lương thực lĩnh \nNet salary")->mergeCells('W9:W11')
                    ->setCellValue('A'.($HighestRow+1),'Total')->mergeCells('A'.($HighestRow+1).':B'.($HighestRow+1))
                    ->setCellValue('C'.($HighestRow+1),'=SUM(C9:C'.$HighestRow.')')
                    ->setCellValue('D'.($HighestRow+1),'=SUM(D9:D'.$HighestRow.')')
                    ->setCellValue('E'.($HighestRow+1),'=SUM(E9:E'.$HighestRow.')')
                    ->setCellValue('F'.($HighestRow+1),'=SUM(F9:F'.$HighestRow.')')
                    ->setCellValue('J'.($HighestRow+1),'=SUM(J9:J'.$HighestRow.')')
                    ->setCellValue('K'.($HighestRow+1),'=SUM(K9:K'.$HighestRow.')')
                    ->setCellValue('L'.($HighestRow+1),'=SUM(L9:L'.$HighestRow.')')
                    ->setCellValue('M'.($HighestRow+1),'=SUM(M9:M'.$HighestRow.')')
                    ->setCellValue('N'.($HighestRow+1),'=SUM(N9:N'.$HighestRow.')')
                    ->setCellValue('O'.($HighestRow+1),'=SUM(O9:O'.$HighestRow.')')
                    ->setCellValue('P'.($HighestRow+1),'=SUM(P9:P'.$HighestRow.')')
                    ->setCellValue('Q'.($HighestRow+1),'=SUM(Q9:Q'.$HighestRow.')')
                    ->setCellValue('R'.($HighestRow+1),'=SUM(R9:R'.$HighestRow.')')
                    ->setCellValue('S'.($HighestRow+1),'=SUM(S9:S'.$HighestRow.')')
                    ->setCellValue('T'.($HighestRow+1),'=SUM(T9:T'.$HighestRow.')')
                    ->setCellValue('U'.($HighestRow+1),'=SUM(U9:U'.$HighestRow.')')
                    ->setCellValue('V'.($HighestRow+1),'=SUM(V9:V'.$HighestRow.')')
                    ->setCellValue('W'.($HighestRow+1),'=SUM(W9:W'.$HighestRow.')')
                    ->setCellValue('B'.($HighestRow+3),'Lương thực nhận - Salary to be paid:')
                    ->setCellValue('B'.($HighestRow+4),'BHXH, BHYT, BHTN - SI, HI, UI (10.5%):')
                    ->setCellValue('B'.($HighestRow+5),'Thuế TNCN - Deduction for PIT:')
                    ->setCellValue('B'.($HighestRow+6),'Phí CK - Transfer fee:')
                    ->setCellValue('B'.($HighestRow+7),'Tổng cộng - Total:')
                    ->setCellValue('D'.($HighestRow+3),'=W' . ($HighestRow+1) )
                    ->setCellValue('D'.($HighestRow+4),'=P' . ($HighestRow+1) . '+Q' . ($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+5),'=U' . ($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+6),'=V' . ($HighestRow+1))
                    ->setCellValue('D'.($HighestRow+7),'=SUM(D' . ($HighestRow+3) . ':D' . ($HighestRow+6) .')')
                    ->setCellValue('E'.($HighestRow+3),'VND')
                    ->setCellValue('E'.($HighestRow+4),'VND')
                    ->setCellValue('E'.($HighestRow+5),'VND')
                    ->setCellValue('E'.($HighestRow+6),'VND')
                    ->setCellValue('C'.($HighestRow+13),'Lập bảng - Made by')
                    ->setCellValue('C'.($HighestRow+18),'Đặng Phương Thùy')
                    ->setCellValue('I'.($HighestRow+13),'Kiểm tra - checked by')
                    ->setCellValue('I'.($HighestRow+18),'Dư Tú Quyên')
                    ->setCellValue('R'.($HighestRow+13),'Giám đốc - Director')
                    ->setCellValue('R'.($HighestRow+18),'Trần Hiếu')
                    ->setCellValue('Q'.($HighestRow+9),"Đà Nẵng, $this->dateStrVn \nDa Nang, $this->dateStrEn")->mergeCells('Q'.($HighestRow+9).':R'.($HighestRow+10))
                    ->setCellValue('Q'.($HighestRow+11),"TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG \nSERVICE CENTRE FOR DA NANG FOREIGN AFFAIRS")->mergeCells('Q'.($HighestRow+11).':R'.($HighestRow+12));
                $event->sheet->getDelegate()->getStyle('A1:W10')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow+1).':U'.($HighestRow+18))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('A9:W' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  11,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B9:C' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C9:U' . ($HighestRow+1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C'.($HighestRow+9) .':R'. ($HighestRow+18))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('B9:W11')->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');;
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A9', "STT \nNo.")->mergeCells('A9:A11');
                for ($i = 12; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 11);
                }
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

     /**
     * @return string
     */
    public function title(): string
    {
        return 'Employees';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B12';
    }

    public function columnFormats(): array
    {
        return [
            'C' => '#,##0',
            'D' => '#,##0',
            'E' => '#,##0',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
            'P' => '#,##0',
            'O' => '#,##0',
            'Q' => '#,##0',
            'R' => '#,##0',
            'S' => '#,##0',
            'T' => '#,##0',
            'U' => '#,##0',
            'V' => '#,##0',
            'W' => '#,##0',
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    private function incomeForPIT($row) {
        //= lương thực nhận + phụ cấp - các khoản bảo hiểm - amout not for PIT (VND)
        // $amt =  ((((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"])
        //     + (((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"])
        //     + ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief)
        //     - ((((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
        //     ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
        //     ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100)))
        //     + (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
        //     ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
        //     ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100)))));
        $amt =  ((((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"])
        + (((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"])
        - ((((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
        ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
        ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100)))
        + (((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
        ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
        ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100)))));
        $amt = $amt - ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief);
        return $amt > 0 ? $amt : 0;
    }

    //private function calPIT($salary, $numOfDependency) {
    private function calPIT($incomeUnderPit) {
        //$incomeUnderPit = $salary - ($this->pitRelief + $this->dependencyPitRelief*$numOfDependency);
        if ($incomeUnderPit == 0) {
            return $pit;
        }
        if ($incomeUnderPit > 80000000) {
           // $pit = 15180000 + 0.35*($incomeUnderPit - 80000000);
            $pit = $incomeUnderPit*0.35 - 9850000;
            return $pit;
        }
        if ($incomeUnderPit > 52000000 && $incomeUnderPit <= 80000000) {
            //$pit = 9750000 + 0.3*($incomeUnderPit  - 52000000);
            $pit = $incomeUnderPit*0.3 - 5850000;
            return $pit;
        }
        if ($incomeUnderPit > 32000000 && $incomeUnderPit <= 52000000) {
            //$pit = 4750000 + 0.25*($incomeUnderPit  - 32000000);
            $pit = $incomeUnderPit*0.25 - 3250000;
            return $pit;
        }
        if ($incomeUnderPit > 18000000 && $incomeUnderPit <= 32000000) {
            //$pit = 1950000 + 0.2*($incomeUnderPit - 18000000);
             $pit = $incomeUnderPit*0.2 - 1650000;
            return $pit;
        }
        if ($incomeUnderPit > 10000000 && $incomeUnderPit <= 18000000) {
            //$pit = 750000 + 0.15*($incomeUnderPit - 10000000);
            $pit = $incomeUnderPit*0.15 - 750000;
            return $pit;
        }
        if ($incomeUnderPit > 5000000 && $incomeUnderPit <= 10000000) {
            //$pit =250000 + 0.1*($incomeUnderPit - 5000000);
            $pit = $incomeUnderPit*0.1 - 250000;
            return $pit;
        }
        if ($incomeUnderPit > 0 && $incomeUnderPit <= 5000000) {
            $pit = 0.05*$incomeUnderPit;
            return $pit;
        }

        return $incomeUnderPit < 0 ? 0 : $incomeUnderPit;
    }

    public function map($row): array
    {
        // $salary = ($row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"];
        // $pit = $this->calPIT($salary, $row["SoNguoiPhuThuoc"]);
        $salaryUSD = ((boolean) $row["USD"]) ? $row["Luong"] : 0;
        $salary = ((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia"]) : $row["Luong"];//basic salary
        $totalWorkingDays = 22;
        $phucapUSD = $row['PhuCap_USD'] ? $row['PhuCap'] : 0;
        if ($phucapUSD == NULL) {
            $phucapUSD = 0;
        }
        $phucap= $row['PhuCap_USD'] ? 0 : $row['PhuCap'];
        if ($phucap == NULL) {
            $phucap = 0;
        }
        $thunhapKhac = $row['ThuNhapKhac_USD'] ? 0 : $row['ThuNhapKhac'];
        //$thunhapKhac = $row['PhuCap_USD'] ? $row['ThuNhapKhac']*$row["TyGia"] : $row['ThuNhapKhac'];
        $thunhapKhacUSD = $row['ThuNhapKhac_USD'] ? $row['ThuNhapKhac'] : 0;
        $amtNotForPIT = ($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief);
        // $incomeForPIT = $this->incomeForPIT($row);
        // $pit = $this->calPIT($incomeForPIT);
        //$vnd_paidWorkingDays = ((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] * $row["TyGia"]) : $row["LuongTheoSoNgayLamViec"];
        $vnd_paidWorkingDays = (($salary +  $phucap )/$totalWorkingDays )*($row["SoNgayLamViec"] + $row["SoNgayNghiCoLuong"]) + $thunhapKhac;
        $usd_paidWorkingDays = ((boolean) $row["USD"]) ? ($row["LuongTheoSoNgayLamViec"] ) : 0;
        $vnd_allowances = ((boolean) $row["USD"]) ? ($row["ThuNhapKhac"] * $row["TyGia"]) : $row["ThuNhapKhac"];
        $si_hi = ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHXH"]) ?
            ($row["GoiHanTinhBHXH"] * (($row["SI_Company"] + $row["HI_Company"])/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * (($row["TrichNop_BHXH"] + $row["BaoHiemYTe"])/100));
        $ui = ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            ($row["GoiHanTinhBHTN"] * ($row["UI_Company"]/100)):
            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) * ($row["BaoHiemThatNghiep"]/100));
        $phi_ck = $row['Phi_CK']? $row['Phi_CK'] : 0;
        $incomeForPIT = $vnd_paidWorkingDays - ($si_hi + $ui) - $amtNotForPIT;
        $pit = $this->calPIT($incomeForPIT);
        //$remainingSalary = round($vnd_paidWorkingDays + $vnd_allowances - $si_hi - $ui - $pit - $phi_ck, 0);
        $netSalary = $vnd_paidWorkingDays - $si_hi - $ui - $pit - $phi_ck;

        return [
            $row["Họ"] .' '. $row["Tên"],
            $salaryUSD ,
            $salary,
            $phucapUSD,
            $phucap,
            $totalWorkingDays,
            $row["SoNgayLamViec"],
            $row["SoNgayNghiCoLuong"],
            $thunhapKhacUSD,
            $thunhapKhac,
            $usd_paidWorkingDays,//L
            $vnd_paidWorkingDays,//M

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"])>$row["GoiHanTinhBHXH"])?
            $row["GoiHanTinhBHXH"] :
            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]),

            ((((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]) > $row["GoiHanTinhBHTN"])?
            $row["GoiHanTinhBHTN"]:
            (((boolean) $row["USD"]) ? ($row["Luong"] * $row["TyGia_Insurance"]) : $row["Luong"]),

            $si_hi,

            $ui,

            $row["SoNguoiPhuThuoc"],

            $amtNotForPIT, //($row["SoNguoiPhuThuoc"] * $this->dependencyPitRelief + $this->pitRelief),
            $incomeForPIT,//T
            $pit,
            $phi_ck,
            $netSalary
            // $remainingSalary && $remainingSalary >=0 ? $remainingSalary : 0
        ];
    }
}
