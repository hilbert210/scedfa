<form method="POST" id="form-DmQuanHuyen" class="form-DmQuanHuyen">
    @csrf
    <div class="modal fade " id="modal-DmQuanHuyen" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cập nhật Quận Huyện</h5>
                    <button type="button" id="button-close-modal" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" name="inputMAQU" id="inputMAQU">Mã quận huyện</span>
                                    </div>
                                    <input type="text" class="form-control" name="MAQU" id="MAQU">
                                </div>
                                <div class="input-group dropdown-selects">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tên tỉnh thành</span>
                                    </div>
                                    <select class="form-control" name="MATT_QU" id="MATT_QU">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tên quận huyện</span>
                                    </div>
                                    <input type="text" class="form-control" name="TENQUAN" id="TENQUAN">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Id quận huyện</span>
                                    </div>
                                    <input type="text" class="form-control" name="IdQuan" id="IdQuan">
                                </div>
                            </div>
                            <div class="col-6">
                                <fieldset style="text-align: justify;">
                                    <legend>Hướng dẫn cập nhật Quận huyện</legend>
                                    <h6>- Mã quận huyện:
                                        <ul>
                                            <li>Mã quận huyện <b>KHÔNG</b> được trùng.</li>
                                            <li>Kiểm tra mã quận huyện cuối cùng ứng với mã tỉnh thành trước khi thêm tỉnh thành mới.</li>
                                            <li>Bao gồm 5 chữ số, 3 chữ số đầu là mã tỉnh thành, 2 chữ số cuối tăng dần từ 01 (Ví dụ: mã tỉnh thành là 101 thì mã quận bắt đầu từ 10101, mã tỉnh thành là 203 thì mã quận huyện bắt đầu từ 20301).</li>
                                        </ul>
                                        <h6>- Id quận huyện: Hai chữ số cuối trong mã quận huyện.</h6>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="saveBtn-DmQuanHuyen" class="btn btn-primary-outline">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</form>