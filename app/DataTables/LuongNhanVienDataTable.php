<?php

namespace App\DataTables;

use App\LuongNhanVien;
use App\DieuChinhLuong;
use App\Transformers\NhanVienTransformer;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;

class LuongNhanVienDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())->setTransformer(new NhanVienTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $IdTCNN =  $this->IdTCNN;
        $IdHoSoNhanVienTCNN =  $this->IdHoSoNhanVienTCNN;
        $month =  $this->request()->get('month');
        $operator =  $this->request()->get('operator');
        $luong =  $this->request()->get('luong');
        $usd =  $this->request()->get('usd');
        $tenNhanVien =  $this->request()->get('tenNhanVien');
        $doiTuong = $this->request()->get('DoiTuong');
        $tochuc = $this->request()->get('ToChuc');
        $pa = $this->request()->get('PhuongAn');
        $thang = (int)$this->request()->get('filterMonth');
        $nam = (int)$this->request()->get('filterYear');
        $query =  LuongNhanVien::leftJoin('HoSoNhanVienTCNN', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN')
        ->leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN.IdTCNN')
        ->leftJoin('DmChucDanh', 'HoSoNhanVienTCNN.CVHT_ChucDanh', '=', 'DmChucDanh.IdChucDanh')
        ->leftJoin('DieuChinhLuong', function($join) {
            $join->on('DieuChinhLuong.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN');
            $join->on('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', '=', 'DieuChinhLuong.NgayDieuChinh');
        })
        ->leftJoin('DmTinhThanh', 'HoSoNhanVienTCNN.MaTT', '=', 'DmTinhThanh.MATT')
        ->leftJoin('DmQuocTich', 'HoSoNhanVienTCNN.IdQuocTich', '=', 'DmQuocTich.IdQuocTich')
        ->leftJoin('DmTinhThanh as Tinh_KCB', 'HoSoNhanVienTCNN.TinhThanh_KCB', '=', 'Tinh_KCB.MATT')
        ->leftJoin('DmDanToc', 'HoSoNhanVienTCNN.IdDanToc', '=', 'DmDanToc.IdDanToc')
        ->leftJoin('DmBenhVien', 'HoSoNhanVienTCNN.IdBenhVien', '=', 'DmBenhVien.Id')
        ->when($tenNhanVien, function ($query, $tenNhanVien){
            $query->where('HoSoNhanVienTCNN.Ten', 'like', $tenNhanVien);
        })
        ->when($doiTuong, function($query, $doiTuong){
            $query->where('HoSoNhanVienTCNN_LUONG_BH.DoiTuong', $doiTuong);
        })
        ->when($tochuc, function($query, $tochuc) {
            $query->where('DmTCNN.IdTCNN', $tochuc);
        })
        ->when($pa, function($query, $pa) {
            $query->where('HoSoNhanVienTCNN_LUONG_BH.MaPA', $pa);
        })
        ->when($IdTCNN, function ($query, $IdTCNN) {
                $query->where('HoSoNhanVienTCNN.IdTCNN', '=', $IdTCNN);
            })
            ->when($IdHoSoNhanVienTCNN, function ($query, $IdHoSoNhanVienTCNN) {
                $query->where('HoSoNhanVienTCNN_LUONG_BH.IdHoSoNhanVienTCNN', '=', $IdHoSoNhanVienTCNN);
            });
        if ($thang) {
            $chonnam = $nam ? $nam : now()->year;
            $query->where(function($q) use ($thang, $chonnam) {
                // $q->whereYear('HoSoNhanVienTCNN_LUONG_BH.NgayThucTe', '=', $chonnam);
                // $q->whereMonth('HoSoNhanVienTCNN_LUONG_BH.NgayThucTe', '=', $thang);
                $q->whereYear('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', '=', $chonnam);
                $q->whereMonth('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', '=', $thang);
            } );
        }        
        if (!is_null($IdHoSoNhanVienTCNN)) {
            $query->when($month, function ($query, $month) {
                list($start, $end) = explode('-', $month);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:59', strtotime($end));
                $query->whereBetween('NgayTinhLuong', [$start, $end]);
            });
        } else {
            $query->when($month, function ($query, $month) {
                list($start, $end) = explode('-', $month);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:59', strtotime($end));
                $query->whereBetween('NgayTinhLuong', [$start, $end]);
            }, function ($query) {
                $date = $query->max('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong');
                $start = date('Y-m-01 00:00:00', strtotime($date));
                $end  = date('Y-m-t 23:59:59', strtotime($date));
                $query->whereBetween('HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong', [$start, $end]);
            });
        }
        $query->when($luong,function ($query, $luong) use ($operator){
            $query->whereRaw('(HoSoNhanVienTCNN_LUONG_BH.LuongMoi '. $operator .' CASE WHEN HoSoNhanVienTCNN_LUONG_BH.USD = 1 THEN ?/HoSoNhanVienTCNN_LUONG_BH.TyGia ElSE ? END)', [$luong, $luong]);
        })->when($usd,function ($query, $usd){
            $query->where('HoSoNhanVienTCNN_LUONG_BH.USD', '=' , $usd);
        });
        $query->select(
            'HoSoNhanVienTCNN_LUONG_BH.IdLuong',
            'HoSoNhanVienTCNN_LUONG_BH.NgayTinhLuong',
            'HoSoNhanVienTCNN_LUONG_BH.LuongCu',
            'HoSoNhanVienTCNN_LUONG_BH.LuongMoi',
            'HoSoNhanVienTCNN_LUONG_BH.USD',
            'HoSoNhanVienTCNN_LUONG_BH.NgayHieuLuc',
            'HoSoNhanVienTCNN_LUONG_BH.NgayThucTe',
            'HoSoNhanVienTCNN_LUONG_BH.GhiChu',
            'HoSoNhanVienTCNN_LUONG_BH.GoiHanTinhBHTN',
            'HoSoNhanVienTCNN_LUONG_BH.GoiHanTinhBHXH',
            'HoSoNhanVienTCNN_LUONG_BH.TrichNop_BHXH',
            'HoSoNhanVienTCNN_LUONG_BH.BaoHiemYTe',
            'HoSoNhanVienTCNN_LUONG_BH.ThuNhapKhac',
            'HoSoNhanVienTCNN_LUONG_BH.PhuCapDongBH',
            'HoSoNhanVienTCNN_LUONG_BH.TyGia_Insurance',
            'HoSoNhanVienTCNN_LUONG_BH.TyGia',
            'HoSoNhanVienTCNN_LUONG_BH.SoNgayLamViec',
            'HoSoNhanVienTCNN_LUONG_BH.SoNgayNghiCoLuong',
            'HoSoNhanVienTCNN_LUONG_BH.LuongTheoSoNgayLamViec',
            'HoSoNhanVienTCNN_LUONG_BH.MaPA',
            'HoSoNhanVienTCNN_LUONG_BH.PhiDichVu',
            'HoSoNhanVienTCNN_LUONG_BH.SI_Company',
            'HoSoNhanVienTCNN_LUONG_BH.HI_Company',
            'HoSoNhanVienTCNN_LUONG_BH.UI_Company',
            'HoSoNhanVienTCNN_LUONG_BH.PhanTram_CSD',
            'HoSoNhanVienTCNN_LUONG_BH.PhanTram_NLD',
            'HoSoNhanVienTCNN_LUONG_BH.SoNguoiPhuThuoc',
            'HoSoNhanVienTCNN_LUONG_BH.BaoHiemThatNghiep',
            'HoSoNhanVienTCNN_LUONG_BH.Ngay_KT_HL',
            'HoSoNhanVienTCNN_LUONG_BH.PhuCap_USD',
            'HoSoNhanVienTCNN_LUONG_BH.Phi_CK',
            'HoSoNhanVienTCNN.Ho',
            'HoSoNhanVienTCNN.Ten',
            'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN',
            'DmChucDanh.ChucDanh',
            'DmTCNN.TenTiengVietTCNN',
            'DmTCNN.TenTiengAnhTCNN',
            'DmTCNN.TenTatTCNN',
            'DmTCNN.DichVu',
            'HoSoNhanVienTCNN_LUONG_BH.DieuChinhLuong',
            'HoSoNhanVienTCNN.SoSoBHXH',
            'HoSoNhanVienTCNN.SoCMND',
            'HoSoNhanVienTCNN.NgaySinh',
            DB::raw("CASE WHEN HoSoNhanVienTCNN.GioiTinh = 1 THEN N'Nam' WHEN HoSoNhanVienTCNN.GioiTinh = 0 THEN N'Nữ' ELSE '' END AS GioiTinh "),
            'HoSoNhanVienTCNN.NoiLamViec',
            'HoSoNhanVienTCNN_LUONG_BH.HeSo',
            'HoSoNhanVienTCNN_LUONG_BH.TinhLai',
            'HoSoNhanVienTCNN.DienThoaiNhaRieng',
            'HoSoNhanVienTCNN_LUONG_BH.PhuCapNghe',
            'HoSoNhanVienTCNN_LUONG_BH.PhuCapVuotKhung',
            'HoSoNhanVienTCNN_LUONG_BH.DaCoSo',
            'HoSoNhanVienTCNN.SoNha_HoKhau',
            'DmTinhThanh.MAVUNG',
            'DmChucDanh.ChucDanh',
            'DmQuocTich.QuocTich',
            'DmDanToc.TenDanToc',
            'Tinh_KCB.TENTT',
            'DmBenhVien.TenBenhVien',
            'HoSoNhanVienTCNN.SoNha_KhaiSinh',
            'HoSoNhanVienTCNN_LUONG_BH.PhuongThucDong',
            'HoSoNhanVienTCNN_LUONG_BH.TyLeDong',
            'HoSoNhanVienTCNN_LUONG_BH.HeSoCV',
            'HoSoNhanVienTCNN.LoaiHopDong',
            'HoSoNhanVienTCNN.MaBHYT',
            'HoSoNhanVienTCNN.ThoiGianHopDong',
            'HoSoNhanVienTCNN.SoVBChapThuan',
            'HoSoNhanVienTCNN.NgayVBChapThuan'
        );
        // die(var_dump($query->get()));
        return $this->applyScopes($query->get()->unique('IdHoSoNhanVienTCNN'));
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['export', 'reload', 'detail_employee'];
        if(Gate::allows('LuongNhanVien_delete')){
            array_unshift($buttons,'delete');
        }
        if(Gate::allows('LuongNhanVien_edit')){
            array_unshift($buttons,'edit_salary');
        }
        if(Gate::allows('LuongNhanVien_create')){
            array_unshift($buttons,'create_salary');
            array_unshift($buttons,'edit_salary_1');
            array_unshift($buttons,'create_salary_all');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(
                '',
                'data.month = $("#filterRangeMonth").val(); data.operator = $("#operator").val(); data.luong = $("#luong").val(); data.usd = $("#USD").val(); data.tenNhanVien = $("#filterTenNhanVien").val();data.DoiTuong = $("#filterDoiTuong").val();data.ToChuc = $("#filterToChuc").val();data.PhuongAn = $("#filterPA").val(); data.filterMonth = $("#filterMonth").val(); data.filterYear = $("#filterYear").val();'
            )
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => true,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'desc']],
                'buttons'      => $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $NgayTinhLuong  = Column::make('NgayTinhLuong')
            ->title('Ngày Tính Lương')
            ->searchable(false)
            ->orderable(true)
            ->render('function(e){
            if(e){
                return moment(e).format("DD/MM/YYYY");   
            }
            return e;
        }')
            ->exportable(true)
            ->printable(true);
        return  [
            ["data" => "IdLuong", 'title' => 'ID'],
            $NgayTinhLuong,
            ["data" => "Ho", 'title' => 'Họ'],
            ["data" => "Ten", 'title' => 'Tên'],
            ["data" => "TenTiengVietTCNN", 'title' => 'TCNN'],
            ["data" => "LuongMoi", 'title' => 'Lương'],
            ["data" => "GhiChu", 'title' => 'Ghi Chú'],
            ["data" => "MaPA", 'title' => 'Phương Án'],
            ["data" => "PhuCapDongBH", 'title' => 'Phụ Cấp'],
            ["data" => "ChucDanh", 'title' => 'ChucDanh', 'className' => 'hide'],
            ["data" => "TyGia", 'title' => 'TyGia', 'className' => 'hide'],
            ["data" => "TyGia_Insurance", 'title' => 'TyGia_Insurance', 'className' => 'hide'],
            ["data" => "USD", 'title' => 'USD', 'className' => 'hide'],
            ["data" => "SoNgayLamViec", 'title' => 'SoNgayLamViec', 'className' => 'hide'],
            ["data" => "SoNgayNghiCoLuong", 'title' => 'SoNgayNghiCoLuong', 'className' => 'hide'],
            ["data" => "LuongTheoSoNgayLamViec", 'title' => 'LuongTheoSoNgayLamViec', 'className' => 'hide'],
            ["data" => "PhiDichVu", 'title' => 'PhiDichVu', 'className' => 'hide'],
            ["data" => "PhanTram_CSD", 'title' => 'PhanTram_CSD', 'className' => 'hide'],
            ["data" => "PhanTram_NLD", 'title' => 'PhanTram_NLD', 'className' => 'hide'],
            ["data" => "SI_Company", 'title' => 'SI_Company', 'className' => 'hide'],
            ["data" => "HI_Company", 'title' => 'HI_Company', 'className' => 'hide'],
            ["data" => "UI_Company", 'title' => 'UI_Company', 'className' => 'hide'],
            ["data" => "Luong", 'title' => 'Luong', 'className' => 'hide'],
            ["data" => "ThuNhapKhac", 'title' => 'ThuNhapKhac', 'className' => 'hide'],
            ["data" => "GoiHanTinhBHXH", 'title' => 'GoiHanTinhBHXH', 'className' => 'hide'],
            ["data" => "GoiHanTinhBHTN", 'title' => 'GoiHanTinhBHTN', 'className' => 'hide'],
            ["data" => "GhiChu", 'title' => 'GhiChu', 'className' => 'hide'],
            ["data" => "TenTatTCNN", 'title' => 'TenTatTCNN', 'className' => 'hide'],
            ["data" => "SoNguoiPhuThuoc", 'title' => 'SoNguoiPhuThuoc', 'className' => 'hide'],
            ["data" => "PhuCap", 'title' => 'PhuCap', 'className' => 'hide'],
            ["data" => "TrichNop_BHXH", 'title' => 'TrichNop_BHXH', 'className' => 'hide'],
            ["data" => "BaoHiemYTe", 'title' => 'BaoHiemYTe', 'className' => 'hide'],
            ["data" => "BaoHiemThatNghiep", 'title' => 'BaoHiemThatNghiep', 'className' => 'hide'],
            ["data" => "IdHoSoNhanVienTCNN", 'title' => 'IdHoSoNhanVienTCNN', 'className' => 'hide'],
            ["data" => "NgayTinhLuong", 'title' =>'NgayTinhLuong', 'className' => 'hide'],
            ["data" => "DieuChinhLuong", 'title' => 'DieuChinhLuong', 'className' => 'hide'],
            ["data" => "SoSoBHXH", 'title' => 'SoSoBHXH', 'className' => 'hide'],
            ["data" => "SoCMND", 'title' => 'SoCMND', 'className' => 'hide'],
            ["data" => "NgaySinh", 'title' => 'NgaySinh', 'className' => 'hide'],
            ["data" => "GioiTinh", 'title' => 'GioiTinh', 'className' => 'hide'],
            ["data" => "NoiLamViec", 'title' => 'NoiLamViec', 'className' => 'hide'],
            ["data" => "HeSo", 'title' => 'HeSo', 'className' => 'hide'],
            ["data" => "TinhLai", 'title' => 'TinhLai', 'className' => 'hide'],
            ["data" => "DienThoaiNhaRieng", 'title' => 'DienThoaiNhaRieng', 'className' => 'hide'],
            ["data" => "PhuCapVuotKhung", 'title' => 'PhuCapVuotKhung', 'className' => 'hide'],
            ["data" => "PhuCapNghe", 'title' => 'PhuCapNghe', 'className' => 'hide'],
            ["data" => "DaCoSo", 'title' => 'DaCoSo', 'className' => 'hide'],
            ["data" => "SoNha_HoKhau", 'title' => 'SoNha_HoKhau', 'className' => 'hide'],
            ["data" => "MAVUNG", 'title' => 'MAVUNG', 'className' => 'hide'],
            ["data" => "ChucDanh", 'title' => 'ChucDanh', 'className' => 'hide'],
            ["data" => "QuocTich", 'title' => 'QuocTich', 'className' => 'hide'],
            ["data" => "TenDanToc", 'title' => 'TenDanToc', 'className' => 'hide'],
            ["data" => "TENTT", 'title' => 'TENTT', 'className' => 'hide'],
            ["data" => "TenBenhVien", 'title' => 'TenBenhVien', 'className' => 'hide'],
            ["data" => "SoNha_KhaiSinh", 'title' => 'SoNha_KhaiSinh', 'className' => 'hide'],
            ["data" => "SoVBChapThuan", 'title' => 'SoVBChapThuan', 'className' => 'hide'],
            ["data" => "NgayVBChapThuan", 'title' => 'NgayVBChapThuan', 'className' => 'hide'],
            ["data" => "PhuongThucDong", 'title' => 'PhuongThucDong', 'className' => 'hide'],
            ["data" => "TyLeDong", 'title' => 'TyLeDong', 'className' => 'hide'],
            ["data" => "HeSoCV", 'title' => 'HeSoCV', 'className' => 'hide'],
            ["data" => "LoaiHopDong", 'title' => 'LoaiHopDong', 'className' => 'hide'],
            ["data" => "ThoiGianHopDong", 'title' => 'ThoiGianHopDong', 'className' => 'hide'],
            ["data" => "MaBHYT", 'title' => 'MaBHYT', 'className' => 'hide'],
            ["data" => "PhuCap_USD", 'title' => 'PhuCap_USD', 'className' => 'hide'],
            ["data" => "ThuNhapKhac_USD", 'title' => 'ThuNhapKhac_USD', 'className' => 'hide'],
            ["data" => "Phi_CK", 'title' => 'Phi_CK', 'className' => 'hide']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'LuongNhanVien_' . date('YmdHis');
    }
}