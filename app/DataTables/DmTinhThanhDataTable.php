<?php

namespace App\DataTables;

use App\DmTinhThanh;
use App\User;
use Yajra\DataTables\Services\DataTable;
use App\Transformers\DmTinhThanhTransformer;
use Illuminate\Support\Facades\Gate;

class DmTinhThanhDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())->setTransformer(new DmTinhThanhTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $query = DmTinhThanh::select('DmTinhThanh.MATT', 'DmTinhThanh.TENTT', 'DmTinhThanh.MAVUNG', 'DmTinhThanh.IdTinhThanh')->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['reload'];
        if(Gate::allows('DmTinhThanh_edit')){
            array_unshift($buttons,'edit');
        }
        if(Gate::allows('DmTinhThanh_create')){
            array_unshift($buttons,'create');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => false,
                'responsive' => false,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'asc']],
                'buttons'      =>  $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ["data" => "MATT", 'title' => 'Mã Tỉnh thành'],
            ["data" => "TENTT", 'title' => 'Tên Tỉnh thành'],
            ["data" => "MAVUNG", 'title' => 'Mã vùng'],
            ["data" => "IdTinhThanh", 'title' => 'Id Tỉnh thành']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DmTinhThanh_' . date('YmdHis');
    }
}
