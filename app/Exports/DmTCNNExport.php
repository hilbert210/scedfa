<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class DmTCNNExport implements FromCollection, WithHeadings, WithCustomStartCell, WithEvents, WithMapping, WithDrawings
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    /**
     * DmTCNNExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection,$parameters)
    {
        $this->collection = $collection;
        $filter = [
            // 'Search' =>  $parameters['search']['value'],
            // 'ThoiSuDung' => $parameters['columns'][23]['search']['value'],
            // 'DiaBanHoatDong' => $parameters['columns'][7]['search']['value'],
            // 'ToChuc' => $parameters['columns'][5]['search']['value'],
            // 'GiaHan' => $parameters['columns'][24]['search']['value'],
            // 'DateRange' => $parameters['range_date'],
        ];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(60);
        $drawing->setOffsetX(40);
        $drawing->setCoordinates('F1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:P' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:P' . $HighestRow)
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('F1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('F1:H1')
                    ->setCellValue('F2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('F2:H2')
                    ->setCellValue('F4', 'DANH SÁCH CÁC TỔ CHỨC, CÁ NHÂN NƯỚC NGOÀI')->mergeCells('F4:H4')
                    ->setCellValue('F5', 'TẠI MIỀN TRUNG - TÂY NGUYÊN')->mergeCells('F5:H5')->getStyle('F4:H5')->applyFromArray(array(
                        'font' => array(
                            'name'      =>  'Times New Roman',
                            'size'      =>  18,
                            'bold'      =>  true
                        )
                    ));
                $event->sheet->getDelegate()->getStyle('B1:B' . $HighestRow)
                    ->getAlignment()->setWrapText(true)->setHorizontal('left');
                $event->sheet->getDelegate()->getStyle('I1:J' . $HighestRow)
                    ->getAlignment()->setWrapText(true)->setHorizontal('left');
                $event->sheet->getDelegate()->getStyle('B5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('F1:H2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A7:P7')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $columns = ['A' => 7, 'B' => 35, 'G' => 40, 'H' => 40, 'I' => 40];
                foreach ($columns as $key => $value) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setWidth($value);
                };

                $columnAutoSize = ['C', 'D','E','F','J','K','L', 'M', 'N', 'O', 'P'];
                foreach ($columnAutoSize as $key ) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setAutoSize(true);
                };
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT');
                for ($i = 8; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 7);
                }

                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('I' . ($HighestRow + 2), 'Đà Nẵng, Ngày         Tháng         Năm 20         ')->mergeCells('I' . ($HighestRow + 2) . ':K' . ($HighestRow + 2))
                    ->setCellValue('I' . ($HighestRow + 3), 'Người Báo Cáo')->mergeCells('I' . ($HighestRow + 3) . ':O' . ($HighestRow + 3));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow + 2) .':O'.($HighestRow + 3))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                        'italic'    => true
                    )
                ));
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B7';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Tên TC, CNNN"],
            $row["Tên Viết Tắt"],
            $row["Quốc Tịch"],
            $row["Loại Hình TC,CN NN"],
            $row["Loại Giấy Phép"],
            $row["Địa Bàn Hoạt Động"],
            $row["Lĩnh Vực Hoạt Động"],
            $row["Địa chỉ TC, CNNN"],
            $row["Người Đại Diện: "],
            $row["Chuc Danh"],
            $row["SĐT: "],
            $row['Họ Tên Admin'],
            $row['Số ĐT Admin'],
            $row['Email Admin']
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Tên TCNN',
            'Tên Viết Tắt',
            'Quốc Tịch',
            'Loại Hình TC,CN NN',
            'Loại Giấy Phép',
            'Địa bàn hoạt động',
            'Lĩnh vực hoạt động',
            'Địa chỉ liên lạc',
            'Trưởng đại diện',
            'Chức danh TĐD',
            'Số điện thoại TĐD',
            'Họ Tên Admin',
            'Số ĐT Admin',
            'Email Admin'
        ];
    }
}