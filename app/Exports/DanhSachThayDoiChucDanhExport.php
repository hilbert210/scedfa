<?php

namespace App\Exports;

use App\HoSoNLD_ChucDanh;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class DanhSachThayDoiChucDanhExport implements FromQuery, WithHeadings, WithCustomStartCell, WithEvents, WithDrawings, WithMapping
{
    use Exportable;

    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     */
    public function __construct($parameters)
    {
        $filter = [
            'filterRangeMonth' =>  $parameters['filterRangeMonth'],
        ];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(60);
        $drawing->setOffsetX(40);
        $drawing->setCoordinates('C1');
        return $drawing;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                // text center
                $event->sheet->getDelegate()->getStyle('A1:I' . ($HighestRow + 3))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:I' . $HighestRow)
                    ->applyFromArray($styleArray);
                // title
                $event->sheet->getDelegate()->setCellValue('C1', 'TRUNG TÂM PHỤC VỤ ĐỐI NGOẠI ĐÀ NẴNG (SCEDFA)')->mergeCells('C1:F1')
                ->setCellValue('C2', 'PHÒNG QUẢN LÝ LAO ĐỘNG')->mergeCells('C2:F2')
                ->setCellValue('C4', 'DANH SÁCH THAY ĐỔI CHỨC DANH NHÂN VIÊN CÁC TC, CNNN ')->mergeCells('C4:F4')
                ->setCellValue('C5', 'TẠI MIỀN TRUNG - TÂY NGUYÊN')->mergeCells('C5:F5')->getStyle('C4:F5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  18,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B5')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('C1:F2')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                // header table set style
                $event->sheet->getDelegate()->getStyle('A7:I7')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'bold'      =>  true
                    )
                ));
                $columns = ['A' => 7, 'B' => 35, 'C' => 45, 'D' => 26, 'E' => 25,'F' => 25,'G' => 25,'H' => 25,'I' => 30];
                foreach ($columns as $key => $value) {
                    $event->sheet->getDelegate()->getColumnDimension($key)->setWidth($value);
                };

                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT');
                for ($i = 8; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 7);
                }

                // footer ngày tháng năm
                $event->sheet->getDelegate()->setCellValue('I' . ($HighestRow + 2), 'Đà Nẵng, Ngày         Tháng         Năm 20         ')->mergeCells('I' . ($HighestRow + 2) . ':I' . ($HighestRow + 2))
                    ->setCellValue('I' . ($HighestRow + 3), 'Người Báo Cáo')->mergeCells('I' . ($HighestRow + 3) . ':I' . ($HighestRow + 3));
                $event->sheet->getDelegate()->getStyle('A'.($HighestRow + 2) .':I'.($HighestRow + 3))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  12,
                        'italic'    => true
                    )
                ));
            },
        ];
    }

    public function query()
    {
        $filterRangeMonth = $this->filter['filterRangeMonth'];
        $IdTCNN = $this->filter['IdTCNN'];
        $HoSoNLD_ChucDanh =  HoSoNLD_ChucDanh::query()->leftJoin('HoSoNhanVienTCNN', 'HoSoNhanVienTCNN.IdHoSoNhanVienTCNN', '=', 'HoSoNhanVienTCNN_ChucDanh.IdHoSoNhanVienTCNN')
        ->leftJoin('DmChucDanh', 'DmChucDanh.IdChucDanh', '=', 'HoSoNhanVienTCNN_ChucDanh.History_ChucDanh')
        ->leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN_ChucDanh.IdTCNN')
        ->leftJoin('DmTinhThanh', 'DmTinhThanh.MaTT', '=', 'HoSoNhanVienTCNN_ChucDanh.MaTT_NoiLamViec')
        ->when($filterRangeMonth,function($query, $filterRangeMonth){
            list($start, $end) = explode('-', $filterRangeMonth);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:00', strtotime($end));
                $query->whereBetween('HoSoNhanVienTCNN_ChucDanh.NgayThayDoiChucDanh', [$start, $end]);
        })
        ->when($IdTCNN, function ($query, $IdTCNN) {
            $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
        })
        ->select(
            'HoSoNhanVienTCNN.Ho',
            'HoSoNhanVienTCNN.Ten',
            'DmChucDanh.ChucDanh',
            'DmTinhThanh.TENTT',
            'HoSoNhanVienTCNN_ChucDanh.NoiLamViec',
            'HoSoNhanVienTCNN_ChucDanh.NgayThayDoiChucDanh',
            'HoSoNhanVienTCNN_ChucDanh.NgayKetThuc',
            'HoSoNhanVienTCNN_ChucDanh.SoVBNghiViec',
            'DmTCNN.TenTatTCNN'
        )->latest('HoSoNhanVienTCNN_ChucDanh.NgayThayDoiChucDanh');
        return $HoSoNLD_ChucDanh;
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B7';
    }

     /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row["Ho"] .' ' . $row["Ten"] ,
            $row["ChucDanh"],
            $row["TENTT"],
            $row["NoiLamViec"],
            $row["NgayThayDoiChucDanh"] ? date('d/m/Y',strtotime($row["NgayThayDoiChucDanh"])) : '',
            $row["NgayKetThuc"] ? date('d/m/Y',strtotime($row["NgayKetThuc"])) : '',
            $row["SoVBNghiViec"],
            $row["TenTatTCNN"],
        ];
    }


    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'HỌ VÀ TÊN',
            'THAY ĐỔI CHỨC VỤ',
            'TỈNH THÀNH LÀM VIỆC',
            'VÙNG LÀM VIỆC',
            'NGÀY HIỆU LỰC',
            'NGÀY KẾT THÚC',
            'SỐ CV',
            'TC, CNNN',
        ];
    }
}
