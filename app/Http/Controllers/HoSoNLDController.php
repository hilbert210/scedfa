<?php

namespace App\Http\Controllers;

use App\DataTables\HoSoNLDDataTable;
use App\HoSoNhanVienTCNN;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HoSoNLDController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HoSoNLDDataTable $dataTable)
    {
        return $dataTable->render('layouts.hoSoNLD.hoSoNLD');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            // 'Ho' => 'required',
            // 'Ten' => 'required',
            // 'NgaySinh' => 'required',
            // 'DienThoaiNhaRieng' => 'required',
            // 'IdQuocTich' => 'required',
            // 'IdDanToc' =>  'required',
            // 'IdTonGiao' => 'required',
            // 'DangDoan' => 'required', --
            // 'TrinhDoVanHoa' => 'required',
            // 'TrinhDoChuyenMon' => 'required',
            'SoCMND' => 'required|unique:HoSoNhanVienTCNN|min:9|max:12',
            // 'NgayCap' => 'required',
            // 'NoiCap' => 'required',
            //-----
            // 'MaTT_HoKhau' => 'required',
            // 'MaQU_HoKhau' => 'required',
            // 'MaPhuongXa_HoKhau' => 'required',
            // 'SoNha_HoKhau' => 'required',
            // 'MaTT_ThuongTru' => 'required',
            // 'MaQU_ThuongTru' => 'required',
            // 'MaPhuongXa_ThuongTru' => 'required',
            // 'SoNha_ThuongTru' => 'required',
        ];
        
        $ruleHSXinViec = [
            // 'TTDK_IdTCNN' => 'required',
            // 'TTDK_ChucDanh' => 'required',
        ];
        $ruleHSDangLamViec = [
            // 'IdTCNN' => 'required',
            // 'IdChucDanh' => 'required',
            // 'NgayBatDauCongTac' => 'required',
        ];
        $ruleHSNghiViec = [
            // 'ThongBaoNghiViec_So' => 'required',
            // 'ThongBaoNghiViec_Ngay' => 'required',
        ];
        if ($request->get('TinhTrangHoSo') == 0) {
            $rule = array_merge($ruleHSXinViec, $rule);
        }
        if ($request->get('TinhTrangHoSo') == 1) {
            $rule = array_merge($ruleHSDangLamViec, $rule);
        }
        if ($request->get('TinhTrangHoSo') == 2) {
            $rule = array_merge($ruleHSNghiViec, $rule);
        }
        $customMessages = [
            'Ho.required' => 'Họ không được để trống!',
            'Ten.required' => 'Tên không được để trống!',
            'NgaySinh.required' => 'Ngày sinh không được để trống!',
            'DienThoaiNhaRieng.required' => 'Số điện thoại không được để trống!',
            'IdQuocTich.required' => 'Quốc tịch không được để trống!',
            'IdDanToc.required' => 'Dân tộc không được để trống!',
            'IdTonGiao.required' =>  'Tôn giáo không được để trống!',
            'DangDoan.required' => 'Đảng/Đoàn không được để trống!',
            'TrinhDoVanHoa.required' => 'Trình độ văn hóa không được để trống!',
            'TrinhDoChuyenMon.required' => 'Trình độ chuyên môn không được để trống!',
            'SoCMND.required' => 'Số CMND không được để trống!',
            'SoCMND.unique' => 'Trùng số CMND, mời nhập lại!',
            'SoCMND.min' => 'Số CMND phải ít nhất 9 chữ số!',
            'SoCMND.max' => 'Số CMND phải nhiều nhất 12 chữ số!',
            'NgayCap.required' => 'Ngày cấp không được để trống!',
            'NoiCap.required' => 'Nơi cấp không được để trống!',
            'MaTT_HoKhau.required' => 'Tỉnh thành không được để trống!',
            'MaQU_HoKhau.required' => 'Quận/Huyện không được để trống!',
            'MaPhuongXa_HoKhau.required' => 'Phường/xã không được để trống!',
            'SoNha_HoKhau.required' => 'Số Nhà không được để trống!',
            'MaTT_ThuongTru.required' => 'Tỉnh thành không được để trống!',
            'MaQU_ThuongTru.required' => 'Quận/Huyện không được để trống!',
            'MaPhuongXa_ThuongTru.required' => 'Phường/xã không được để trống!',
            'SoNha_ThuongTru.required' => 'Số Nhà không được để trống!',
            'TTDK_IdTCNN.required' => 'TCNN không được để trống!',
            'TTDK_ChucDanh.required' => 'Chức danh không được để trống!',
            'IdTCNN.required' => 'TCNN không được để trống!',
            'CVHT_ChucDanh.required' => 'Chức danh không được để trống!',
            'History_ChucDanh.required' => 'Chức danh không được để trống!',
            'TTCN_ChucDanh.required' => 'Chức danh không được để trống!',
            'NgayBatDauCongTac.required' => 'Ngày bắt đầu công tác không được để trống!',
        ];
        if ($request->get('isUpdate')) {
            $IdHoSoNhanVienTCNN = $request->get('IdHoSoNhanVienTCNN');
            $rule = [
                'SoCMND' => "required|unique:HoSoNhanVienTCNN,SoCMND,{$IdHoSoNhanVienTCNN},IdHoSoNhanVienTCNN|min:9|max:12",
            ];
            $customMessages = [
                'SoCMND.required' => 'Số CMND không được để trống!',
                'SoCMND.unique' => 'Trùng số CMND, mời nhập lại!',
                'SoCMND.min' => 'Số CMND phải ít nhất 9 chữ số!',
                'SoCMND.max' => 'Số CMND phải nhiều nhất 12 chữ số!',
            ];
        }
        $this->validate($request, $rule, $customMessages);
        $IdHoSoNhanVienTCNN = $request->get('IdHoSoNhanVienTCNN');
        $name = null;
        $avatar = null;
        if ($request->file('avatar')) {
            $avatar = $request->file('avatar');
            if ($avatar->isValid()) {
                $name = $avatar->getClientOriginalName();
                if (file_exists(public_path() . '/avatars/' . $name)) {
                    $name = pathinfo($name)['filename'] . "-" . time() . "." . pathinfo($name)['extension'];
                }
            }
        }
        $maNhanVien = $request->get('MaNhanVien');
        if (!$IdHoSoNhanVienTCNN) {
            $maNhanVien = DB::table('HoSoNhanVienTCNN')->orderByDesc('MaNhanVien')->select('HoSoNhanVienTCNN.MaNhanVien')->first()->MaNhanVien + 1;
            if (strlen((string)$maNhanVien) < 5) {
                $maNhanVien = '0' . $maNhanVien;
            }
        }

        $HSNhanVien = HoSoNhanVienTCNN::updateOrCreate(
            ['IdHoSoNhanVienTCNN' => $IdHoSoNhanVienTCNN],
            [
                'MaNhanVien' =>  $maNhanVien,
                'Ho' => $request->get('Ho'),
                'Ten' => $request->get('Ten'),
                'NgaySinh' => getDateFormat($request->get('NgaySinh')),
                'GioiTinh' => $request->get('GioiTinh'),
                'DienThoaiNhaRieng' => $request->get('DienThoaiNhaRieng'),
                'EMail' => $request->get('EMail'),
                'IdQuocTich' => $request->get('IdQuocTich'),
                'IdDanToc' => $request->get('IdDanToc'),
                'IdTonGiao' => $request->get('IdTonGiao'),
                'DangDoan' => $request->get('DangDoan'),
                'TrinhDoVanHoa' => $request->get('TrinhDoVanHoa'),
                'TrinhDoChuyenMon' => $request->get('TrinhDoChuyenMon'),
                'SoTruong' => $request->get('SoTruong'),
                'SoCMND' => $request->get('SoCMND'),
                'NgayCap' => getDateFormat($request->get('NgayCap')),
                'NoiCap' => $request->get('NoiCap'),
                'SoSoBHXH' => $request->get('SoSoBHXH'),
                'MaBHYT' => $request->get('MaBHYT'),
                'IdBenhVien' => $request->get('IdBenhVien'),
                'MaTT_KhaiSinh' => $request->get('MaTT_KhaiSinh'),
                'MaQU_KhaiSinh' => $request->get('MaQU_KhaiSinh'),
                'MaPhuongXa_KhaiSinh' => $request->get('MaPhuongXa_KhaiSinh'),
                'SoNha_KhaiSinh' => $request->get('SoNha_KhaiSinh'),
                'MaTT_NguyenQuan' => $request->get('MaTT_NguyenQuan'),
                'MaQU_NguyenQuan' => $request->get('MaQU_NguyenQuan'),
                'MaPhuongXa_NguyenQuan' => $request->get('MaPhuongXa_NguyenQuan'),
                'SoNha_NguyenQuan' => $request->get('SoNha_NguyenQuan'),
                'MaTT_HoKhau' => $request->get('MaTT_HoKhau'),
                'MaQU_HoKhau' => $request->get('MaQU_HoKhau'),
                'MaPhuongXa_HoKhau' => $request->get('MaPhuongXa_HoKhau'),
                'SoNha_HoKhau' => $request->get('SoNha_HoKhau'),
                'MaTT_ThuongTru' => $request->get('MaTT_ThuongTru'),
                'MaQU_ThuongTru' => $request->get('MaQU_ThuongTru'),
                'MaPhuongXa_ThuongTru' => $request->get('MaPhuongXa_ThuongTru'),
                'SoNha_ThuongTru' => $request->get('SoNha_ThuongTru'),
                'TinhTrangHoSo' => $request->get('TinhTrangHoSo'),

                'TTDK_IdTCNN' => $request->get('TTDK_IdTCNN'),
                'TTDK_ChucDanh' =>  $request->get('TTDK_ChucDanh'),
                'IdTCNN' =>  $request->get('IdTCNN'),
                'LoaiHopDong' =>  $request->get('LoaiHopDong'),
                'ThoiGianHopDong' =>  $request->get('ThoiGianHopDong'),
                'CVHT_ChucDanh' =>  $request->get('CVHT_ChucDanh'),
                'History_ChucDanh' => $request->get('History_ChucDanh'),
                'MaTT_NoiLamViec' =>  $request->get('MaTT_NoiLamViec'),
                'NoiLamViec' =>  $request->get('NoiLamViec'),
                'NgayBatDauCongTac' => getDateFormat($request->get('NgayBatDauCongTac')),
                'NgayThamGiaBHXH' => getDateFormat($request->get('NgayThamGiaBHXH')),
                'GhiChu_ttct' => $request->get('GhiChu_ttct'),

                'ThongBaoNghiViec_So' =>  $request->get('ThongBaoNghiViec_So'),
                'ThongBaoNghiViec_Ngay' =>  getDateFormat($request->get('ThongBaoNghiViec_Ngay')),
                'NgayThoiThamGiaBHXH' => getDateFormat($request->get('NgayThoiThamGiaBHXH')),

                'SoVBThamTra' =>  $request->get('SoVBThamTra'),
                'NgayThamTra' =>  getDateFormat($request->get('NgayThamTra')),
                'SoVBThamTraAnNinh' =>  $request->get('SoVBThamTraAnNinh'),
                'NgayVBThamTraAnNinh' =>  getDateFormat($request->get('NgayVBThamTraAnNinh')),
                'LoaiCQAN' =>  $request->get('LoaiCQAN'),
                'SoVBChapThuan' =>  $request->get('SoVBChapThuan'),
                'NgayVBChapThuan' =>  getDateFormat($request->get('NgayVBChapThuan')),
            ]
        );
        if($request->get('ThongBaoNghiViec')==1){
            if($request->get('TinhTrangHoSo')==2){
                $HSNhanVien->ThongBaoNghiViec = false;
            };
        };
        if ($request->get('IdTCNN') != NULL) {
            $HSNhanVien->IdTCNN = $request->get('IdTCNN');
        }

        if ($request->get('IdQuocTich')) {
            $HSNhanVien->IdQuocTich = $request->get('IdQuocTich');
        }
        if ($request->get('IdDanToc') ) {
            $HSNhanVien->IdDanToc = $request->get('IdDanToc');
        }
        if ($request->get('IdTonGiao')) {
            $HSNhanVien->IdTonGiao = $request->get('IdTonGiao');
        }
        if ($request->get('DangDoan')) {
            $HSNhanVien->DangDoan = $request->get('DangDoan');
        }

        if ($request->get('NoiCap')) {
            $HSNhanVien->NoiCap = $request->get('NoiCap');
        }
        if ( $request->get('MaTT_KhaiSinh')) {
            $HSNhanVien->MaTT_KhaiSinh = $request->get('MaTT_KhaiSinh');
        }
        if ($request->get('MaQU_KhaiSinh')) {
            $HSNhanVien->MaQU_KhaiSinh = $request->get('MaQU_KhaiSinh');
        }
        if ($request->get('MaPhuongXa_KhaiSinh')) {
            $HSNhanVien->MaPhuongXa_KhaiSinh = $request->get('MaPhuongXa_KhaiSinh');
        }
        if ($request->get('SoNha_KhaiSinh')) {
            $HSNhanVien->SoNha_KhaiSinh = $request->get('SoNha_KhaiSinh');
        }
        if ($request->get('MaTT_NguyenQuan')) {
            $HSNhanVien->MaTT_NguyenQuan = $request->get('MaTT_NguyenQuan');
        }
        if ($request->get('MaQU_NguyenQuan')) {
            $HSNhanVien->MaQU_NguyenQuan = $request->get('MaQU_NguyenQuan');
        }
        if ($request->get('MaPhuongXa_NguyenQuan')) {
            $HSNhanVien->MaPhuongXa_NguyenQuan = $request->get('MaPhuongXa_NguyenQuan');
        }
        if ($request->get('SoNha_NguyenQuan')) {
            $HSNhanVien->SoNha_NguyenQuan = $request->get('SoNha_NguyenQuan');
        }
        if ($request->get('MaTT_HoKhau')) {
            $HSNhanVien->MaTT_HoKhau = $request->get('MaTT_HoKhau');
        }
        if ($request->get('MaQU_HoKhau')) {
            $HSNhanVien->MaQU_HoKhau = $request->get('MaQU_HoKhau');
        }
        if ($request->get('MaPhuongXa_HoKhau')) {
            $HSNhanVien->MaPhuongXa_HoKhau = $request->get('MaPhuongXa_HoKhau');
        }
        if ($request->get('SoNha_HoKhau')) {
            $HSNhanVien->SoNha_HoKhau = $request->get('SoNha_HoKhau');
        }
        if ($request->get('MaTT_ThuongTru')) {
            $HSNhanVien->MaTT_ThuongTru = $request->get('MaTT_ThuongTru');
        }
        if ($request->get('MaQU_ThuongTru')) {
            $HSNhanVien->MaQU_ThuongTru = $request->get('MaQU_ThuongTru');
        }
        if ($request->get('MaPhuongXa_ThuongTru')) {
            $HSNhanVien->MaPhuongXa_ThuongTru = $request->get('MaPhuongXa_ThuongTru');
        }
        if ($request->get('SoNha_ThuongTru')) {
            $HSNhanVien->SoNha_ThuongTru = $request->get('SoNha_ThuongTru');
        }
        if ($request->get('TinhTrangHoSo')) {
            $HSNhanVien->TinhTrangHoSo = $request->get('TinhTrangHoSo');
        }
        if ($request->get('TTDK_IdTCNN')) {
            $HSNhanVien->TTDK_IdTCNN = $request->get('TTDK_IdTCNN');
        }
        if ($request->get('TTDK_ChucDanh')) {
            $HSNhanVien->TTDK_ChucDanh =  $request->get('TTDK_ChucDanh');
        }
        if ($request->get('LoaiHopDong')) {
            $HSNhanVien->LoaiHopDong =  $request->get('LoaiHopDong');
        }
        if ($request->get('CVHT_ChucDanh')) {
            $HSNhanVien->CVHT_ChucDanh =  $request->get('CVHT_ChucDanh');
        }
        if ($request->get('History_ChucDanh')) {
            $HSNhanVien->History_ChucDanh =  $request->get('History_ChucDanh');
        }
        if ($request->get('TCNN_NoiLamViec')) {
            $HSNhanVien->MaTT_NoiLamViec =  $request->get('TCNN_NoiLamViec');
        }
        if ($request->get('NoiLamViec')) {
            $HSNhanVien->NoiLamViec =  $request->get('NoiLamViec');
        }
        if ($request->get('LoaiCQAN')) {
            $HSNhanVien->LoaiCQAN =  $request->get('LoaiCQAN');
        }
        if($request->get('TinhTrangHoSo')==2){
            // $HSNhanVien->IdTCNN = null;
            $HSNhanVien->LoaiHopDong = null;
            $HSNhanVien->ThoiGianHopDong = null;
            // $HSNhanVien->IdChucDanh = null;
            // $HSNhanVien->MaTT_NoiLamViec = null;
            // $HSNhanVien->NoiLamViec = null;
            $HSNhanVien->NgayBatDauCongTac = null;
            // $HSNhanVien->NgayThamGiaBHXH = null;
            // $id =DB::table('HoSoNhanVienTCNN_ChucDanh')->where('IdHoSoNhanVienTCNN','=',$IdHoSoNhanVienTCNN)->orderByDesc('Id')->first()->Id;
            // DB::table('HoSoNhanVienTCNN_ChucDanh')->where('Id','=',$id)->update(['NgayKetThuc'=> getDateFormat($request->get('ThongBaoNghiViec_Ngay'))]);
            $hsChucDanh = DB::table('HoSoNhanVienTCNN_ChucDanh')->where('IdHoSoNhanVienTCNN','=',$IdHoSoNhanVienTCNN)->orderByDesc('Id')->first();
            if ($hsChucDanh) {
                DB::table('HoSoNhanVienTCNN_ChucDanh')->where('Id','=',$hsChucDanh->Id)->update(['NgayKetThuc'=> getDateFormat($request->get('ThongBaoNghiViec_Ngay'))]);
            }
        }
        if($request->get('TinhTrangHoSo')==1){
            $HSNhanVien->ThongBaoNghiViec_So = null;
            $HSNhanVien->ThongBaoNghiViec_Ngay = null;
            $HSNhanVien->NgayThoiThamGiaBHXH = null;
        };
        if (!is_null($avatar)) {
            $HSNhanVien->avatar = '/avatars/' . $name;
        };
        $HSNhanVien->save();
        if (!is_null($avatar)) {
            $avatar->move(public_path() . '/avatars/', $name);
        };
        $TenTiengVietTCNN =$request->get('IdTCNN') ?   DB::table('DmTCNN')->where('IdTCNN','=',$request->get('IdTCNN'))->select('DmTCNN.TenTiengVietTCNN')->first()->TenTiengVietTCNN : null;
        $ChucDanh =$request->get('CVHT_ChucDanh') ? DB::table('DmChucDanh')->where('IdChucDanh','=',$request->get('CVHT_ChucDanh'))->select('DmChucDanh.ChucDanh')->first()->ChucDanh : null;
        if ($request->get('IdTCNN')) {
            $TenTiengVietTCNN = DB::table('DmTCNN')->where('IdTCNN','=',$request->get('IdTCNN'))->select('DmTCNN.TenTiengVietTCNN')->first()->TenTiengVietTCNN;
            $HSNhanVien->TenTiengVietTCNN = $TenTiengVietTCNN;
        }
        $HSNhanVien->TenTiengVietTCNN = $TenTiengVietTCNN;
        $HSNhanVien->ChucDanh = $ChucDanh;
        $HSNhanVien->Gender =  $request->get('GioiTinh')==0?'Nữ':'Nam';
        $HSNhanVien->TENTT = $request->get('MaTT_ThuongTru') ? DB::table('DmTinhThanh')->where('MATT','=',$request->get('MaTT_ThuongTru'))->select('DmTinhThanh.TENTT')->first()->TENTT : null;
        $HSNhanVien->actions = $request->get('actions');
        $HSNhanVien->NgaySinh =$request->get('NgaySinh') ? date("m/d/Y", strtotime(str_replace('/', '-', $request->get('NgaySinh')))) : null;
        return response()->json(['success' => 'Hồ sơ người lao động lưu thành công.', 'data'=> $HSNhanVien]);
    }
    public function edit($id)
    {
        $TCNN  = HoSoNhanVienTCNN::find($id);
        return response()->json($TCNN);
    }
}