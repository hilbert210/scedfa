<?php

namespace App\DataTables;

use App\HoSoTTThuHo;
use App\Transformers\HoSoTTThuHoTransformer;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class HoSoTTThuHoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())->setTransformer(new HoSoTTThuHoTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $IdTCNN = $this->IdTCNN? $this->IdTCNN : $this->request()->get('IdTCNN');
        $month =  $this->request()->get('month');
        $query =   HoSoTTThuHo::leftJoin('DmTCNN', 'DmTCNN.IdTCNN', '=', 'NopTienTCNN.IdTCNN')
            ->when($IdTCNN, function ($query, $IdTCNN) {
                $query->where('DmTCNN.IdTCNN', '=', $IdTCNN);
            })
            ->when($month, function ($query, $month) {
                list($start, $end) = explode('-', $month);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:00', strtotime($end));
                $query->whereBetween('NgayChungTu', [$start, $end]);
            })
            ->select(
                'NopTienTCNN.IdChungTu',
                'NopTienTCNN.SoChungTu',
                'NopTienTCNN.NgayChungTu',
                'NopTienTCNN.NopTuThang',
                'NopTienTCNN.NopDenThang',
                'NopTienTCNN.TongTienNop',
                'NopTienTCNN.BHXH',
                'NopTienTCNN.TTN',
                'NopTienTCNN.Luong',
                'NopTienTCNN.BHTN',
                'NopTienTCNN.DVP',
                'NopTienTCNN.TroCapBHXH',
                'NopTienTCNN.PhiNganHang',
                'DmTCNN.TenTiengVietTCNN',
                'DmTCNN.TenTiengAnhTCNN',
                'DmTCNN.TenTatTCNN'
            );
        return $this->applyScopes($query->get());
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['export', 'reload'];
        if(Gate::allows('HoSoTTThuHo_delete')){
            array_unshift($buttons,'delete');
        }
        if(Gate::allows('HoSoTTThuHo_edit')){
            array_unshift($buttons,'edit');
        }
        if(Gate::allows('HoSoTTThuHo_create')){
            array_unshift($buttons,'create');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->columnDefs($this->columnDefs())
            ->minifiedAjax(
                '',
                'data.IdTCNN = $("#DmTCNNFilter").val(); data.month = $("#filterRangeMonth").val();'
            )
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => true,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'desc']],
                'buttons'      => $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function columnDefs()
    {
        return [
            ["targets"=>[4,5] , "type"=> 'stringMonthYear']
        ];
    }
    protected function getColumns()
    {
        $NgayChungTu  = Column::make('NgayChungTu')
            ->title('Ngày Chứng Từ')
            ->searchable(false)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");   
                }
                return e;
            }')
            ->exportable(true)
            ->printable(true);
        $NopTuThang  = Column::make('NopTuThang')
            ->title('Nộp từ tháng')
            ->searchable(false)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");   
                }
                return e;
            }')
            ->exportable(true)
            ->printable(true);
        $NopDenThang  = Column::make('NopDenThang')
            ->title('Nộp đến tháng')
            ->searchable(false)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");   
                }
                return e;
            }')
            ->exportable(true)
            ->printable(true);
        return [
            ["data" => "IdChungTu", 'title' => 'ID', 'exportable' => false, 'printable' => false],
            ["data" => "TenTiengVietTCNN", 'title' => 'TCNN'],
            ["data" => "SoChungTu", 'title' => 'Số Chứng Từ'],
            $NgayChungTu,
            $NopTuThang,
            $NopDenThang,
            ["data" => "TongTienNop", 'title' => 'Tổng tiền nộp'],
            ["data" => "BHXH", 'title' => 'BHXH', 'className' => 'hide'],
            ["data" => "TTN", 'title' => 'TTN', 'className' => 'hide'],
            ["data" => "Luong", 'title' => 'Luong', 'className' => 'hide'],
            ["data" => "BHTN", 'title' => 'BHTN', 'className' => 'hide'],
            ["data" => "DVP", 'title' => 'DVP', 'className' => 'hide'],
            ["data" => "TroCapBHXH", 'title' => 'TroCapBHXH', 'className' => 'hide'],
            ["data" => "PhiNganHang", 'title' => 'PhiNganHang', 'className' => 'hide'],
            ["data" => "TenTatTCNN", 'title' => 'TenTatTCNN', 'className' => 'hide']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'HoSoTTThuHo_' . date('YmdHis');
    }
}
