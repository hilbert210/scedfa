<?php

namespace App\Http\Controllers;

use App\DataTables\DmTinhThanhDataTable;
use App\DmTinhThanh;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DmTinhThanhController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DmTinhThanhDataTable $dataTable)
    {
        return $dataTable->render('layouts.tinhthanh.tinhthanh');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $rule = [
            'MATT' => 'required|unique:DmTinhThanh|min:3|max:3',
            'TENTT' => 'required|unique:DmTinhThanh',
            'MAVUNG' => 'required|min:1|max:1',
            //'IdTinhThanh' => 'required|unique:DmTinhThanh|min:2|max:2',
        ];
        $customMessages = [
            'MATT.required' => 'Mã tỉnh thành không được để trống!',
            'MATT.unique' => 'Trùng mã tỉnh thành, mời nhập lại!',
            'MATT.min' => "Mã tỉnh thành chỉ bao gồm 3 kí tự!",
            'MATT.max' => "Mã tỉnh thành chỉ bao gồm 3 kí tự!",
            'TENTT.required' => 'Tên tỉnh thành không được để trống!',
            'TENTT.unique' => 'Trùng tên tỉnh thành, mời nhập lại!',
            'MAVUNG.required' => 'Mã vùng không được để trống!',
            'MAVUNG.min' => "Mã vùng chỉ bao gồm 1 kí tự!",
            'MAVUNG.max' => "Mã vùng chỉ bao gồm 1 kí tự!",
            'IdTinhThanh.required' => 'Id tỉnh thành không được để trống!',
            'IdTinhThanh.unique' => 'Trùng Id tỉnh thành, mời nhập lại!',
            'IdTinhThanh.min' => "Id tỉnh thành chỉ bao gồm 2 kí tự!",
            'IdTinhThanh.max' => "Id tỉnh thành chỉ bao gồm 2 kí tự!",
        ];
        if ($request->get('isUpdateTinhThanh')) {
            $MATT = $request->get('MATT');
            $rule = [
                'TENTT' => "required|unique:DmTinhThanh,TENTT,{$MATT},MATT",
                'MAVUNG' => 'required|min:1|max:1',
                //'IdTinhThanh' => "required|unique:DmTinhThanh,IdTinhThanh,{$MATT},MATT|min:2|max:2",
            ];
            $customMessages = [
                'TENTT.required' => 'Tên tỉnh thành không được để trống!',
                'TENTT.unique' => 'Trùng tên tỉnh thành, mời nhập lại!',
                'MAVUNG.min' => "Mã vùng chỉ bao gồm 1 kí tự!",
                'MAVUNG.max' => "Mã vùng chỉ bao gồm 1 kí tự!",
                'IdTinhThanh.required' => 'Id tỉnh thành không được để trống!',
                'IdTinhThanh.unique' => 'Trùng Id Tỉnh thành, mời nhập lại!',
                'IdTinhThanh.min' => "Id tỉnh thành chỉ bao gồm 2 kí tự!",
                'IdTinhThanh.max' => "Id tỉnh thành chỉ bao gồm 2 kí tự!",
            ];
        }
        $this->validate($request,$rule,$customMessages);
        $MATT = $request->get('MATT');

        $DmTinhThanh = DmTinhThanh::updateOrCreate(
            ['MATT' => $MATT],
            [
                'TENTT' => $request->get('TENTT'),
                'MAVUNG' => $request->get('MAVUNG'),
                'IdTinhThanh' => $request->get('IdTinhThanh')
            ]
        );
        $DmTinhThanh->save();
        return response()->json(['success' => "Tỉnh thành được lưu thành công.", 'data'=> $DmTinhThanh]);
    }

    public function edit($MATT)
    {
        $DmTinhThanh  = DmTinhThanh::find($MATT);
        return response()->json($DmTinhThanh);
    }
}
