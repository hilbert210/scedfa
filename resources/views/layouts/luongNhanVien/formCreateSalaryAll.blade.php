<form method="POST" id="form-CreateSalaryAll" class="form-CreateSalaryAll">
    @csrf
    <div class="modal fade " id="modal-CreateSalaryAll" tabindex="-1" role="dialog"
      aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" id="button-close-modal-create" class="close" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" style="min-height: 40rem;">
            
          </div>
          <div class="modal-footer">
            <button type="submit" id="saveBtnCreateSalary" class="btn btn-primary-outline">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
  </form>