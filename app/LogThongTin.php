<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogThongTin extends Model
{
    protected $guarded  = ['Id'];
    protected $primaryKey = "Id";
    public $incrementing = true;
    protected $table = 'Log_ThongTin';
    const TYPE_TAO_LUONG = 'Tao luong moi';
    const TYPE_SUA_LUONG = 'Sua luong';
    const TYPE_DIEU_CHINH_LUONG = 'Dieu chinh luong';
    public static function saveLog($objData, $type, $oldData = null, $requestData = null) {
        $log = new LogThongTin();
        $log->IdHoSoNhanVienTCNN = $objData->IdHoSoNhanVienTCNN;
        $log->Type = $type;
        if ($objData->IdTCNN) {
            $log->IdTCNN = $objData->IdTCNN;
        }
        if ($objData->IdLuong) {
            $log->IdLuong = $objData->IdLuong;
        }
        $log->LuongCu = $objData->LuongCu ? $objData->LuongCu : 0;
        $log->LuongMoi = $objData->LuongMoi ? $objData->LuongMoi : 0;
        $log->LuongDieuChinh = $objData->LuongDieuChinh ? $objData->LuongDieuChinh : 0;
        if ($objData->NgayTinhLuong) {
            $log->NgayTinhLuong = $objData->NgayTinhLuong;
        }
        if ($objData->NgayDieuChinh) {
            $log->NgayDieuChinh = $objData->NgayDieuChinh;
        }
        if ($oldData) {
            $log->OldData = json_encode($oldData);
        }
        if ($requestData) {
            $log->requestData = json_encode($requestData);
        }
        return $log->save();
    }
}