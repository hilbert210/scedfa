<form method="POST" id="form-HoSoTTThuHo" class="form-HoSoTTThuHo">
  @csrf
  <div class="modal fade " id="modal-HoSoTTThuHo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" id="button-close-modal" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <input type="hidden" class="form-control" name="IdChungTu" id="IdChungTu">
            <div class="col-8">
              <fieldset>
                <legend>Thông Tin Thu Hộ</legend>
                <div class="row">
                  <div class="col-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">TCNN</span>
                      </div>
                      <select class="form-control" name="IdTCNN" id="IdTCNN">
                        <option value=""></option>
                      </select>
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Số Chứng từ</span>
                      </div>
                      <input type="text" class="form-control" id="SoChungTu" name="SoChungTu" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ngày Chứng Từ</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NgayChungTu" name="NgayChungTu" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Nộp Từ Tháng</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NopTuThang" name="NopTuThang" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Nộp đến Tháng</span>
                      </div>
                      <input type="text" class="form-control datepicker" id="NopDenThang" name="NopDenThang" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Tổng tiền nộp</span>
                      </div>
                      <input type="text" class="money form-control" id="TongTienNop" name="TongTienNop" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Người Nộp</span>
                      </div>
                      <input type="text" class="form-control" id="NguoiNop" name="NguoiNop" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Ghi Chú</span>
                      </div>
                      <input type="text" class="form-control" id="GhiChu" name="GhiChu" value="" />
                    </div>
                  </div>
                  <div id='chi_tiet' class="col-6">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">BHXH</span>
                      </div>
                      <input type="text" class="money form-control" id="BHXH" name="BHXH" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">TTN</span>
                      </div>
                      <input type="text" class="money form-control" id="TTN" name="TTN" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Luong</span>
                      </div>
                      <input type="text" class="money form-control" id="Luong" name="Luong" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">BHTN</span>
                      </div>
                      <input type="text" class="money form-control" id="BHTN" name="BHTN" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">DVP</span>
                      </div>
                      <input type="text" class="money form-control" id="DVP" name="DVP" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">TroCapBHXH</span>
                      </div>
                      <input type="text" class="money form-control" id="TroCapBHXH" name="TroCapBHXH" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">PhiNganHang</span>
                      </div>
                      <input type="text" class="money form-control" id="PhiNganHang" name="PhiNganHang" value="" />
                    </div>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">Lãi</span>
                      </div>
                      <input type="text" class="money form-control" id="Lai" name="Lai" value="" />
                    </div>
                  </div>
                </div>
              </fieldset>
            </div>
            <div class="col-4">
              <fieldset>
                <legend>So Sánh</legend>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Tháng</span>
                  </div>
                  <input type="text" class="form-control" id="daterangerpickerStartEnd" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">BHXH</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-BHXH" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">TTN</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-TTN" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Luong</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-Luong" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">BHTN</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-BHTN" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">DVP</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-DVP" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">TroCapBHXH</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-TroCapBHXH" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">PhiNganHang</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-PhiNganHang" value="" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Lãi</span>
                  </div>
                  <input type="text" class="money form-control" id="dc-Lai" value="" />
                </div>
              </fieldset>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="saveBtn-HoSoTTThuHo" class="btn btn-primary-outline">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>