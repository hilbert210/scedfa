<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('Settings', function ($user) {
            return in_array($user->level,[1]);
        });
        Gate::define('DmTinhThanh_edit', function ($user) {
            return json_decode($user->DmTinhThanh)->{'edit'}==true;
        });
        Gate::define('DmTinhThanh_create', function ($user) {
            return json_decode($user->DmTinhThanh)->{'create'}==true;
        });
        Gate::define('DmQuanHuyen_edit', function ($user) {
            return json_decode($user->DmQuanHuyen)->{'edit'}==true;
        });
        Gate::define('DmQuanHuyen_create', function ($user) {
            return json_decode($user->DmQuanHuyen)->{'create'}==true;
        });
        Gate::define('DmPhuongXa_edit', function ($user) {
            return json_decode($user->DmPhuongXa)->{'edit'}==true;
        });
        Gate::define('DmPhuongXa_create', function ($user) {
            return json_decode($user->DmPhuongXa)->{'create'}==true;
        });
        Gate::define('HoSoVPTCNN_edit', function ($user) {
            return json_decode($user->HSVPTCNN)->{'edit'}==true;
        });
        Gate::define('HoSoVPTCNN_create', function ($user) {
            return json_decode($user->HSVPTCNN)->{'create'}==true;
        });
        Gate::define('HoSoNLD_edit', function ($user) {
            return json_decode($user->HSNLD)->{'edit'}==true;
        });
        Gate::define('HoSoNLD_create', function ($user) {
            return json_decode($user->HSNLD)->{'create'}==true;
        });
        Gate::define('HoSoTTThuHo_create', function ($user) {
            return json_decode($user->HSTTTH)->{'create'}==true;
        });
        Gate::define('HoSoTTThuHo_edit', function ($user) {
            return json_decode($user->HSTTTH)->{'edit'}==true;
        });
        Gate::define('HoSoTTThuHo_delete', function ($user) {
            return json_decode($user->HSTTTH)->{'delete'}==true;
        });
        Gate::define('LuongNhanVien_create', function ($user) {
            return json_decode($user->LBHNV)->{'create'}==true;
        });
        Gate::define('LuongNhanVien_edit', function ($user) {
            return json_decode($user->LBHNV)->{'edit'}==true;
        });
        Gate::define('LuongNhanVien_delete', function ($user) {
            return json_decode($user->LBHNV)->{'delete'}==true;
        });
    }
}
