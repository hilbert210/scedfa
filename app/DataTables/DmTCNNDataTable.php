<?php

namespace App\DataTables;

use App\DmTCNN;
use App\Transformers\TCNNTransformer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class DmTCNNDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable()
    {
        return datatables($this->query())->setTransformer(new TCNNTransformer);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $range_date = $this->request()->get('range_date');
        $LoaiGPFilter = $this->request()->get('LoaiGPFilter');
        $CheckThamGiaBHXH = is_numeric($this->request()->get('ThamGiaBHXH')) ? true  : false;
        $BHXH = $this->request()->get('ThamGiaBHXH');
        $query =  DmTCNN::leftJoin('DmToChuc', 'DmTCNN.LoaiTCNN', '=', 'DmToChuc.IdToChuc')
            ->leftJoin('DmQuocTich', 'DmTCNN.QuocTichTSC', '=', 'DmQuocTich.MaQuocTich')
            ->leftJoin('DmChucDanh', 'DmTCNN.TCNN_ChucDanh', '=', 'DmChucDanh.IdChucDanh')
            ->leftJoin('HoSoNhanVienTCNN', function($join){
                $join->on('DmTCNN.IdTCNN', '=', 'HoSoNhanVienTCNN.IdTCNN')
                ->where('HoSoNhanVienTCNN.TinhTrangHoSo' ,1);
            })
            ->when($range_date, function ($query, $range_date) {
                list($start, $end) = explode('-', $range_date);
                $start = date('Y-m-d 00:00:00', strtotime($start));
                $end  = date('Y-m-d 23:59:59', strtotime($end));
                $query->whereBetween('NgayThoiHoatDong', [$start, $end]);
            })
            ->when($LoaiGPFilter, function ($query, $LoaiGPFilter) {
                $query->where('DmTCNN.LoaiGP', $LoaiGPFilter);
            })
            ->when($CheckThamGiaBHXH, function ($query) use ($BHXH) {
                $query->where('DmTCNN.ThamGiaBHXH', '=', $BHXH);
            })
            ->groupBy('DmTCNN.IdTCNN','DmTCNN.TenTiengVietTCNN','DmTCNN.TenTatTCNN','DmTCNN.TruSoChinh','DmTCNN.QuocTichTSC','DmTCNN.CoQuanQuanLy',
            'DmTCNN.DiaBanHoatDong','DmTCNN.LinhVucHoatDong', 'DmTCNN.SLLaoDongVN','DmTCNN.LoaiGP',
            'DmTCNN.HoVaTen_admin','DmTCNN.SDT_admin','DmTCNN.Email_admin','DmTCNN.Email','DmTCNN.NamSinh','DmTCNN.NgayHetHanGiayDKHD',
            'DmTCNN.TenNguoiDaiDien', 'DmTCNN.DienThoaiNguoiDaiDien', 'DmTCNN.DangGiaHan','DmTCNN.DiaChi','DmTCNN.TenTiengAnhTCNN',
            'DmTCNN.NgayThoiHoatDong','DmTCNN.ThoiSuDung','DmToChuc.TenToChuc','DmQuocTich.QuocTich', 'DmChucDanh.ChucDanh', 'DmTCNN.ThamGiaBHXH' )
            ->select(
                'DmTCNN.IdTCNN','DmTCNN.TenTiengVietTCNN','DmTCNN.TenTatTCNN','DmTCNN.TruSoChinh','DmTCNN.QuocTichTSC','DmTCNN.CoQuanQuanLy',
                'DmTCNN.DiaBanHoatDong','DmTCNN.LinhVucHoatDong','DmTCNN.SLLaoDongVN', 'DmTCNN.TenNguoiDaiDien','DmTCNN.DienThoaiNguoiDaiDien',
                'DmTCNN.DangGiaHan','DmTCNN.DiaChi','DmTCNN.LoaiGP','DmTCNN.TenTiengAnhTCNN',
                'DmTCNN.HoVaTen_admin','DmTCNN.SDT_admin','DmTCNN.Email_admin','DmTCNN.Email','DmTCNN.NamSinh','DmTCNN.NgayHetHanGiayDKHD',
                'DmTCNN.NgayThoiHoatDong','DmTCNN.ThoiSuDung', 'DmChucDanh.ChucDanh',
                DB::raw('COUNT(HoSoNhanVienTCNN.IdHoSoNhanVienTCNN) as SoLuong'),
                'DmToChuc.TenToChuc','DmQuocTich.QuocTich', 'DmTCNN.ThamGiaBHXH',
                DB::raw("CASE WHEN DmTCNN.ThamGiaBHXH = 1 THEN N'Có' WHEN DmTCNN.ThamGiaBHXH = 0 THEN N'Không' ELSE '' END AS ThamGiaBHXH ")
            )
            ->get();
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = ['export', 'reload', 'bangluongTCNN', 'hoSoTTThuHo'];
        if(Gate::allows('HoSoVPTCNN_edit')){
            array_unshift($buttons,'edit');
        }
        if(Gate::allows('HoSoVPTCNN_create')){
            array_unshift($buttons,'create');
        }
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax(
                '',
                'data.range_date = $("#filterRangeDate").val();
                 data.LoaiGPFilter = $("#LoaiGPFilter").val();
                 data.ThamGiaBHXH = $("#ThamGiaBHXHFilter").val();'
            )
            ->createdRow('function(row, data, dataIndex){
                if(moment().isAfter(moment(data.NgayHetHanGiayDKHD))){
                    $(row).addClass("bg-danger");
                }
            }')
            ->parameters([
                'select' => true,
                'select' => 'single',
                'fixedHeader' => true,
                "autoWidth" => false,
                'responsive' => false,
                'dom'     => '<"top row"<"col-6"B><"col-6"f><"col-6 pt-1"l><"col-6"p>>rt<"bottom row"<"col-6"i><"col-6"p>>',
                'order'   => [[0, 'asc']],
                'buttons'      =>  $buttons,
            ]);
    }


    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $status = Column::make('ThoiSuDung')
            ->title('Hiện Trạng: ')
            ->searchable(true)
            ->orderable(false)
            ->className('hide')
            ->render('function(e){
                if(e==0){
                    return "Đang Quản Lý";
                }
                if(e==1){
                    return "Thôi Quản Lý";
                }
                return e;
            }')
            ->exportable(false)
            ->printable(false);
        $NgayThoiHoatDong = Column::make('NgayThoiHoatDong')
            ->title('Ngày Dừng Hoạt Động')
            ->className('hide')
            ->searchable(true)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");
                }
                return e;
            }')
            ->exportable(false)
            ->printable(false);
        $NgayHetHanGiayDKHD = Column::make('NgayHetHanGiayDKHD')
            ->title('Ngày Hết Hạn GP')
            //->className('hide')
            ->searchable(true)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");
                }
                return e;
            }');
        $NamSinh = Column::make('NamSinh')
            ->title('Ngày Sinh NĐD')
            ->className('hide')
            ->searchable(true)
            ->orderable(true)
            ->render('function(e){
                if(e){
                    return moment(e).format("DD/MM/YYYY");
                }
                return e;
            }');
        return [
            ["data" => "IdTCNN", 'title' => 'ID', 'exportable' => false, 'printable' => false],
            $status,
            $NgayThoiHoatDong,
            ["data" => "DangGiaHan", 'title' => 'Đang Gia Hạn: ', 'className' => 'hide', 'exportable' => false, 'printable' => false],
            ["data" => "TenTiengVietTCNN", 'title' => 'Tên TC, CNNN'],
            ["data" => "TenTatTCNN", 'title' => 'Tên Viết Tắt'],
            ["data" => "TruSoChinh", 'title' => 'Trụ Sở Chính', 'className' => 'hide', 'exportable' => false, 'printable' => false],
            ["data" => "QuocTich", 'title' => 'Quốc Tịch'],
            ["data" => "DiaBanHoatDong", 'title' => 'Địa Bàn Hoạt Động'],
            ["data" => "TenToChuc", 'title' => 'Loại Hình TC,CN NN'],
            ["data" => "LoaiGP", 'title' => 'Loại Giấy Phép'],
            ["data" => "LinhVucHoatDong", 'title' => 'Lĩnh Vực Hoạt Động'],
            ["data" => "DiaChi", 'title' => 'Địa chỉ TC, CNNN'],
            ["data" => "SLLaoDongVN", 'title' => 'Số Lượng NLĐ Cấp Phép'],
            ["data" => "SoLuong", 'title' => 'Số Lượng NLĐ ĐLV'],
            ["data" => "ThamGiaBHXH", 'title' => 'Tham Gia BHXH'],
            ["data" => "TenNguoiDaiDien", 'title' => 'Người Đại Diện: ', 'className' => 'hide'],
            ["data" => "Email", 'title' => 'Email NĐD', 'className' => 'hide'],
            $NamSinh,
            $NgayHetHanGiayDKHD,
            ["data" => "DienThoaiNguoiDaiDien", 'title' => 'SĐT: ', 'className' => 'hide'],
            ["data" => "HoVaTen_admin", 'title' => 'Người Liên Hệ', 'className' => 'hide', 'exportable' => true],
            ["data" => "SDT_admin", 'title' => 'Số điện thoại NLH', 'className' => 'hide', 'exportable' => true],
            ["data" => "Email_admin", 'title' => 'Email NLH', 'className' => 'hide', 'exportable' => true],
            ["data" => "HoVaTen_admin", 'title' => 'Họ Tên Admin', 'className' => 'hide', 'exportable' => true],
            ["data" => "SDT_admin", 'title' => 'Số ĐT Admin', 'className' => 'hide', 'exportable' => true],
            ["data" => "Email_admin", 'title' => 'Email Admin', 'className' => 'hide', 'exportable' => true],
            ["data" => "ChucDanh", 'title' => 'Chuc Danh', 'className' => 'hide']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DmTCNN_' . date('YmdHis');
    }
}
