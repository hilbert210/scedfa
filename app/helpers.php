<?php
if (!function_exists('getDateFormat')) {
    function getDateFormat($value)
    {
        if (isset($value)) {
            if (!empty($value)) {
                return DateTime::createFromFormat('d/m/Y', $value);
            }
        }
        return null;
    }
}
if (!function_exists('convertArrayToString')) {
    function convertArrayToString($value)
    {
        if (isset($value)) {
            if (!empty($value)) {
                return implode(', ',$value);
            }
        }
        return null;
    }
}