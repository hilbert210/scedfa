<?php

return [
    'PhuongAn' => [
        'AD' => 'Bổ sung tăng nguyên lương',
        'AT' => 'Truy đóng thoe MLCS tại thời điểm',
        'CD' => 'Điều chỉnh chức danh',
        'DC' => 'Điều chỉnh lương',
        'DN' => 'Điều chỉnh tham gia thất nghiệp (MLCS thời điểm)',
        'GC' => 'Giảm do chuyển tỉnh',
        'GD' => 'Giảm do chuyển đơn vị',
        'GH' => 'Giảm hẳn',
        'GN' => 'Giảm tham gia thất nghiệp',
        'KL' => 'Nghĩ Không lương',
        'OF' => 'Nghĩ do ốm đau/nghĩ không lương',
        'ON' => 'Đi làm lại',
        'SB' => 'Bổ Sung giảm nguyên lương',
        'TC' => 'Tăng do chuyển tỉnh',
        'TD' => 'Tăng do chuyển đơn vị',
        'TM' => 'Tăng mới',
        'TN' => 'Tăng tham gia thất nghiệp',
        'TS' => 'Thai sản',
        'TT' => 'Bổ sung tăng quỹ KCB',
        'TU' => 'Bổ sung giảm quỹ KCB',
    ],
    'DoiTuong' => [
        '1' => 'Nhân Viên Mới',
        '2' => 'Nhân Viên Nghỉ Việc',
        '3' => 'Tăng Lương',
        '4' => 'Giảm Lương',
        '5' => 'Thai Sản',
        '6' => 'Truy Thu',
    ],
    'SysConf' =>[
        'BHXH' => 25.5,
        'BHYT' => 4.5,
        'BHTN' => 2,
        'BHXH_BHYT_CSD' => 20.5,
        'BHTN_CSD' => 1,
        'BHXH_BHYT_NLD' => 9.5,
        'BHTN_NLD' => 1,
        'GIOIHAN_BHXH_BHYT' => 29800000,
        'GIOIHAN_BHTN' => 78400000,
        'GIAMTRU_NGUOIPHUTHUOC' => 4400000,
        'GIAMTRU_THUE' => 11000000,
        'GIAMTRU_OLD' => 9000000,
        'GIAMTRU_PHUTHUOC_OLD' => 3600000
    ]
];
