<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\User;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('layouts.settings.settings');
    }

    public function store(Request $request)
    {
        $rule = [
            'name' => 'required',
            'username' => 'required',
        ];
        $customMessages = [
            'name.required' => 'Tên không được để trống!',
            'username.required' => 'Username không được để trống!'
        ];
        $this->validate($request, $rule, $customMessages);
        $data = json_decode($request->get('data'),true);
        $id = $data['id'];
        $User = User::updateOrCreate(
            ['id' => $id],
            [
                'name' => $data['name'],
                'username' => $data['username'],
                'password' => $data['password'],
                'active' => $data['active'],
                'level' => $data['level'],
                'HSVPTCNN' => json_encode($data['HSVPTCNN']),
                'HSNLD' => json_encode($data['HSNLD']),
                'HSTTTH' =>json_encode ($data['HSTTTH']),
                'LBHNV' => json_encode($data['LBHNV']),
                'DmTinhThanh' => json_encode($data['DmTinhThanh']),
                'DmQuanHuyen' => json_encode($data['DmQuanHuyen']),
                'DmPhuongXa' => json_encode($data['DmPhuongXa'])
            ]
        );
        $User->save();
        return response()->json(['success' => 'User saved successfully.', 'data' => $User]);
    }

    public function edit($id)
    {
        $User = User::where('id',$id)->select('id','level', 'password','name','username','active','HSVPTCNN','HSNLD','HSTTTH','LBHNV', 'DmTinhThanh', 'DmQuanHuyen', 'DmPhuongXa')->first();
        return response()->json($User);
    }
}
