@extends('master')
@section('title','SCEDFA OPTION CONFIG')
@section('content')

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.min.css')}} " />
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.toast.min.css')}} " />
@endpush

{{$dataTable->table(['id' => 'OptionConfig','class'=>'table table-striped table-bordered'])}}

{{-- modal create and edit --}}

@include('layouts.option.formOption')

@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('DataTables/datatables.min.js')}} "></script>
<script>
    $(function() {
      {{$dataTable->generateScripts()}}
      $('#button-close-modal').on('click',function(){
        let r = confirm("Bạn có muốn thoát!");
        if (r) {
          $('#form-OptionConfig').trigger("reset");
          $("form select").trigger("chosen:updated");
          $('#modal-OptionConfig').modal('hide');
        } else {
          $('#modal-OptionConfig').modal('show');
        }
      });
    })
  </script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}} "></script>
<script type="text/javascript" src="{{asset('chosen/chosen.jquery.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.yadcf.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/app.js')}} "></script>
<script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/lodash.min.js')}} "></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.toast.min.js')}} "></script>
@endpush
