// header

var table;
$(document).ready(async function () {

  $("input[type='checkbox']").each(function () {
    $(this).on("change", function () {
      this.value = this.checked ? 1 : 0;
    });
  });
  const DmQT = $('#QuocTich,#QuocTichTSC,#IdQuocTich');
  if (DmQT.length > 0) {
    $.ajax({
      url: route("DmQT"),
      dataType: "json",
      success: function (data) {
        $.map(DmQT, function (itemDmQT) {
          $(itemDmQT).chosen({
            placeholder_text_single: " ",
            width: "100%",
            no_results_text: 'Ấn nút Enter để thêm Quốc Tịch:'
          });
          $.map(data, function (item) {
            $(itemDmQT).append('<option value="' + item.MaQuocTich + '">' + item.QuocTich + "</option>");
          });
          $(itemDmQT).trigger("chosen:updated");
          var chosen = $(itemDmQT).data('chosen')
          chosen.dropdown.find('input').on('keyup', function (e) {
            if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0) {
              var QuocTich = this.value;
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
                data: {
                  'QuocTich': QuocTich,
                },
                url: route('AddDmQT'),
                type: "POST",
                dataType: "json",
                success: function success(data) {
                  var option = $("<option>").val(data.IdQuocTich).text(QuocTich);
                  $(itemDmQT).prepend(option);
                  $(itemDmQT).find(option).prop('selected', true);
                  $(itemDmQT).trigger("chosen:updated");
                },
              });
            }
          });
        });
      }
    });
  }

  // const DmCD = $('#IdChucDanh,#TTDK_ChucDanh');
  // if (DmCD.length > 0) {
  //   $.ajax({
  //     url: route("DmCD"),
  //     dataType: "json",
  //     success: function (data) {
  //       $.map(DmCD, function (itemDmCD) {
  //         $(itemDmCD).chosen({
  //           placeholder_text_single: " ",
  //           width: "100%",
  //           no_results_text: 'Ấn nút Enter để thêm Chức Danh:'
  //         });
  //         $.map(data, function (item) {
  //           $(itemDmCD).append('<option value="' + item.IdChucDanh + '">' + item.ChucDanh + '</option>');;
  //         });

  //         $(itemDmCD).trigger("chosen:updated");
  //         var chosen = $(itemDmCD).data('chosen')
  //         chosen.dropdown.find('input').on('keyup', function (e) {
  //           if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0) {
  //             var ChucDanh = this.value;
  //             $.ajaxSetup({
  //               headers: {
  //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //               }
  //             });
  //             $.ajax({
  //               data: {
  //                 'ChucDanh': ChucDanh,
  //               },
  //               url: route('AddDmCD'),
  //               type: "POST",
  //               dataType: "json",
  //               success: function success(data) {
  //                 var option = $("<option>").val(data.IdChucDanh).text(ChucDanh);
  //                 $(itemDmCD).prepend(option);
  //                 $(itemDmCD).find(option).prop('selected', true);
  //                 $(itemDmCD).trigger("chosen:updated");
  //               },
  //             });
  //           }
  //         });
  //       })
  //     }
  //   });
  // }

  const DmCD = $('#History_ChucDanh,#TTDK_ChucDanh,#CVHT_ChucDanh,#TCNN_ChucDanh');
  if (DmCD.length > 0) {
    $.ajax({
      url: route('DmCD'),
      dataType: "json",
      success: function (data) {
        $.map(data, function (item) {
          DmCD.append('<option value="' + item.IdChucDanh + '">' + item.ChucDanh + '</option>');
        });
        DmCD.trigger("chosen:updated");
      }
    });
    DmCD.chosen({
      placeholder_text_single: " ",
      width: "100%"
    });
  }

  const DmTonGiao = $('#IdTonGiao');
  if (DmTonGiao.length > 0) {
    $.ajax({
      url: route('DmTonGiao'),
      dataType: "json",
      success: function (data) {
        $.map(data, function (item) {
          DmTonGiao.append('<option value="' + item.MaTonGiao + '">' + item.TonGiao + '</option>');
        });
        DmTonGiao.trigger("chosen:updated");
      }
    });
    DmTonGiao.chosen({
      placeholder_text_single: " ",
      width: "100%"
    });
  }

  // const DmDanToc =  $('#IdDanToc');
  // if(DmDanToc.length>0){
  //   $.ajax({
  //       url: route('DmDanToc'),
  //       dataType: "json",
  //       success: function( data ) {
  //          $.map( data, function( item ) {
  //           DmDanToc.append('<option value="'+item.IdDanToc+'">' + item.TenDanToc + '</option>');
  //         });
  //         DmDanToc.trigger("chosen:updated");
  //       }
  //     });
  //     DmDanToc.chosen({
  //       placeholder_text_single: " ",
  //       width: "100%"
  //     });
  //   }
  const DmDanToc = $('#IdDanToc');
  if (DmDanToc.length > 0) {
    $.ajax({
      url: route("DmDanToc"),
      dataType: "json",
      success: function (data) {
        $.map(DmDanToc, function (itemDmDT) {
          $(itemDmDT).chosen({
            placeholder_text_single: " ",
            width: "100%",
            no_results_text: 'Ấn nút Enter để thêm Dân Tộc:'
          });
          $.map(data, function (item) {
            $(itemDmDT).append('<option value="' + item.MaDanToc + '">' + item.TenDanToc + "</option>");
          });
          $(itemDmDT).trigger("chosen:updated");
          var chosen = $(itemDmDT).data('chosen')
          chosen.dropdown.find('input').on('keyup', function (e) {
            if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0) {
              var TenDanToc = this.value;
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
                data: {
                  'TenDanToc': TenDanToc,
                },
                url: route('AddDmDT'),
                type: "POST",
                dataType: "json",
                success: function success(data) {
                  var option = $("<option>").val(data.IdDanToc).text(TenDanToc);
                  $(itemDmDT).prepend(option);
                  $(itemDmDT).find(option).prop('selected', true);
                  $(itemDmDT).trigger("chosen:updated");
                },
              });
            }
          });
        });
      }
    });
  }

  const maTT = $('#MaTT,#MaTT_KhaiSinh, #MaTT_NguyenQuan, #MaTT_HoKhau,#MaTT_ThuongTru, #MaTT_NoiLamViec, #TinhThanhLamViecFilter');
  if (maTT.length > 0) {
    $.ajax({
      url: route('DmTT'), dataType: "json", success: function (data) {
        $.map(maTT, function (itemmaTT) {
          if ($(itemmaTT)[0].id && $(itemmaTT)[0].id == 'TinhThanhLamViecFilter') {
            $(itemmaTT).chosen({
              placeholder_text_multiple: " ",
              width: "85%"
            });
          } else {
            $(itemmaTT).chosen({
              placeholder_text_single: " ",
            });
          }
          $(itemmaTT).append('<option value="">Select an Option</option>');
          $.map(data, function (item) {
            $(itemmaTT).append('<option value="' + item.MATT + '">' + item.TENTT + '</option>');
          });

          $(itemmaTT).trigger("chosen:updated");
        })
      }
    });
  }

  const DmQuanHuyen = $('#MaQU_KhaiSinh,#MaQU_NguyenQuan,#MaQU_HoKhau,#MaQU_ThuongTru');
  if (DmQuanHuyen.length > 0) {
    $.map(DmQuanHuyen, function (itemDmQuanHuyen) {
      $(itemDmQuanHuyen).chosen({
        placeholder_text_single: " ",
      });
      $(itemDmQuanHuyen).trigger("chosen:updated");
    })
  }

  const updateTT = $('#MATT_QU');
  if (updateTT.length > 0) {
    $.ajax({
      url: route('DmTT'), dataType: "json", success: function (data) {
        $.map(updateTT, function (itemmaTT) {
          $(itemmaTT).chosen({
            placeholder_text_single: " ",
          })
          $(itemmaTT).append('<option value="">Select an Option</option>');
          $.map(data, function (item) {
            $(itemmaTT).append('<option value="' + item.MATT + '">' + item.TENTT + ' - ' + item.MATT + '</option>');
          });

          $(itemmaTT).trigger("chosen:updated");
        })
      }
    });
  }

  const checkMavung = $('#MAVUNG1');
  if (checkMavung.length > 0) {
    $.ajax({
      url: route('maVung'), dataType: "json", success: function (data) {
        $.map(checkMavung, function (itemmaTT) {
          $(itemmaTT).chosen({
            placeholder_text_single: " ",
          })
          $(itemmaTT).append('<option value="">Select an Option</option>');
          $.map(data, function (item) {
            $(itemmaTT).append('<option value="' + item.MAVUNG + '">' + item.MAVUNG + '</option>');
          });

          $(itemmaTT).trigger("chosen:updated");
        })
      }
    });
  }
 
  const updateQH = $('#MAQU_PXA');
  if (updateQH.length > 0) {
    $.ajax({
      url: route('DmQuanHuyen'), dataType: "json", success: function (data) {
        $.map(updateQH, function (itemmaQH) {
          $(itemmaQH).chosen({
            placeholder_text_single: " ",
          });
          $(itemmaQH).append('<option value="">Select an Option</option>');
          $.map(data, function (item) {
            $(itemmaQH).append('<option value="' + item.MAQU + '">' + item.TENQUAN + ' - ' + item.MAQU + '</option>');
          });

          $(itemmaQH).trigger("chosen:updated");
        });
      }
    });
  }

  const DmPhuongXa = $('#MaPhuongXa_ThuongTru,#MaPhuongXa_NguyenQuan,#MaPhuongXa_HoKhau,#MaPhuongXa_KhaiSinh');
  if (DmPhuongXa.length > 0) {
    $.map(DmPhuongXa, function (itemDmPhuongXa) {
      $(itemDmPhuongXa).chosen({
        placeholder_text_single: " ",
      });
      $(itemDmPhuongXa).trigger("chosen:updated");
    })
  }


  const DiaBanHoatDong = $('#DiaBanHoatDong');
  if (DiaBanHoatDong.length > 0) {
    $.ajax({
      url: route('DmTT'),
      dataType: "json",
      success: function (data) {
        $.map(data, function (item) {
          DiaBanHoatDong.append('<option value="' + item.TENTT + '">' + item.TENTT + '</option>');
        });
        DiaBanHoatDong.trigger("chosen:updated");
      }
    });
    DiaBanHoatDong.chosen({
      placeholder_text_multiple: " ",
      width: "100%"
    });
  }

  const DmTCNNFilter = $('#DmTCNNFilter');
  if (DmTCNNFilter.length > 0) {
    $.ajax({
      url: route('DmTCNN'),
      dataType: "json",
      success: function (data) {
        DmTCNNFilter.append('<option value="">Select an Option</option>');
        $.map(data, function (item) {
          DmTCNNFilter.append('<option value="' + item.IdTCNN + '">' + item.TenTiengVietTCNN + ' - [' + item.TenTatTCNN + ']</option>');
        });
        DmTCNNFilter.trigger("chosen:updated");
      }
    });
    DmTCNNFilter.chosen({
      placeholder_text_multiple: " ",
      width: "85%"
    });
  }


  const dmTC = $('#LoaiTCNN');
  if (dmTC.length > 0) {
    $.ajax({
      url: route('DmTC'),
      dataType: "json",
      success: function (data) {
        $.map(data, function (item) {
          dmTC.append('<option value="' + item.IdToChuc + '">' + item.TenToChuc + '</option>');
        });
        dmTC.trigger("chosen:updated");
      }
    });
    dmTC.chosen({
      placeholder_text_single: " ",
      width: "100%"
    });
  }
  $(".datepicker").daterangepicker({
    autoUpdateInput: false,
    showDropdowns: true,
    singleDatePicker: true,
    linkedCalendars: false,
    locale: {
      format: "DD/MM/YYYY",
      cancelLabel: "Clear"
    }
  });
  $(".datepicker").on("apply.daterangepicker", function (
    ev,
    picker
  ) {
    $(this).val(
      picker.startDate.format("DD/MM/YYYY")
    );
  });
  $(".datepicker").on('show.daterangepicker', function (ev, picker) {
    if (picker.element.offset().top - $(window).scrollTop() + picker.container.outerHeight() > $(window).height()) {
      picker.drops = 'up';
    } else {
      picker.drops = 'down';
    }
    picker.move();
  });
  table = $.fn.dataTable.tables({ visible: true, api: true });
  table.on("select deselect", function (e, dt, type, indexes) {
    dt.button(".buttons-edit").enable(dt.rows({ selected: true }).any());
    dt.button(".buttons-bangluongNV").enable(dt.rows({ selected: true }).any());
    dt.button(".buttons-detail_employee").enable(dt.rows({ selected: true }).any());
    dt.button(".buttons-bangluongTCNN").enable(dt.rows({ selected: true }).any());
    dt.button(".buttons-hoSoTTThuHo").enable(dt.rows({ selected: true }).any());
    dt.button(".buttons-delete").enable(dt.rows({ selected: true }).any());
    dt.button(".buttons-edit_salary_1").enable(dt.rows({ selected: true }).any());
  });
  table.on("draw", function (e, dt, type, indexes) {
    table.button(".buttons-edit").enable(table.rows({ selected: true }).any());
    table.button(".buttons-bangluongNV").enable(table.rows({ selected: true }).any());
    table.button(".buttons-detail_employee").enable(table.rows({ selected: true }).any());
    table.button(".buttons-bangluongTCNN").enable(table.rows({ selected: true }).any());
    table.button(".buttons-hoSoTTThuHo").enable(table.rows({ selected: true }).any());
    table.button(".buttons-delete").enable(table.rows({ selected: true }).any());
    table.button(".buttons-edit_salary_1").enable(table.rows({ selected: true }).any());
    $('table input[type="checkbox"]').on("change", function () {
      const element = this;
      $.ajax({
        url: route("thongBaoNghiViec", [$(element).val(), $(element).is(":checked")]),
        type: "GET",
        dataType: "json",
        success: function (data) {
          const elementTR = $(element).parent().parent().parent();
          if ($(element).is(":checked")) {
            // $(element).attr('disabled',true);
            elementTR.addClass("bg-warning");
          } else {
            elementTR.removeClass("bg-warning");
            elementTR.removeClass("bg-danger");
          }
          $.toast({
            heading: "Success",
            text: data.success,
            showHideTransition: "fade",
            icon: "success"
          });
        }
      });
    });
  });
  $(".filter-table").on("click", function () {
    yadcf.exFilterExternallyTriggered(table);
    table.rows(".selected").deselect();
  });
  $(".reset-table").on("click", function () {
    yadcf.exResetAllFilters(table);
    table.rows(".selected").deselect();
  });

  $(".navbar-light .dmenu").hover(
    function () {
      $(this)
        .find(".sm-menu")
        .first()
        .stop(true, true)
        .slideDown(150);
    },
    function () {
      $(this)
        .find(".sm-menu")
        .first()
        .stop(true, true)
        .slideUp(105);
    }
  );
  $('.daterangerpicker').daterangepicker({
    autoUpdateInput: false,
    showDropdowns: true,
    linkedCalendars: false,
    locale: {
      cancelLabel: "Clear"
    }
  });

  $(".daterangerpicker").on("apply.daterangepicker", function (ev, picker) {
    $(this).val(
      picker.startDate.format("YYYY/MM/DD") +
      "-" +
      picker.endDate.format("YYYY/MM/DD")
    );
  });
  $("input[type='text'], input[type='number']").on("click", function () {
    $(this).select();
  });
});
