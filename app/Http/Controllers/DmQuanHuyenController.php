<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DmQuanHuyen;
use App\DataTables\DmQuanHuyenDataTable;

class DmQuanHuyenController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DmQuanHuyenDataTable $dataTable)
    {
        return $dataTable->render('layouts.quanhuyen.quanhuyen');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $rule = [
            'MAQU' => 'required|unique:DmQuanHuyen|min:5|max:5',
            'MATT_QU' => 'required',
            'TENQUAN' => 'required',
            //'IdQuan' => 'required|min:2|max:2',
        ];
        $customMessages = [
            'MAQU.required' => 'Mã quận huyện không được để trống!',
            'MAQU.unique' => 'Trùng mã quận huyện, mời nhập lại!',
            'MAQU.min' => "Mã quận huyện chỉ bao gồm 5 kí tự!",
            'MAQU.max' => "Mã quận huyện chỉ bao gồm 5 kí tự!",
            'MATT_QU.required' => 'Tên tỉnh thành không được để trống!',
            'TENQUAN.required' => 'Tên quận huyện không được để trống!',
            'IdQuan.required' => 'Id quận huyện không được để trống!',
            'IdQuan.min' => "Id quận huyện chỉ bao gồm 2 kí tự!",
            'IdQuan.max' => "Id quận huyện chỉ bao gồm 2 kí tự!",
        ];
        if ($request->get('isUpdateQuanHuyen')) {
            $MAQU = $request->get('MAQU');
            $rule = [
                'MAQU' => "required|unique:DmQuanHuyen,MAQU,{$MAQU},MAQU",
                //'IdQuan' => "required|min:2|max:2",
            ];
            $customMessages = [
                'MAQU.required' => 'Mã quận huyện không được để trống!',
                'MAQU.unique' => 'Trùng mã quận huyện, mời nhập lại!',
                'MAQU.min' => "Mã quận huyện chỉ bao gồm 5 kí tự!",
                'MAQU.max' => "Mã quận huyện chỉ bao gồm 5 kí tự!",
                'IdQuan.required' => 'Id quận huyện không được để trống!',
                'IdQuan.min' => "Id quận huyện chỉ bao gồm 2 kí tự!",
                'IdQuan.max' => "Id quận huyện chỉ bao gồm 2 kí tự!",
            ];
        }
        $this->validate($request,$rule,$customMessages);
        $MAQU = $request->get('MAQU');

        $DmQuanHuyen = DmQuanHuyen::updateOrCreate(
            ['MAQU' => $MAQU],
            [
                'MATT_QU' => $request->get('MATT_QU'),
                'TENQUAN' => $request->get('TENQUAN'),
                'IdQuan' => $request->get('IdQuan')
            ]
        );
        $DmQuanHuyen->save();
        return response()->json(['success' => 'Quận huyện được lưu thành công.', 'data'=> $DmQuanHuyen]);
    }

    public function edit($MAQU)
    {
        $DmQuanhuyen  = DmQuanHuyen::find($MAQU);
        return response()->json($DmQuanhuyen);
    }
}
