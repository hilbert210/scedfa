<form method="POST" id="form-DmTinhThanh" class="form-DmTinhThanh">
    @csrf
    <div class="modal fade " id="modal-DmTinhThanh" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cập nhật Tỉnh Thành</h5>
                    <button type="button" id="button-close-modal" class="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="row align-items-center">
                            <div class="col-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" name="inputMATT" id="inputMATT">Mã tỉnh thành</span>
                                    </div>
                                    <input type="text" class="form-control" name="MATT" id="MATT">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Tên tỉnh thành</span>
                                    </div>
                                    <input type="text" class="form-control" name="TENTT" id="TENTT">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Mã vùng</span>
                                    </div>
                                    <input type="text" class="form-control" name="MAVUNG" id="MAVUNG">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Id tỉnh thành</span>
                                    </div>
                                    <input type="text" class="form-control" name="IdTinhThanh" id="IdTinhThanh">
                                </div>
                            </div>

                            <div class="col-6">
                                <fieldset style="text-align: justify;">
                                    <legend>Hướng dẫn cập nhật Tỉnh thành</legend>
                                    <h6>- Mã vùng: </h6>
                                    <ol>
                                        <li>Đồng bằng Sông Hồng</li>
                                        <li>Đông Bắc Bộ</li>
                                        <li>Tây Bắc Bộ</li>
                                        <li>Bắc Trung Bộ</li>
                                        <li>Duyên hải Nam Trung Bộ</li>
                                        <li>Tây Nguyên</li>
                                        <li>Đông Nam Bộ</li>
                                        <li>Đồng bằng Sông Cửu Long</li>
                                    </ol>
                                    <h6>- Mã tỉnh thành: 
                                    <ul>
                                        <li>Mã tỉnh thành <b>KHÔNG</b> được trùng.</li>
                                        <li>Kiểm tra mã tỉnh thành cuối cùng ứng với mã vùng trước khi thêm tỉnh thành mới.</li>
                                        <li>Bao gồm 3 chữ số, chữ số thứ nhất là mã vùng, hai chữ số còn lại là số lẻ tăng dần bắt đầu từ 01 (Ví dụ: Mã vùng là 1 thì mã tỉnh thành bắt đầu từ 101, mã vùng là 2 thì mã tỉnh thành bắt đầu từ 201).</li>
                                    </ul>    
                                    <h6>- Id tỉnh thành: Mã tỉnh thành của số căn cước công dân. Tham khảo: <a target="_blank" href="https://nhankiet.vn/vi/w2787/Ma-tinhthanh-pho-cua-so-can-cuoc-cong-dan-CCCD.html">Mã tỉnh thành của số căn cước công dân.</a></h6>
                                </fieldset>

                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="saveBtn-DmTinhThanh" class="btn btn-primary-outline">Save Changes</button>
                </div>
            </div>
        </div>
    </div>
</form>