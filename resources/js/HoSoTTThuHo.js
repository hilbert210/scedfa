var table;
$(document).ready(async function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    table = new $.fn.dataTable.Api('#HoSoTTThuHo');
    yadcf.init(
        table,
        [
        ],
        { externally_triggered: true }
    );
    $("#filterHoSoTTThuHo").on("click", function() {
        yadcf.exFilterExternallyTriggered(table);
    });
    $("#resetHoSoTTThuHo").on("click", function() {
        $('input[name="filterRangeMonth"]').val('');
        $('#DmTCNNFilter').val("");
        $('#DmTCNNFilter').trigger("chosen:updated");
        yadcf.exResetAllFilters(table);
    });
    $('.money').autoNumeric('init',{
        mDec: 0
    });
    // $(".daterangerpickerStartEnd").daterangepicker({
    //     autoUpdateInput: false,
    //     showDropdowns: true,
    //     linkedCalendars: false,
    //     locale: {
    //         cancelLabel: "Clear"
    //     }
    // });
    const DmTCNN =  $('#IdTCNN');
    if(DmTCNN.length>0){
        $.ajax({
            url: route('DmTCNN'),
            dataType: "json",
            success: function( data ) {
                $.map(DmTCNN,function(itemDmTCNN){
                    $(itemDmTCNN).chosen({
                      placeholder_text_single: " ",
                    });
                    $.map( data, function( item ) {
                      $(itemDmTCNN).append('<option value="'+item.IdTCNN+'">' + item.TenTiengVietTCNN + ' - [' + item.TenTatTCNN + ']</option>');
                    });
                    
                    $(itemDmTCNN).trigger("chosen:updated");
                  })
            }
          });
        DmTCNN.chosen({
            placeholder_text_single: " ",
            width: "100%"
        });
    }
    $('#chi_tiet :input').change(function(){

        let total = 0;
        $('#chi_tiet .money').map(function(index,element){
            total = total +  parseInt($(element).autoNumeric('get')?$(element).autoNumeric('get'):0);
        });
        $('#TongTienNop').autoNumeric('set', total);
    });
    
    function soSanh(IdTCNN,dateRange){
        $.ajax({
            url: route("getSalaryByMonth"),
            data:{'IdTCNN':IdTCNN ,'dateRange':dateRange} ,
            dataType: "json",
            type: 'GET',
            success: function(data) {
                const DichVu = data.DichVu;
                if(DichVu){

                }else{

                }
            }
        }); 
    }
    // $(".daterangerpickerStartEnd").on("apply.daterangepicker", function(
    //     ev,
    //     picker
    // ) { 
    //     const dateRange = moment(picker.startDate).startOf("month").format("DD/MM/YYYY") +
    //     "-" + moment(picker.endDate).endOf("month").format("DD/MM/YYYY");
    //     $(this).val(dateRange);
    //     if($('#IdTCNN').val()){
    //        soSanh($('#IdTCNN').val(), dateRange)
    //     }else{
    //         $('#IdTCNN ~ .chosen-container').addClass('border border-danger');
    //         $.toast({
    //             heading: "Error",
    //             text: 'Xin hãy chọn TCNN Trước',
    //             showHideTransition: "fade",
    //             position: 'top-right',
    //             icon: "error"
    //         });
    //     }
    // });
    $('input[name="filterRangeMonth"]').daterangepicker({
        showDropdowns: true,
        autoUpdateInput: false,
        // startDate: moment().startOf('month').format('YYYY/MM/DD'),
        // endDate: moment().endOf('month').format('YYYY/MM/DD'),
        locale: {
            cancelLabel: "Clear",
            format: 'YYYY/MM/DD'
        }
    });
    $('input[name="filterRangeMonth"]').on("apply.daterangepicker", function(
        ev,
        picker
    ) {
        $(this).val(
            moment(picker.startDate).startOf('month').format('YYYY/MM/DD') +
                "-" +
            moment(picker.endDate).endOf('month').format('YYYY/MM/DD') 
        );
    });
});
