<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BHXHExport implements WithMultipleSheets
{
    use Exportable;

    public function __construct(Collection $collection,$parameters)
    {
        $this->collection = $collection;
        $filter = [
        ];
        $this->filter = $filter;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        
        $sheets[] = new BaoCaoBHXH($this->collection, $this->filter);
        $sheets[] = new PhuLucHGD($this->collection, $this->filter);

        return $sheets;
    }
}