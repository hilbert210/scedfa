<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $guarded  = ['id'];
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $table = 'Options';
    /**
     * get system configuration
     */
    public static function getSystemConfig($optKey = '') {
        $conf = [];
        $sysConf = config('scedfa.SysConf');
        $keys = array_keys($sysConf); // array key config
        if ($optKey) {
            $option = Option::where('name', $optKey)->first();
            return ($option && $option->value) ? $option->value : $sysConf[$optKey];
        }
        $options = Option::select(['name', 'value'])->get()->toArray();
        foreach ($keys as $key) {
            foreach ($options as $row) {
                if ($row["name"] == $key) {
                    $conf[$key] = $row["value"];
                    break;
                } else {
                    $conf[$key] =  $sysConf[$key];
                }
            }
        }
        return $conf;
    }
}