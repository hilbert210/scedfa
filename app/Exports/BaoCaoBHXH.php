<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use App\Option;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use Illuminate\Support\Facades\DB;

class BaoCaoBHXH implements FromCollection, WithCustomStartCell, WithEvents, WithMapping, WithDrawings, WithTitle, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    /**
     * @var Collection
     */
    protected $collection;
    protected $filter;
    /**
     * HoSoNLDExport constructor.
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection, $parameters)
    {
        $this->collection = $collection;
        $filter = [];
        $this->filter = $filter;
    }

    /**
     * @return BaseDrawing|BaseDrawing[]
     */
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('This is my logo');
        $drawing->setPath(public_path('/img/logo.png'));
        $drawing->setHeight(83);
        $drawing->setOffsetX(80);
        $drawing->setCoordinates('B1');
        return $drawing;
    }

    private function loadOption($dropDownCol, $options, $rcount, $lcount, $event, $prompt = null) {
        // set dropdown list for first data row
        $validation = $event->sheet->getCell("{$dropDownCol}10")->getDataValidation();
        $validation->setType(DataValidation::TYPE_LIST);
        $validation->setErrorStyle(DataValidation::STYLE_INFORMATION);
        $validation->setAllowBlank(false);
        $validation->setShowInputMessage(true);
        $validation->setShowErrorMessage(true);
        $validation->setShowDropDown(true);
        $validation->setErrorTitle('Input error');
        $validation->setError('Value is not in list.');
        //$validation->setPromptTitle('Pick from list');
        if ($prompt) {
            $validation->setPrompt($prompt);
        }
        //$option_conv = " " . implode(", ", $options) . " ";
        //$validation->setFormula1(sprintf('"%s"', implode(',', $options)));
        $validation->setFormula1(sprintf('"%s"', implode(',', $options)));
        // clone validation to remaining rows
        
        for ($i = 11; $i <= $rcount; $i++) {
            $event->sheet->getCell("{$dropDownCol}{$i}")->setDataValidation(clone $validation);
        }

        // set columns to autosize
        for ($i = 1; $i <= $lcount; $i++) {
            $column = Coordinate::stringFromColumnIndex($i);
            $event->sheet->getColumnDimension($column)->setAutoSize(true);
        }
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $HighestRow = $event->sheet->getDelegate()->getHighestRow();
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK],
                        ],
                    ],
                ];
                //dropdown
                $row_count = $event->sheet->getHighestRow();
                $column_count = $event->sheet->getHighestColumn();
                $lstPA = [
                    "Tăng mới",
                    "Tăng mới chuyển từ nơi khác đến",
                    "Tăng mới do chuyển tỉnh khác đến",
                    "Nghỉ đi làm lại",
                    "Nghỉ thai sản đi làm lại",
                    'Bổ sung tăng nguyên lương',
                    "Truy đóng theo MLTT tại thời điểm",
                    "Tăng mức đóng",
                    //"Điều chỉnh chức vụ chức danh nghề điều kiện công việc",
                    //"Điều chỉnh chức danh",
                    "Tăng BHYT",
                    "Tăng BHTN",
                    "Giảm hẳn",
                    "Nghỉ ốm dài ngày",
                    "Nghỉ thai sản",
                    "Giảm đến nơi khác",
                    "Giảm đến tỉnh khác",
                    // "Bổ sung giảm nguyên lương",
                    // "Giảm mức đóng",
                    // "Giảm BHYT",
                    // "Giảm BHTN",
                    // "Nghỉ không lương",
                    // "Giảm quỹ HTTT (Hưu trí tử tuất)",
                    // "Tăng quỹ HTTT (Hưu trí tử tuất)",
                    // "Điều chỉnh lương  (Lao động ký HĐLĐ từ 1 đến dưới 3 tháng)",
                    // "Truy đóng HTTT (Hưu trí tử tuất)"
                ];

                //$this->loadOption('B', $lstPA, $row_count, $column_count, $event);
                $options = [
                    'Có',
                    'Không'
                ];

                $this->loadOption('F', $options, $row_count, $column_count, $event);
                $this->loadOption('J', $options, $row_count, $column_count, $event);
                $genders = [
                    'Nam',
                    'Nữ'
                ];
                $this->loadOption('K', $genders, $row_count, $column_count, $event);
                $this->loadOption('BE', $options, $row_count, $column_count, $event);
                $this->loadOption('BF', $options, $row_count, $column_count, $event);
                $mavung = [
                    'K1',
                    'K2',
                    'K3',
                    'K4',
                    'K5'
                ];
                $mavungLTT = [
                    '01',
                    '02',
                    '03',
                    '04'
                ];
                $prompt = '01- Vùng I: 4.420.000 đồng/tháng
                02- Vùng II : 3.920.000 đồng/tháng
                03- Vùng III : 3.420.000 đồng/tháng
                04- Vùng IV : 3.070.000 đồng/tháng';
                $this->loadOption('O', $mavung, $row_count, $column_count, $event);
                $this->loadOption('H', $mavungLTT, $row_count, $column_count, $event, $prompt);
                $muchuongBH = [
                    '1','2','3','4','5'
                ];
                $this->loadOption('G', $muchuongBH, $row_count, $column_count, $event);
                
                // text center
                $event->sheet->getDelegate()->getStyle('A1:BF' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('center')->setVertical('center');
                //border table
                $event->sheet->getDelegate()->getStyle('A7:BF' . ($HighestRow + 1))
                    ->applyFromArray($styleArray);
                // title
                 $event->sheet->getDelegate()
                ->setCellValue('B1', 'BÁO CÁO BẢO HIỂM MẪU DC-02')->mergeCells('B1:K1')
                    ->setCellValue('B2', 'Tháng ' . strtoupper(date('m/Y', strtotime(str_replace('/', '-', $this->collection[0]["Ngày Tính Lương"])))))->mergeCells('B2:K2')
                    // ->setCellValue('D4', 'Tên đơn vị:')
                    // ->setCellValue('F4', $this->collection[0]["TCNN"])->mergeCells('F4:I4')
                    ->setCellValue('A7', 'TT')->mergeCells('A7:A9')
                    ->setCellValue('B7', 'Phương Án')->mergeCells('B7:B9')
                    ->setCellValue('C7', 'Họ và Tên')->mergeCells('C7:C9')
                    ->setCellValue('D7', 'Mã số BHXH')->mergeCells('D7:D9')
                    ->setCellValue('E7', 'Số sổ BHXH')->mergeCells('E7:E9')
                    ->setCellValue('F7', 'Đã có sổ (Có/Không)')->mergeCells('F7:F9')
                    ->setCellValue('G7', 'Mức hưởng BHYT')->mergeCells('G7:G9')
                    ->setCellValue('H7', 'Mã vùng LTT')->mergeCells('H7:H9')
                    ->setCellValue('I7', 'Ngày sinh(Chỉ nhập năm nếu chỉ nhớ năm sinh)')->mergeCells('I7:I9')
                    ->setCellValue('J7', 'Chỉ có năm sinh (Có/Không)')->mergeCells('J7:J9')
                    ->setCellValue('K7', 'Giới tính (Nam/Nữ)')->mergeCells('K7:K9')
                    ->setCellValue('L7', 'Vị trí')->mergeCells('L7:L9')
                    ->setCellValue('M7', 'Phòng ban')->mergeCells('M7:M9')
                    ->setCellValue('N7', 'Nơi làm việc')->mergeCells('N7:N9')
                    ->setCellValue('O7', 'Mã vùng sinh sống')->mergeCells('O7:O9')
                    ->setCellValue('P7', 'Tiền lương(VNĐ)')->mergeCells('P7:P9')
                    ->setCellValue('Q7', 'Hệ số lương')->mergeCells('Q7:Q9')
                    ->setCellValue('R7', 'Phụ cấp chức vụ')->mergeCells('R7:R9')
                    ->setCellValue('S7', 'Phụ cấp TNVK')->mergeCells('S7:S9')
                    ->setCellValue('T7', 'Phụ cấp TN nghề')->mergeCells('T7:T9')
                    ->setCellValue('U7', 'Phụ cấp lương')->mergeCells('U7:U9')
                    ->setCellValue('V7', 'Phụ cấp các khoản bổ sung')->mergeCells('V7:V9')
                    ->setCellValue('W7', 'Từ tháng')->mergeCells('W7:W9')
                    ->setCellValue('X7', 'Đến tháng')->mergeCells('X7:X9')
                    ->setCellValue('Y7', 'Tỷ lệ đóng')->mergeCells('Y7:Y9')
                    ->setCellValue('Z7', 'Số văn bản')->mergeCells('Z7:Z9')
                    ->setCellValue('AA7', 'Ngày ban hành')->mergeCells('AA7:AA9')
                    ->setCellValue('AB7', 'Ghi chú')->mergeCells('AB7:AB9')
                    ->setCellValue('AC7', 'Dân tộc')->mergeCells('AC7:AC9')
                    ->setCellValue('AD7', 'Quốc tịch')->mergeCells('AD7:AD9')
                    ->setCellValue('AE7', 'Số CMT')->mergeCells('AE7:AE9')
                    ->setCellValue('AF7', 'Nơi cấp giấy khai sinh')->mergeCells('AF7:AF9')
                    ->setCellValue('AG7', 'Địa chỉ liên hệ')->mergeCells('AG7:AG9')
                    ->setCellValue('AH7', 'Tỉnh KCB')->mergeCells('AH7:AH9')
                    ->setCellValue('AI7', 'Bệnh viện')->mergeCells('AI7:AI9')
                    ->setCellValue('AJ7', 'Mức đóng BHXH')->mergeCells('AJ7:AJ9')
                    ->setCellValue('AK7', 'Phương thức đóng BHXH')->mergeCells('AK7:AK9')
                    ->setCellValue('AL7', 'Nội dung thay đổi')->mergeCells('AL7:AL9')
                    ->setCellValue('AM7', 'Tài liệu kèm theo')->mergeCells('AM7:AM9')
                    ->setCellValue('AN7', 'Số điện thoại')->mergeCells('AN7:AN9')
                    ->setCellValue('AO7', 'Mã hộ gia đình')->mergeCells('AO7:AO9')
                    ->setCellValue('AP7', 'Ngày bắt đầu vị trí nhà quản lý')->mergeCells('AP7:AP9')
                    ->setCellValue('AQ7', 'Ngày kết thúc vị trí nhà quản lý')->mergeCells('AQ7:AQ9')
                    ->setCellValue('AR7', 'Ngày bắt đầu chuyên môn vị trí kỹ thuật bậc cao(ngày/tháng năm)')->mergeCells('AR7:AR9')
                    ->setCellValue('AS7', 'Ngày kết thúc chuyên môn vị trí kỹ thuật bậc cao')->mergeCells('AS7:AS9')
                    ->setCellValue('AT7', 'Ngày bắt đầu chuyên môn vị trí kỹ thuật bậc trung')->mergeCells('AT7:AT9')
                    ->setCellValue('AU7', 'Ngày kết thúc chuyên môn vị trí kỹ thuật bậc trung')->mergeCells('AU7:AU9')
                    ->setCellValue('AV7', 'Ngày bắt đầu vị trí làm việc khác')->mergeCells('AV7:AV9')
                    ->setCellValue('AW7', 'Ngày kết thúc vị trí làm việc khác')->mergeCells('AW7:AW9')
                    ->setCellValue('AX7', 'Ngày bắt đầu ngành nghề nặng nhọc')->mergeCells('AX7:AX9')
                    ->setCellValue('AY7', 'Ngày kết thúc ngành nghề nặng nhọc')->mergeCells('AY7:AY9')
                    ->setCellValue('AZ7', 'Ngày bắt đầu HĐLĐ không xác định thời hạn')->mergeCells('AZ7:AZ9')
                    ->setCellValue('BA7', 'Ngày bắt đầu HĐLĐ xác định thời hạn')->mergeCells('BA7:BA9')
                    ->setCellValue('BB7', 'Ngày kết thúc HĐLĐ xác định thời hạn')->mergeCells('BB7:BB9')
                    ->setCellValue('BC7', 'Ngày bắt đầu HĐLĐ khác(dưới 1 tháng, thử việc)')->mergeCells('BC7:BC9')
                    ->setCellValue('BD7', 'Ngày kết thúc HĐLĐ khác(dưới 1 tháng, thử việc)')->mergeCells('BD7:BD9')
                    ->setCellValue('BE7', 'Giảm BHXH')->mergeCells('BE7:BE9')
                    ->setCellValue('BF7', 'Tính lãi')->mergeCells('BF7:BF9');
                $event->sheet->getColumnDimension('B')->setAutoSize(false);
                $event->sheet->getColumnDimension('B')->setWidth(32);
                $event->sheet->getDelegate()->getStyle('B' . ($HighestRow + 6) . ':BF' . ($HighestRow + 6))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'bold'      =>  true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('BF' . ($HighestRow + 5))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  13,
                        'italic'    => true
                    )
                ));
                $event->sheet->getDelegate()->getStyle('BF' . ($HighestRow + 5))
                    ->getAlignment()->setWrapText(false)->setHorizontal('right');
                $event->sheet->getDelegate()->getStyle('A1:BF9')->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  10,
                        'bold'      =>  true
                    )
                ));

                $event->sheet->getDelegate()->getStyle('A' . ($HighestRow + 1) . ':BF' . ($HighestRow + 4))->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      => 10,
                        'bold'      =>  true
                    )
                ));

                $event->sheet->getDelegate()->getStyle('A10:BF' . $HighestRow)->applyFromArray(array(
                    'font' => array(
                        'name'      =>  'Times New Roman',
                        'size'      =>  10,
                    )
                ));
                $event->sheet->getDelegate()->getStyle('B10:B' . ($HighestRow))
                    ->getAlignment()->setWrapText(true)->setHorizontal('left')->setVertical('center');
                $event->sheet->getDelegate()->getStyle('C10:L' . ($HighestRow + 1))
                    ->getAlignment()->setWrapText(true)->setHorizontal('right')->setVertical('center');
                // Add column STT
                $event->sheet->getDelegate()->setCellValue('A7', 'STT');
                for ($i = 10; $i <= $HighestRow; $i++) {
                    $event->sheet->getDelegate()->setCellValue('A' . $i, $i - 9);
                }
            },
        ];
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return $this->collection;
    }

    public function columnFormats(): array
    {
        return [
            'C' => '#,##0',
            'D' => '####',
            'E' => '####',
            'F' => '#,##0',
            'G' => '#,##0',
            'H' => '#,##0',
            'I' => '#,##0',
            'J' => '#,##0',
            'K' => '#,##0',
            'L' => '#,##0',
            'M' => '#,##0',
            'N' => '#,##0',
            'O' => '#,##0',
            'P' => '#,##0',
            'Q' => '#,##0',
            'R' => '#,##0',
            'S' => '#,##0',
            'T' => '#,##0',
            'U' => '#,##0',
            'V' => '#,##0',
            'W' => '#,##0',
            'X' => '#,##0',
            'Y' => '#,##0',
            'Z' => '####',
            'AA' => '#,##0',
            'AB' => '#,##0', 
            'AC' => '#,##0',
            'AD' => '#,##0',
            'AE' => '####',
            'AF' => '#,##0',
            'AG' => '#,##0',
            'AH' => '#,##0',
            'AI' => '#,##0',
            'AJ' => '#,##0',
            'AK' => '#,##0',
            'AL' => '#,##0',
            'AM' => '#,##0',
            'AN' => '#,##0',
            'AO' => '#,##0',
            'AP' => '#,##0',
            'AQ' => '#,##0',
            'AR' => '#,##0',
            'AS' => '#,##0',
            'AT' => '#,##0',
            'AU' => '#,##0',
            'AV' => '#,##0',
            'AW' => '#,##0',
            'AX' => '#,##0',
            'AY' => '#,##0',
            'AZ' => '#,##0',
            'BA' => '#,##0',
            'BB' => '#,##0',
            'BC' => '#,##0',
            'BD' => '#,##0',
            'BE' => '#,##0',
            'BF' => '#,##0',
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'BaoHiem';
    }

    /**
     * @return string
     */
    public function startCell(): string
    {
        return (string) 'B10';
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        //BHXH, YT and BHTN
        $luong = $row["Luong"];
        if ($row["USD"]) {
            $luong =  $row["Luong"] * $row["TyGia"];
        }
        $bhxh = $bhtn = $luong;
        if ($luong > $row["GoiHanTinhBHXH"]) {
            $bhxh = $row["GoiHanTinhBHXH"];
        }
        if ($luong > $row["GoiHanTinhBHTN"]) {
            $bhtn = $row["GoiHanTinhBHTN"];
        }

        //luong va phu cap
        $bhxhVaPc = $bhxh;
        $bhtnVaPc = $bhtn;
        $phucap = 0;
        if (isset($row["PhuCap"]) && $row["PhuCap"] > 0) {
            $phucap =  $row["PhuCap"];
            if ($row["USD"]) {
                $bhxhVaPc = $bhxhVaPc + $phucap * $row["TyGia"];
                $bhtnVaPc = $bhtnVaPc + $phucap * $row["TyGia"];
            } else {
                $bhxhVaPc = $bhxhVaPc + $phucap;
                $bhtnVaPc = $bhtnVaPc + $phucap;
            }
        }

        // $bhCSD = $bhxh*($row["PhanTram_CSD"]-1)/100 + 0.01*$bhtn;
        // $bhNLD = $bhxh*($row["PhanTram_NLD"]-1)/100 + 0.01*$bhtn;
        $conf = Option::getSystemConfig();
        $bhxh_csd = $conf['BHXH_BHYT_CSD'] / 100;
        $bhtn_csd = $conf['BHTN_CSD'] / 100;
        $bhxh_nld = $conf['BHXH_BHYT_NLD'] / 100;
        $bhtn_nld = $conf['BHTN_NLD'] / 100;
        $bhCSD = $bhxh * $bhxh_csd + $bhtn_csd * $bhtn;
        $bhNLD = $bhxh * $bhxh_nld + $bhtn_nld * $bhtn;
        $ngayHDKXD = '';//ngay hd khong xac dinh thoi han
        $ngayBDHDXDTH = '';//ngay bat dau hop dong xac dinh thoi han
        $ngayKTHDXDTH = '';
        $ngayBDHDKhac = '';
        $ngayKTHDKhac = '';
        if ($row['LoaiHopDong'] == 'Xác Định Thời Hạn') {
            $ngayHD = explode('-', $row['ThoiGianHopDong']);
            if (!empty($ngayHD)) {
                $ngayBDHDXDTH = (isset($ngayHD[0])) ? $ngayHD[0] : '';
                $ngayKTHDXDTH = (isset($ngayHD[1])) ? $ngayHD[1] : '';
            }
        }
        if ($row['LoaiHopDong'] == 'Không Xác Định Thời Hạn') {
            $ngayHDKXD = $row['ThoiGianHopDong'];
        }
        if ($row['LoaiHopDong'] == 'Thử Việc') {
            $ngayHD = $row['ThoiGianHopDong'];
            if (!empty($ngayHD)) {
                $ngayBDHDKhac = $ngayBDHDXDTH = (isset($ngayHD[0])) ? $ngayHD[0] : '';
                $ngayKTHDKhac = $ngayBDHDXDTH = (isset($ngayHD[1])) ? $ngayHD[1] : '';
            }
        }
        return [
            $row['Phương Án'],
            $row["Họ"] . ' ' . $row["Tên"],
            $row['MaBHYT'] ? substr($row['MaBHYT'], strlen($row['MaBHYT']) - 10) : '', //ma bao hiem
            $row["SoSoBHXH"],
            $row["DaCoSo"] ? "Có" : "Không",//da co so ?
            '',//muc huong BHYT
            '', //Ma vung LTT
            $row["NgaySinh"] ? date('d/m/Y', strtotime($row["NgaySinh"])) : '',//ngay sinh
            '', //chi co nam sinh ?
            $row['GioiTinh'], //gioi tinh
            $row["ChucDanh"],//vi tri
            $row['TenTatTCNN'] ? $row['TenTatTCNN'] : '', //phong ban
            $row["NoiLamViec"],
            $row["MAVUNG"] ? 'K'. $row["MAVUNG"]: '', //Ma vung sinh song
            $luong, //tien luong (vnd)
            $row["HeSo"],//he so luong
            $row['HeSoCV'] ? $row['HeSoCV'] : '', //phu cap chuc vu
            $row["PhuCapVuotKhung"], //phu cap TNVK
            $row["PhuCapNghe"], //phu cap TN Nghe
            $phucap,//phu cap luong
            '', //phu cap cac khoan bo sung
            strtoupper(date('m/Y', strtotime(str_replace('/', '-', $this->collection[0]["Ngày Tính Lương"])))), //tu thang
            strtoupper(date('m/Y', strtotime(str_replace('/', '-', $this->collection[0]["Ngày Tính Lương"])))),//den thang
            $row['TyLeDong'] ? $row['TyLeDong'] : '', //ty le dong
            $row["SoVBChapThuan"], //so van ban
            $row["NgayVBChapThuan"] ? strtoupper(date('m/Y', strtotime(str_replace('/', '-', $row["NgayVBChapThuan"])))) : '',//ngay ban hanh
            $row["GhiChu"],//ghi chu
            $row['TenDanToc'],//dan toc
            $row['QuocTich'], //quoc tich
            $row["SoCMND"], //so cmt
            $row["SoNha_KhaiSinh"], //noi cap giay khai sinh
            $row["SoNha_HoKhau"],//dia chi lien he
            $row["TENTT"], //tinh kcb
            $row["TenBenhVien"], //benh vien
            $bhxhVaPc,//muc dong BHXH
            $row['PhuongThucDong'] ? $row['PhuongThucDong'] . " Năm": '',//phuong thuc dong BHXH
            '',//noi dung thay doi
            '', //tai lieu kem theo
            $row["DienThoaiNhaRieng"],//so dien thoai
            '',//ma ho gia dinh
            '',//ngay bat dau vi tri nha quan ly
            '',//ngay ket thuc vi tri nha quan ly
            '',//ngay bat dau vi tri chuyen mon ky thuat bac cao
            '',//ngay ket thuc vi tri chuyen mon ky thuat bac cao
            '',//ngay bat dau vi tri chuyen mon ky thuat bac trung
            '',//ngay bat dau vi tri chuyen mon ky thuat bac trung
            '',//ngay bat dau vi tri lam viec khac
            '',//ngay ket thuc vi tri lam viec khac
            '',//ngay bat dau nganh nghe nang nhoc
            '',//ngay ket thuc nganh nghe nang nhoc
            $ngayHDKXD,//ngay bat dau HĐLĐ khong xac dinh thoi han
            $ngayBDHDXDTH,//ngay bat dau HĐLĐ xac dinh thoi han
            $ngayKTHDXDTH, //ngay ket thuc HĐLĐ xac dinh thoi han
            $ngayBDHDKhac, //ngay bat dau hop dong lao dong khac
            $ngayKTHDKhac,//ngay ket thuc hop dong lao dong khac
            '', //giam BHYT
            $row["TinhLai"] ? "Có" : "Không",//tinh lai
        ];
    }
}