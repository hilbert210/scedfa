<form method="POST" id="form-EditSalary" class="form-EditSalary">
    @csrf
    <div class="modal fade " id="modal-EditSalary" tabindex="-1" role="dialog"
      aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Chỉnh Sửa Lương Nhân Viên</h5>
            <button type="button" id="button-close-modal-edit" class="close" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body p-4" style="min-height: 40rem;">
            <div class="row">
              <div class="col-6">
                <input type="hidden" class="form-control" name="IdLuong" id="IdLuong">
                <input type="hidden" class="form-control" name="NgayTinhLuong" id="NgayTinhLuong">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Họ</span>
                  </div>
                  <input readonly class="form-control" name="Ho" id="Ho"/>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Tên</span>
                  </div>
                  <input readonly class="form-control" name="Ten" id="Ten"/>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Lương Cũ</span>
                  </div>
                  <input class="form-control money" name="LuongCu" id="LuongCu"/>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Lương Mới</span>
                  </div>
                  <input class="form-control money" name="LuongMoi" id="LuongMoi"/>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">USD</span>
                  </div>
                  <input style="margin: auto" type="checkbox" name="USD" id="USD" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Ngày Hiệu Lực</span>
                  </div>
                  <input class="form-control datepicker" name="NgayHieuLuc" id="NgayHieuLuc" />
                </div>
                
              </div>
              <div class="col-6">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Số người phụ thuộc</span>
                  </div>
                  <input class="form-control" type="number" name="SoNguoiPhuThuoc" id="SoNguoiPhuThuoc" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Thu Nhập Khác</span>
                  </div>
                  <input class="form-control money" name="ThuNhapKhac" id="ThuNhapKhac" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Đối Tượng</span>
                  </div>
                  <select class="form-control" name="DoiTuong" id="DoiTuong">
                    <option value=""></option>
                    @foreach (config('scedfa.DoiTuong') as $key=>$val)
                      <option value="{{ $key }}">{{ $val}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Phương Án</span>
                  </div>
                  <select class="form-control" name="MaPA" id="MaPA">
                    <option value=""></option>
                    @foreach ( config('scedfa.PhuongAn') as $key => $val)
                      <option value="{{ $key }}">{{ $val}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Tiền Lãi</span>
                  </div>
                  <input class="form-control money" name="Lai" id="Lai" />
                </div>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Ghi Chú</span>
                  </div>
                  <textarea class="form-control" name="GhiChu" id="GhiChu"></textarea>
                </div>
              </div>
              <input type="hidden" name="IdHoSoNhanVienTCNN" value="" id="IdHoSoNhanVienTCNN"/>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" id="saveBtnEditSalary" class="btn btn-primary-outline">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
  </form>