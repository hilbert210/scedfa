<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoSoTTThuHo extends Model
{
    protected $guarded  = ['IdChungTu'];
    protected $primaryKey = "IdChungTu";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'NopTienTCNN';
}