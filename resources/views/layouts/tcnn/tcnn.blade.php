@extends('master')
@section('title','SCEDFA TCNN')
@section('content')

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.min.css')}} " />
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.toast.min.css')}} " />
@endpush

<div class="filter p-3">
  <div class=" d-flex justify-content-around p-1">
    <div class="input-group row col ">
      <div class="input-group-prepend col-12">
        <span>Trạng Thái TCNN: </span>
      </div>
      <div id="ThoiSuDungFilter" class="ThoiSuDungFilter col-12"></div>
    </div>

    <div class="input-group row col">
      <div class="input-group-prepend col-12">
        <span>Địa Bàn Hoạt Động: </span>
      </div>
      <div id="DiaBanHoatDongFilter" class="DiaBanHoatDongFilter col-12"></div>
    </div>

    <div class="input-group row col">
      <div class="input-group-prepend col-12">
        <span>Loại Hình TC,CN NN: </span>
      </div>
      <div id="ToChucFilter" class="ToChucFilter col-12"></div>
    </div>

    <div class="input-group row col">
      <div class="input-group-prepend col-12">
        <span>Trạng Thái ĐKHĐ: </span>
      </div>
      <div id="GiaHanFilter" class="GiaHanFilter col-12"></div>
    </div>

    <div class="input-group row col">
      <div class="input-group-prepend col-12">
        <span>Loại Giấy Phép: </span>
      </div>
      <select id="LoaiGPFilter" class="LoaiGPFilter col-12">
        <option>
          <option />
      </select>
    </div>

    <div class="input-group NgayThoiGiaHanFilter d-none row col" id="NgayThoiGiaHanFilter">
      <div class="input-group-prepend col-12">
        <span for="ThoiSuDung">Ngày Thôi Hoạt Động: </span>
      </div>
      <div class="d-flex flex-row col-12">
        <input type="text" class="daterangerpicker" style="height: fit-content;" id="filterRangeDate" name="datefilter" value="" />
      </div>
    </div>
    <div class="input-group row col">
      <div class="input-group-prepend col-12">
        <span>Loại Export: </span>
      </div>
      <select id="LoaiExport" class="LoaiExport col-12" style="width: 85%">
        <option value="1" selected>Dùng cho báo cáo </option>
        <option value="2">Dùng cho công việc hằng ngày </option>
      </select>
    </div>
    <div class="input-group row col ml-3">
      <div class="input-group-prepend col-12">
      <span>Trạng Thái Tham Gia BHXH </span>
      </div>
      <select id="ThamGiaBHXHFilter" class="ThamGiaBHXHFilter" style="width: 85%">
        <option value="all">Tất Cả</option>  
        <option value="1">Tham Gia BHXH </option>
        <option value="0">Không Tham Gia BHXH </option>
      </select>
    </div>
  </div>
  <div class="d-flex justify-content-center">
    <button type="button" id="filterTCNN" style="width: 6rem" class="btn btn-primary m-2">Filter</button>
    <button type="button" id="resetTCNN" style="width: 6rem" class="btn btn-secondary m-2">Reset</button>
  </div>
</div>

{{$dataTable->table(['id' => 'tcnn','class'=>'table table-striped table-bordered','style'=>"width: -webkit-fill-available;"])}}

{{-- modal create and edit --}}

@include('layouts.tcnn.formTcNN')

@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('DataTables/datatables.min.js')}} "></script>
<script>
  $(function() {
    {{$dataTable->generateScripts()}}
    $('#button-close-modal').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-tcnn').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-tcnn').modal('hide');
        $( "form .text-danger").remove();
        $( "form .pb-0").removeClass('pb-0')
        $('#file-dinh-kem tbody').empty();
        $( "form .border-danger").removeClass('border border-danger')
      } else {
        $('#modal-tcnn').modal('show');
      }
    });
    $('#button-close-modal-VPLVTCT').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#modal-VPLVTCT').modal('hide');
      } else {
        $('#modal-VPLVTCT').modal('show');
      }
    });
    $('#button-close-modal-DmChucDanh-tcnn').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#modal-DmChucDanh-tcnn').modal('toggle');
      } else {
        $('#modal-DmChucDanh-tcnn').modal('show');
      }
    });
  })
</script>

<script type="text/javascript" src="{{asset('js/moment.min.js')}} "></script>
<script type="text/javascript" src="{{asset('chosen/chosen.jquery.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.yadcf.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/app.js')}} "></script>
<script type="text/javascript" src="{{asset('js/tcnn.js')}} "></script>
<script type="text/javascript" src="{{asset('js/lodash.min.js')}} "></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.toast.min.js')}} "></script>
@endpush
