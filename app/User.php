<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $guarded  = ['id'];
    protected $primaryKey = "id";
    public function getRememberToken()
    {
        return $this->remember_token;
    }
    public $incrementing = true;
    public $timestamps = false;
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }
    protected $table = 'users';
}
