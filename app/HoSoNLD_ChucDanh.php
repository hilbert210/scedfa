<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoSoNLD_ChucDanh extends Model
{
    protected $guarded  = ['Id'];
    protected $primaryKey = "Id";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'HoSoNhanVienTCNN_ChucDanh';
}
