@extends('master')
@section('title','SCEDFA | Thống Kê Lương Nhân Viên ')
@section('content')

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('DataTables/datatables.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/daterangepicker.min.css')}} " />
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.toast.min.css')}} " />
@endpush

<div class="filter p-3">
  <div class="d-flex flex-wrap p-3">
    <div class="input-group row col-1 ">
      <div class="input-group-prepend col-8">
        <span>Tháng:</span>
      </div>
      <select class="d-flex flex-row col-8" name="filterMonth" id="filterMonth">
        <option value=""></option>
        @foreach( range(1,12) as $m)
          <option value="{{ $m }}">{{ $m < 10 ? '0'.$m : $m }}</option>
        @endforeach
      </select>
    </div>
    <div class="input-group row col-1 ">
      <div class="input-group-prepend col-10">
        <span>Năm:</span>
      </div>
      <select class="d-flex flex-row col-10" name="filterYear" id="filterYear">
        <option value=""></option>
        @foreach ( range( now()->year - 100, now()->year + 100  ) as $y)
          <option value="{{ $y }}">{{ $y }}</option>
        @endforeach
      </select>
    </div>
    <div class="input-group row col-2 ">
      <div class="input-group-prepend col-12">
        <span>Tên Nhân Viên:</span>
      </div>
      <div class="d-flex flex-row col-12">
        <input type="text" id="filterTenNhanVien" name="filterTenNhanVien" value="" />
      </div>
    </div>
    <div class="input-group row col-2 ">
      <div class="input-group-prepend col-12">
        <span>Tạo Từ:</span>
      </div>
      <div class="d-flex flex-row col-12">
        <input type="text" id="filterRangeMonth" name="filterRangeMonth" value="" />
      </div>
    </div>

    <div class="input-group row col-2">
      <div class="col-12">
        <span>Lương (VNĐ):</span>
      </div>
      <div class="input-group-prepend">
        <select name="operator" id="operator">
          <option value="<=">&#60;=</option>
          <option value=">=">&#62;=</option>
          <option value="<">&#60;</option>
          <option value=">">&#62;</option>
        </select>
      </div>
      <input type="number" class="form-control" id="luong" name="luong" value="">
    </div>

  </div>
  <div class="d-flex flex-wrap p-3">
    <div class="input-group row col-2">
      <div class="input-group-prepend col-12">
        <span >Đối Tượng</span>
      </div>
      <select class="form-control d-flex flex-row col-12" name="DoiTuong" id="filterDoiTuong">
        <option value=""></option>
        <option value="1">Nhân Viên Mới</option>
        <option value="2">Nhân Viên Nghỉ Việc</option>
        <option value="3">Tăng Lương</option>
        <option value="4">Giảm Lương</option>
        <option value="5">Thai Sản</option>
        <option value="6">Truy Thu</option>
      </select>
    </div>
    <div class="input-group row col-2">
      <div class="input-group-prepend col-12">
        <span >Tổ Chức</span>
      </div>
      <select class="form-control d-flex flex-row col-12" name="ToChuc" id="filterToChuc">
        <option value=""></option>
        @if(isset($tochuc) && $tochuc->count())
          @foreach($tochuc as $row)
            <option value="{{ $row->Id }}">{{ $row->TenToChuc}}</option>
          @endforeach
        @endif
      </select>
    </div>
    <div class="input-group row col-2">
      <div class="input-group-prepend col-12">
        <span >Phương Án</span>
      </div>
      <select class="form-control d-flex flex-row col-12" name="PhuongAn" id="filterPA">
        <option value=""></option>
        <option value="AD">Bổ sung tăng nguyên lương</option>
        <option value="AT">Truy đóng thoe MLCS tại thời điểm</option>
        <option value="CD">Điều chỉnh chức danh</option>
        <option value="DC">Điều chỉnh lương</option>
        <option value="DN">Điều chỉnh tham gia thất nghiệp (MLCS thời điểm)</option>
        <option value="GC">Giảm do chuyển tỉnh</option>
        <option value="GD">Giảm do chuyển đơn vị</option>
        <option value="GH">Giảm hẳn</option>
        <option value="GN">Giảm tham gia thất nghiệp</option>
        <option value="KL">Nghĩ Không lương</option>
        <option value="OF">Nghĩ do ốm đau/nghĩ không lương</option>
        <option value="ON">Đi làm lại</option>
        <option value="SB">Bổ Sung giảm nguyên lương</option>
        <option value="TC">Tăng do chuyển tỉnh</option>
        <option value="TD">Tăng do chuyển đơn vị</option>
        <option value="TM">Tăng mới</option>
        <option value="TN">Tăng tham gia thất nghiệp</option>
        <option value="TS">Thai sản</option>
        <option value="TT">Bổ sung tăng quỹ KCB</option>
        <option value="TU">Bổ sung giảm quỹ KCB</option>
      </select>
    </div>
    <div class="input-group row col-2">
      <div class="input-group-prepend col-12">
        <span>Tiền tệ: </span>
      </div>
      <select id="USD" name="USD" class="form-control USD col-12">
        <option value="">ALL</option>
        <option value="1">USD</option>
        <option value="0">VNĐ</option>
      </select>
    </div>
  </div>
  <div class="d-flex justify-content-center">
    <button type="button" id="filterLuongNhanVien" style="width: 6rem" class="btn btn-primary m-2">Filter</button>
    <button type="button" id="resetLuongNhanVien" style="width: 6rem" class="btn btn-secondary m-2">Reset</button>
  </div>
  <input type="hidden" id="exportLuongNhanVienBH" value="" />
</div>

{{$dataTable->table(['id' => 'LuongNhanVien','class'=>'table table-striped table-bordered'])}}

{{-- modal create and edit --}}

@include('layouts.luongNhanVien.formLuongNhanVien')
@include('layouts.luongNhanVien.formCreateSalaryAll')
@include('layouts.luongNhanVien.formEditSalary')
@include('layouts.hoSoNLD.formHoSoNLD')

@endsection

@push('scripts')
<script type="text/javascript" src="{{asset('DataTables/datatables.min.js')}} "></script>
<script>
  $(function() {
    {{$dataTable->generateScripts()}}
    $('#button-close-modal-LuongNhanVien').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-LuongNhanVien').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-LuongNhanVien').modal('hide');
      } else {
        $('#modal-LuongNhanVien').modal('show');
      }
    });

    $('#button-close-modal').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-HoSoNLD').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-HoSoNLD').modal('hide');
      } else {
        $('#modal-HoSoNLD').modal('show');
      }
    });
    
    $('#button-close-modal-create').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-CreateSalaryAll').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-CreateSalaryAll').modal('hide');
      } else {
        $('#modal-CreateSalaryAll').modal('show');
      }
    });
    $('#button-close-modal-edit').on('click',function(){
      let r = confirm("Bạn có muốn thoát!");
      if (r) {
        $('#form-EditSalary').trigger("reset");
        $("form select").trigger("chosen:updated");
        $('#modal-EditSalary').modal('hide');
      } else {
        $('#modal-EditSalary').modal('show');
      }
    });
  })
</script>

<script type="text/javascript" src="{{asset('js/moment.min.js')}} "></script>
<script type="text/javascript" src="{{asset('chosen/chosen.jquery.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/jquery.dataTables.yadcf.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/daterangepicker.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/app.js')}} "></script>
<script type="text/javascript" src="{{asset('js/autoNumeric.min.js')}} "></script>
<script type="text/javascript" src="{{asset('js/LuongNhanVien.js')}} "></script>
<script type="text/javascript" src="{{asset('js/lodash.min.js')}} "></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.toast.min.js')}} "></script>
@endpush