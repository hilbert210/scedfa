<?php
namespace App\Transformers;

use App\LuongNhanVien;
use League\Fractal\TransformerAbstract;

class NhanVienTransformer extends TransformerAbstract
{
    /**
     * @return array
     */
    public function transform(LuongNhanVien $LuongNhanVien)
    {
        return [
            'IdLuong'         => $LuongNhanVien->IdLuong,
            'NgayTinhLuong'         => $LuongNhanVien->NgayTinhLuong,
            'Ho'         => (string) $LuongNhanVien->Ho,
            'Ten'       => (string)$LuongNhanVien->Ten,
            'TenTiengVietTCNN'      => (string) ($LuongNhanVien->TenTiengAnhTCNN ? $LuongNhanVien->TenTiengAnhTCNN : $LuongNhanVien->TenTiengVietTCNN),
            'LuongMoi' =>$LuongNhanVien->USD ? '$ '. number_format( $LuongNhanVien->LuongMoi) : number_format( $LuongNhanVien->LuongMoi) .' VND' ,
            'GhiChu' => (string) $LuongNhanVien->GhiChu,
            'MaPA' => (string) config('scedfa.PhuongAn.'.$LuongNhanVien->MaPA),
            'ChucDanh' => (String) $LuongNhanVien->ChucDanh,
            'TyGia' => $LuongNhanVien->TyGia,
            'TyGia_Insurance' => $LuongNhanVien->TyGia_Insurance,
            'USD' => $LuongNhanVien->USD,
            'PhuCap_USD' =>  $LuongNhanVien->PhuCap_USD,
            'ThuNhapKhac_USD' =>  $LuongNhanVien->ThuNhapKhac_USD,
            'SoNgayLamViec' => $LuongNhanVien->SoNgayLamViec,
            'SoNgayNghiCoLuong' => $LuongNhanVien->SoNgayNghiCoLuong,
            'LuongTheoSoNgayLamViec' => $LuongNhanVien->LuongTheoSoNgayLamViec,
            'PhiDichVu' => $LuongNhanVien->PhiDichVu,
            'PhanTram_CSD' => $LuongNhanVien->PhanTram_CSD,
            'PhanTram_NLD' => $LuongNhanVien->PhanTram_NLD,
            'SI_Company' => $LuongNhanVien->SI_Company,
            'HI_Company' => $LuongNhanVien->HI_Company,
            'UI_Company' => $LuongNhanVien->UI_Company,
            'Luong' => $LuongNhanVien->LuongMoi,
            'ThuNhapKhac' => $LuongNhanVien->ThuNhapKhac,
            'GoiHanTinhBHTN' => $LuongNhanVien->GoiHanTinhBHTN,
            'GoiHanTinhBHXH' => $LuongNhanVien->GoiHanTinhBHXH,
            'GhiChu' => $LuongNhanVien->GhiChu,
            'TenTatTCNN' => $LuongNhanVien->TenTatTCNN,
            'SoNguoiPhuThuoc' => $LuongNhanVien->SoNguoiPhuThuoc,
            'PhuCapDongBH' => ($LuongNhanVien->PhuCapDongBH > 0 ) ? ($LuongNhanVien->PhuCap_USD ? '$ ' . number_format($LuongNhanVien->PhuCapDongBH) : number_format($LuongNhanVien->PhuCapDongBH) . ' VND ') : '',
            'PhuCap' => ($LuongNhanVien->PhuCapDongBH > 0) ? $LuongNhanVien->PhuCapDongBH : '',
            'TrichNop_BHXH' => $LuongNhanVien->TrichNop_BHXH,
            'BaoHiemYTe' => $LuongNhanVien->BaoHiemYTe,
            'BaoHiemThatNghiep' => $LuongNhanVien->BaoHiemThatNghiep,
            'IdHoSoNhanVienTCNN' => $LuongNhanVien->IdHoSoNhanVienTCNN,
            'DieuChinhLuong' => $LuongNhanVien->DieuChinhLuong,
            'SoSoBHXH' => $LuongNhanVien->SoSoBHXH,
            'SoCMND' => $LuongNhanVien->SoCMND,
            'NgaySinh' => $LuongNhanVien->NgaySinh,
            'GioiTinh' => $LuongNhanVien->GioiTinh,
            'NoiLamViec' => $LuongNhanVien->NoiLamViec,
            'HeSo' => $LuongNhanVien->HeSo,
            'TinhLai' => $LuongNhanVien->TinhLai,
            'DienThoaiNhaRieng' => $LuongNhanVien->DienThoaiNhaRieng,
            'PhuCapNghe' =>  $LuongNhanVien->PhuCapNghe,
            'PhuCapVuotKhung' =>  $LuongNhanVien->PhuCapVuotKhung,
            'DaCoSo' => $LuongNhanVien->DaCoSo,
            'MAVUNG' => $LuongNhanVien->MAVUNG,
            'ChucDanh' => $LuongNhanVien->ChucDanh,
            'QuocTich' => $LuongNhanVien->QuocTich,
            'TenDanToc' => $LuongNhanVien->TenDanToc,
            'SoNha_HoKhau' => $LuongNhanVien->SoNha_HoKhau,
            'TENTT' => $LuongNhanVien->TENTT,
            'TenBenhVien' => $LuongNhanVien->TenBenhVien,
            'SoNha_KhaiSinh' => $LuongNhanVien->SoNha_KhaiSinh,
            'SoVBChapThuan' => $LuongNhanVien->SoVBChapThuan,
            'NgayVBChapThuan' => $LuongNhanVien->NgayVBChapThuan,
            'PhuongThucDong' => $LuongNhanVien->PhuongThucDong,
            'TyLeDong' => $LuongNhanVien->TyLeDong,
            'HeSoCV' => $LuongNhanVien->HeSoCV,
            'LoaiHopDong' => $LuongNhanVien->LoaiHopDong,
            'ThoiGianHopDong' => $LuongNhanVien->ThoiGianHopDong,
            'MaBHYT' => $LuongNhanVien->MaBHYT,
            'Phi_CK' => $LuongNhanVien->Phi_CK
        ];
    }
}