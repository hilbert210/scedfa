var table;
$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
    table = new $.fn.dataTable.Api('#HoSoNLD');

    function Address(type) {
        $('#MaTT_' + type).on("change", function() {
            const MaTT = $(this).val();
            if (MaTT) {
                $.ajax({
                    url: route('DmQuanHuyenId', MaTT),
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $('#MaQU_' + type).empty();
                        $('#MaQU_' + type).prop('disabled', false).trigger("chosen:updated");
                        $('#MaPhuongXa_' + type).empty();
                        $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
                        $('#MaQU_' + type).append('<option value=""></option>');
                        $.map(data, function(item) {
                            $('#MaQU_' + type).append('<option value="' + item.MAQU + '">' + item.TENQUAN + '</option>');
                        });
                        $('#MaQU_' + type).trigger("chosen:updated");
                    }
                });
            } else {
                $('#MaQU_' + type).empty();
                $('#MaQU_' + type).prop('disabled', true).trigger("chosen:updated");
                $('#MaPhuongXa_' + type).empty();
                $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
                $('#SoNha_' + type).val('');
            }
        });
        $('#MaQU_' + type).on("change", function() {
            const MaQU = $(this).val();
            if (MaQU) {
                $.ajax({
                    url: route('DmPhuongXaId', MaQU),
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $('#MaPhuongXa_' + type).empty();
                        $('#MaPhuongXa_' + type).append('<option value=""></option>');
                        $('#MaPhuongXa_' + type).prop('disabled', false).trigger("chosen:updated");
                        $.map(data, function(item) {
                            $('#MaPhuongXa_' + type).append('<option value="' + item.MAPHUONGXA + '">' + item.TENPXA + '</option>');
                        });
                        $('#MaPhuongXa_' + type).trigger("chosen:updated");
                    }
                });
            } else {
                $('#MaPhuongXa_' + type).empty();
                $('#MaPhuongXa_' + type).prop('disabled', true).trigger("chosen:updated");
            }
        });
    }

    Address('KhaiSinh');
    Address('NguyenQuan');
    Address('HoKhau');
    Address('ThuongTru');

    const DmTCNN = $('#IdTCNN, #TTDK_IdTCNN');
    if (DmTCNN.length > 0) {
        $.ajax({
            url: route('DmTCNN'),
            dataType: "json",
            success: function(data) {
                $.map(DmTCNN, function(itemDmTCNN) {
                    $(itemDmTCNN).chosen({
                        placeholder_text_single: " ",
                    });
                    $.map(data, function(item) {
                        $(itemDmTCNN).append('<option value="' + item.IdTCNN + '">' + item.TenTiengVietTCNN + ' - [' + item.TenTatTCNN + ']</option>');
                    });

                    $(itemDmTCNN).trigger("chosen:updated");
                })
            }
        });
        DmTCNN.chosen({
            placeholder_text_single: " ",
            width: "100%"
        });
    }
    $('#TinhTrangHoSoFilter').chosen({
        placeholder_text_single: " ",
        width: "85%"
    });
    $('#TinhTrangHoSoFilter').trigger("chosen:updated");
    yadcf.init(
        table, [{
            column_number: 3,
            filter_type: "text",
            filter_delay: 500,
            filter_default_label: "",
            select_type_options: { theme: "bootstrap", width: "85%" }
        }],
    );
    $("#filterHoSoNLD").on("click", function() {
        yadcf.exFilterExternallyTriggered(table);
    });
    $("#resetHoSoNLD").on("click", function() {
        $('#LoaiTCNN').val("");
        $('#LoaiTCNN').trigger("chosen:updated");
        $('#DmTCNNFilter').val("");
        $('#DmTCNNFilter').trigger("chosen:updated");
        $('#TinhThanhLamViecFilter').val("");
        $('#TinhThanhLamViecFilter').trigger("chosen:updated");
        $('#TinhTrangHoSoFilter').val("");
        $('#TinhTrangHoSoFilter').trigger("chosen:updated");
        $('#filterRangeMonth').val("");
        yadcf.exResetAllFilters(table);
    });

    $('select[name="TinhTrangHoSo"]').on('change', function() {
        if (this.value == '0') {
            $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(4)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(1)').removeClass('hide');
            $('.nav-tabs-bottom li:nth-child(1) a').tab('show');
        }
        if (this.value == '1') {
            $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(3)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
            $('.nav-tabs-bottom li:nth-child(2)').removeClass('hide');
            $('.nav-tabs-bottom li:nth-child(2) a').tab('show');
        }
        if (this.value == '2') {
            $('.nav-tabs-bottom li:nth-child(1)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(2)').addClass('hide');
            $('.nav-tabs-bottom li:nth-child(4)').removeClass('hide');
            $('.nav-tabs-bottom li:nth-child(3)').removeClass('hide');
            $('.nav-tabs-bottom li:nth-child(3) a').tab('show');
        }
    });

    $('.chucVuHienTai select, .chucVuHienTai input, .chucVuHienTai table').on('change', function() {
        $('.update-CD').removeClass('hide');
    });
    $('#button-upload-avatar').on('click', function() {
        $('#upload-avatar').click();
    })
    $('#upload-avatar').change(function(event) {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            let filename = $('#upload-avatar').val();
            filename = filename.substring(filename.lastIndexOf('\\') + 1);
            reader.onload = function(e) {
                $('#picture').attr('src', e.target.result);
                $('#picture').hide();
                $('#picture').fadeIn(500);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#button-upload-file-dinh-kem').on('click', function() {
        $('#upload-file-dinh-kem').click();
    })
    $('#upload-file-dinh-kem').change(function(event) {
        let dataURLs = new FormData();
        $('#loader').removeClass('d-none');
        $.each($('#upload-file-dinh-kem')[0].files, function(i, file) {
            dataURLs.append('file[' + i + ']', file);
        });
        dataURLs.append('Idtype', $('#MaNhanVien').val());
        dataURLs.append('type', 1);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            data: dataURLs,
            url: route('uploadfile'),
            type: "POST",
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data) {
                    $('#file-dinh-kem tbody').empty();
                    data.forEach(function(item, index) {
                        $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                        </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"  
                        onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                    });

                }
                $.toast({
                    heading: "Success",
                    text: "Upload file thành công",
                    showHideTransition: "fade",
                    icon: "success"
                });
                $('#loader').addClass('d-none');
            },
            error: function(data) {
                $.toast({
                    heading: "Error",
                    text: "Upload file không thàng công",
                    showHideTransition: "fade",
                    icon: "error"
                });
                $('#loader').addClass('d-none');
            }
        });
    });
    window.removeFile = function(IdPicture, name) {
        let dataURLs = new FormData();
        $('#loader').removeClass('d-none');
        dataURLs.append('IdLoai', $('#MaNhanVien').val());
        dataURLs.append('loai', 1);
        dataURLs.append('IdPicture', IdPicture);
        dataURLs.append('name', name);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            data: dataURLs,
            url: route('removeFile'),
            type: "POST",
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data) {
                    $('#file-dinh-kem tbody').empty();
                    data.forEach(function(item, index) {
                        $('#file-dinh-kem tbody').append(`<tr><td>${item.IdPicture}</td><td> <a target="_blank" href="${item.url}">${item.name}
                    </a></td><td><button type="button" class="btn btn-primary btn-sm mb-2 mt-1 remove-file"  
                    onclick="window.removeFile('${item.IdPicture}','${item.name}')">remove</button></td></tr>`);
                    });

                }
                $.toast({
                    heading: "Success",
                    text: "Xóa file thành công",
                    showHideTransition: "fade",
                    icon: "success"
                });
                $('#loader').addClass('d-none');
            },
            error: function(data) {
                $.toast({
                    heading: "Error",
                    text: "Xóa file không thành công",
                    showHideTransition: "fade",
                    icon: "error"
                });
                $('#loader').addClass('d-none');
            }
        });
    };

    function loadTableHistoryHSVN(dataID) {
        const historyHSNV = new $.fn.DataTable.Api('#historyHSNV');
        historyHSNV.clear().draw(false);
        $.get(route('QuaTrinhLamViec', dataID), function(data) {
            if (data) {
                if (!$.fn.DataTable.isDataTable("#historyHSNV")) {
                    $("#historyHSNV").DataTable({
                        lengthMenu: [
                            [3, 10, 20, -1],
                            [3, 10, 20, "All"]
                        ],
                        select: true,
                        buttons: ['edit', 'delete'],
                        dom: '<"top row"<"col-6"B><"col-6"p>>rt'
                    });
                }
                const table = $.fn.DataTable.Api('#historyHSNV');
                table.on("select deselect", function(e, dt, type, indexes) {
                    dt.button(".buttons-edit").enable(dt.rows({ selected: true }).any());
                });
                data.forEach(function(item, index) {
                    const ngayKetThuc = item.NgayKetThuc ? moment(item.NgayKetThuc).format('DD/MM/YYYY') : '';
                    table.row.add([
                        item.Id,
                        moment(item.NgayThayDoiChucDanh).format('DD/MM/YYYY'),
                        ngayKetThuc,
                        item.ChucDanh,
                        item.TenTiengVietTCNN,
                        item.TENTT
                    ]).draw(false);
                });
            }
        });
    }
    $('#button-update-CD').on('click', function() {
        IdHoSoNhanVienTCNN = $("#IdHoSoNhanVienTCNN").val();
        NgayThayDoiChucDanh = $("#NgayThayDoiChucDanh").val();
        IdChucDanh = $("#CVHT_ChucDanh").val();
        IdTCNN = $("#IdTCNN").val();
        MaTT_NoiLamViec = $("#MaTT_NoiLamViec").val();
        NoiLamViec = $("#NoiLamViec").val();
        SoVBNghiViec = $("#SoVBNghiViec").val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            data: {
                'IdHoSoNhanVienTCNN': IdHoSoNhanVienTCNN,
                'NgayThayDoiChucDanh': NgayThayDoiChucDanh,
                'CVHT_ChucDanh': IdChucDanh,
                'IdTCNN': IdTCNN,
                'MaTT_NoiLamViec': MaTT_NoiLamViec,
                'NoiLamViec': NoiLamViec,
                'SoVBNghiViec': SoVBNghiViec
            },
            url: route('updateChucDanh'),
            type: "POST",
            dataType: "json",
            success: function(data) {
                $('.update-CD').addClass('hide');
                $.toast().reset("all");
                $.toast({
                    heading: "Success",
                    text: data.success,
                    showHideTransition: "fade",
                    icon: "success"
                });
                const dataHoSoNLD = $.fn.DataTable.Api('#HoSoNLD').rows('.selected').data()[0];
                const dataID = dataHoSoNLD[Object.keys(dataHoSoNLD)[0]];
                loadTableHistoryHSVN(dataID);
                table.cell(table.row({ selected: true }).index(), 5).data(data.ChucDanh);
            },
            error: function(data) {
                const errors = data.responseJSON.errors;
                $("form .text-danger").remove();
                $("form .pb-0").removeClass('pb-0')
                $("form .border-danger").removeClass('border border-danger')
                $.each(errors, function(key, value) {
                    const ItemDOM = document.getElementById(key);
                    const ErrorMessage = value;
                    ItemDOM.parentElement.classList.add('pb-0');
                    ItemDOM.parentElement.insertAdjacentHTML('afterend', `<div class="text-danger text-right">${ErrorMessage}</div>`);
                    ItemDOM.classList.add('border', 'border-danger');
                });
            }
        });
    })
    $('input[name="filterRangeMonth"]').daterangepicker({
        showDropdowns: true,
        autoUpdateInput: false,
        // startDate: moment().startOf('month').format('YYYY/MM/DD'),
        // endDate: moment().endOf('month').format('YYYY/MM/DD'),
        locale: {
            cancelLabel: "Clear",
            format: 'YYYY/MM/DD'
        }
    });
    $('input[name="filterRangeMonth"]').on("apply.daterangepicker", function(
        ev,
        picker
    ) {
        $(this).val(
            moment(picker.startDate).startOf('month').format('YYYY/MM/DD') +
            "-" +
            moment(picker.endDate).endOf('month').format('YYYY/MM/DD')
        );
    });
});