<!DOCTYPE html>
<html lang="en">
@routes

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @stack('css')
</head>

<body>
    @auth
    <div class="container-fluid">
        <div class="row fixed-footer">
            <div class="header col-12 mb-1">
                @include('header')
            </div>
            <div class="col-12">
                @endauth
                @yield('content')
                @auth
            </div>
        </div>
        <div class="row w-100 footer justify-content-center">
            @include('footer')
        </div>
    </div>
    @endauth
    <div id="loader" class="d-none"></div>
    @stack('scripts')
    <script>
        $(function() {
            $('#button-close-modal-updatePassword').on('click',function(){
            let r = confirm("Bạn có muốn thoát!");
            if (r) {
                $('#form-updatePassword').trigger("reset");
                $('#modal-updatePassword').modal('hide');
            } else {
                $('#modal-updatePassword').modal('show');
            }
            });
            $('#changePassword').on('click',function(){
            $('#modal-updatePassword').modal('show');
            $('#saveBtnCP').on('click',function(e){
            $("#saveBtnCP").off('click');
            $(this).html("Sending..");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let data = new FormData($("#form-updatePassword")[0]);
            $.ajax({
                data:data,
                url: route('updatePassword'),
                type: "POST",
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(data) {
                    $.toast().reset("all");
                    console.log('dadasdas',data);
                    if(array_key_exists('success', data)){
                        $.toast({
                        heading: "Success",
                        text: data.success,
                        showHideTransition: "fade",
                        icon: "success"
                        });
                        $('#modal-updatePassword').modal('show');
                        $("#form-updatePassword").trigger("reset");
                    }else{
                        $.toast({
                        heading: "Error",
                        text: data.error,
                        showHideTransition: "fade",
                        icon: "error"
                        });
                    }
                }
            });
        });
            });
        })
      </script>
</body>

</html>