<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DmTCNN extends Model
{
    protected $guarded  = ['IdTCNN'];
    protected $primaryKey = "IdTCNN";
    public $incrementing = true;
    public $timestamps = false;
    protected $table = 'DmTCNN';
}
