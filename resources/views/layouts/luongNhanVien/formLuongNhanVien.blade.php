<form method="POST" id="form-LuongNhanVien" class="form-LuongNhanVien">
  @csrf
  <div class="modal fade " id="modal-LuongNhanVien" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" id="button-close-modal-LuongNhanVien" class="close" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="min-height: 40rem;">
          <div class=" d-flex justify-content-around ">
            <div class="input-group row col-2">
              <div class="input-group-prepend col-12">
                <span for="DichVu">Dịch vụ: </span>
              </div>
              <div class="d-flex flex-row col-12">
                <select disabled style="width: inherit" name="DichVu" id="DichVu">
                  <option value="0">Không</option>
                  <option value="1">Có</option>
                </select>
              </div>
            </div>
            <div class="input-group row col-2">
              <div class="input-group-prepend col-12">
                <span for="NgayTinhLuong">Tháng Tính Lương: </span>
              </div>
              <div class="d-flex flex-row col-12">
                <input type="text" class="datepicker" style="height: fit-content;" id="NgayTinhLuong" name="NgayTinhLuong" />
              </div>
            </div>
            <div class="input-group row col-2">
              <div class="input-group-prepend col-12">
                <span for="GoiHanTinhBHXH">Giới Hạn Tính BHXH,YT: </span>
              </div>
              <div class="d-flex flex-row col-12">
                <input type="text" class="money" style="height: fit-content;" id="GoiHanTinhBHXH"
                  name="GoiHanTinhBHXH" />
              </div>
            </div>
            <div class="input-group row col-2">
              <div class="input-group-prepend col-12">
                <span for="GoiHanTinhBHTN">Giới Hạn Tính BHTN: </span>
              </div>
              <div class="d-flex flex-row col-12">
                <input type="text" class="money" style="height: fit-content;" id="GoiHanTinhBHTN"
                  name="GoiHanTinhBHTN" />
              </div>
            </div>
            <div class="input-group row col-2">
              <div class="input-group-prepend col-12">
                <span for="TyGia">Tỷ Giá USD: </span>
              </div>
              <div class="d-flex flex-row col-12">
                <input type="text" class="money" style="height: fit-content;" id="TyGia" name="TyGia" />
              </div>
            </div>
            <div class="input-group row col-2">
              <div class="input-group-prepend col-12">
                <span for="TyGia_Insurance">Tỷ Giá USD( Insurance ): </span>
              </div>
              <div class="d-flex flex-row col-12">
                <input type="text" class="money" style="width: inherit" style="height: fit-content;"
                  id="TyGia_Insurance" name="TyGia_Insurance" />
              </div>
            </div>
          </div>
          <div id="service" class='service d-none justify-content-around ' >
            <fieldset>
              <legend>Company</legend>
              <div class='company d-flex justify-content-around '>
                <div class="input-group row col-3">
                  <div class="input-group-prepend col-12">
                    <span for="SI_Company">SI: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="SI_Company" name="SI_Company" />
                  </div>
                </div>
                <div class="input-group row col-3 service">
                  <div class="input-group-prepend col-12">
                    <span for="HI_Company">HI: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="HI_Company" name="HI_Company" />
                  </div>
                </div>
                <div class="input-group row col-3">
                  <div class="input-group-prepend col-12">
                    <span for="UI_Company">UI: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="UI_Company" name="UI_Company" />
                  </div>
                </div>
                <div class="input-group row col-3">
                  <div class="input-group-prepend col-12">
                    <span for="PhiDichVu">Service fee: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="PhiDichVu" name="PhiDichVu" />
                  </div>
                </div>
              </div>
            </fieldset>
            <fieldset>
              <legend>Employee</legend>
              <div class='employees d-flex justify-content-around '>
                <div class="input-group row col-4">
                  <div class="input-group-prepend col-12">
                    <span for="TrichNop_BHXH">SI: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="TrichNop_BHXH" name="TrichNop_BHXH" />
                  </div>
                </div>
                <div class="input-group row col-4 service">
                  <div class="input-group-prepend col-12">
                    <span for="BaoHiemYTe">HI: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="BaoHiemYTe" name="BaoHiemYTe" />
                  </div>
                </div>
                <div class="input-group row col-4">
                  <div class="input-group-prepend col-12">
                    <span for="BaoHiemThatNghiep">UI: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="BaoHiemThatNghiep" name="BaoHiemThatNghiep" />
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
          <div id='no_service' class='no_service d-none justify-content-around'>
            <fieldset>
              <legend>Company</legend>
              <div class='company d-flex justify-content-around '>
                <div class="input-group row col-6">
                  <div class="input-group-prepend col-12">
                    <span for="PhanTram_CSD">Chủ Sử Dụng: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="PhanTram_CSD" name="PhanTram_CSD" />
                  </div>
                </div>
                <div class="input-group row col-6">
                  <div class="input-group-prepend col-12">
                    <span for="PhanTram_NLD">Người Lao Động: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="PhanTram_NLD" name="PhanTram_NLD" />
                  </div>
                </div>
              </div>
            </fieldset>
            <fieldset>
              <legend>Employee</legend>
              <div class='employees d-flex justify-content-around '>
                <div class="input-group row col-4">
                  <div class="input-group-prepend col-12">
                    <span for="TrichNop_BHXH">Quỹ Hưu Trí Tử Tuất: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="TrichNop_BHXH" name="TrichNop_BHXH" />
                  </div>
                </div>
                <div class="input-group row col-4 service">
                  <div class="input-group-prepend col-12">
                    <span for="BaoHiemYTe">Quỹ KCB: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="BaoHiemYTe" name="BaoHiemYTe" />
                  </div>
                </div>
                <div class="input-group row col-4">
                  <div class="input-group-prepend col-12">
                    <span for="BaoHiemThatNghiep">Qũy BHTN: </span>
                  </div>
                  <div class="d-flex flex-row col-12">
                    <input type="number" step="0.1" style="width: inherit" max="100" min="0"
                      style="height: fit-content;" id="BaoHiemThatNghiep" name="BaoHiemThatNghiep" />
                  </div>
                </div>
              </div>
            </fieldset>
          </div>
          <table id="createSalary" style="font-size: 12px;" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th scope="col">Mã Nhân Viên</th>
                <th scope="col">Họ</th>
                <th scope="col">Tên</th>
                <th scope="col">Lương Cũ</th>
                <th scope="col">Lương Mới</th>
                <th scope="col">USD</th>
                <th scope="col">Ngày Hiệu Lực</th>
                <th scope="col">Số Người Phụ Thuộc</th>
                <th scope="col">Số Ngày Làm Việc</th>
                <th scope="col">Số Ngày Nghĩ có Lương</th>
                <th scope="col">Lương Theo Số Ngày Làm Việc</th>
                <th scope="col">Phụ Cấp Lương</th>
                <th scope="col">Thu Nhập Khác</th>
                <th scope="col">Đối Tượng</th>
                <th scope="col">Phương Án</th>
                <th scope="col">Tiền Lãi</th>
                <th scope="col">Ghi Chú</th>
                <th scope="col">Ngày KT Hiệu Lực</th>
                <th scope="col">Phí CK</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="submit" id="saveBtn-LuongNhanVien" class="btn btn-primary-outline">Save Changes</button>
        </div>
      </div>
    </div>
  </div>
</form>