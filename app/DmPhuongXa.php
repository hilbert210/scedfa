<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DmPhuongXa extends Model
{
    protected $guarded  = [];
    protected $primaryKey = "MAPHUONGXA";
    public $timestamps = false;
    protected $table = 'DmPhuongXa';
}
