<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'userName' => $faker->unique()->userName,
        'level' => Str::random(10),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'active' => 1,
        'HSVPTCNN' => json_encode(array('create'=> true, 'edit' => true)),
        'HSNLD' => json_encode(array('create'=> true, 'edit' => true)),
        'HSTTTH' => json_encode(array('create'=> true, 'edit' => true, 'delete'=> true)),
        'LBHNV' => json_encode(array('create'=> true, 'edit' => true, 'delete'=> true)),
        'DmTinhThanh' => json_encode(array('create'=> true, 'edit' => true)),
        'DmQuanHuyen' => json_encode(array('create'=> true, 'edit' => true)),
        'DmPhuongXa' => json_encode(array('create'=> true, 'edit' => true))
    ];
});
